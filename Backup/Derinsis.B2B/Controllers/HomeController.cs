﻿using System.Xml;

namespace DerinSIS.B2B.Controllers
{
    using System;
    using System.Web.Mvc;
    using Infrastructure.Auth;
    using Models;
    using ViewModels;

    [B2BAuthorize(AccountType.Customer | AccountType.Supplier)]
    public class HomeController : BaseController
    {
        public HomeController(DbEntities db) : base(db)
        {
        }

        public ActionResult Index()
        {
            if(CurrentAccount.ytkEpostaSiparis != 0)
            {
                if (CurrentAccountType == AccountType.Customer)
                {
                    return RedirectToAction("Index", "Store");
                }

                if (CurrentAccountType == AccountType.Supplier)
                {
                    return RedirectToAction("Index", "Supplier");
                }    
            }

            if(CurrentAccount.ytkEpostaSevkiyat != 0)
            {
                return RedirectToAction("Index", "Sales");
            }

            if(CurrentAccount.ytkEpostaFatura != 0 || CurrentAccount.ytkEpostaMutabakat != 0)
            {
                return RedirectToAction("Index", "Accounting");
            }

            throw new InvalidOperationException("Account Type not supported");
        }

        [ChildActionOnly]
        public ActionResult MainMenu()
        {
            try
            {
                XmlTextReader okuyucu = new XmlTextReader("http://www.tcmb.gov.tr/kurlar/today.xml");
                XmlDocument dokuman = new XmlDocument();
                dokuman.Load(okuyucu);
                XmlNode dolar = dokuman.SelectSingleNode("/Tarih_Date/Currency[CurrencyName='US DOLLAR']");
                XmlNode euro = dokuman.SelectSingleNode("/Tarih_Date/Currency[CurrencyName='EURO']");
                String doviz = "$:" + dolar.ChildNodes[4].InnerText + ", €:" + euro.ChildNodes[4].InnerText;
                var model = new MainMenuModel
                {
                    AccountType = CurrentAccountType.Value,
                    FaturaYetkisi = CurrentAccount.ytkEpostaFatura != 0,
                    SiparisYetkisi = CurrentAccount.ytkEpostaSiparis != 0,
                    MutabakatYetkisi = CurrentAccount.ytkEpostaMutabakat != 0,
                    SevkiyatYetkisi = CurrentAccount.ytkEpostaSevkiyat != 0,
                    YoneticiYetkisi = CurrentAccount.ytkB2B == 3,
                    Doviz = doviz,
                };
                return PartialView(model);
            }
            catch (Exception)
            {

                var model = new MainMenuModel
                {
                    AccountType = CurrentAccountType.Value,
                    FaturaYetkisi = CurrentAccount.ytkEpostaFatura != 0,
                    SiparisYetkisi = CurrentAccount.ytkEpostaSiparis != 0,
                    MutabakatYetkisi = CurrentAccount.ytkEpostaMutabakat != 0,
                    SevkiyatYetkisi = CurrentAccount.ytkEpostaSevkiyat != 0,
                    YoneticiYetkisi = CurrentAccount.ytkB2B == 3,
                    Doviz =  "$:, €:",
                };
                return PartialView(model);
            }
         
            return PartialView();
        }
    }
}