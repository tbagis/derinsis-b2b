﻿namespace DerinSIS.B2B.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using System.Web.Security;
    using Helpers;
    using Infrastructure.Auth;
    using Emails;
    using Models;
    using ViewModels;

    public class AccountController : BaseController
    {
        private readonly Mailer _mailer;

        public AccountController(DbEntities db, Mailer mailer) : base(db)
        {
            _mailer = mailer;
        }

        #region LogOn & LogOut
        
        public ActionResult LogOn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LogOn(LogOnModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                ShowErrorMessage("Hatalı kullanıcı adı ve/veya şifre");
                return View(model);
            }

            var email = model.Email.ToLowerInvariant();
            var user = Db.frmYetkili.SingleOrDefault(x => x.ytkEposta.ToLower() == email);

            if (user == null || !user.CheckPassword(model.Password))
            {
                ModelState.AddModelError("Email", " ");
                ModelState.AddModelError("Password", " ");
                ShowErrorMessage("Hatalı kullanıcı adı ve/veya şifre");
                return View();
            }

            if (user.ytkB2B == 0 || user.ytkB2B == 1)
            {
                ModelState.AddModelError("Email", " ");
                ModelState.AddModelError("Password", " ");
                ShowErrorMessage("B2B Sistemine Giriş Yetkiniz Bulunmamaktadır.");
                return View();
            }



            if(user.ytkEpostaFatura == 0 && user.ytkEpostaMutabakat == 0 && user.ytkEpostaSevkiyat == 0 && user.ytkEpostaSiparis == 0)
            {
                ModelState.AddModelError("Email", " ");
                ModelState.AddModelError("Password", " ");
                ShowErrorMessage("Yetkileriniz belirlenmemiştir. Lütfen firma yetkiliniz ile iletişime geçiniz.");
                return View();
            }

            var frmTip = user.frm.frmTip;
            if (frmTip != 0 /* Suppliers */ && frmTip != 1 /* Customers */)
            {
                ModelState.AddModelError("Email", " ");
                ModelState.AddModelError("Password", " ");
                ShowErrorMessage("B2B Sistemi Sadece Müşteriler ve Ürün Sağlayıcılar içindir.");
                return View();
            }

            // Everything is OK. Log user in
            FormsAuthentication.SetAuthCookie(model.Email, model.RememberMe);

            // User now logged in; here we must ensure that the user has at least one cart in db
            // otherwise we need to crete one for him.
            var hasCartDefined = Db.b2bSepet.Any(x => x.frmYetkili.ytkID == user.ytkID);
            if (!hasCartDefined)
            {
                var firstCartForUser = new b2bSepet { Ad = "Sepetim", frmYetkili = user, Aktif = true, Aciklama = ""};
                Db.b2bSepet.Add(firstCartForUser);
                Db.SaveChanges();
            }


            if (CheckSafeRedirectUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return RedirectToAction("Index", "Home");
        }

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        #endregion

        #region Profile

        [B2BAuthorize(AccountType.Supplier | AccountType.Customer)]
        public ActionResult MyProfile()
        {
            var model = new MyProfileModel
                            {
                                YetkiliIsmi = CurrentAccount.ytkAd,
                                YetkiliEPosta = CurrentAccount.ytkEposta,

                                FirmaIsmi = CurrentCompany.frmAd,

                                FirmaAdresDetay = string.Join(" ", CurrentCompany.adres1, CurrentCompany.adres2),
                                FirmaAdresIlce = CurrentCompany.ilce,
                                FirmaAdresSehir = CurrentCompany.frmKent.ilAd,
                                FirmaAdresPostaKodu = CurrentCompany.postaKod,
                                FirmaAdresUlke = Db.frmUlke.Single(x => x.ulkeID == CurrentCompany.ulke).ulkeAd,

                                FirmaTel1 = CurrentCompany.tel1,
                                FirmaTel2 = CurrentCompany.tel2,
                                FirmaTelGsm = CurrentCompany.telMobil,
                                FirmaTelFax = CurrentCompany.telFaks,

                                FirmaVergiDairesi = CurrentCompany.VD,
                                FirmaVergiNo = CurrentCompany.VN,
                            };

            return View(model);
        }

        [ChildActionOnly]
        public ActionResult CurrentCompanyInfo()
        {
            ViewBag.CompanyName = string.Join(" ", new[] { CurrentAccount.frm.frmAd, CurrentAccount.frm.frmAd2 });
            ViewBag.ContactName = CurrentAccount.ytkAd;
            ViewBag.ContactEmail = CurrentAccount.ytkEposta;
            ViewBag.Efatura = CurrentAccount.frm.etkinEfatura;
            return PartialView();
        }

        #endregion

        #region Change Password

        [B2BAuthorize(AccountType.Supplier | AccountType.Customer)]
        public ActionResult ChangePassword()
        {
            return View();
        }

        [B2BAuthorize(AccountType.Supplier | AccountType.Customer)]
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (!ModelState.IsValid)
            {
                ShowErrorMessage();
                return View();
            }

            // validate old password
            if (!CurrentAccount.CheckPassword(model.OldPassword))
            {
                ModelState.AddModelError("OldPassword", "Eski şifreniz hatalı.");
                ShowErrorMessage();
                return View();
            }

            // set new password and save
            CurrentAccount.SetPassword(model.NewPassword);
            Db.SaveChanges();

            ShowSuccessMessage("Şifreniz başarıyla güncellenmiştir.");

            return RedirectToAction("MyProfile");
        }

        #endregion

        #region Recover Password

        public ActionResult RecoverPasswordComplete(string id)
        {
            var hash = new Guid(id).ToString();
            //var resetlemeTalebi = Db.b2bSifreResetlemeTalep.SingleOrDefault(x => x.Hash == hash);
            //Check.NotNullOr404(resetlemeTalebi);
            //Check.CheckOr404(resetlemeTalebi.talepDrm == 0);
            //Check.CheckOr404(resetlemeTalebi.gTarih > DateTime.Now.AddDays(-3));

            return View();
        }

        [HttpPost]
        public ActionResult RecoverPasswordComplete(string id, ResetPasswordCompleteModel model)
        {
            var hash = new Guid(id).ToString();
            //var resetlemeTalebi = Db.b2bSifreResetlemeTalep.SingleOrDefault(x => x.Hash == hash);
            //Check.NotNullOr404(resetlemeTalebi);
            //Check.CheckOr404(resetlemeTalebi.talepDrm == 0);
            //Check.CheckOr404(resetlemeTalebi.gTarih > DateTime.Now.AddDays(-3));

            //if(!ModelState.IsValid)
            //{
            //    ShowErrorMessage();
            //    return View();
            //}

            //var yetkili = resetlemeTalebi.frmYetkili;
            //yetkili.SetPassword(model.NewPassword);
            Db.SaveChanges();

            return RedirectToAction("RecoverPasswordDone");
        }

        public ActionResult RecoverPasswordDone()
        {
            return View();
        }

        #endregion

        #region Error Pages

        public ActionResult Unauthorized()
        {
            return View();
        }
        #endregion

        #region Utility Functions

        private bool CheckSafeRedirectUrl(string url)
        {
            return Url.IsLocalUrl(url) &&
                   url.Length > 1 &&
                   url.StartsWith("/") &&
                   !url.StartsWith("//") &&
                   !url.StartsWith("/\\");
        }

        #endregion
    }
}