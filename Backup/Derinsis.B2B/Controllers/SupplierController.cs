﻿namespace DerinSIS.B2B.Controllers
{
    using System.Web.Mvc;
    using Infrastructure.Auth;
    using Models;

    [B2BAuthorize(AccountType.Supplier, Privilege.SIPARIS)]
    public class SupplierController : BaseController
    {
        public SupplierController(DbEntities db) : base(db)
        {
        }

        public ActionResult Index()
        {
            return View();
        }
    }
}