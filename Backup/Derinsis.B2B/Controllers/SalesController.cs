﻿


namespace DerinSIS.B2B.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Linq;
    using Helpers;
    using Infrastructure.Auth;
    using Models;
    using ViewModels;
using System.Collections;
   

    [B2BAuthorize(AccountType.Supplier|AccountType.Customer, Privilege.SEVKIYAT)]
    public class SalesController : BaseController
    {
        private readonly SystemSettings _settings;
        private readonly List<int> saleTypes;
        private readonly IEnumerable<int> product;
        private readonly List<int> totalSale;
        private readonly List<int> refundofSale;
       
        
        public  SalesController(DbEntities db, SystemSettings settings): base(db)
        {
            _settings = settings;
             saleTypes = new List<int>(){ 1, 3, 4, 5, 100, 101 };
             product = Db.urnFrm.Where(x => x.urnFrmFirmaID == CurrentCompany.frmID).Select(x => x.urnFrmStkID);
             totalSale = new List<int>() { 1, 4, 100 };
             refundofSale = new List<int>() { 3, 5, 101 };
             
        }

        #region Dashboard

        public ActionResult Index()
        {
            // Bekleyen Siparişler
            var firma = CurrentAccount.frm;
            var bekleyenSiparisler = Db.sip
                .Where(x => x.frm.frmID == CurrentAccount.frm.frmID)
                .Where(x => x.eDurum == 0) // Açık Siparişler
                .OrderBy(x => x.eTarihS)
                .Select(x => new SalesDashboardModel.BekleyenSiparis
                             {
                                 Id = x.eID,
                                 SiparisTarihi = x.eTarih,
                                 SevkTarihi = x.eTarihS,
                                 OnayliMi = x.onay == 1,
                             })
                .ToList();

            // Yeni eklenen ürünler
            var newProductDaysRange = 30;
            IEnumerable<SalesDashboardModel.YeniUrun> yeniUrunler;
            if(CurrentAccountType == AccountType.Customer)
            {
                var setting = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 760);
                if(setting != null)
                {
                    newProductDaysRange = int.Parse(setting.ayarDeger);
                }
                var newProductRange = DateTime.Today.AddDays(-1 * newProductDaysRange);

                yeniUrunler = Db.urnSatis_vw
                    .Where(x => x.gTarih >= newProductRange)
                    .Select(x => new SalesDashboardModel.YeniUrun
                                 {
                                     Id = x.stkID,
                                     UrunAdi = x.stkAd,
                                     UrunFiyati = x.fiyatS,
                                     EklenmeTarihi = x.gTarih
                                 });
            }
            else
            {
                var setting = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 761);
                if (setting != null)
                {
                    newProductDaysRange = int.Parse(setting.ayarDeger);
                }
                var newProductRange = DateTime.Today.AddDays(-1 * newProductDaysRange);

                yeniUrunler = Db.urnAlis_vw
                    .Where(x => x.urnFrmFirmaID == CurrentCompany.frmID)
                    .Where(x => x.gTarih >= newProductRange)
                    .Select(x => new SalesDashboardModel.YeniUrun
                                 {
                                     Id = x.stkID,
                                     UrunAdi = x.stkAd,
                                     UrunFiyati = x.fiyatA,
                                     EklenmeTarihi = x.gTarih,
                                 });
            }


            // Sipariş Günleri
            var siparisGunleri = new List<SalesDashboardModel.SiparisGunu>
                                     {
                                         new SalesDashboardModel.SiparisGunu
                                             {
                                                 HaftaninGunu = 1, 
                                                 GunIsmi = "Pazartesi",
                                                 SiparisVerilebilirMi = firma.frmSipGun1 > 0
                                             },
                                         new SalesDashboardModel.SiparisGunu
                                             {
                                                 HaftaninGunu = 2, 
                                                 GunIsmi = "Salı",
                                                 SiparisVerilebilirMi = firma.frmSipGun2 > 0
                                             },
                                         new SalesDashboardModel.SiparisGunu
                                             {
                                                 HaftaninGunu = 3, 
                                                 GunIsmi = "Çarşamba",
                                                 SiparisVerilebilirMi = firma.frmSipGun3 > 0,
                                             },
                                         new SalesDashboardModel.SiparisGunu
                                             {
                                                 HaftaninGunu = 4, 
                                                 GunIsmi = "Perşembe",
                                                 SiparisVerilebilirMi = firma.frmSipGun4 > 0
                                             },
                                         new SalesDashboardModel.SiparisGunu
                                             {
                                                 HaftaninGunu = 5, 
                                                 GunIsmi = "Cuma",
                                                 SiparisVerilebilirMi = firma.frmSipGun5 > 0
                                             },
                                         new SalesDashboardModel.SiparisGunu
                                             {
                                                 HaftaninGunu = 6,
                                                 GunIsmi = "Cumartesi",
                                                 SiparisVerilebilirMi = firma.frmSipGun6 > 0,
                                             },
                                         new SalesDashboardModel.SiparisGunu
                                             {
                                                 HaftaninGunu = 7,
                                                 GunIsmi = "Pazar",
                                                 SiparisVerilebilirMi = firma.frmSipGun7 > 0
                                             },
                                     };

            var model = new SalesDashboardModel
                            {
                                BekleyenSiparisler = bekleyenSiparisler,
                                YeniUrunler = yeniUrunler,
                                SiparisGunleri = siparisGunleri,
                            };
            
            return View(model);
        }

        #endregion

        #region Orders

        public ActionResult Orders(DocumentDateFilterModel dateFilter, int? productFilter, int? documentTypeFilter)
        {
            dateFilter.NotNullOrSetDefault(DateHelper.ThisMonthBegin, DateTime.Today);

            var orders =
                (
                    from sip in Db.sip
                    join frm in Db.frm on sip.eFirma equals frm.frmID
                    join sifTip in Db.sifTip on sip.eTip equals sifTip.hrktTipID
                    join sipNeden in Db.sipNeden on sip.Neden equals sipNeden.nedenID
                    join posMagaza in Db.posMagaza on sip.eMekan equals posMagaza.mekanID
                    join firmaMekan in Db.frm on sip.eFirmaMkn equals firmaMekan.frmID
                    /* Only Orders for siftip */
                    where sifTip.hrktSIF == 0
                    where documentTypeFilter == null || sifTip.hrktTipID == documentTypeFilter
                    where frm.frmID == CurrentAccount.frm.frmID
                    where sip.eTarih >= dateFilter.Begin && sip.eTarih <= dateFilter.End
                    where productFilter == null || sip.sipAyr.Select(x => x.ehStkID).Contains(productFilter.Value)
                    orderby sip.eTarih descending, sip.eID descending
                    select new OrdersModel.Order
                               {
                                   Id = sip.eID,
                                   Tarih = sip.eTarih,
                                   EvrakNo = sip.eNo,
                                   HareketTipi = sifTip.hrktTipAd,
                                   Durum = (OrdersModel.OrderStatus) sip.eDurum,
                                   OnayliMi = sip.onay == 1,
                                   VadeTarihi = sip.eTarihV,
                                   EvrakNot = sip.eNot,
                                   KDVHaricToplam = sip.sipAyr.Sum(x => (decimal?)x.ehTutar) ?? 0m,
                                   ToplamIndirim = sip.sipAyr.Sum(x => (decimal?)x.ehIndirim) ?? 0m,
                                   HareketGrubu = sipNeden.nedenAd,
                                   SevkAdresi = frm.frmTip == 1 ? (sip.eFirmaMkn == 0 ? "" : firmaMekan.frmAd) : posMagaza.mekanAd
                               }
                )
                    .ToList();

            return View(new OrdersModel
                            {
                                Orders = orders,
                                DateFilter = dateFilter,
                                ProductFilterId = productFilter,
                                ProductFilterLabel = productFilter.HasValue ? Db.urn.Single(x => x.stkID == productFilter).stkAd : "",
                                GosterimSecenekleri =
                                    new OrdersModel.Settings
                                        {
                                            EvrakNotuGosterilsinMi = _settings.EvrakNotlariGosterilsin
                                        }
                            });
        }

        public ActionResult OrderDetail(int? id)
        {
            Check.NotNullOr404(id);

            var sip = Db.sip.SingleOrDefault(x => x.eID == id);
            Check.NotNullOr404(sip);

            var frm = sip.frm;
            // Başka firmalara ait kayıt gösterilmemeli
            Check.CheckOr404(frm.frmID == CurrentAccount.frm.frmID);

            var sifTip = Db.sifTip.Where(x => x.hrktSIF == 0).Single(x => x.hrktTipID == sip.eTip);
            var sipNeden = Db.sipNeden.Single(x => x.nedenID == sip.Neden);
            
            var sevkAdresiAd = "";
            frm sevkAdresi;
            if (frm.frmTip == 1)
            {
                // sevk adresi ismini firma mekandan alıyoruz.
                var firmaMekan = sip.eFirmaMkn == 0 ? sip.frm : Db.frm.Single(x => x.frmID == sip.eFirmaMkn);
                sevkAdresiAd = firmaMekan.frmAd;
                sevkAdresi = firmaMekan;
            }
            else
            {
                // sevk adresi ismini posMagaza'dan alıyoruz    
                var posMagaza = Db.posMagaza.Single(x => x.mekanID == sip.eMekan);
                sevkAdresiAd = posMagaza.mekanAd;
                sevkAdresi = Db.frm.Single(x => x.frmID == sip.eMekan);
            }

            var model = new OrderDetailModel
                            {
                                Id = sip.eID,
                                EvrakNo = sip.eNo,
                                Tarih = sip.eTarih,
                                SevkTarihi = sip.eTarihS,
                                HareketTipi = sifTip.hrktTipAd,
                                EvrakNot = sip.eNot,
                                Evrak3SatirNot = sip.sipNot != null ? sip.sipNot.enNot : "",
                                VadeTarihi = sip.eTarihV,
                                HareketGrubu = sipNeden.nedenAd,
                                Durum = (OrderDetailModel.OrderStatus)sip.eDurum,
                                Onaylimi = sip.onay == 1,

                                SevkAdresiAd = sevkAdresiAd,
                                SevkAdresi1 = sevkAdresi.adres1,
                                SevkAdresi2 = sevkAdresi.adres2,
                                SevkAdresiIlce = sevkAdresi.ilce,
                                SevkAdresiIl = Db.frmKent.Single(x => x.alanKodu == sevkAdresi.il).ilAd,
                                SevkAdresiPostaKodu = sevkAdresi.postaKod,
                                SevkAdresiTel = sevkAdresi.tel1,
                                SevkAdresiFaks = sevkAdresi.telFaks,

                                // Sipariş Hareketleri (Ayrıntılar)
                                EvrakHareketleri = from x in sip.sipAyr
                                                   let barkod = Db.urnBarkod_vw.FirstOrDefault(y => y.urnBrkdStkID == x.ehStkID)
                                                   select new OrderDetailModel.OrderDetail
                                                               {
                                                                   Id = x.ehID,
                                                                   Sira = x.ehSira,
                                                                   UrunId = x.urn.stkID,
                                                                   UrunIsmi = x.urn.stkAd,
                                                                   UrunKod = x.urn.stkKod,
                                                                   UrunBarkod = barkod != null ? barkod.barkod : "",
                                                                   ToplamAdet = x.ehAdet,
                                                                   KoliIciAdet = x.urn.urnKoli,
                                                                   PaletIciAdet = x.urn.urnPalet,
                                                                   UrunAgirligi = x.urn.paketOlcu,
                                                                   AgirlikBirim = x.urn.paketBrm,
                                                                   KalanAdet = x.ehAdetN,
                                                                   Durum = (OrderDetailModel.OrderDetailStatus)x.ehDurum,
                                                                   Indirim1 = x.ehi1,
                                                                   Indirim2 = x.ehi2,
                                                                   Indirim3 = x.ehi3,
                                                                   Indirim4 = x.ehi4,
                                                                   Indirim5 = x.ehi5,
                                                                   IndirimTutari = x.ehIndirim,
                                                                   KDVHaricToplam = x.ehTutar,
                                                                   KDVYuzde = Db.urnKDV.Single(y => y.kdvID == x.ehKDV).kdvYuzde,
                                                                   KDVTutar = x.ehTutarKDV,
                                                               },

                                // İlişkili İrsaliyeler
                                IlgiliIrsaliyeler = Db.irsAyr
                                    .Where(x => x.ehSipID == sip.eID)
                                    .Select(x => x.irs)
                                    .Distinct()
                                    .Select(x => new OrderDetailModel.RelatedDelivery
                                                     {
                                                         Id = x.eID,
                                                         Tarih = x.eTarih,
                                                         EvrakNo = x.eNo
                                                     }),

                                // İlişkili Faturalar
                                IlgiliFaturalar = Db.fatAyr
                                    .Where(x => x.ehSipID == sip.eID)
                                    .Select(x => x.fat)
                                    .Distinct()
                                    .Select(x => new OrderDetailModel.RelatedInvoice
                                                     {
                                                         Id = x.eID,
                                                         Tarih = x.eTarih,
                                                         EvrakNo = x.eNo
                                                     }),

                                // Gösterim Seçenekleri
                                GosterimSecenekleri =
                                    new OrderDetailModel.Settings
                                        {
                                            EvrakNotuGosterilsinMi = _settings.EvrakNotlariGosterilsin,
                                        }
                            };


            return View(model);
        }

        #endregion

        #region Deliveries

        public ActionResult Deliveries(DocumentDateFilterModel dateFilter, int? productFilter, int? documentTypeFilter)
        {
            dateFilter.NotNullOrSetDefault(DateHelper.ThisMonthBegin, DateTime.Today);

            var deliveries =
                (
                    from irs in Db.irs
                    join frm in Db.frm on irs.eFirma equals frm.frmID
                    join sifTip in Db.sifTip on irs.eTip equals sifTip.hrktTipID
                    join sipNeden in Db.sipNeden on irs.Neden equals sipNeden.nedenID
                    join posMagaza in Db.posMagaza on irs.eMekan equals posMagaza.mekanID
                    join firmaMekan in Db.frm on irs.eFirmaMkn equals firmaMekan.frmID
                    /* Only Orders for siftip */
                    where sifTip.hrktSIF == 1
                    where documentTypeFilter == null || sifTip.hrktTipID == documentTypeFilter
                    where frm.frmID == CurrentAccount.frm.frmID
                    where irs.eTarih >= dateFilter.Begin && irs.eTarih <= dateFilter.End
                    where productFilter == null || irs.irsAyr.Select(x => x.ehStkID).Contains(productFilter.Value)
                    orderby irs.eTarih descending, irs.eID descending
                    select new DeliveriesModel.Delivery
                    {
                        Id = irs.eID,
                        Tarih = irs.eTarih,
                        EvrakNo = irs.eNo,
                        HareketTipi = sifTip.hrktTipAd,
                        Durum = (DeliveriesModel.DeliveryStatus)irs.eDurum,
                        VadeTarihi = irs.eTarihV,
                        EvrakNot = irs.eNot,
                        KDVHaricToplam = irs.irsAyr.Sum(x => x.ehTutar),
                        ToplamIndirim = irs.irsAyr.Sum(x => x.ehIndirim),
                        HareketGrubu = sipNeden.nedenAd,
                        SevkAdresi = frm.frmTip == 1 ? (irs.eFirmaMkn == 0 ? "" : firmaMekan.frmAd) : posMagaza.mekanAd
                    }
                )
                    .ToList();

            return View(new DeliveriesModel
            {
                Deliveries = deliveries,
                DateFilter = dateFilter,
                ProductFilterId = productFilter,
                ProductFilterLabel = productFilter.HasValue ? Db.urn.Single(x => x.stkID == productFilter).stkAd : "",
                GosterimSecenekleri =
                    new DeliveriesModel.Settings
                    {
                        EvrakNotuGosterilsinMi = _settings.EvrakNotlariGosterilsin
                    }
            });
        }

        public ActionResult DeliveryDetail(int? id)
        {
            Check.NotNullOr404(id);

            var irs = Db.irs.SingleOrDefault(x => x.eID == id);
            Check.NotNullOr404(irs);

            var frm = irs.frm;
            // Başka firmalara ait kayıt gösterilmemeli
            Check.CheckOr404(frm.frmID == CurrentAccount.frm.frmID);

            var sifTip = Db.sifTip.Where(x => x.hrktSIF == 1).Single(x => x.hrktTipID == irs.eTip);
            var sipNeden = Db.sipNeden.Single(x => x.nedenID == irs.Neden);

            var sevkAdresiAd = "";
            frm sevkAdresi;
            if (frm.frmTip == 1)
            {
                // sevk adresi ismini firma mekandan alıyoruz.
                var firmaMekan = irs.eFirmaMkn == 0 ? irs.frm : Db.frm.Single(x => x.frmID == irs.eFirmaMkn);
                sevkAdresiAd = firmaMekan.frmAd;
                sevkAdresi = firmaMekan;
            }
            else
            {
                // sevk adresi ismini posMagaza'dan alıyoruz    
                var posMagaza = Db.posMagaza.Single(x => x.mekanID == irs.eMekan);
                sevkAdresiAd = posMagaza.mekanAd;
                sevkAdresi = Db.frm.Single(x => x.frmID == irs.eMekan);
            }

            var model = new DeliveryDetailModel
            {
                Id = irs.eID,
                EvrakNo = irs.eNo,
                Tarih = irs.eTarih,
                SevkTarihi = irs.eTarihS,
                HareketTipi = sifTip.hrktTipAd,
                EvrakNot = irs.eNot,
                Evrak3SatirNot = irs.irsNot != null ? irs.irsNot.enNot : "",
                VadeTarihi = irs.eTarihV,
                HareketGrubu = sipNeden.nedenAd,
                Durum = (DeliveryDetailModel.DeliveryStatus)irs.eDurum,

                SevkAdresiAd = sevkAdresiAd,
                SevkAdresi1 = sevkAdresi.adres1,
                SevkAdresi2 = sevkAdresi.adres2,
                SevkAdresiIlce = sevkAdresi.ilce,
                SevkAdresiIl = Db.frmKent.Single(x => x.alanKodu == sevkAdresi.il).ilAd,
                SevkAdresiPostaKodu = sevkAdresi.postaKod,
                SevkAdresiTel = sevkAdresi.tel1,
                SevkAdresiFaks = sevkAdresi.telFaks,

                // Sipariş Hareketleri (Ayrıntılar)
                EvrakHareketleri = from x in irs.irsAyr
                                   let barkod = Db.urnBarkod_vw.FirstOrDefault(y => y.urnBrkdStkID == x.ehStkID)
                                   select new DeliveryDetailModel.DeliveryDetail
                                   {
                                       Id = x.ehID,
                                       Sira = x.ehSira,
                                       UrunId = x.urn.stkID,
                                       UrunIsmi = x.urn.stkAd,
                                       UrunKod = x.urn.stkKod,
                                       UrunBarkod = barkod != null ? barkod.barkod : "",
                                       ToplamAdet = x.ehAdet,
                                       KoliIciAdet = x.urn.urnKoli,
                                       PaletIciAdet = x.urn.urnPalet,
                                       UrunAgirligi = x.urn.paketOlcu,
                                       AgirlikBirim = x.urn.paketBrm,
                                       KalanAdet = x.ehAdetN,
                                       Indirim1 = x.ehi1,
                                       Indirim2 = x.ehi2,
                                       Indirim3 = x.ehi3,
                                       Indirim4 = x.ehi4,
                                       Indirim5 = x.ehi5,
                                       IndirimTutari = x.ehIndirim,
                                       KDVHaricToplam = x.ehTutar,
                                       KDVYuzde = Db.urnKDV.Single(y => y.kdvID == x.ehKDV).kdvYuzde,
                                       KDVTutar = x.ehTutarKDV,
                                   },

                // Gösterim Seçenekleri
                GosterimSecenekleri =
                    new DeliveryDetailModel.Settings
                    {
                        EvrakNotuGosterilsinMi = _settings.EvrakNotlariGosterilsin,
                    }
            };

            model.IlgiliSiparisler = (from irsAyr in Db.irsAyr
                                      where irsAyr.ehID == id
                                      join sip in Db.sip on irsAyr.ehSipID equals sip.eID
                                      select new DeliveryDetailModel.RelatedOrder
                                          {
                                              Id = sip.eID,
                                              Tarih = sip.eTarih,
                                              EvrakNo = sip.eNo
                                          }).Distinct().ToList();

            model.IlgiliFaturalar = (from fatAyr in Db.fatAyr
                                     join irs1 in Db.irs on fatAyr.ehIrsID equals irs1.eID
                                     where irs1.eID == id
                                     select new DeliveryDetailModel.RelatedInvoice
                                         {
                                             Id = fatAyr.ehID,
                                             Tarih = fatAyr.fat.eTarih,
                                             EvrakNo = fatAyr.fat.eNo
                                         }).Distinct().ToList();
            


            return View(model);
        }

        #endregion

        #region Sale MyDiscounts

        public ActionResult MyDiscounts()
        {
            var discounts = Db.sakMrk_vw
                .Where(x => x.skFrmID == CurrentAccount.frm.frmID)
                .Where(x => x.skTur == CurrentCompany.frmVadeTur)
                .Select(x => new MyDiscountModel
                    {
                        MarkaId = x.skMrkID,
                        MarkaIsmi = Db.urnMrk.FirstOrDefault(y => y.mrkID == x.skMrkID).mrkAd,
                        Indirim1 = x.skY1,
                        Indirim2 = x.skY2,
                        Indirim3 = x.skY3,
                    })
                .OrderBy(x => x.MarkaIsmi)
                .ToList();

            return View(discounts);
        }

        #endregion

        #region Kategori Bazlı Satış Raporu


        public ActionResult SalesPerCategoryReport(DocumentDateFilterModel dateFilter, string cityFilter = "", string groupFilter = "", string statueFilter = "", string categoryFilter = "", string brandFilter = "", string storeCount = "", string productCount = "")
        {
         
        dateFilter.NotNullOrSetDefault(DateHelper.LastMonthBegin,DateHelper.LastMonthEnd);
        var stores = (from irsAyr in Db.irsAyr
                      join irs in Db.irs on irsAyr.ehID equals irs.eID
                      join urn in Db.urn on irsAyr.ehStkID equals urn.stkID
                      join posMagaza in Db.posMagaza on irs.eMekan equals posMagaza.mekanID
                      join urnKtgrAg in Db.urnKtgrAg on urn.urnKtgrID equals urnKtgrAg.k0
                      where urnKtgrAg.k1 == 0
                      where irs.eTarih >= dateFilter.Begin && irs.eTarih <= dateFilter.End
                      where product.Contains(irsAyr.ehStkID)
                      where saleTypes.Contains(irs.eTip) == false
                      select new { irs, irsAyr, urn, posMagaza, urnKtgrAg });
        
            
           ViewData["storeCount"] = storeCount;
           ViewData["productCount"] = productCount;
           ViewData["group"] = groupFilter;
           ViewData["city"] = cityFilter;
           ViewData["brand"] = brandFilter;
           ViewData["category"] = categoryFilter;
           ViewData["statue"] = statueFilter;

           var category = new List<int>();
           var brand = new List<int>();
           var city = new List<int>();
           var groups = new List<int>();
           var status = new List<int>();
           var statusList = new List<string>();
            string categoryName = string.Empty,brandName = string.Empty,
           cityName = string.Empty, groupName = string.Empty, statusName = string.Empty;

           if (categoryFilter != "")
           {
               category.AddRange(categoryFilter.Split(',').Select(item => Convert.ToInt32(item)));
               categoryName = string.Join(",", Db.urnKtgrAg.Where(x => category.Contains(x.kID)).Select(x => x.kAd));
           
               stores = stores.Where(x =>category.Contains(x.urnKtgrAg.kID));
           }
           if (brandFilter != "")
           {

               brand.AddRange(brandFilter.Split(',').Select(item => Convert.ToInt32(item)));
               brandName = string.Join(",", Db.urnMrk.Where(x => brand.Contains(x.mrkID)).Select(x => x.mrkAd));

               stores = stores.Where(x => brand.Contains(x.urn.urnMrkID));
           }
           if (cityFilter != "")
           {

               city.AddRange(cityFilter.Split(',').Select(item => Convert.ToInt32(item)));
               cityName = string.Join(",", Db.frmKent.Where(x => city.Contains(x.alanKodu)).Select(x => x.ilAd));

               stores = from store in stores
                        join frm in Db.frm on store.irs.eMekan equals frm.frmID
                        where city.Contains(frm.il)
                        select store;
              
           }
           if (groupFilter != "")
           {
               groups.AddRange(groupFilter.Split(',').Select(item => Convert.ToInt32(item)));
               groupName = string.Join(",", Db.posMagazaGrup.Where(x => groups.Contains(x.mekanGrupID)).Select(x => x.mekanGrupAd));
               stores = stores.Where(x => groups.Contains(x.posMagaza.mekanGrup));

           }
           if (statueFilter != "")
           {
               status.AddRange(statueFilter.Split(',').Select(item => Convert.ToInt32(item)));
               if (status.Contains(0)) { statusList.Add("Açık"); }
               if (status.Contains(1)) { statusList.Add("Kapalı"); }
               if (status.Contains(2)) { statusList.Add("Açılmamış"); }

               statusName = string.Join(",", statusList);

               if (status.Count == 1)
               {
                   stores = !status.Contains(2) ?
                       (status.Contains(0) ? stores.Where(x => x.posMagaza.mekanDurum == 0)
                       : stores.Where(x => x.posMagaza.mekanDurum != 0))
                       : stores.Where(x => x.posMagaza.mekanDurum == 0 && x.posMagaza.mekanAcilisTarih > DateTime.Now);
               }
               if (status.Count == 2)
               {
                   if (!status.Contains(1))
                   {
                       stores = stores.Where(x => x.posMagaza.mekanDurum == 0);
                   }
                   if (!status.Contains(0))
                   {
                       stores = stores.Where(x => (x.posMagaza.mekanDurum == 0
                                      && x.posMagaza.mekanAcilisTarih > DateTime.Now)
                                      || x.posMagaza.mekanDurum != 0);
                   }
               }
           }
      var model = new SalesPerCategoryModel
                            {
                  Reports = (from store in stores
                             group store.irsAyr by new { store.urnKtgrAg.kID} into g
                             select new SalesPerCategoryModel.SalesPerCategory
                                    {
                      CategoryName =Db.urnKtgrAg.FirstOrDefault(x=>x.kID==g.Key.kID).kAd,
                      TotalSaleCount = g.Count(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
                      (-1) * g.Where(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehAdet) : 0,

                      RefundofSaleCount = g.Count(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
                         g.Where(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehAdet) : 0,


                      TotalSale = g.Count(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
 g.Where(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x =>x.ehTutar -x.ehIndirim + x.ehTutarKDV) : 0,

                      RefundofSale = g.Count(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
 g.Where(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x =>x.ehTutar-x.ehIndirim+x.ehTutarKDV) : 0
                       
                                    }
                             ).ToList(),
              
                  DateFilter = dateFilter,
                  Categories = categoryName,
                  Brands = brandName,
                  ProductCount = productCount,
                  Cities = cityName,
                  Groups = groupName,
                  Status = statusName,
                  StoreCount = storeCount

                            };

            return View(model);
        }

        #endregion

        #region Mağaza Bazlı Satış Raporu

        public ActionResult SalesPerStoreReport(DocumentDateFilterModel dateFilter, string cityFilter = "", string groupFilter = "", string statueFilter = "", string categoryFilter = "", string brandFilter = "", string storeCount = "", string productCount = "")
        {
           
                dateFilter.NotNullOrSetDefault(DateHelper.LastMonthBegin, DateHelper.LastMonthEnd);

                var stores = (from irsAyr in Db.irsAyr
                              join irs in Db.irs on irsAyr.ehID equals irs.eID
                              join urn in Db.urn on irsAyr.ehStkID equals urn.stkID
                              join posMagaza in Db.posMagaza on irs.eMekan equals posMagaza.mekanID
                              where irs.eTarih >= dateFilter.Begin && irs.eTarih <= dateFilter.End
                              where product.Contains(irsAyr.ehStkID)
                              where saleTypes.Contains(irs.eTip)
                              select new { irs, irsAyr, urn, posMagaza });
       
                ViewData["storeCount"] = storeCount;
                ViewData["productCount"] = productCount;
                ViewData["group"] = groupFilter;
                ViewData["city"] = cityFilter;
                ViewData["brand"] = brandFilter;
                ViewData["category"] = categoryFilter;
                ViewData["statue"] = statueFilter;

                var category = new List<int>();
                var brand = new List<int>();
                var city = new List<int>();
                var groups = new List<int>();
                var status = new List<int>();
                var statusList = new List<string>();
                string categoryName = string.Empty, brandName = string.Empty,
                cityName = string.Empty, groupName = string.Empty, statusName = string.Empty;

                if (categoryFilter != "")
                {
                    category.AddRange(categoryFilter.Split(',').Select(item => Convert.ToInt32(item)));

                    categoryName = string.Join(",", Db.urnKtgrAg.Where(x => category.Contains(x.kID)).Select(x => x.kAd));

                    stores = (from store in stores
                              join urnKtgrAg in Db.urnKtgrAg on store.urn.urnKtgrID equals urnKtgrAg.k0
                              where urnKtgrAg.k1 == 0
                              where category.Contains(urnKtgrAg.kID)
                              select store);
                    //stores = stores.Where(x => x.urnKtgrAg.k1 == 0 && category.Contains(x.urnKtgrAg.kID));
                }
                if (brandFilter != "")
                {

                    brand.AddRange(brandFilter.Split(',').Select(item => Convert.ToInt32(item)));
                    brandName = string.Join(",", Db.urnMrk.Where(x => brand.Contains(x.mrkID)).Select(x => x.mrkAd));

                    stores = stores.Where(x => brand.Contains(x.urn.urnMrkID));
                }
                if (cityFilter != "")
                {

                    city.AddRange(cityFilter.Split(',').Select(item => Convert.ToInt32(item)));
                    cityName = string.Join(",", Db.frmKent.Where(x => city.Contains(x.alanKodu)).Select(x => x.ilAd));

                    stores = from store in stores
                             join frm in Db.frm on store.irs.eMekan equals frm.frmID
                             where city.Contains(frm.il)
                             select store;
                    //stores = stores.Where(x => city.Contains(x.frm.il));
                }
                if (groupFilter != "")
                {
                    groups.AddRange(groupFilter.Split(',').Select(item => Convert.ToInt32(item)));
                    groupName = string.Join(",", Db.posMagazaGrup.Where(x => groups.Contains(x.mekanGrupID)).Select(x => x.mekanGrupAd));
                    stores = stores.Where(x => groups.Contains(x.posMagaza.mekanGrup));

                }
                if (statueFilter != "")
                {
                    status.AddRange(statueFilter.Split(',').Select(item => Convert.ToInt32(item)));
                    if (status.Contains(0)) { statusList.Add("Açık"); }
                    if (status.Contains(1)) { statusList.Add("Kapalı"); }
                    if (status.Contains(2)) { statusList.Add("Açılmamış"); }

                    statusName = string.Join(",", statusList);

                    if (status.Count == 1)
                    {
                        stores = !status.Contains(2) ?
                            (status.Contains(0) ? stores.Where(x => x.posMagaza.mekanDurum == 0)
                            : stores.Where(x => x.posMagaza.mekanDurum != 0))
                            : stores.Where(x => x.posMagaza.mekanDurum == 0 && x.posMagaza.mekanAcilisTarih > DateTime.Now);
                    }
                    if (status.Count == 2)
                    {
                        if (!status.Contains(1))
                        {
                            stores = stores.Where(x => x.posMagaza.mekanDurum == 0);
                        }
                        if (!status.Contains(0))
                        {
                            stores = stores.Where(x => (x.posMagaza.mekanDurum == 0
                                           && x.posMagaza.mekanAcilisTarih > DateTime.Now)
                                           || x.posMagaza.mekanDurum != 0);
                        }
                    }
                }
            var model =new SalesPerStoreModel
                             {
                    Reports  = from store in stores   
                               group store.irsAyr by new{store.irs.eMekan} into g
                               select new SalesPerStoreModel.SalesPerStore
                            {
             StoreName = Db.posMagaza.FirstOrDefault(x =>x.mekanID==g.Key.eMekan).mekanAd,
          
             TotalSaleCount = g.Count(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
                      (-1) * g.Where(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehAdet) : 0,

             RefundofSaleCount = g.Count(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
                g.Where(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehAdet) : 0,


             TotalSale = g.Count(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
g.Where(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehTutar - x.ehIndirim + x.ehTutarKDV) : 0,

             RefundofSale = g.Count(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
g.Where(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehTutar - x.ehIndirim + x.ehTutarKDV) : 0
                           },
                    DateFilter = dateFilter,
                    Categories = categoryName,
                    Brands =   brandName ,
                    ProductCount = productCount,
                    Cities = cityName,
                    Groups = groupName,
                    Status = statusName,
                    StoreCount = storeCount
                          }; 

            return View(model);
        }
        #endregion

        #region Marka Bazlı Satış Raporu

        public ActionResult SalesPerBrandReport(DocumentDateFilterModel dateFilter, string cityFilter = "", string groupFilter = "", string statueFilter = "", string categoryFilter = "", string brandFilter = "", string storeCount = "", string productCount = "")
        {
           dateFilter.NotNullOrSetDefault(DateHelper.LastMonthBegin, DateHelper.LastMonthEnd);

           var stores = (from irsAyr in Db.irsAyr
                         join irs in Db.irs on irsAyr.ehID equals irs.eID
                         join urn in Db.urn on irsAyr.ehStkID equals urn.stkID
                         join posMagaza in Db.posMagaza on irs.eMekan equals posMagaza.mekanID
                         where irs.eTarih >= dateFilter.Begin && irs.eTarih <= dateFilter.End
                         where product.Contains(irsAyr.ehStkID)
                         where saleTypes.Contains(irs.eTip) == false
                         select new { irs, irsAyr, urn, posMagaza });

           ViewData["storeCount"] = storeCount;
           ViewData["productCount"] = productCount;
           ViewData["group"] = groupFilter;
           ViewData["city"] = cityFilter;
           ViewData["brand"] = brandFilter;
           ViewData["category"] = categoryFilter;
           ViewData["statue"] = statueFilter;

           var category = new List<int>();
           var brand = new List<int>();
           var city = new List<int>();
           var groups = new List<int>();
           var status = new List<int>();
           var statusList = new List<string>();
           string categoryName = string.Empty, brandName = string.Empty,
           cityName = string.Empty, groupName = string.Empty, statusName = string.Empty;

           if (categoryFilter != "")
           {
               category.AddRange(categoryFilter.Split(',').Select(item => Convert.ToInt32(item)));

               categoryName = string.Join(",", Db.urnKtgrAg.Where(x => category.Contains(x.kID)).Select(x => x.kAd));

               stores = (from store in stores
                         join urnKtgrAg in Db.urnKtgrAg on store.urn.urnKtgrID equals urnKtgrAg.k0
                         where urnKtgrAg.k1 == 0
                         where category.Contains(urnKtgrAg.kID)
                         select store);
               //stores = stores.Where(x => x.urnKtgrAg.k1 == 0 && category.Contains(x.urnKtgrAg.kID));
           }
           if (brandFilter != "")
           {

               brand.AddRange(brandFilter.Split(',').Select(item => Convert.ToInt32(item)));
               brandName = string.Join(",", Db.urnMrk.Where(x => brand.Contains(x.mrkID)).Select(x => x.mrkAd));

               stores = stores.Where(x => brand.Contains(x.urn.urnMrkID));
           }
           if (cityFilter != "")
           {

               city.AddRange(cityFilter.Split(',').Select(item => Convert.ToInt32(item)));
               cityName = string.Join(",", Db.frmKent.Where(x => city.Contains(x.alanKodu)).Select(x => x.ilAd));

               stores = from store in stores
                        join frm in Db.frm on store.irs.eMekan equals frm.frmID
                        where city.Contains(frm.il)
                        select store;
               //stores = stores.Where(x => city.Contains(x.frm.il));
           }
           if (groupFilter != "")
           {
               groups.AddRange(groupFilter.Split(',').Select(item => Convert.ToInt32(item)));
               groupName = string.Join(",", Db.posMagazaGrup.Where(x => groups.Contains(x.mekanGrupID)).Select(x => x.mekanGrupAd));
               stores = stores.Where(x => groups.Contains(x.posMagaza.mekanGrup));

           }
           if (statueFilter != "")
           {
               status.AddRange(statueFilter.Split(',').Select(item => Convert.ToInt32(item)));
               if (status.Contains(0)) { statusList.Add("Açık"); }
               if (status.Contains(1)) { statusList.Add("Kapalı"); }
               if (status.Contains(2)) { statusList.Add("Açılmamış"); }

               statusName = string.Join(",", statusList);

               if (status.Count == 1)
               {
                   stores = !status.Contains(2) ?
                       (status.Contains(0) ? stores.Where(x => x.posMagaza.mekanDurum == 0)
                       : stores.Where(x => x.posMagaza.mekanDurum != 0))
                       : stores.Where(x => x.posMagaza.mekanDurum == 0 && x.posMagaza.mekanAcilisTarih > DateTime.Now);
               }
               if (status.Count == 2)
               {
                   if (!status.Contains(1))
                   {
                       stores = stores.Where(x => x.posMagaza.mekanDurum == 0);
                   }
                   if (!status.Contains(0))
                   {
                       stores = stores.Where(x => (x.posMagaza.mekanDurum == 0
                                      && x.posMagaza.mekanAcilisTarih > DateTime.Now)
                                      || x.posMagaza.mekanDurum != 0);
                   }
               }
           }


            var model = new SalesPerBrandModel
            {
                Reports = (from store in stores
                           group store.irsAyr by new { store.urn.urnMrkID } into g
                           select new SalesPerBrandModel.SalesPerBrand
                           {
                BrandName =Db.urnMrk.FirstOrDefault(x=>x.mrkID==g.Key.urnMrkID).mrkAd,
               TotalSaleCount = g.Count(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
                         (-1) * g.Where(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehAdet) : 0,

               RefundofSaleCount = g.Count(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
                  g.Where(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehAdet) : 0,


               TotalSale = g.Count(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
  g.Where(x =>totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x =>x.ehTutar-x.ehIndirim + x.ehTutarKDV) : 0,

               RefundofSale = g.Count(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
 g.Where(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x =>x.ehTutar-x.ehIndirim+x.ehTutarKDV) : 0
                           }
                     ).ToList(),
                DateFilter = dateFilter,
                Categories = categoryName,
                Brands = brandName,
                ProductCount = productCount,
                Cities = cityName,
                Groups = groupName,
                Status = statusName,
                StoreCount = storeCount
            };

            return View(model);
        }
        #endregion

        #region Aylık Satış Raporu

        public ActionResult SalesPerMonthReport(DocumentDateFilterModel dateFilter, string cityFilter = "", string groupFilter = "", string statueFilter = "", string categoryFilter = "", string brandFilter = "", string storeCount = "", string productCount = "")
        {
            dateFilter.NotNullOrSetDefault(DateHelper.LastMonthBegin, DateHelper.LastMonthEnd);

            var stores = (from irsAyr in Db.irsAyr
                          join irs in Db.irs on irsAyr.ehID equals irs.eID
                          join urn in Db.urn on irsAyr.ehStkID equals urn.stkID
                          join posMagaza in Db.posMagaza on irs.eMekan equals posMagaza.mekanID
                          where irs.eTarih >= dateFilter.Begin && irs.eTarih <= dateFilter.End
                          where product.Contains(irsAyr.ehStkID)
                          where saleTypes.Contains(irs.eTip) == false
                          select new { irs, irsAyr, urn, posMagaza });

            ViewData["storeCount"] = storeCount;
            ViewData["productCount"] = productCount;
            ViewData["group"] = groupFilter;
            ViewData["city"] = cityFilter;
            ViewData["brand"] = brandFilter;
            ViewData["category"] = categoryFilter;
            ViewData["statue"] = statueFilter;

            var category = new List<int>();
            var brand = new List<int>();
            var city = new List<int>();
            var groups = new List<int>();
            var status = new List<int>();
            var statusList = new List<string>();
            string categoryName = string.Empty, brandName = string.Empty,
            cityName = string.Empty, groupName = string.Empty, statusName = string.Empty;

            if (categoryFilter != "")
            {
                category.AddRange(categoryFilter.Split(',').Select(item => Convert.ToInt32(item)));

                categoryName = string.Join(",", Db.urnKtgrAg.Where(x => category.Contains(x.kID)).Select(x => x.kAd));

                stores = (from store in stores
                          join urnKtgrAg in Db.urnKtgrAg on store.urn.urnKtgrID equals urnKtgrAg.k0
                          where urnKtgrAg.k1 == 0
                          where category.Contains(urnKtgrAg.kID)
                          select store);
                //stores = stores.Where(x => x.urnKtgrAg.k1 == 0 && category.Contains(x.urnKtgrAg.kID));
            }
            if (brandFilter != "")
            {

                brand.AddRange(brandFilter.Split(',').Select(item => Convert.ToInt32(item)));
                brandName = string.Join(",", Db.urnMrk.Where(x => brand.Contains(x.mrkID)).Select(x => x.mrkAd));

                stores = stores.Where(x => brand.Contains(x.urn.urnMrkID));
            }
            if (cityFilter != "")
            {

                city.AddRange(cityFilter.Split(',').Select(item => Convert.ToInt32(item)));
                cityName = string.Join(",", Db.frmKent.Where(x => city.Contains(x.alanKodu)).Select(x => x.ilAd));

                stores = from store in stores
                         join frm in Db.frm on store.irs.eMekan equals frm.frmID
                         where city.Contains(frm.il)
                         select store;
                //stores = stores.Where(x => city.Contains(x.frm.il));
            }
            if (groupFilter != "")
            {
                groups.AddRange(groupFilter.Split(',').Select(item => Convert.ToInt32(item)));
                groupName = string.Join(",", Db.posMagazaGrup.Where(x => groups.Contains(x.mekanGrupID)).Select(x => x.mekanGrupAd));
                stores = stores.Where(x => groups.Contains(x.posMagaza.mekanGrup));

            }
            if (statueFilter != "")
            {
                status.AddRange(statueFilter.Split(',').Select(item => Convert.ToInt32(item)));
                if (status.Contains(0)) { statusList.Add("Açık"); }
                if (status.Contains(1)) { statusList.Add("Kapalı"); }
                if (status.Contains(2)) { statusList.Add("Açılmamış"); }

                statusName = string.Join(",", statusList);

                if (status.Count == 1)
                {
                    stores = !status.Contains(2) ?
                        (status.Contains(0) ? stores.Where(x => x.posMagaza.mekanDurum == 0)
                        : stores.Where(x => x.posMagaza.mekanDurum != 0))
                        : stores.Where(x => x.posMagaza.mekanDurum == 0 && x.posMagaza.mekanAcilisTarih > DateTime.Now);
                }
                if (status.Count == 2)
                {
                    if (!status.Contains(1))
                    {
                        stores = stores.Where(x => x.posMagaza.mekanDurum == 0);
                    }
                    if (!status.Contains(0))
                    {
                        stores = stores.Where(x => (x.posMagaza.mekanDurum == 0
                                       && x.posMagaza.mekanAcilisTarih > DateTime.Now)
                                       || x.posMagaza.mekanDurum != 0);
                    }
                }
            }


            var model = new SalesPerMonthModel
            {
                Reports = (from store in stores
                           group store.irsAyr by new { store.irs.eTarih.Year, store.irs.eTarih.Month } into g
                           select new SalesPerMonthModel.SalesPerMonth
                           {
                           Year = g.Key.Year,
                          Month = g.Key.Month,
                         TotalSaleCount = g.Count(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
                         (-1) * g.Where(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehAdet) : 0,

                        RefundofSaleCount = g.Count(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
                          g.Where(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehAdet) : 0,


                        TotalSale = g.Count(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
   g.Where(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehTutar - x.ehIndirim + x.ehTutarKDV) : 0,

                        RefundofSale = g.Count(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
 g.Where(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehTutar - x.ehIndirim + x.ehTutarKDV) : 0
                           }
                     ).ToList(),
                DateFilter = dateFilter,
                Categories = categoryName,
                Brands = brandName,
                ProductCount = productCount,
                Cities = cityName,
                Groups = groupName,
                Status = statusName,
                StoreCount = storeCount
            };

            return View(model);
        }
        #endregion

        #region Haftalık Satış Raporu

        public ActionResult SalesPerWeekReport(DocumentDateFilterModel dateFilter)
        {
           
            dateFilter.NotNullOrSetDefault(DateHelper.YearBegin, DateTime.Today);
        
            var model = new SalesPerWeekModel
            {
                Reports = (from irs in Db.irs
                           join irsAyr in Db.irsAyr on irs.eID equals irsAyr.ehID
                           where irs.eFirma == CurrentCompany.frmID
                           where irs.eTarih >= dateFilter.Begin && irs.eTarih <= dateFilter.End
                         
                      group irsAyr by new {irs.eTarih.Year, irs.eTarih.Month} into g
                      orderby g.Key.Month
                           select new SalesPerWeekModel.SalesPerWeek
                           {
                               Year = g.Key.Year,
                               Month = g.Key.Month
                               
                               
                           }
                     ).ToList(),
                DateFilter = dateFilter
            };

            return View(model);
        }
          
         
        #endregion

        #region Bölgesel Satış Raporu

        public ActionResult SalesPerRegionReport(DocumentDateFilterModel dateFilter, string cityFilter = "", string groupFilter = "", string statueFilter = "", string categoryFilter = "", string brandFilter = "", string storeCount = "", string productCount = "")
        {

            dateFilter.NotNullOrSetDefault(DateHelper.YearBegin, DateTime.Today);
            var stores = (from irsAyr in Db.irsAyr
                          join irs in Db.irs on irsAyr.ehID equals irs.eID
                          join urn in Db.urn on irsAyr.ehStkID equals urn.stkID
                          join posMagaza in Db.posMagaza on irs.eMekan equals posMagaza.mekanID
                          join frm in Db.frm on irs.eMekan equals frm.frmID
                          join frmKent in Db.frmKent on frm.il equals frmKent.alanKodu
                          where irs.eTarih >= dateFilter.Begin && irs.eTarih <= dateFilter.End
                          where product.Contains(irsAyr.ehStkID)
                          where saleTypes.Contains(irs.eTip)
                          select new { irs, irsAyr, urn, posMagaza ,frm,frmKent});

            ViewData["storeCount"] = storeCount;
            ViewData["productCount"] = productCount;
            ViewData["group"] = groupFilter;
            ViewData["city"] = cityFilter;
            ViewData["brand"] = brandFilter;
            ViewData["category"] = categoryFilter;
            ViewData["statue"] = statueFilter;

            var category = new List<int>();
            var brand = new List<int>();
            var city = new List<int>();
            var groups = new List<int>();
            var status = new List<int>();
            var statusList = new List<string>();
            string categoryName = string.Empty, brandName = string.Empty,
            cityName = string.Empty, groupName = string.Empty, statusName = string.Empty;

            if (categoryFilter != "")
            {
                category.AddRange(categoryFilter.Split(',').Select(item => Convert.ToInt32(item)));

                categoryName = string.Join(",", Db.urnKtgrAg.Where(x => category.Contains(x.kID)).Select(x => x.kAd));

                stores = (from store in stores
                          join urnKtgrAg in Db.urnKtgrAg on store.urn.urnKtgrID equals urnKtgrAg.k0
                          where urnKtgrAg.k1 == 0
                          where category.Contains(urnKtgrAg.kID)
                          select store);
                //stores = stores.Where(x => x.urnKtgrAg.k1 == 0 && category.Contains(x.urnKtgrAg.kID));
            }
            if (brandFilter != "")
            {

                brand.AddRange(brandFilter.Split(',').Select(item => Convert.ToInt32(item)));
                brandName = string.Join(",", Db.urnMrk.Where(x => brand.Contains(x.mrkID)).Select(x => x.mrkAd));

                stores = stores.Where(x => brand.Contains(x.urn.urnMrkID));
            }
            if (cityFilter != "")
            {

                city.AddRange(cityFilter.Split(',').Select(item => Convert.ToInt32(item)));
                cityName = string.Join(",", Db.frmKent.Where(x => city.Contains(x.alanKodu)).Select(x => x.ilAd));

                stores = stores.Where(x => city.Contains(x.frm.il));
            }
            if (groupFilter != "")
            {
                groups.AddRange(groupFilter.Split(',').Select(item => Convert.ToInt32(item)));
                groupName = string.Join(",", Db.posMagazaGrup.Where(x => groups.Contains(x.mekanGrupID)).Select(x => x.mekanGrupAd));
                stores = stores.Where(x => groups.Contains(x.posMagaza.mekanGrup));

            }
            if (statueFilter != "")
            {
                status.AddRange(statueFilter.Split(',').Select(item => Convert.ToInt32(item)));
                if (status.Contains(0)) { statusList.Add("Açık"); }
                if (status.Contains(1)) { statusList.Add("Kapalı"); }
                if (status.Contains(2)) { statusList.Add("Açılmamış"); }

                statusName = string.Join(",", statusList);

                if (status.Count == 1)
                {
                    stores = !status.Contains(2) ?
                        (status.Contains(0) ? stores.Where(x => x.posMagaza.mekanDurum == 0)
                        : stores.Where(x => x.posMagaza.mekanDurum != 0))
                        : stores.Where(x => x.posMagaza.mekanDurum == 0 && x.posMagaza.mekanAcilisTarih > DateTime.Now);
                }
                if (status.Count == 2)
                {
                    if (!status.Contains(1))
                    {
                        stores = stores.Where(x => x.posMagaza.mekanDurum == 0);
                    }
                    if (!status.Contains(0))
                    {
                        stores = stores.Where(x => (x.posMagaza.mekanDurum == 0
                                       && x.posMagaza.mekanAcilisTarih > DateTime.Now)
                                       || x.posMagaza.mekanDurum != 0);
                    }
                }
            }
            var model = new SalesPerRegionModel
            {
                Reports = (from store in stores
                           group store.irsAyr by new{store.frmKent.ilBolge} into g
                           select new SalesPerRegionModel.SalesPerRegion
                           {
                      Region =Db.frmKentBolge.FirstOrDefault(x=>x.bolgeID==g.Key.ilBolge).bolgeAd,

                        TotalSaleCount = g.Count(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
             (-1) * g.Where(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehAdet) : 0,

               RefundofSaleCount = g.Count(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
                   g.Where(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehAdet) : 0,


                TotalSale = g.Count(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
 g.Where(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x =>x.ehTutar -x.ehIndirim+x.ehTutarKDV) : 0,

               RefundofSale = g.Count(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
  g.Where(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x=>x.ehTutar -x.ehIndirim+x.ehTutarKDV):0
 
                           }
                     ).ToList(),
                DateFilter = dateFilter,
                Categories = categoryName,
                Brands = brandName,
                ProductCount = productCount,
                Cities = cityName,
                Groups = groupName,
                Status = statusName,
                StoreCount = storeCount
            };

            return View(model);
        }


        #endregion

        #region İl Bazlı Satış Raporu

        public ActionResult SalesPerCityReport(DocumentDateFilterModel dateFilter, string cityFilter = "", string groupFilter = "", string statueFilter = "", string categoryFilter = "", string brandFilter = "", string storeCount = "", string productCount = "")
        {

            dateFilter.NotNullOrSetDefault(DateHelper.YearBegin, DateTime.Today);
            var stores = (from irsAyr in Db.irsAyr
                          join irs in Db.irs on irsAyr.ehID equals irs.eID
                          join urn in Db.urn on irsAyr.ehStkID equals urn.stkID
                          join posMagaza in Db.posMagaza on irs.eMekan equals posMagaza.mekanID
                          join frm in Db.frm on irs.eMekan equals frm.frmID
                          where irs.eTarih >= dateFilter.Begin && irs.eTarih <= dateFilter.End
                          where product.Contains(irsAyr.ehStkID)
                          where saleTypes.Contains(irs.eTip)
                          select new { irs, irsAyr, urn, posMagaza, frm});

            ViewData["storeCount"] = storeCount;
            ViewData["productCount"] = productCount;
            ViewData["group"] = groupFilter;
            ViewData["city"] = cityFilter;
            ViewData["brand"] = brandFilter;
            ViewData["category"] = categoryFilter;
            ViewData["statue"] = statueFilter;

            var category = new List<int>();
            var brand = new List<int>();
            var city = new List<int>();
            var groups = new List<int>();
            var status = new List<int>();
            var statusList = new List<string>();
            string categoryName = string.Empty, brandName = string.Empty,
            cityName = string.Empty, groupName = string.Empty, statusName = string.Empty;

            if (categoryFilter != "")
            {
                category.AddRange(categoryFilter.Split(',').Select(item => Convert.ToInt32(item)));

                categoryName = string.Join(",", Db.urnKtgrAg.Where(x => category.Contains(x.kID)).Select(x => x.kAd));

                stores = (from store in stores
                          join urnKtgrAg in Db.urnKtgrAg on store.urn.urnKtgrID equals urnKtgrAg.k0
                          where urnKtgrAg.k1 == 0
                          where category.Contains(urnKtgrAg.kID)
                          select store);
                //stores = stores.Where(x => x.urnKtgrAg.k1 == 0 && category.Contains(x.urnKtgrAg.kID));
            }
            if (brandFilter != "")
            {

                brand.AddRange(brandFilter.Split(',').Select(item => Convert.ToInt32(item)));
                brandName = string.Join(",", Db.urnMrk.Where(x => brand.Contains(x.mrkID)).Select(x => x.mrkAd));

                stores = stores.Where(x => brand.Contains(x.urn.urnMrkID));
            }
            if (cityFilter != "")
            {

                city.AddRange(cityFilter.Split(',').Select(item => Convert.ToInt32(item)));
                cityName = string.Join(",", Db.frmKent.Where(x => city.Contains(x.alanKodu)).Select(x => x.ilAd));

                stores = stores.Where(x => city.Contains(x.frm.il));
            }
            if (groupFilter != "")
            {
                groups.AddRange(groupFilter.Split(',').Select(item => Convert.ToInt32(item)));
                groupName = string.Join(",", Db.posMagazaGrup.Where(x => groups.Contains(x.mekanGrupID)).Select(x => x.mekanGrupAd));
                stores = stores.Where(x => groups.Contains(x.posMagaza.mekanGrup));

            }
            if (statueFilter != "")
            {
                status.AddRange(statueFilter.Split(',').Select(item => Convert.ToInt32(item)));
                if (status.Contains(0)) { statusList.Add("Açık"); }
                if (status.Contains(1)) { statusList.Add("Kapalı"); }
                if (status.Contains(2)) { statusList.Add("Açılmamış"); }

                statusName = string.Join(",", statusList);

                if (status.Count == 1)
                {
                    stores = !status.Contains(2) ?
                        (status.Contains(0) ? stores.Where(x => x.posMagaza.mekanDurum == 0)
                        : stores.Where(x => x.posMagaza.mekanDurum != 0))
                        : stores.Where(x => x.posMagaza.mekanDurum == 0 && x.posMagaza.mekanAcilisTarih > DateTime.Now);
                }
                if (status.Count == 2)
                {
                    if (!status.Contains(1))
                    {
                        stores = stores.Where(x => x.posMagaza.mekanDurum == 0);
                    }
                    if (!status.Contains(0))
                    {
                        stores = stores.Where(x => (x.posMagaza.mekanDurum == 0
                                       && x.posMagaza.mekanAcilisTarih > DateTime.Now)
                                       || x.posMagaza.mekanDurum != 0);
                    }
                }
            }
            var model = new SalesPerRegionModel
            {
                Reports = (from store in stores
                           group store.irsAyr by new { store.frm.il } into g
                           select new SalesPerRegionModel.SalesPerRegion
                           {
                               Region =Db.frmKent.FirstOrDefault(x=>x.alanKodu==g.Key.il).ilAd,

                               TotalSaleCount = g.Count(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
                    (-1) * g.Where(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehAdet) : 0,

                       RefundofSaleCount = g.Count(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
                      g.Where(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehAdet) : 0,


                      TotalSale = g.Count(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
   g.Where(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehTutar - x.ehIndirim + x.ehTutarKDV) : 0,

            RefundofSale = g.Count(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
 g.Where(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehTutar - x.ehIndirim + x.ehTutarKDV) : 0

                           }
                     ).ToList(),
                DateFilter = dateFilter,
                Categories = categoryName,
                Brands = brandName,
                ProductCount = productCount,
                Cities = cityName,
                Groups = groupName,
                Status = statusName,
                StoreCount = storeCount
            };

            return View(model);
        }



        #endregion

        #region Grup Bazlı Satış Raporu

        public ActionResult SalesPerGroupReport(DocumentDateFilterModel dateFilter, string cityFilter = "", string groupFilter = "", string statueFilter = "", string categoryFilter = "", string brandFilter = "", string storeCount = "", string productCount = "")
        {

            dateFilter.NotNullOrSetDefault(DateHelper.LastMonthBegin, DateHelper.LastMonthEnd);

            var stores = (from irsAyr in Db.irsAyr
                          join irs in Db.irs on irsAyr.ehID equals irs.eID
                          join urn in Db.urn on irsAyr.ehStkID equals urn.stkID
                          join posMagaza in Db.posMagaza on irs.eMekan equals posMagaza.mekanID
                          where irs.eTarih >= dateFilter.Begin && irs.eTarih <= dateFilter.End
                          where product.Contains(irsAyr.ehStkID)
                          where saleTypes.Contains(irs.eTip)
                          select new { irs, irsAyr, urn, posMagaza });

            ViewData["storeCount"] = storeCount;
            ViewData["productCount"] = productCount;
            ViewData["group"] = groupFilter;
            ViewData["city"] = cityFilter;
            ViewData["brand"] = brandFilter;
            ViewData["category"] = categoryFilter;
            ViewData["statue"] = statueFilter;

            var category = new List<int>();
            var brand = new List<int>();
            var city = new List<int>();
            var groups = new List<int>();
            var status = new List<int>();
            var statusList = new List<string>();
            string categoryName = string.Empty, brandName = string.Empty,
            cityName = string.Empty, groupName = string.Empty, statusName = string.Empty;

            if (categoryFilter != "")
            {
                category.AddRange(categoryFilter.Split(',').Select(item => Convert.ToInt32(item)));

                categoryName = string.Join(",", Db.urnKtgrAg.Where(x => category.Contains(x.kID)).Select(x => x.kAd));

                stores = (from store in stores
                          join urnKtgrAg in Db.urnKtgrAg on store.urn.urnKtgrID equals urnKtgrAg.k0
                          where urnKtgrAg.k1 == 0
                          where category.Contains(urnKtgrAg.kID)
                          select store);
                //stores = stores.Where(x => x.urnKtgrAg.k1 == 0 && category.Contains(x.urnKtgrAg.kID));
            }
            if (brandFilter != "")
            {

                brand.AddRange(brandFilter.Split(',').Select(item => Convert.ToInt32(item)));
                brandName = string.Join(",", Db.urnMrk.Where(x => brand.Contains(x.mrkID)).Select(x => x.mrkAd));

                stores = stores.Where(x => brand.Contains(x.urn.urnMrkID));
            }
            if (cityFilter != "")
            {

                city.AddRange(cityFilter.Split(',').Select(item => Convert.ToInt32(item)));
                cityName = string.Join(",", Db.frmKent.Where(x => city.Contains(x.alanKodu)).Select(x => x.ilAd));

                stores = from store in stores
                         join frm in Db.frm on store.irs.eMekan equals frm.frmID
                         where city.Contains(frm.il)
                         select store;
                //stores = stores.Where(x => city.Contains(x.frm.il));
            }
            if (groupFilter != "")
            {
                groups.AddRange(groupFilter.Split(',').Select(item => Convert.ToInt32(item)));
                groupName = string.Join(",", Db.posMagazaGrup.Where(x => groups.Contains(x.mekanGrupID)).Select(x => x.mekanGrupAd));
                stores = stores.Where(x => groups.Contains(x.posMagaza.mekanGrup));

            }
            if (statueFilter != "")
            {
                status.AddRange(statueFilter.Split(',').Select(item => Convert.ToInt32(item)));
                if (status.Contains(0)) { statusList.Add("Açık"); }
                if (status.Contains(1)) { statusList.Add("Kapalı"); }
                if (status.Contains(2)) { statusList.Add("Açılmamış"); }

                statusName = string.Join(",", statusList);

                if (status.Count == 1)
                {
                    stores = !status.Contains(2) ?
                        (status.Contains(0) ? stores.Where(x => x.posMagaza.mekanDurum == 0)
                        : stores.Where(x => x.posMagaza.mekanDurum != 0))
                        : stores.Where(x => x.posMagaza.mekanDurum == 0 && x.posMagaza.mekanAcilisTarih > DateTime.Now);
                }
                if (status.Count == 2)
                {
                    if (!status.Contains(1))
                    {
                        stores = stores.Where(x => x.posMagaza.mekanDurum == 0);
                    }
                    if (!status.Contains(0))
                    {
                        stores = stores.Where(x => (x.posMagaza.mekanDurum == 0
                                       && x.posMagaza.mekanAcilisTarih > DateTime.Now)
                                       || x.posMagaza.mekanDurum != 0);
                    }
                }
            }
            var model = new SalesPerGroupModel
            {
                Reports = from store in stores
                          group store.irsAyr by new { store.posMagaza.mekanGrup } into g
                          select new SalesPerGroupModel.SalesPerGroup
                          {
                Group = Db.posMagazaGrup.FirstOrDefault(x=>x.mekanGrupID==g.Key.mekanGrup).mekanGrupAd,

               TotalSaleCount = g.Count(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
              (-1) * g.Where(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehAdet) : 0,

               RefundofSaleCount = g.Count(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
              g.Where(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehAdet) : 0,

                 TotalSale = g.Count(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
   g.Where(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehTutar - x.ehIndirim + x.ehTutarKDV) : 0,

                     RefundofSale = g.Count(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
  g.Where(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehTutar - x.ehIndirim + x.ehTutarKDV) : 0
                          },
                DateFilter = dateFilter,
                Categories = categoryName,
                Brands = brandName,
                ProductCount = productCount,
                Cities = cityName,
                Groups = groupName,
                Status = statusName,
                StoreCount = storeCount
            };

            return View(model);
        }


        #endregion

        #region Boyut Bazlı Satış Raporu

        public ActionResult SalesPerSizeReport(DocumentDateFilterModel dateFilter, string cityFilter = "", string groupFilter = "", string statueFilter = "", string categoryFilter = "", string brandFilter = "", string storeCount = "", string productCount = "")
        {

            dateFilter.NotNullOrSetDefault(DateHelper.LastMonthBegin, DateHelper.LastMonthEnd);

            var stores = (from irsAyr in Db.irsAyr
                          join irs in Db.irs on irsAyr.ehID equals irs.eID
                          join urn in Db.urn on irsAyr.ehStkID equals urn.stkID
                          join posMagaza in Db.posMagaza on irs.eMekan equals posMagaza.mekanID
                          where irs.eTarih >= dateFilter.Begin && irs.eTarih <= dateFilter.End
                          where product.Contains(irsAyr.ehStkID)
                          where saleTypes.Contains(irs.eTip)
                          select new { irs, irsAyr, urn, posMagaza });

            ViewData["storeCount"] = storeCount;
            ViewData["productCount"] = productCount;
            ViewData["group"] = groupFilter;
            ViewData["city"] = cityFilter;
            ViewData["brand"] = brandFilter;
            ViewData["category"] = categoryFilter;
            ViewData["statue"] = statueFilter;

            var category = new List<int>();
            var brand = new List<int>();
            var city = new List<int>();
            var groups = new List<int>();
            var status = new List<int>();
            var statusList = new List<string>();
            string categoryName = string.Empty, brandName = string.Empty,
            cityName = string.Empty, groupName = string.Empty, statusName = string.Empty;

            if (categoryFilter != "")
            {
                category.AddRange(categoryFilter.Split(',').Select(item => Convert.ToInt32(item)));

                categoryName = string.Join(",", Db.urnKtgrAg.Where(x => category.Contains(x.kID)).Select(x => x.kAd));

                stores = (from store in stores
                          join urnKtgrAg in Db.urnKtgrAg on store.urn.urnKtgrID equals urnKtgrAg.k0
                          where urnKtgrAg.k1 == 0
                          where category.Contains(urnKtgrAg.kID)
                          select store);
                //stores = stores.Where(x => x.urnKtgrAg.k1 == 0 && category.Contains(x.urnKtgrAg.kID));
            }
            if (brandFilter != "")
            {

                brand.AddRange(brandFilter.Split(',').Select(item => Convert.ToInt32(item)));
                brandName = string.Join(",", Db.urnMrk.Where(x => brand.Contains(x.mrkID)).Select(x => x.mrkAd));

                stores = stores.Where(x => brand.Contains(x.urn.urnMrkID));
            }
            if (cityFilter != "")
            {

                city.AddRange(cityFilter.Split(',').Select(item => Convert.ToInt32(item)));
                cityName = string.Join(",", Db.frmKent.Where(x => city.Contains(x.alanKodu)).Select(x => x.ilAd));

                stores = from store in stores
                         join frm in Db.frm on store.irs.eMekan equals frm.frmID
                         where city.Contains(frm.il)
                         select store;
                //stores = stores.Where(x => city.Contains(x.frm.il));
            }
            if (groupFilter != "")
            {
                groups.AddRange(groupFilter.Split(',').Select(item => Convert.ToInt32(item)));
                groupName = string.Join(",", Db.posMagazaGrup.Where(x => groups.Contains(x.mekanGrupID)).Select(x => x.mekanGrupAd));
                stores = stores.Where(x => groups.Contains(x.posMagaza.mekanGrup));

            }
            if (statueFilter != "")
            {
                status.AddRange(statueFilter.Split(',').Select(item => Convert.ToInt32(item)));
                if (status.Contains(0)) { statusList.Add("Açık"); }
                if (status.Contains(1)) { statusList.Add("Kapalı"); }
                if (status.Contains(2)) { statusList.Add("Açılmamış"); }

                statusName = string.Join(",", statusList);

                if (status.Count == 1)
                {
                    stores = !status.Contains(2) ?
                        (status.Contains(0) ? stores.Where(x => x.posMagaza.mekanDurum == 0)
                        : stores.Where(x => x.posMagaza.mekanDurum != 0))
                        : stores.Where(x => x.posMagaza.mekanDurum == 0 && x.posMagaza.mekanAcilisTarih > DateTime.Now);
                }
                if (status.Count == 2)
                {
                    if (!status.Contains(1))
                    {
                        stores = stores.Where(x => x.posMagaza.mekanDurum == 0);
                    }
                    if (!status.Contains(0))
                    {
                        stores = stores.Where(x => (x.posMagaza.mekanDurum == 0
                                       && x.posMagaza.mekanAcilisTarih > DateTime.Now)
                                       || x.posMagaza.mekanDurum != 0);
                    }
                }
            }
            var model = new SalesPerSizeModel
            {
                Reports = from store in stores
                          group store.irsAyr by new { store.posMagaza.mekanGrup1 } into g
                          select new SalesPerSizeModel.SalesPerSize
                          {
                     Size = Db.posMagazaGrup1.FirstOrDefault(x => x.mekanGrup1ID == g.Key.mekanGrup1).mekanGrup1Ad,

                    TotalSaleCount = g.Count(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
                   (-1) * g.Where(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehAdet) : 0,

                   RefundofSaleCount = g.Count(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
                    g.Where(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehAdet) : 0,

                    TotalSale = g.Count(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
  g.Where(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehTutar - x.ehIndirim + x.ehTutarKDV) : 0,

                   RefundofSale = g.Count(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
 g.Where(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehTutar - x.ehIndirim + x.ehTutarKDV) : 0
                          },
                DateFilter = dateFilter,
                Categories = categoryName,
                Brands = brandName,
                ProductCount = productCount,
                Cities = cityName,
                Groups = groupName,
                Status = statusName,
                StoreCount = storeCount
            };

            return View(model);
        }


        #endregion

        #region Rapor Filtesi
        public ActionResult LoadStoreFilter()
        {
            
            var model = new ReportFilterModel
                            {
                                OpenStoreCount =
                                        (from irs in Db.irs
                                         join irsAyr in Db.irsAyr on irs.eID equals irsAyr.ehID
                                         join posMagaza in Db.posMagaza on irs.eMekan equals posMagaza.mekanID
                                         where irs.eFirma == CurrentCompany.frmID
                                         where posMagaza.mekanDurum == 0
                                         where posMagaza.mekanAcilisTarih < DateTime.Now
                                         where product.Contains(irsAyr.ehStkID)
                                         where saleTypes.Contains(irs.eTip)
                                         group posMagaza by new { posMagaza.mekanAd } into g
                                         select g).Count(),
                                PreparedStoreCount =
                                         (from irs in Db.irs
                                          join irsAyr in Db.irsAyr on irs.eID equals irsAyr.ehID
                                          join posMagaza in Db.posMagaza on irs.eMekan equals posMagaza.mekanID
                                          where irs.eFirma == CurrentCompany.frmID
                                          where posMagaza.mekanDurum == 0
                                          where posMagaza.mekanAcilisTarih > DateTime.Now
                                          where product.Contains(irsAyr.ehStkID)
                                          where saleTypes.Contains(irs.eTip)
                                          group posMagaza by new { posMagaza.mekanAd } into g
                                          select g).Count(),
                                ClosedStoreCount =
                                         (from irs in Db.irs
                                          join irsAyr in Db.irsAyr on irs.eID equals irsAyr.ehID
                                          join posMagaza in Db.posMagaza on irs.eMekan equals posMagaza.mekanID
                                          where irs.eFirma == CurrentCompany.frmID
                                          where posMagaza.mekanDurum != 0
                                          where product.Contains(irsAyr.ehStkID)
                                          where saleTypes.Contains(irs.eTip)
                                          group posMagaza by new { posMagaza.mekanAd } into g
                                          select g).Count(),
                                Cities =
                                        from irs in Db.irs
                                        join irsAyr in Db.irsAyr on irs.eID equals irsAyr.ehID
                                        join posMagaza in Db.posMagaza on irs.eMekan equals posMagaza.mekanID
                                        join frm in Db.frm on irs.eMekan equals frm.frmID
                                        join frmKent in Db.frmKent on frm.il equals frmKent.alanKodu
                                        where irs.eFirma == CurrentCompany.frmID
                                        group new { posMagaza, irs, irsAyr } by 
                                        new { frmKent.alanKodu, frmKent.ilAd, frmKent.ilBolge } into g
                                        select new ReportFilterModel.City
                                               {
                                                   RegionId = g.Key.ilBolge,
                                                   CityId = g.Key.alanKodu,
                                                   CityName = g.Key.ilAd,
                                                   StoreCount = g.Where(x => product.Contains(x.irsAyr.ehStkID))
                                                                         .Where(x => saleTypes.Contains(x.irs.eTip))
                                                                         .GroupBy(x => x.posMagaza.mekanAd).Count()
                                                   
                                               },
                                Regions =
                                        from irs in Db.irs
                                        join irsAyr in Db.irsAyr on irs.eID equals irsAyr.ehID
                                        join posMagaza in Db.posMagaza on irs.eMekan equals posMagaza.mekanID
                                        join frm in Db.frm on irs.eMekan equals frm.frmID
                                        join frmKent in Db.frmKent on frm.il equals frmKent.alanKodu
                                        join frmKentBolge in Db.frmKentBolge on frmKent.ilBolge
                                        equals frmKentBolge.bolgeID
                                        where irs.eFirma == CurrentCompany.frmID
                                        group new {posMagaza,irs,irsAyr} by new { frmKentBolge.bolgeAd, frmKentBolge.bolgeID } into g
                                        select new ReportFilterModel.Region
                                                       {
                                                           RegionId = g.Key.bolgeID,
                                                           RegionName = g.Key.bolgeAd,
                                                           StoreCount = g.Where(x=>product.Contains(x.irsAyr.ehStkID))
                                                                         .Where(x => saleTypes.Contains(x.irs.eTip))
                                                                         .GroupBy(x=>x.posMagaza.mekanAd).Count()

                                                            
                                                       },
                                Groups =
                                     from irs in Db.irs
                                     join irsAyr in Db.irsAyr on irs.eID equals irsAyr.ehID
                                     join posMagaza in Db.posMagaza on irs.eMekan equals posMagaza.mekanID
                                     join posMagazaGrup in Db.posMagazaGrup on posMagaza.mekanGrup
                                     equals posMagazaGrup.mekanGrupID
                                     where irs.eFirma == CurrentCompany.frmID
                                     group new { posMagaza, irs, irsAyr } by 
                                     new { posMagazaGrup.mekanGrupAd, posMagazaGrup.mekanGrupID } into g
                                     select new ReportFilterModel.Group
                                                    {
                                                        GroupId = g.Key.mekanGrupID,
                                                        GroupName = g.Key.mekanGrupAd,
                                                        StoreCount = g.Where(x => product.Contains(x.irsAyr.ehStkID))
                                                                         .Where(x => saleTypes.Contains(x.irs.eTip))
                                                                         .GroupBy(x => x.posMagaza.mekanAd).Count()
                                                    },

                            };

            return Json(model);
        }
        public ActionResult LoadProductFilter()
        {
            var model = new ReportFilterModel
                {
                    TotalCount = (from irsAyr in Db.irsAyr
                         join irs in Db.irs on irsAyr.ehID equals irs.eID
                         join urn in Db.urn on irsAyr.ehStkID equals urn.stkID
                         join urnKtgrAg in Db.urnKtgrAg on urn.urnKtgrID equals urnKtgrAg.k0
                         where urnKtgrAg.k1 == 0
                         where irs.eFirma==CurrentCompany.frmID
                         where product.Contains(irsAyr.ehStkID)
                         where saleTypes.Contains(irs.eTip)
                         group urn by urn.stkID into g
                         select g).Count(),

                    Categories = from irsAyr in Db.irsAyr
                         join irs in Db.irs on irsAyr.ehID equals irs.eID
                         join urn in Db.urn on irsAyr.ehStkID equals urn.stkID
                         join urnKtgrAg in Db.urnKtgrAg on urn.urnKtgrID equals urnKtgrAg.k0
                         where urnKtgrAg.k1==0
                         where irs.eFirma==CurrentCompany.frmID
                         group new { urn ,irs,irsAyr} by new { urnKtgrAg.kID, urnKtgrAg.kAd, urnKtgrAg.k0 } into g
                         select new ReportFilterModel.Category{
                               CategoryId = g.Key.kID,
                               CategoryName = g.Key.kAd,
                               ProductCount = g.Where(x => product.Contains(x.irsAyr.ehStkID))
                                               .Where(x => saleTypes.Contains(x.irs.eTip))
                                               .GroupBy(x => x.urn.stkID).Count()
                           },

                    Producers = from urn in Db.urn
                         join irsAyr in Db.irsAyr on urn.stkID equals irsAyr.ehStkID
                         join irs in Db.irs on irsAyr.ehID equals irs.eID
                         join urnMrk in Db.urnMrk on urn.urnMrkID equals urnMrk.mrkID
                         join urnMrkUrt in Db.urnMrkUrt on urnMrk.mrkUretici equals urnMrkUrt.urtID
                         //join urnKtgrAg in Db.urnKtgrAg on urn.urnKtgrID equals urnKtgrAg.k0
                         //where urnKtgrAg.k1 == 0
                         where irs.eFirma==CurrentCompany.frmID
                         group new { urn, irs, irsAyr } by new { urnMrkUrt.urtID, urnMrkUrt.urtAd } into g
                         select new ReportFilterModel.Producer
                             {
                                 ProducerId = g.Key.urtID,
                                 ProducerName = g.Key.urtAd,
                                 ProductCount = g.Where(x => product.Contains(x.irsAyr.ehStkID))
                                               .Where(x => saleTypes.Contains(x.irs.eTip))
                                               .GroupBy(x => x.urn.stkID).Count()
                             },
                    Brands = from urn in Db.urn
                             join irsAyr in Db.irsAyr on urn.stkID equals irsAyr.ehStkID
                             join irs in Db.irs on irsAyr.ehID equals irs.eID
                             join urnMrk in Db.urnMrk on urn.urnMrkID equals urnMrk.mrkID
                             //join urnKtgrAg in Db.urnKtgrAg on urn.urnKtgrID equals urnKtgrAg.k0
                             //where urnKtgrAg.k1 == 0
                             where irs.eFirma == CurrentCompany.frmID
                             group new { urn, irs, irsAyr } by new { urnMrk.mrkID, urnMrk.mrkAd, urnMrk.mrkUretici } into g
                             select new ReportFilterModel.Brand
                             {
                                ProducerId = g.Key.mrkUretici,
                                BrandId = g.Key.mrkID,
                                BrandName = g.Key.mrkAd,
                                ProductCount = g.Where(x => product.Contains(x.irsAyr.ehStkID))
                                                .Where(x => saleTypes.Contains(x.irs.eTip))
                                                .GroupBy(x => x.urn.stkID).Count()
                             }
                    
                     
                };

            return Json(model);
        }
        public ActionResult GetStoresCount(IList<int> cities, IList<int> groups, IList<int> status)
        {
            var stores = (from irs in Db.irs
                          join irsAyr in Db.irsAyr on irs.eID equals irsAyr.ehID
                          join posMagaza in Db.posMagaza on irs.eMekan equals posMagaza.mekanID
                          join frm in Db.frm on irs.eMekan equals frm.frmID
                          join frmKent in Db.frmKent on frm.il equals frmKent.alanKodu
                          join posMagazaGrup in Db.posMagazaGrup on posMagaza.mekanGrup
                          equals posMagazaGrup.mekanGrupID
                          where irs.eFirma == CurrentCompany.frmID
                          where product.Contains(irsAyr.ehStkID)
                          where saleTypes.Contains(irs.eTip)
                          select new{posMagaza,frmKent});


            if (cities != null)
            {
               stores = stores.Where(x => cities.Contains(x.frmKent.alanKodu));
            }
            if (groups != null)
            {
                stores = stores.Where(x => groups.Contains(x.posMagaza.mekanGrup));
            }
            if (status != null)
            {
                if (status.Count == 1)
                {
                stores = !status.Contains(2) ? 
                    (status.Contains(0)?stores.Where(x =>x.posMagaza.mekanDurum==0)
                    :stores.Where(x => x.posMagaza.mekanDurum!=0))
                    :stores.Where(x => x.posMagaza.mekanDurum == 0 && x.posMagaza.mekanAcilisTarih> DateTime.Now);
                }
                if (status.Count == 2 )
                {
                    if(!status.Contains(1))
                   {
                    stores = stores.Where(x => x.posMagaza.mekanDurum == 0);
                   }
                    if (!status.Contains(0))
                    {
                      stores = stores.Where(x =>(x.posMagaza.mekanDurum==0 
                                     && x.posMagaza.mekanAcilisTarih>DateTime.Now)
                                     || x.posMagaza.mekanDurum!=0);
                    }
                }
               

            }


                return Json(stores.GroupBy(x=>x.posMagaza.mekanAd).Count());
       }

        public ActionResult GetProductsCount(IList<int> categories,List<int> brands )
        {
            var products =
                  from irsAyr in Db.irsAyr
                  join irs in Db.irs on irsAyr.ehID equals irs.eID
                  join urn in Db.urn on irsAyr.ehStkID equals urn.stkID
                  join urnKtgrAg in Db.urnKtgrAg on urn.urnKtgrID equals urnKtgrAg.k0
                  //where urnKtgrAg.k1 == 0
                  where product.Contains(irsAyr.ehStkID)
                  where saleTypes.Contains(irs.eTip)
                  select new {urn,urnKtgrAg};

              if (categories != null)
              {
                 products = products.Where(x =>x.urnKtgrAg.k1 == 0 && categories.Contains(x.urnKtgrAg.kID));
                  
              }
              if (brands != null)
              {
                  products = products.Where(x=>brands.Contains(x.urn.urnMrkID));
              }
              return Json(products.GroupBy(x => x.urn.stkID).Count());
          }

        #endregion

        
    }
}