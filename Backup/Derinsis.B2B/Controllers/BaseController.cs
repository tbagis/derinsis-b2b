﻿namespace DerinSIS.B2B.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using Models;
    using Infrastructure.Auth;

    public abstract class BaseController : Controller
    {
        protected BaseController(DbEntities db)
        {
            Db = db;
        }

        protected DbEntities Db { get; private set; }

        #region Aktif Hesap Bilgileri

        private frmYetkili _currentAccount;
        public frmYetkili CurrentAccount
        {
            get
            {
                if (!User.Identity.IsAuthenticated)
                {
                    return null;
                }

                if (_currentAccount == null)
                {
                    var userEmail = User.Identity.Name;
                    _currentAccount = Db.frmYetkili
                        .SingleOrDefault(x => x.ytkEposta == userEmail);    
                }
                return _currentAccount;
            }
        }

        public frm CurrentCompany
        {
            get
            {
                if (CurrentAccount == null)
                {
                    return null;
                }

                return CurrentAccount.frm;
            }
        }

        public AccountType? CurrentAccountType
        {
            get
            {
                if (CurrentCompany == null)
                {
                    return null;
                }

                if (CurrentCompany.frmTip == 1)
                {
                
                    return AccountType.Customer;
                }

                if (CurrentCompany.frmTip == 0)
                {
                    return AccountType.Supplier;
                }

                throw new InvalidOperationException("Sadece frmTip=1 ve frmTip=0 olan kullanıcılar giriş yapabilirler.");
            }
        }

        #endregion

        #region Messages

        protected void ShowErrorMessage(string message="İşlem yapılamadı. Lütfen bilgilerinizi kontrol edip tekrar deneyiniz")
        {
            var messages = (List<string>) TempData["_error_messages"] ?? new List<string>();
            messages.Add(message);
            TempData["_error_messages"] = messages;
        }

        protected void ShowSuccessMessage(string message)
        {
            var messages = (List<string>)TempData["_success_messages"] ?? new List<string>();
            messages.Add(message);
            TempData["_success_messages"] = messages;
        }

        #endregion
    }
}