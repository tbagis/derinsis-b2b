﻿namespace DerinSIS.B2B.Controllers
{
    using System;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using Helpers;
    using Infrastructure.Auth;
    using Models;
    using ViewModels;

    [B2BAuthorize(AccountType.Supplier | AccountType.Customer)]
    public class ThumbnailController : BaseController
    {
        public ThumbnailController(DbEntities db) : base(db)
        {
        }

        #region Slide Shows
        
        [ChildActionOnly]
        public ActionResult ProductSlideShow(int id)
        {
            var folderName = string.Format("U{0}", id);
            var folderPath = Path.Combine(ThumbRoot, folderName);

            var model = new ProductSlideShowModel();
            if(Directory.Exists(folderPath) && Directory.EnumerateFiles(folderPath).Any())
            {
                model.Images = Directory.EnumerateFiles(folderPath)
                    .Select(Path.GetFileName)
                    .Select(x => Path.Combine(folderName, x))
                    .ToList();
            }

            return PartialView(model);
        }

        [ChildActionOnly]
        public ActionResult ProductGroupSlideShow(int id)
        {
            var folderName = string.Format("G{0}", id);
            var folderPath = Path.Combine(ThumbRoot, folderName);

            var model = new ProductGroupSlideShowModel();
            if (Directory.Exists(folderPath) && Directory.EnumerateFiles(folderPath).Any())
            {
                model.Images = Directory.EnumerateFiles(folderPath)
                    .Select(Path.GetFileName)
                    .Select(x => Path.Combine(folderName, x))
                    .ToList();
            }

            return PartialView(model);
        }

        #endregion

        #region Thubmanil Image Generators

        public ActionResult ProductImage(int id, int width, int height)
        {
            var klasorAdi = string.Format("U{0}", id);
            var dosyaAdi = string.Format("U{0}-1.jpg", id);

            return FitThumb(Path.Combine(klasorAdi, dosyaAdi), width, height);
        }

        public ActionResult ModelImage(int id, int width, int height)
        {
            var klasorAdi = string.Format("G{0}", id);
            var dosyaAdi = string.Format("G{0}-1.jpg", id);

            return FitThumb(Path.Combine(klasorAdi, dosyaAdi), width, height);
        }

        public ActionResult PlaceHolderThumb(int width, int height)
        {
            return Thumnail(PlaceHolderImagePath(), width, height);
        }

        public ActionResult FitThumb(string name, int width, int height)
        {
            Check.NotNullOr404(name);
            var path = Path.Combine(ThumbRoot, name);

            if(!System.IO.File.Exists(path))
            {
                return Thumnail(PlaceHolderImagePath(), width, height); 
            }

            return Thumnail(path, width, height);
        }

        #endregion

        #region Utility Functions

        private ActionResult Thumnail(string path, int width, int height)
        {
            using (var original = Image.FromFile(path))
            using (var resizedImage = ResizeImage(original, width, height))
            using (var stream = new MemoryStream())
            {
                resizedImage.Save(stream, ImageFormat.Png);
                return File(stream.ToArray(), "image/png");
            }
        }

        private Image ResizeImage(Image original, int width, int height)
        {
            var newImage = new Bitmap(width, height);

            var widthRatio = (double) width/original.Width;
            var heightRatio = (double) height/original.Height;
            var aspectRatio = Math.Min(widthRatio, heightRatio);

            var scaledWidth = (int) (original.Width*aspectRatio);
            var scaledHeight = (int) (original.Height*aspectRatio);

            var scaledXOffset = (width - scaledWidth)/2;
            var scaledYOffset = (height - scaledHeight)/2;

            using (var graphics = Graphics.FromImage(newImage))
            {
                graphics.SmoothingMode = SmoothingMode.AntiAlias;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
                graphics.DrawImage(original, new Rectangle(scaledXOffset, scaledYOffset, scaledWidth, scaledHeight));
            }

            return newImage;
        }

        private string PlaceHolderImagePath()
        {
            return Server.MapPath("~/Assets/theme/img/img-placeholder.png");
        }

        private string ThumbRoot
        {
            get
            {
                return Db.drn2ayar
                    .Single(x => x.ayarID == 410)
                    .ayarDeger;
            }
        }

        #endregion
    }
}