﻿using System.Web.Routing;

namespace DerinSIS.B2B.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using Helpers;
    using Infrastructure;
    using Infrastructure.Auth;
    using Models;
    using ViewModels;

    [B2BAuthorize(AccountType.Supplier | AccountType.Customer, Privilege.FATURA | Privilege.CARI)]
    public class AccountingController : BaseController
    {
        private readonly SystemSettings _settings;

        public AccountingController(DbEntities db, SystemSettings settings) : base(db)
        {
            _settings = settings;
        }

        #region Dashboard
        
        public ActionResult Index()
        {
            // Banka Hesap Bilgileri
            var bankaHesapBilgileri = Db.frmBnk
                .Where(x => x.frmBnkFirmaID == CurrentCompany.frmID)
                .Select(x => new AccountingDashboardModel.BankaHesapBilgisi
                             {
                                 HesapNo = x.frmBnkHesapNo,
                                 Sube = x.bnkSube.subeAd,
                                 Banka = x.bnkSube.bnk.bankaAd,
                             })
                .ToList();

            // Ödenmemiş Faturalar
            var odenmemisFaturalar = Db.car
                .Where(x => x.cKod == CurrentCompany.frmID)
                .Where(x => x.cKalan != 0)
                .Join(Db.fat, car => car.cFatID, fat => fat.eID, (car, fat) => new {car, fat})
                .OrderByDescending(x => x.car.cTarih)
                .Select(x => new AccountingDashboardModel.OdenmemisFatura
                             {
                                 Id = x.fat.eID,
                                 KalanTutar = x.car.cKalan,
                                 EvrakNo = x.fat.eNo,
                                 FaturaTarihi = x.fat.eTarih,
                                 VadeTarihi = x.fat.eTarihV,
                             })
                .ToList();

            // Bakiye Bilgileri
            var borc = Db.car
                           .Where(x => x.cKod == CurrentCompany.frmID)
                           .Where(x => x.cBA == 1 /* Borç */)
                           .Sum(x => (decimal?) x.cTutar) ?? 0;

            var alacak = Db.car
                             .Where(x => x.cKod == CurrentCompany.frmID)
                             .Where(x => x.cBA == 0 /* Alacak */)
                             .Sum(x => (decimal?) x.cTutar) ?? 0;


            // Model
            var model = new AccountingDashboardModel
                        {
                            BankaHesapBilgileri = bankaHesapBilgileri,
                            OdenmemisFaturalar = odenmemisFaturalar,
                            Borc = Math.Abs(borc),
                            Alacak = Math.Abs(alacak),
                            Bakiye = Math.Abs(borc) - Math.Abs(alacak),
                            SistemMutabakatTarihi = CurrentCompany.frmCariTarih,
                            FirmaMutabakatTarihi = CurrentCompany.frmCariTarihB2B ?? CurrentCompany.frmCariTarih,
                        };
            return View(model);
        }

        #endregion

        #region Mutabakat


        [B2BAuthorize(AccountType.Supplier | AccountType.Customer, Privilege.FATURA)]
        public ActionResult Reconsilation()
        {
        
            var reconsilations = new ReconsilationModel
            {
                Reconsilations =
                                Db.carMtbkt
                                .Select(x => new ReconsilationModel.Reconsilation
                                {
                                    BillAmount = x.mtbktAdet,
                                    EndDate = x.mtbktBitisTarih,
                                    TotalAmount = x.mtbktTutar,
                                    StartDate = x.mtbktBaslangicTarih,
                                    ReconsilationId = x.mtbktID
                                }).OrderByDescending(x => x.EndDate).ToList(),

                UnConfirmedReconsilations = new ReconsilationModel.Reconsilation
                {
                    BillAmount =
                          (from fat in Db.fat
                           let a = Db.carMtbkt.Max(x => x.mtbktBitisTarih)
                           let b = CurrentCompany.frmCariTarih
                           where fat.eFirma == CurrentCompany.frmID
                           where fat.eTarih >= a && fat.eTarih < b
                           select fat).Count(),
                    TotalAmount =
                           (from fatAyr in Db.fatAyr
                            join fat in Db.fat on fatAyr.ehID equals fat.eID
                            let a = Db.carMtbkt.Max(x => x.mtbktBitisTarih)
                            let b = CurrentCompany.frmCariTarih
                            where fat.eFirma == CurrentCompany.frmID
                            where fat.eTarih >= a && fat.eTarih < b
                            select (decimal?)fatAyr.ehTutar).Sum()
                }
            };

            return View(reconsilations);
        }

        [B2BAuthorize(AccountType.Supplier | AccountType.Customer, Privilege.FATURA)]
        public ActionResult ReconsilationDetails(int? id)
        {
            if (id != null)
            {
                var mutabakat = Db.carMtbkt.Single(x => x.mtbktID == id);

                var model = new ReconsilationDetailsModel{
                Reconsilate =true,
                StartDate = mutabakat.mtbktBaslangicTarih,
                EndDate = mutabakat.mtbktBitisTarih,
                Invoices = 
                   ( from fat in Db.fat
                    join frm in Db.frm on fat.eFirma equals frm.frmID
                    join sifTip in Db.sifTip on fat.eTip equals sifTip.hrktTipID
                    join sipNeden in Db.sipNeden on fat.Neden equals sipNeden.nedenID
                    join posMagaza in Db.posMagaza on fat.eMekan equals posMagaza.mekanID
                    join firmaMekan in Db.frm on fat.eFirmaMkn equals firmaMekan.frmID
                   /* Only Invoices for siftip */
                    where sifTip.hrktSIF == 2
                    where frm.frmID == CurrentAccount.frm.frmID
                    where fat.eTarih>=mutabakat.mtbktBaslangicTarih &&                                                   fat.eTarih<mutabakat.mtbktBitisTarih
                    orderby fat.eTarih descending, fat.eID descending
                    select new ReconsilationDetailsModel.Invoice
                    {
                        Id = fat.eID,
                        Tarih = fat.eTarih,
                        EvrakNo = fat.eNo,
                        HareketTipi = sifTip.hrktTipAd,
                        Durum = (ReconsilationDetailsModel.InvoiceStatus)fat.eDurum,
                        VadeTarihi = fat.eTarihV,
                        EvrakNot = fat.eNot,
                        KDVHaricToplam = fat.fatAyr.Sum(x => x.ehTutar),
                        ToplamIndirim = fat.fatAyr.Sum(x => x.ehIndirim),
                        HareketGrubu = sipNeden.nedenAd,
                        SevkAdresi = frm.frmTip == 1 ? (fat.eFirmaMkn == 0 ? "" : firmaMekan.frmAd) : posMagaza.mekanAd
                    }).ToList()
                };
                return View(model);
            }
            else
            {
                var mutabakat = Db.carMtbkt.Where(x => x.mtbktFrmID == CurrentCompany.frmID)
                                           .OrderByDescending(x => x.mtbktBitisTarih)
                                           .First();

                var model = new ReconsilationDetailsModel
                {
                    Reconsilate = false,
                    EndDate = mutabakat.mtbktBitisTarih,
                    Invoices = (
                        from fat in Db.fat
                        join frm in Db.frm on fat.eFirma equals frm.frmID
                        join sifTip in Db.sifTip on fat.eTip equals sifTip.hrktTipID
                        join sipNeden in Db.sipNeden on fat.Neden equals sipNeden.nedenID
                        join posMagaza in Db.posMagaza on fat.eMekan equals posMagaza.mekanID
                        join firmaMekan in Db.frm on fat.eFirmaMkn equals firmaMekan.frmID
                        /* Only Invoices for siftip */
                        where sifTip.hrktSIF == 2
                        where frm.frmID == CurrentAccount.frm.frmID
                        where fat.eTarih > mutabakat.mtbktBitisTarih
                        orderby fat.eTarih descending, fat.eID descending
                        select new ReconsilationDetailsModel.Invoice
                        {
                            Id = fat.eID,
                            Tarih = fat.eTarih,
                            EvrakNo = fat.eNo,
                            HareketTipi = sifTip.hrktTipAd,
                            Durum = (ReconsilationDetailsModel.InvoiceStatus)fat.eDurum,
                            VadeTarihi = fat.eTarihV,
                            EvrakNot = fat.eNot,
                            KDVHaricToplam = fat.fatAyr.Sum(x => x.ehTutar),
                            ToplamIndirim = fat.fatAyr.Sum(x => x.ehIndirim),
                            HareketGrubu = sipNeden.nedenAd,
                            SevkAdresi = frm.frmTip == 1 ? (fat.eFirmaMkn == 0 ? "" : firmaMekan.frmAd) : posMagaza.mekanAd
                        }).ToList()
                };
                return View(model);
            }

      }
         [B2BAuthorize(AccountType.Supplier | AccountType.Customer, Privilege.FATURA)]
       public ActionResult ConfirmReconsilation(DateTime startdate,int count,decimal total)
         {
             var newReconsilation = new carMtbkt
                                        {
                                           mtbktBaslangicTarih = startdate,
                                           mtbktBitisTarih = DateTime.Now,
                                           mtbktAdet = count,
                                           mtbktTutar = total,
                                           gTarih = DateTime.Now,
                                           gKisi = CurrentAccount.ytkInsID,
                                           mtbktFrmID = CurrentCompany.frmID
                                        };
             Db.carMtbkt.Add(newReconsilation);
             Db.SaveChanges();
             ShowSuccessMessage("Mutabakat başarıyla onaylandı!");
             return Redirect("ReconsilationDetails/"+newReconsilation.mtbktID+"");
         }

            #endregion

        #region Hesap Hareketleri

        [B2BAuthorize(AccountType.Supplier | AccountType.Customer, Privilege.CARI)]
        public ActionResult Activities(DocumentDateFilterModel dateFilter)
        {
            dateFilter.NotNullOrSetDefault(DateHelper.LastMonthBegin, DateHelper.LastMonthEnd);

            var activities =
                (
                    from car in Db.car
                    join carTipF in Db.carTipF on car.cFatTip equals carTipF.ctfID
                    join frm in Db.frm on car.cKod equals frm.frmID
                    where frm.frmID == CurrentAccount.frm.frmID
                    where (car.cTarih >= dateFilter.Begin) && (car.cTarih <= dateFilter.End)
                    orderby car.cTarih
                    let fat = Db.fat.FirstOrDefault(x => x.eID == car.cFatID)
                    select new AccountActivitiesModel.AccountActivity()
                               {
                                   Id = car.cID,
                                   IlgiliFaturaId = fat != null ? fat.eID : (int?) null,
                                   IlgiliFaturaNo = fat != null ? fat.eNo : "",
                                   Tarih = car.cTarih,
                                   HareketTipi = carTipF.ctfAd,
                                   EvrakNo = car.cEvrakNo,
                                   VadeTarihi = car.cTarihVade,
                                   Borc = (car.cBA == 1 ? (decimal?) Math.Abs(car.cTutar) : null),
                                   Alacak = (car.cBA == 0 ? (decimal?) Math.Abs(car.cTutar) : null),
                               }
                ).ToList();
            

            // calculate activity balances
            var previousDebit = Db.car
                                    .Where(x => x.cKod == CurrentAccount.frm.frmID)
                                    .Where(x => x.cTarih < dateFilter.Begin)
                                    .Sum(x => ((decimal?) x.cTutar)) ?? 0;
                
            activities.Aggregate(previousDebit,
                                 (balance, activity) =>
                                     {
                                         if (activity.Alacak.HasValue)
                                         {
                                             balance += activity.Alacak.Value;
                                         }
                                         if (activity.Borc.HasValue)
                                         {
                                             balance -= activity.Borc.Value;
                                         }

                                         activity.Bakiye = balance;
                                         return balance;
                                     });


            return View(new AccountActivitiesModel
                            {
                                Activities = activities,
                                DateFilter = dateFilter,
                                MutabakatTarihi = CurrentCompany.frmCariTarih,
                                DevredenBakiye = previousDebit,
                            });
        }

        #endregion

        #region Faturalar

        [B2BAuthorize(AccountType.Supplier | AccountType.Customer, Privilege.FATURA)]
        public ActionResult Invoices(DocumentDateFilterModel dateFilter, int? productFilter, int? typeFilterId)
        {
            dateFilter.NotNullOrSetDefault(DateHelper.LastMonthBegin, DateHelper.LastMonthEnd);

            var invoices =
                (
                    from fat in Db.fat
                    join frm in Db.frm on fat.eFirma equals frm.frmID
                    join sifTip in Db.sifTip on fat.eTip equals sifTip.hrktTipID
                    join sipNeden in Db.sipNeden on fat.Neden equals sipNeden.nedenID
                    join posMagaza in Db.posMagaza on fat.eMekan equals posMagaza.mekanID
                    join firmaMekan in Db.frm on fat.eFirmaMkn equals firmaMekan.frmID
                    /* Only Invoices for siftip */
                    where sifTip.hrktSIF == 2
                    where typeFilterId == null || sifTip.hrktTipID == typeFilterId
                    where frm.frmID == CurrentAccount.frm.frmID
                    where fat.eTarih >= dateFilter.Begin && fat.eTarih <= dateFilter.End
                    where productFilter == null || fat.fatAyr.Select(x => x.ehStkID).Contains(productFilter.Value)
                    orderby fat.eTarih descending , fat.eID descending
                    select new InvoicesModel.Invoice
                               {
                                   Id = fat.eID,
                                   Tarih = fat.eTarih,
                                   EvrakNo = fat.eNo,
                                   HareketTipi = sifTip.hrktTipAd,
                                   Durum = (InvoicesModel.InvoiceStatus) fat.eDurum,
                                   VadeTarihi = fat.eTarihV,
                                   EvrakNot = fat.eNot,
                                   KDVHaricToplam = fat.fatAyr.Sum(x => x.ehTutar),
                                   ToplamIndirim = fat.fatAyr.Sum(x => x.ehIndirim),
                                   HareketGrubu = sipNeden.nedenAd,
                                   SevkAdresi = frm.frmTip == 1 ? (fat.eFirmaMkn == 0 ? "" : firmaMekan.frmAd) : posMagaza.mekanAd
                               }
                )
                    .ToList();

            return View(new InvoicesModel
                        {
                            Invoices = invoices,
                            DateFilter = dateFilter,
                            ProductFilterId = productFilter,
                            ProductFilterLabel = productFilter.HasValue ? Db.urn.Single(x => x.stkID == productFilter).stkAd : "",
                            TypeFilterId = typeFilterId,
                            AvailableTypes = Db.sifTip
                                .Where(x => x.hrktSIF == 2)
                                .Select(x => new DocumentType
                                             {
                                                 Id = x.hrktTipID,
                                                 Isim = x.hrktTipAd
                                             })
                                .ToList(),
                            GosterimSecenekleri =
                            new InvoicesModel.Settings
                            {
                                EvrakNotuGosterilsinMi = _settings.EvrakNotlariGosterilsin
                            }
                        });
        }

        [B2BAuthorize(AccountType.Supplier | AccountType.Customer, Privilege.FATURA)]
        public ActionResult InvoiceDetail(int? id)
        {
            Check.NotNullOr404(id);

            var fat = Db.fat.SingleOrDefault(x => x.eID == id);
            Check.NotNullOr404(fat);

            var frm = fat.frm;
            // Başka firmalara ait kayıt gösterilmemeli
            Check.CheckOr404(frm.frmID == CurrentAccount.frm.frmID);
            
            var sifTip = Db.sifTip.Where(x => x.hrktSIF == 2).Single(x => x.hrktTipID == fat.eTip);
            var sipNeden = Db.sipNeden.Single(x => x.nedenID == fat.Neden);

            var sevkAdresiAd = "";
            frm sevkAdresi;
            if (frm.frmTip == 1)
            {
                // sevk adresi ismini firma mekandan alıyoruz.
                var firmaMekan = fat.eFirmaMkn == 0 ? fat.frm : Db.frm.Single(x => x.frmID == fat.eFirmaMkn);
                sevkAdresiAd = firmaMekan.frmAd;
                sevkAdresi = firmaMekan;
            }
            else
            {
                // sevk adresi ismini posMagaza'dan alıyoruz    
                var posMagaza = Db.posMagaza.Single(x => x.mekanID == fat.eMekan);
                sevkAdresiAd = posMagaza.mekanAd;
                sevkAdresi = Db.frm.Single(x => x.frmID == fat.eMekan);
            }


            var model = new InvoiceDetailModel
                        {
                            Id = fat.eID,
                            EvrakNo = fat.eNo,
                            Tarih = fat.eTarih,
                            SevkTarihi = fat.eTarihS,
                            HareketTipi = sifTip.hrktTipAd,
                            EvrakNot = fat.eNot,
                            Evrak3SatirNot = fat.fatNot != null ? fat.fatNot.enNot : "",
                            VadeTarihi = fat.eTarihV,
                            HareketGrubu = sipNeden.nedenAd,
                            Durum = (InvoiceDetailModel.InvoiceStatus)fat.eDurum,

                            SevkAdresiAd = sevkAdresiAd,
                            SevkAdresi1 = sevkAdresi.adres1,
                            SevkAdresi2 = sevkAdresi.adres2,
                            SevkAdresiIlce = sevkAdresi.ilce,
                            SevkAdresiIl = Db.frmKent.Single(x => x.alanKodu == sevkAdresi.il).ilAd,
                            SevkAdresiPostaKodu = sevkAdresi.postaKod,
                            SevkAdresiTel = sevkAdresi.tel1,
                            SevkAdresiFaks = sevkAdresi.telFaks,

                            // Hareketler (Ayrıntılar)
                            EvrakHareketleri = from x in fat.fatAyr
                                               let barkod = Db.urnBarkod_vw.FirstOrDefault(y => y.urnBrkdStkID == x.ehStkID)
                                               select new InvoiceDetailModel.InvoiceDetail
                                                            {
                                                                Id = x.ehID,
                                                                Sira = x.ehSira,
                                                                UrunId = x.urn.stkID,
                                                                UrunIsmi = x.urn.stkAd,
                                                                UrunKod = x.urn.stkKod,
                                                                UrunBarkod = barkod != null ? barkod.barkod : "",
                                                                ToplamAdet = x.ehAdet,
                                                                KoliIciAdet = x.urn.urnKoli,
                                                                PaletIciAdet = x.urn.urnPalet,
                                                                UrunAgirligi = x.urn.paketOlcu,
                                                                AgirlikBirim = x.urn.paketBrm,
                                                                Indirim1 = x.ehi1,
                                                                Indirim2 = x.ehi2,
                                                                Indirim3 = x.ehi3,
                                                                Indirim4 = x.ehi4,
                                                                Indirim5 = x.ehi5,
                                                                IndirimTutari = x.ehIndirim,
                                                                KDVHaricToplam = x.ehTutar,
                                                                KDVYuzde = Db.urnKDV.Single(y => y.kdvID == x.ehKDV).kdvYuzde,
                                                                KDVTutar = x.ehTutarKDV,
                                                            },

                            // Gösterim Seçenekleri
                            GosterimSecenekleri =
                                new InvoiceDetailModel.Settings
                                {
                                    EvrakNotuGosterilsinMi = _settings.EvrakNotlariGosterilsin,
                                }
                        };

            model.IlgiliSiparisler = (from fatAyr in Db.fatAyr
                                      where fatAyr.ehID == id
                                      join sip in Db.sip on fatAyr.ehSipID equals sip.eID
                                      select new InvoiceDetailModel.RelatedOrder
                                          {
                                              Id = sip.eID,
                                              Tarih = sip.eTarih,
                                              EvrakNo = sip.eNo,
                                          })
                .Distinct()
                .ToList();
            
            

            model.IlgiliIrsaliyeler = (from fatAyr in Db.fatAyr
                                       where fatAyr.ehID == id
                                       join irs in Db.irs on fatAyr.ehIrsID equals irs.eID
                                       select new InvoiceDetailModel.RelatedDelivery
                                           {
                                               Id = irs.eID,
                                               Tarih = irs.eTarihS,
                                               EvrakNo = irs.eNo
                                           })
                .Distinct()
                .ToList();

            
            
                
            return View(model);
        }

        #endregion

    }
}