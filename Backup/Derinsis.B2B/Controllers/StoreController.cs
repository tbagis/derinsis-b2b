﻿using System.Collections.ObjectModel;
using System.Data;

namespace DerinSIS.B2B.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using Helpers;
    using Helpers.Html;
    using Helpers.Paging;
    using Infrastructure.Auth;
    using Models;
    using ViewModels;

    [B2BAuthorize(AccountType.Customer, Privilege.SIPARIS)]
    public class StoreController : BaseController
    {
        public StoreController(DbEntities db): base(db)
        {
        }
        
        #region Product List

        public ActionResult Index(string q, int? categoryId, int? brandId, int? producerId, int? page,string kmp,string price,string priceLabel)
        {

            var model = new ProductListModel();
            model.OzelFiyat = kmp == "on" ? true : false;

            var query = (
                            from urnSatis_vw in Db.urnSatis_vw
                            join urnBarkod_vw in Db.urnBarkod_vw on urnSatis_vw.stkID equals urnBarkod_vw.urnBrkdStkID
                            select new
                                {
                                    urnSatis_vw.stkID,
                                    urnSatis_vw.stkKod,
                                    urnSatis_vw.stkAd,
                                    urnSatis_vw.mrkAd,
                                    urnSatis_vw.grpAd,
                                    urnSatis_vw.urnKtgrID,
                                    urnSatis_vw.urnKtgrID1,
                                    urnSatis_vw.urnKtgrID2,
                                    urnSatis_vw.urnKtgrID3,
                                    urnSatis_vw.urtID,
                                    urnSatis_vw.urnMrkID,
                                    urnSatis_vw.urnGrpID,
                                    urnBarkod_vw.barkod,
                                    urnSatis_vw.rtb,
                                }
                        )
                .AsQueryable();

            if (!string.IsNullOrWhiteSpace(q))
            {

                model.Sorgu = q;
                if (isNumeric(q) && q.Length >= 8)
                {
                    var barkodlar = Db.urnBrkd.Where(x => x.urnBarkod.Contains(q)).Select(x=>x.urnBrkdStkID).ToList();
                    query = query.Where(x => barkodlar.Contains(x.stkID));
                }
                else
                {
                    var qstr = q.Split(' ').Select(x => x.ToLower()).ToArray();
                    query = query.Where(x =>
                                        qstr.All(y => x.stkAd.Contains(y)) ||
                                        x.stkKod.Contains(q) ||
                                        x.mrkAd.Contains(q) ||
                                        x.grpAd.Contains(q));
                }
            }

            /* Category Filter */
            if(categoryId != null)
            {
                var kategori = Db.urnKtgrAg_vw.Single(x => x.kID == categoryId);
                model.KategoriIdFiltresi = kategori.kID;
                model.KategoriFiltreEtiketi = kategori.kAd;

                query = query.Where(x => x.urnKtgrID == kategori.k0);

                if (kategori.k1 != 0)
                {
                    query = query.Where(x => x.urnKtgrID1 == kategori.k1);
                }

                if(kategori.k2 != 0)
                {
                    query = query.Where(x => x.urnKtgrID2 == kategori.k2);
                }
                if (kategori.k3 != 0)
                {
                    query = query.Where(x => x.urnKtgrID3 == kategori.k3);
                }
            }

            /* Producer Filter */
            if(producerId != null)
            {
                var uretici = Db.urnMrkUrt.Single(x => x.urtID == producerId);
                model.UreticiIdFiltresi = uretici.urtID;
                model.UreticiFiltreEtiketi = uretici.urtAd;
                
                query = query.Where(x => x.urtID == uretici.urtID);
            }

            /* Brand Filter */
            if(brandId != null)
            {
                var marka = Db.urnMrk.Single(x => x.mrkID == brandId);
                model.MarkaIdFiltresi = marka.mrkID;
                model.MarkaFiltreEtiketi = marka.mrkAd;

                query = query.Where(x => x.urnMrkID == marka.mrkID);
                

            }
            if (model.OzelFiyat)
            {
                var ekIndirim = Db.fytOzl
                    .Where(x => x.fTip == 4)
                    //.Where(x => x.fFrmID == 0)
                    .Where(x => x.fTarih <= DateTime.Today.Date && x.fTarihSon >= DateTime.Today.Date)
                    .Where(x => x.fTur == 0)
                    .Where(x => x.onay == 1);
                query = query.Where(x => ekIndirim.Any(y => y.fStkID == x.stkID));
            }
            if (price != null && price != "")
            {
                var esik = price.Split('-');
                int min = int.Parse(esik[0]);
                int max = esik[1] != "" ? int.Parse(esik[1]) : 99999999;
                model.FiyatFiltresi = price;
                model.FiyatFiltreEtiketi = priceLabel;

                query = query.
                    Where(x => 
                        Db.urn.
                       Join(Db.posOdeme,urun=>urun.fiyatSDvz,odeme=>odeme.odemeDvzID,(urun,odeme)=>new {Urun=urun,Odeme=odeme}).
                        Where(x1=>Db.urnSatis_vw.Any(x2=>x2.stkID==x1.Urun.stkID)).
                            Where(x3 => (x3.Urun.fiyatSDvz == 1 ? x3.Urun.fiyatS : x3.Urun.fiyatS * x3.Odeme.odemeKur) >= min && (x3.Urun.fiyatSDvz == 1 ? x3.Urun.fiyatS : x3.Urun.fiyatS * x3.Odeme.odemeKur) <= max).Any(x4 => x4.Urun.stkID == x.stkID));
            }
            
            // Arama Sonuç Listesi
            // Sıralı bir şekilde Model ve/veya Ürünler içerir.
            var searchResultList = query
                .GroupBy(x => new { Id = x.urnGrpID == 0 ? x.stkID : x.urnGrpID, Rutbe = x.rtb, Isim = x.urnGrpID == 0 ? x.stkAd : x.mrkAd, ModelMi = x.urnGrpID != 0 })
                .OrderBy(x=>x.Key.Rutbe).ThenBy(x=>x.Key.Isim)
                .ToPagedList(page ?? 1, 28);


            // Arama sonuçlarında bulunanan ürünlerin listesi
            var urunIdleri = searchResultList.Where(x => x.Key.ModelMi == false).Select(x => x.Key.Id);
            var urunler = (from urn in Db.urnSatis_vw
                           join mainUrn in Db.urn on urn.stkID equals mainUrn.stkID
                           join posOdeme in Db.posOdeme on mainUrn.fiyatSDvz equals posOdeme.odemeDvzID
                           where urunIdleri.Contains(urn.stkID)
                           let barkod = Db.urnBarkod_vw.FirstOrDefault(x => x.urnBrkdStkID == urn.stkID)
                           let mrkIndirim = Db.sakMrk_vw
                               .Where(x => x.skFrmID == CurrentCompany.frmID)
                               .Where(x => x.skTur == CurrentCompany.frmVadeTur)
                               .FirstOrDefault(x => x.skMrkID == urn.urnMrkID)
                           let ekIndirim = Db.fytOzl
                               .Where(x => x.fStkID == urn.stkID)
                               .Where(x => x.fTip == 4)
                               //.Where(x => x.fFrmID == 0)
                               .Where(x => x.fTarih <= DateTime.Today.Date && x.fTarihSon >= DateTime.Today.Date)
                               .Where(x => x.fTur == 0)
                               .Where(x => x.onay == 1)
                               .OrderByDescending(x => x.fTarih)
                               .ThenByDescending(x => x.fID)
                               .FirstOrDefault()
                           select new ProductListModel.ProductModel
                                      {
                                          Id = urn.stkID,
                                          Isim = urn.stkAd,
                                          StokKodu = urn.stkKod,
                                          Barkod = barkod != null ? barkod.barkod : "",
                                          KDVHaricFiyat = urn.fiyatS,
                                          Indirim1 = mrkIndirim != null ? mrkIndirim.skY1 : 0,
                                          Indirim2 = mrkIndirim != null ? mrkIndirim.skY2 : 0,
                                          Indirim3 = mrkIndirim != null ? mrkIndirim.skY3 : 0,
                                          Indirim4 = ekIndirim != null ? ekIndirim.fInd1 : 0,
                                          Birim = posOdeme.odemeKisaAd,
                                          Kur = posOdeme.odemeKur,
                                          Rutbe = urn.rtb,
                                      })
                .ToList()
                .Cast<ProductListModel.ProductListItemModel>();
                                 
                
            // Aramaa sonuçlarında bulunan modellerin listesi
            var modelIdleri = searchResultList.Where(x => x.Key.ModelMi).Select(x => x.Key.Id);
            var modeller = (from urnGrp in Db.urnGrp
                            where modelIdleri.Contains(urnGrp.grpID)
                            let indirim = Db.sakMrk_vw
                                .Where(x => x.skFrmID == CurrentCompany.frmID)
                                .Where(x => x.skTur == CurrentCompany.frmVadeTur)
                                .FirstOrDefault(x => x.skMrkID == urnGrp.grpMrkID)
                            select new ProductListModel.ProductGroupModel
                                   {
                                       Id = urnGrp.grpID,
                                       Isim = urnGrp.grpAd,
                                       Indirim1 = indirim != null ? indirim.skY1 : 0,
                                       Indirim2 = indirim != null ? indirim.skY2 : 0,
                                       Indirim3 = indirim != null ? indirim.skY3 : 0, 
                                       Urunler = (from urn in Db.urnSatis_vw
                                                  join mainUrn in Db.urn on urn.stkID equals mainUrn.stkID
                                                  join posOdeme in Db.posOdeme on mainUrn.fiyatSDvz equals posOdeme.odemeDvzID
                                                  where urn.urnGrpID == urnGrp.grpID
                                                  let barkod =
                                                      Db.urnBarkod_vw.FirstOrDefault(x => x.urnBrkdStkID == urn.stkID)
                                                  let mrkIndirim = Db.sakMrk_vw
                                                      .Where(x => x.skFrmID == CurrentCompany.frmID)
                                                      .Where(x => x.skTur == CurrentCompany.frmVadeTur)
                                                      .FirstOrDefault(x => x.skMrkID == urn.urnMrkID)
                                                  let ekIndirim = Db.fytOzl
                                                      .Where(x => x.fStkID == urn.stkID)
                                                      .Where(x => x.fTip == 4)
                                                      //.Where(x => x.fFrmID == 0)
                                                      .Where(x => x.fTarih <= DateTime.Today && x.fTarihSon >= DateTime.Today)
                                                      .Where(x => x.fTur == 0)
                                                      .Where(x => x.onay == 1)
                                                      .OrderByDescending(x => x.fTarih)
                                                      .ThenByDescending(x => x.fID)
                                                      .FirstOrDefault()
                                                  select new ProductListModel.ProductModel
                                                         {
                                                             Id = urn.stkID,
                                                             Isim = urn.stkAd,
                                                             StokKodu = urn.stkKod,
                                                             Barkod = barkod != null ? barkod.barkod : "",
                                                             KDVHaricFiyat = urn.fiyatS,
                                                             Indirim1 = mrkIndirim != null ? mrkIndirim.skY1 : 0,
                                                             Indirim2 = mrkIndirim != null ? mrkIndirim.skY2 : 0,
                                                             Indirim3 = mrkIndirim != null ? mrkIndirim.skY3 : 0,
                                                             Indirim4 = ekIndirim != null ? ekIndirim.fInd1 : 0,
                                                             BagliOlduguModelId = urnGrp.grpID,
                                                             Birim = posOdeme.odemeKisaAd,
                                                             Kur = posOdeme.odemeKur,
                                                             Rutbe = 9999999,
                                                         })
                                   }).ToList();

            // kampanyalı ürünleri getir
          
                //urunler = urunler.
                //    Where(x => Db.fytOzl.
                //                   Where(z => z.fInd1 != 0).
                //                   Where(z => z.fTip ==4 ).
                //                   Where(z => z.onay == 1).
                //                   Where(z => z.fTarih <= DateTime.Today && z.fTarihSon >= DateTime.Today).
                //                   Any(y => y.fStkID == x.Id));

            // ürünleri ve modelleri, harmanlıyıp tekrar sıralıyoruz
            var itemList = urunler.Union(modeller).OrderBy(x=>x.Rutbe).ThenBy(x => x.Isim);
          

            // Ürün görsellerini oluşturalım
            foreach(var item in itemList)
            {
                if(item.ModelMi)
                {
                    item.KucukResimUrl = Url.Action("ModelImage", "Thumbnail", new {id = item.Id, width = 55, height = 55});
                    item.BuyukResimUrl = Url.Action("ModelImage", "Thumbnail", new { id = item.Id, width = 185, height = 165 });

                    foreach(var modelItem in ((ProductListModel.ProductGroupModel)item).Urunler)
                    {
                        modelItem.KucukResimUrl = Url.Action("ProductImage", "Thumbnail", new { id = modelItem.Id, width = 55, height = 55 });
                        modelItem.BuyukResimUrl = Url.Action("ProductImage", "Thumbnail", new { id = modelItem.Id, width = 185, height = 165 });
                    }
                }
                else
                {
                    item.KucukResimUrl = Url.Action("ProductImage", "Thumbnail", new { id = item.Id, width = 55, height = 55 });
                    item.BuyukResimUrl = Url.Action("ProductImage", "Thumbnail", new { id = item.Id, width = 185, height = 165 });
                }
            }
            model.AramaSonuclari = new PagedList<ProductListModel.ProductListItemModel>(
                itemList,
                searchResultList.Page,
                searchResultList.ItemsPerPage,
                searchResultList.TotalItems);
            
            return View(model);
        }

        private bool isNumeric(string s)
        {
            try
            {
                decimal a = decimal.Parse(s);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        #endregion

        #region Category & Brand Trees & Prices

        [ChildActionOnly]
        public ActionResult CategoryTree()
        {
            // Level 1
            var level1Categories = Db.urnKtgrAg_vw
                .Where(x => x.k1 == 0)
                .OrderBy(x => x.kAd)
                .Select(x => new CategoryTreeModel.CategoryItem
                    {
                        Id = x.kID,
                        Isim = x.kAd,
                        Order = x.k0,
                        ParentId = null,
                    });

            // Level 2
            var level2Categories = Db.urnKtgrAg_vw
                .Where(x => x.k1 != 0 && x.k2 == 0)
                .OrderBy(x => x.kAd)
                .Select(x => new CategoryTreeModel.CategoryItem
                    {
                        Id = x.kID,
                        Isim = x.kAd,
                        Order = x.k1,
                        ParentId = Db.urnKtgrAg_vw.FirstOrDefault(y => y.k0 == x.k0 && y.k1 == 0).kID
                    });

            // Level 3
            var level3Categories = Db.urnKtgrAg_vw
                .Where(x => x.k2 != 0 &&  x.k3 == 0)
                .OrderBy(x => x.kAd)
                .Select(x => new CategoryTreeModel.CategoryItem
                    {
                        Id = x.kID,
                        Isim = x.kAd,
                        Order = x.k2,
                        ParentId = Db.urnKtgrAg_vw.FirstOrDefault(y => y.k0 == x.k0 && y.k1 == x.k1 && y.k2 == 0).kID
                    });
            var level4Categories = Db.urnKtgrAg_vw
             .Where(x => x.k3 != 0)
             .OrderBy(x => x.kAd)
             .Select(x => new CategoryTreeModel.CategoryItem
             {
                 Id = x.kID,
                 Isim = x.kAd,
                 Order = x.k3,
                 ParentId = Db.urnKtgrAg_vw.FirstOrDefault(y => y.k0 == x.k0 && y.k1 == x.k1 && y.k2 == x.k2 && y.k3 == 0).kID,
             });

            var allCategories = level1Categories
                .Union(level2Categories)
                .Union(level3Categories)
                .Union(level4Categories)
                .ToList();

            var model = new CategoryTreeModel { Items = allCategories };
            return PartialView(model);
        }
        [ChildActionOnly]
        public ActionResult Promotion()
        {
            var model = new PromoModel();
            model.Promolar = Db.b2bKampanyaResimleri
                .Select(x => new PromoModel.promo
                                 {
                                     sira = x.sira,
                                     baslik = x.baslik,
                                     resim = x.resim,
                                     etkiTip = x.etkiTip,
                                     etkiId = x.etkiID
                                 }).ToList();
            return PartialView(model);
        }
        [ChildActionOnly]
        public ActionResult BrandTree(string q, int? categoryId, string price)
        {
            var model=new BrandTreeModel();
            var urunListe =
                Db.urn.
                    Join(Db.posOdeme, urun => urun.fiyatSDvz, odeme => odeme.odemeDvzID,
                         (urun, odeme) => new {Urun = urun, Odeme = odeme}).
                    Join(Db.urnSatis_vw, urun => urun.Urun.stkID, satis => satis.stkID,
                         (urun, satis) => new {Urun = urun, Satis = satis,})
                    .Select(u => new
                                     {
                                         u.Urun.Odeme,
                                         u.Urun.Urun,
                                         u.Satis
                                     });

            if (categoryId != null)
            {
                var kategori = Db.urnKtgrAg_vw.Single(x => x.kID == categoryId);

                urunListe = urunListe.Where(x => x.Urun.urnKtgrID == kategori.k0);

                if (kategori.k1 != 0)
                {
                    urunListe = urunListe.Where(x => x.Urun.urnKtgrID1 == kategori.k1);
                }

                if (kategori.k2 != 0)
                {
                    urunListe = urunListe.Where(x => x.Urun.urnKtgrID2 == kategori.k2);
                }
                if (kategori.k3 != 0)
                {
                    urunListe = urunListe.Where(x => x.Urun.urnKtgrID3 == kategori.k3);
                }
            }

            if (!string.IsNullOrEmpty(price))
            {
                var esik = price.Split('-');
                int min = int.Parse(esik[0]);
                int max = esik[1] != "" ? int.Parse(esik[1]) : 99999999;
                urunListe = urunListe.
                    Where(
                        x3 =>
                        (x3.Urun.fiyatSDvz == 1 ? x3.Urun.fiyatS : x3.Urun.fiyatS*x3.Odeme.odemeKur) >= min &&
                        (x3.Urun.fiyatSDvz == 1 ? x3.Urun.fiyatS : x3.Urun.fiyatS*x3.Odeme.odemeKur) <= max);
            }

            if (!string.IsNullOrWhiteSpace(q) && q.Length >= 8)
            {
                var qstr = q.Split(' ').Select(x => x.ToLower()).ToArray();
                var producers = Db.urnMrkUrt
                    .Where(x => Db.urnSatis_vw.Where(
                        x2=>qstr.All(y => x2.stkAd.Contains(y)) ||
                                x2.stkKod.Contains(q) ||
                                x2.mrkAd.Contains(q) ||
                                x2.grpAd.Contains(q))
                        .Any(y => y.urtID == x.urtID))
                    .Select(producer => new BrandTreeModel.Producer
                                            {
                                                Id = producer.urtID,
                                                Name = producer.urtAd,
                                                Brands = producer.urnMrk
                                                    .Where(x => urunListe.Where(x2 => 
                                                        qstr.All(y => x2.Urun.stkAd.Contains(y)) ||
                                                                                x2.Urun.stkKod.Contains(q) ||
                                                                                x2.Satis.mrkAd.Contains(q) ||
                                                                                x2.Satis.grpAd.Contains(q)).
                                                     Any(y => y.Satis.urnMrkID == x.mrkID))
                                                    .OrderBy(x => x.mrkAd)
                                                    .Select(brand => new BrandTreeModel.Brand
                                                                         {
                                                                             Id = brand.mrkID,
                                                                             Name = brand.mrkAd,
                                                                             Count = urunListe.Where(x2 => x2.Urun.stkAd.Contains(q) ||
                                                                                                           x2.Urun.stkKod.Contains(q) ||
                                                                                                           x2.Satis.mrkAd.Contains(q) ||
                                                                                                           x2.Satis.grpAd.Contains(q)).Count(x => x.Satis.urnMrkID == brand.mrkID)
                                                                         })
                                            });
               
                model = new BrandTreeModel { Producers = producers };
            }
            else
            {
                var producers = Db.urnMrkUrt
                    .Where(x => Db.urnSatis_vw.Any(y => y.urtID == x.urtID))
                    .Select(producer => new BrandTreeModel.Producer
                                            {
                                                Id = producer.urtID,
                                                Name = producer.urtAd,
                                                Brands = producer.urnMrk
                                                    .Where(x => urunListe.Any(y => y.Satis.urnMrkID == x.mrkID))
                                                    .OrderBy(x => x.mrkAd)
                                                    .Select(brand => new BrandTreeModel.Brand
                                                                         {
                                                                             Id = brand.mrkID,
                                                                             Name = brand.mrkAd,
                                                                             Count = urunListe.Where(x => x.Satis.urnMrkID == brand.mrkID).Count()
                                                                         })
                                            });
                model = new BrandTreeModel { Producers = producers };
            }

          
            return PartialView(model);
        }

        [ChildActionOnly]
        public ActionResult Prices()
        {
       
            var prices = new Collection<PricesModel.Price>();
            var price = new PricesModel.Price();
            price = new PricesModel.Price {Max = 5, Min = 0, Name = "0-5"};
            prices.Add(price);
            price = new PricesModel.Price {Max = 10, Min = 5, Name = "5-10"};
            prices.Add(price);
            price = new PricesModel.Price {Max = 20, Min = 10, Name = "10-20"};
            prices.Add(price);
            price = new PricesModel.Price {Max = 50, Min = 20, Name = "20-50"};
            prices.Add(price);
            price = new PricesModel.Price {Max = 100, Min = 50, Name = "50-100"};
            prices.Add(price);
          price = new PricesModel.Price {Max = -1, Min = 500, Name = "500-"};
            prices.Add(price);
            var model = new PricesModel {Prices = prices};
            return PartialView(model);
        }

        #endregion

        #region Product Detail Pages

        public ActionResult ProductDetail(int? id, int? kmpid)
        {
            Check.NotNullOr404(id);
            var urn = Db.urn.SingleOrDefault(x => x.stkID == id);
            Check.NotNullOr404(urn);

            var urunKod1 = Db.urnKod1.SingleOrDefault(x => x.kod1ID == urn.kod1ID);
            var urunKod2 = Db.urnkod2.SingleOrDefault(x => x.kod2ID == urn.kod2ID);
            var urunKod3 = Db.urnkod3.SingleOrDefault(x => x.kod3ID == urn.kod3ID);
            var urunKod4 = Db.urnkod4.SingleOrDefault(x => x.kod4ID == urn.kod4ID);
            var urunKod5 = Db.urnkod5.SingleOrDefault(x => x.kod5ID == urn.kod5ID);

            var urunKod1Baslik = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 101);
            var urunKod2Baslik = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 102);
            var urunKod3Baslik = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 103);
            var urunKod4Baslik = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 104);
            var urunKod5Baslik = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 105);

            var urunKod1GosterilsinMi = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 751);
            var urunKod2GosterilsinMi = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 752);
            var urunKod3GosterilsinMi = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 753);
            var urunKod4GosterilsinMi = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 754);
            var urunKod5GosterilsinMi = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 755);

            var kdvHaricSatisFiyati = urn.fiyatS;
            
            var indirim1 = 0m;
            var indirim2 = 0m;
            var indirim3 = 0m;
            var indirim4 = 0m;
            if (CurrentAccount.frm.frmTip == 1)
            {
                var mrkIndirim = Db.sakMrk_vw
                    .Where(x => x.skFrmID == CurrentAccount.frm.frmID)
                    .Where(x => x.skTur == CurrentCompany.frmVadeTur)
                    .SingleOrDefault(x => x.skMrkID == urn.urnMrkID);

                if(mrkIndirim != null)
                {
                    indirim1 = mrkIndirim.skY1;
                    indirim2 = mrkIndirim.skY2;
                    indirim3 = mrkIndirim.skY3;    
                }

                if (kmpid == null)
                {
                    var ekIndirim = Db.fytOzl
                        .Where(x => x.fStkID == urn.stkID)
                        .Where(x => x.fTip == 4)
                        .Where(x => x.fTarih <= DateTime.Today && x.fTarihSon >= DateTime.Today)
                        .Where(x => x.fTur == 0)
                        .Where(x => x.onay == 1)
                        .OrderByDescending(x => x.fTarih)
                        .ThenByDescending(x => x.fID)
                        .FirstOrDefault();

                    if (ekIndirim != null)
                    {
                        indirim4 = ekIndirim.fInd1;
                    }
                }
                else
                {
                    var ekIndirim = Db.fytOzl
                        .Where(x => x.fhID == kmpid)
                        .Where(x => x.fStkID == urn.stkID)
                        .FirstOrDefault();

                    if (ekIndirim != null)
                    {
                        indirim4 = ekIndirim.fInd1;
                    }
                }
            }


            // Ürün kategorisini urnSatis_vw'den sorgulamalıyız
            var urnVw = Db.urnSatis_vw.Single(x => x.stkID == urn.stkID);
            var kategori1 = Db.urnKtgrAg_vw.SingleOrDefault(x => x.k0 == urnVw.urnKtgrID && x.k1 == 0 && x.k2 == 0);
            var kategori2 = urnVw.urnKtgrID2 != 0
                                ? Db.urnKtgrAg_vw.SingleOrDefault(x => x.k0 == urnVw.urnKtgrID && x.k1 == urnVw.urnKtgrID1 && x.k2 == 0)
                                : null;
            var kategori3 = urnVw.urnKtgrID3 != 0
                                ? Db.urnKtgrAg_vw.SingleOrDefault(x => x.k0 == urnVw.urnKtgrID && x.k1 == urnVw.urnKtgrID1 && x.k2 == urnVw.urnKtgrID2 && x.k3==0)
                                : null;

            var posOdeme = Db.posOdeme.SingleOrDefault(x => x.odemeDvzID == urn.fiyatSDvz);
            if (posOdeme == null)
            {
                posOdeme = new posOdeme
                    {
                        odemeDvzID = 1,
                        odemeKur = 1m,
                        odemeKisaAd = "TL",
                    };
            }

            var model = new ProductDetailModel
                            {
                                Id = urn.stkID,
                                Isim = urn.stkAd,
                                UrunKodu = urn.stkKod,
                                StokKisaAd = urn.stkAdKisa,

                                // Ürün Kodları
                                UrunKod1Baslik = urunKod1Baslik != null ? urunKod1Baslik.ayarDeger : "",
                                UrunKod2Baslik = urunKod2Baslik != null ? urunKod2Baslik.ayarDeger : "",
                                UrunKod3Baslik = urunKod3Baslik != null ? urunKod3Baslik.ayarDeger : "",
                                UrunKod4Baslik = urunKod4Baslik != null ? urunKod4Baslik.ayarDeger : "",
                                UrunKod5Baslik = urunKod5Baslik != null ? urunKod5Baslik.ayarDeger : "",

                                UrunKod1 = urunKod1 != null ? urunKod1.kod1Ad : "",
                                UrunKod2 = urunKod2 != null ? urunKod2.kod2Ad : "",
                                UrunKod3 = urunKod3 != null ? urunKod3.kod3Ad : "",
                                UrunKod4 = urunKod4 != null ? urunKod4.kod4Ad : "",
                                UrunKod5 = urunKod5 != null ? urunKod5.kod5Ad : "",

                                UrunKod1GosterilsinMi = urunKod1GosterilsinMi != null && urunKod1GosterilsinMi.ayarDeger != "0",
                                UrunKod2GosterilsinMi = urunKod2GosterilsinMi != null && urunKod2GosterilsinMi.ayarDeger != "0",
                                UrunKod3GosterilsinMi = urunKod3GosterilsinMi != null && urunKod3GosterilsinMi.ayarDeger != "0",
                                UrunKod4GosterilsinMi = urunKod4GosterilsinMi != null && urunKod4GosterilsinMi.ayarDeger != "0",
                                UrunKod5GosterilsinMi = urunKod5GosterilsinMi != null && urunKod5GosterilsinMi.ayarDeger != "0",


                                // Kategoriler
                                Kategori0Id = kategori1 != null ? kategori1.kID : 0,
                                Kategori1Id = kategori2 != null ? kategori2.kID : 0,
                                Kategori2Id = kategori3 != null ? kategori3.kID : 0,
                                Kategori0Ad = kategori1 != null ? kategori1.kAd : "",
                                Kategori1Ad = kategori2 != null ? kategori2.kAd : "",
                                Kategori2Ad = kategori3 != null ? kategori3.kAd : "",


                                // Üretici - Marka - Model
                                UreticiId = urnVw.urtID,
                                UreticiAd = urnVw.urtAd,
                                MarkaId = urnVw.urnMrkID,
                                MarkaAd = urnVw.mrkAd,


                                // Ürün Özellikleri
                                UrunBirimi = urn.urnBrm,
                                PaketOlcu = urn.paketOlcu,
                                PaketBirimi = urn.paketBrm,
                                KoliOlcu = urn.urnKoli,
                                KoliBirimi = urn.urnKoliBrm,
                                KutuOlcu = urn.urnKutu,
                                PaletOlcu = urn.urnPalet,


                                // Ürün Bilgileri
                                BilgiGruplari = Db.urnBilgiGrp
                                    .OrderBy(x => x.grupSira)
                                    .Select(x => new ProductDetailModel.BilgiGrubu
                                                     {
                                                         GrupAdi = x.grupAd,
                                                         Bilgiler = Db.urnBilgi
                                                             .Where(y => y.bVeriID == urn.stkID)
                                                             .Where(y => y.urnBilgiTnm.bilgiGrup == x.grupID)
                                                             .OrderBy(y => y.urnBilgiTnm.BilgiSira)
                                                             .Select(y => new ProductDetailModel.BilgiGrubu.Bilgi
                                                                              {
                                                                                  Ad = y.urnBilgiTnm.BilgiAd,
                                                                                  Deger = y.bDeger
                                                                              })
                                                     }),


                                // Barkod Bilgileri
                                BarkodBilgileri = urn.urnBrkd
                                    .OrderBy(x => x.urnBrkdOnce)
                                    .Select(x => new ProductDetailModel.BarkodBilgisi
                                                     {
                                                         Barkod = x.urnBarkod,
                                                         Adet = x.urnBrkdAdet,
                                                         AdetBirim = x.urnBrkdBrm,
                                                     }),


                                // Stok Bilgileri
                                StokBilgileri = Db.stokSon_vw
                                    .Where(x => x.ehstkID == urn.stkID)
                                    .Join(Db.posMagaza, x => x.ehMekan, y => y.mekanID, (stok, mekan) => new { stok, mekan })
                                    .Where(x => x.mekan.mekanMerkezDepo != 2)
                                    .OrderByDescending(x => x.mekan.mekanMerkezDepo)
                                    .Select(x => new ProductDetailModel.StokBilgisi
                                                     {
                                                         MekanAdi = x.mekan.mekanAd,
                                                         Stok = x.stok.stok.Value
                                                     }),


                                // Fiyat ve İndirim Bilgileri
                                KDVHaricSatisFiyati = kdvHaricSatisFiyati,
                                Birim = posOdeme.odemeKisaAd,
                                SatisKur = posOdeme.odemeKur,
                                Indirim1 = indirim1,
                                Indirim2 = indirim2,
                                Indirim3 = indirim3,
                                Indirim4 = indirim4,
                                KDVOrani = Db.urnKDV.Single(x => x.kdvID == urn.KDVs).kdvYuzde,


                                // Ürün Siparişleri
                                Siparisler = urn.sipAyr
                                    .Select(x => x.sip)
                                    .Distinct()
                                    .Where(x => x.eTarih >= DateHelper.LastMonthBegin)
                                    .Where(x => x.eTarih <= DateTime.Today)
                                    .Where(x => x.frm.frmID == CurrentAccount.frm.frmID)
                                    .OrderByDescending(x => x.eTarih)
                                    .Join(Db.sifTip.Where(y => y.hrktSIF == 0), sip => sip.eTip, tip => tip.hrktTipID, (sip, tip) => new { sip, tip })
                                    .Join(Db.sipNeden, x => x.sip.Neden, neden => neden.nedenID, (x, neden) => new { x.sip, x.tip, neden })
                                    .Select(x => new ProductDetailModel.UrunSiparis
                                                     {
                                                         Id = x.sip.eID,
                                                         EvrakNo = x.sip.eNo,
                                                         Tarih = x.sip.eTarih,
                                                         SevkTarihi = x.sip.eTarihS,
                                                         HareketTipi = x.tip.hrktTipAd,
                                                         HareketGrubu = x.neden.nedenAd,
                                                         UrunToplamAdet = x.sip.sipAyr.Where(y => y.ehStkID == urn.stkID).Sum(y => y.ehAdet),
                                                         UrunKoliIciAdet = urn.urnKoli,
                                                         UrunIndirimTutari = x.sip.sipAyr.Where(y => y.ehStkID == urn.stkID).Sum(y => y.ehIndirim),
                                                         UrunKDVHaricToplam = x.sip.sipAyr.Where(y => y.ehStkID == urn.stkID).Sum(y => y.ehTutar),
                                                         ToplamKDVHaricNet = x.sip.sipAyr.Sum(y => y.ehTutar) - x.sip.sipAyr.Sum(y => y.ehIndirim),
                                                     }),


                                // Ürün İrsaliyeler
                                Irsaliyeler = urn.irsAyr
                                    .Select(x => x.irs)
                                    .Distinct()
                                    .Where(x => x.eTarih >= DateHelper.LastMonthBegin)
                                    .Where(x => x.eTarih <= DateTime.Today)
                                    .Where(x => x.frm.frmID == CurrentAccount.frm.frmID)
                                    .OrderByDescending(x => x.eTarih)
                                    .Join(Db.sifTip.Where(y => y.hrktSIF == 1), irs => irs.eTip, tip => tip.hrktTipID, (irs, tip) => new { irs, tip })
                                    .Join(Db.sipNeden, x => x.irs.Neden, neden => neden.nedenID, (x, neden) => new { x.irs, x.tip, neden })
                                    .Select(x => new ProductDetailModel.UrunIrsaliye
                                    {
                                        Id = x.irs.eID,
                                        EvrakNo = x.irs.eNo,
                                        Tarih = x.irs.eTarih,
                                        SevkTarihi = x.irs.eTarihS,
                                        HareketTipi = x.tip.hrktTipAd,
                                        HareketGrubu = x.neden.nedenAd,
                                        UrunToplamAdet = x.irs.irsAyr.Where(y => y.ehStkID == urn.stkID).Sum(y => y.ehAdet),
                                        UrunKoliIciAdet = urn.urnKoli,
                                        UrunIndirimTutari = x.irs.irsAyr.Where(y => y.ehStkID == urn.stkID).Sum(y => y.ehIndirim),
                                        UrunKDVHaricToplam = x.irs.irsAyr.Where(y => y.ehStkID == urn.stkID).Sum(y => y.ehTutar),
                                        ToplamKDVHaricNet = x.irs.irsAyr.Sum(y => y.ehTutar) - x.irs.irsAyr.Sum(y => y.ehIndirim),
                                    }),



                                // Ürün Faturaları
                                Faturalar = urn.fatAyr
                                    .Select(x => x.fat)
                                    .Distinct()
                                    .Where(x => x.eTarih >= DateHelper.LastMonthBegin)
                                    .Where(x => x.eTarih <= DateTime.Today)
                                    .Where(x => x.frm.frmID == CurrentAccount.frm.frmID)
                                    .OrderByDescending(x => x.eTarih)
                                    .Join(Db.sifTip.Where(y => y.hrktSIF == 0), sip => sip.eTip, tip => tip.hrktTipID, (fat, tip) => new { fat, tip })
                                    .Join(Db.sipNeden, x => x.fat.Neden, neden => neden.nedenID, (x, neden) => new { x.fat, x.tip, neden })
                                    .Select(x => new ProductDetailModel.UrunFatura
                                    {
                                        Id = x.fat.eID,
                                        EvrakNo = x.fat.eNo,
                                        Tarih = x.fat.eTarih,
                                        VadeTarihi = x.fat.eTarihV,
                                        HareketTipi = x.tip.hrktTipAd,
                                        HareketGrubu = x.neden.nedenAd,
                                        UrunToplamAdet = x.fat.fatAyr.Where(y => y.ehStkID == urn.stkID).Sum(y => y.ehAdet),
                                        UrunKoliIciAdet = urn.urnKoli,
                                        UrunIndirimTutari = x.fat.fatAyr.Where(y => y.ehStkID == urn.stkID).Sum(y => y.ehIndirim),
                                        UrunKDVHaricToplam = x.fat.fatAyr.Where(y => y.ehStkID == urn.stkID).Sum(y => y.ehTutar),
                                        ToplamKDVHaricNet = x.fat.fatAyr.Sum(y => y.ehTutar) - x.fat.fatAyr.Sum(y => y.ehIndirim),
                                    }),

                            };

            return View(model);
        }

        public ActionResult ProductGroupDetail(int? id)
        {
            Check.NotNullOr404(id);

            var urunModeli = Db.urnGrp.SingleOrDefault(x => x.grpID == id);
            Check.NotNullOr404(urunModeli);

            var marka = Db.urnMrk.Single(x => x.mrkID == urunModeli.grpMrkID);
            var uretici = marka.urnMrkUrt;

            var kategori0 = Db.urnKtgrAg_vw.SingleOrDefault(x => x.kID == urunModeli.urnGrpKtgrID);
            var kategori1 = Db.urnKtgrAg_vw.SingleOrDefault(x => x.kID == urunModeli.urnGrpKtgrID1);
            var kategori2 = Db.urnKtgrAg_vw.SingleOrDefault(x => x.kID == urunModeli.urnGrpKtgrID2);

            var markaIndirimi = Db.sakMrk_vw
                    .Where(x => x.skFrmID == CurrentAccount.frm.frmID)
                    .Where(x => x.skTur == CurrentCompany.frmVadeTur)
                    .SingleOrDefault(x => x.skMrkID == marka.mrkID);
            
            var enUcuzModelUrunuVw = Db.urnSatis_vw
                .Where(x => x.urnGrpID == urunModeli.grpID)
                .OrderBy(x => x.fiyatS)
                .First();

            var enUcuzModelUrunu = Db.urn.Single(x => x.stkID == enUcuzModelUrunuVw.stkID);

            var ekIndirim = Db.fytOzl
                    .Where(x => x.fStkID == enUcuzModelUrunu.stkID)
                    .Where(x => x.fTip == 4)
                    //.Where(x => x.fFrmID == 0)
                    .Where(x => x.fTarih <= DateTime.Today && x.fTarihSon >= DateTime.Today)
                    .Where(x => x.fTur == 0)
                    .Where(x => x.onay == 1)
                    .OrderByDescending(x => x.fTarih)
                    .ThenByDescending(x => x.fID)
                    .FirstOrDefault();

            var model = new ProductGroupDetailModel
                        {
                            Id = urunModeli.grpID,
                            ModelAdi = urunModeli.grpAd,

                            MarkaId = marka.mrkID,
                            MarkaAdi = marka.mrkAd,
                            UreticiId = uretici.urtID,
                            UreticiAdi = uretici.urtAd,

                            Kategori0Id = kategori0 != null ? kategori0.kID : 0,
                            Kategori0Ad = kategori0 != null ? kategori0.kAd : "",
                            Kategori1Id = kategori1 != null ? kategori1.kID : 0,
                            Kategori1Ad = kategori1 != null ? kategori1.kAd : "",
                            Kategori2Id = kategori2 != null ? kategori2.kID : 0,
                            Kategori2Ad = kategori2 != null ? kategori2.kAd : "",


                            KDVHaricSatisFiyati = enUcuzModelUrunu.fiyatS,
                            Indirim1 = markaIndirimi != null ? markaIndirimi.skY1 : 0,
                            Indirim2 = markaIndirimi != null ? markaIndirimi.skY2 : 0,
                            Indirim3 = markaIndirimi != null ? markaIndirimi.skY3 : 0,
                            Indirim4 = ekIndirim != null ? ekIndirim.fInd1 : 0,
                            KDVOrani = Db.urnKDV.Single(x => x.kdvID == enUcuzModelUrunu.KDVs).kdvYuzde,

                            Products = (from urn in Db.urnSatis_vw
                                        where urn.urnGrpID == id
                                        let urunIndirimi = Db.sakMrk_vw
                                        .Where(x => x.skTur == CurrentCompany.frmVadeTur)
                                        .FirstOrDefault(x => x.skMrkID == marka.mrkID && x.skFrmID == CurrentCompany.frmID)
                                        let ekUrunIndirimi = Db.fytOzl
                                            .Where(x => x.fStkID == urn.stkID)
                                            .Where(x => x.fTip == 4)
                                            //.Where(x => x.fFrmID == 0)
                                            .Where(x => x.fTarih <= DateTime.Today && x.fTarihSon >= DateTime.Today)
                                            .Where(x => x.fTur == 0)
                                            .Where(x => x.onay == 1)
                                            .OrderByDescending(x => x.fTarih)
                                            .ThenByDescending(x => x.fID)
                                            .FirstOrDefault()
                                        let kdv = Db.urnKDV.FirstOrDefault(x => x.kdvID == urn.KDVs)
                                        let barkod = Db.urnBarkod_vw.FirstOrDefault(x => x.urnBrkdStkID == urn.stkID)
                                        select new ProductGroupDetailModel.Product
                                               {
                                                   Id = urn.stkID,
                                                   Isim = urn.stkAd,
                                                   StokKodu = urn.stkKod,
                                                   Barkod = barkod != null ? barkod.barkod : "",
                                                   KDVHaricSatisFiyati = urn.fiyatS,
                                                   Indirim1 = urunIndirimi != null ? urunIndirimi.skY1 : 0,
                                                   Indirim2 = urunIndirimi != null ? urunIndirimi.skY2 : 0,
                                                   Indirim3 = urunIndirimi != null ? urunIndirimi.skY3 : 0,
                                                   Indirim4 = ekUrunIndirimi != null ? ekUrunIndirimi.fInd1 : 0,
                                                   KDVOrani = kdv.kdvYuzde
                                               })
                                .ToList(),
                        };

            return View(model);
        }

        public ActionResult ProductPromoDetail(int? id,string resim)
        {
            Check.NotNullOr404(id);

            var fiyatbaslik = Db.fytB.SingleOrDefault(x => x.feID == id);
            Check.NotNullOr404(fiyatbaslik);

      
            var model = new ProductPromoDetailModel()
            {
                Id = fiyatbaslik.feID,
                Ad = fiyatbaslik.feNot,
                resim = resim,

                Products = (from fytOzl in Db.fytOzl
                            join urn in Db.urnSatis_vw on fytOzl.fStkID equals urn.stkID
                            join mainUrn in Db.urn on urn.stkID equals mainUrn.stkID
                            join posOdeme in Db.posOdeme on mainUrn.fiyatSDvz equals posOdeme.odemeDvzID
                            where fytOzl.fhID == id
                            let markaIndirimi = Db.sakMrk_vw
                              .Where(x => x.skFrmID == CurrentCompany.frmID)
                               .Where(x => x.skTur == CurrentCompany.frmVadeTur)
                               .FirstOrDefault(x => x.skMrkID == urn.urnMrkID)
                            let kdv = Db.urnKDV.FirstOrDefault(x => x.kdvID == urn.KDVs)
                            let barkod = Db.urnBarkod_vw.FirstOrDefault(x => x.urnBrkdStkID == fytOzl.fStkID)
                            select new ProductPromoDetailModel.Product
                            {
                                Id = urn.stkID,
                                Isim = urn.stkAd,
                                StokKodu = urn.stkKod,
                                Barkod = barkod != null ? barkod.barkod : "",
                                KDVHaricSatisFiyati = urn.fiyatS,
                                Indirim1 = markaIndirimi != null ? markaIndirimi.skY1 : 0,
                                Indirim2 = markaIndirimi != null ? markaIndirimi.skY2 : 0,
                                Indirim3 = markaIndirimi != null ? markaIndirimi.skY3 : 0,
                                Indirim4 =fytOzl.fInd1,
                                KDVOrani = kdv.kdvYuzde,
                                Birim = posOdeme.odemeKisaAd
                            })
                    .ToList(),
            };

            return View(model);
        }
        
        #endregion

        #region My Carts

        public ActionResult MyCarts()
        {
            var myCarts = Db.b2bSepet
                .Where(x => x.frmYetkili.ytkID == CurrentAccount.ytkID)
                .Select(x => new MyCartModel
                                 {
                                     Id = x.Id,
                                     Isim = x.Ad,
                                     Aciklamalar = x.Aciklama,
                                     AktifMi = x.Aktif,
                                     Adet = x.b2bSepetAyr.Count()
                                 })
                .ToList();

            return View(myCarts);
        }

        [HttpPost]
        public ActionResult MakeCartActive(int id)
        {
            // Make every cart is disactive
            SetAllUserCartsAsDisactive();

            var newActiveCart = Db.b2bSepet
                .Where(x => x.frmYetkili.ytkID == CurrentAccount.ytkID)
                .Single(x => x.Id == id);
            newActiveCart.Aktif = true;

            Db.SaveChanges();
            return RedirectToAction("MyCarts");
        }

        [HttpPost]
        public ActionResult ChangeActiveCartAsync(int cartId)
        {
            // Make every cart is disactive
            SetAllUserCartsAsDisactive();

            var newActiveCart = Db.b2bSepet
                .Where(x => x.frmYetkili.ytkID == CurrentAccount.ytkID)
                .Single(x => x.Id == cartId);
            newActiveCart.Aktif = true;

            Db.SaveChanges();
            return Json(new
                            {
                                Result = true,
                                CartName = newActiveCart.Ad,
                                CartDistinctProductCount = newActiveCart.b2bSepetAyr.Count()
                            });
        }

        public ActionResult CreateNewCart()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateNewCart(CreateNewCartModel model)
        {
            if(!ModelState.IsValid)
            {
                ShowErrorMessage();
                return CreateNewCart();
            }

            // Aynı isimle iki sepet olamaz
            var cartWithSameName = Db.b2bSepet
                .Where(x => x.frmYetkili.ytkID == CurrentAccount.ytkID)
                .SingleOrDefault(x => x.Ad == model.Isim);
            if (cartWithSameName != null)
            {
                ModelState.AddModelError("Isim", "Aynı isimli bir sepetiniz zaten var.");
                ShowErrorMessage();
                return CreateNewCart();
            }

            var newCart = new b2bSepet
            {
                Aktif = false,
                frmYetkili = CurrentAccount,
                Ad = model.Isim,
                Aciklama = model.Aciklamalar ?? ""
            };
            Db.b2bSepet.Add(newCart);
            Db.SaveChanges();

            ShowSuccessMessage("Sepetiniz başarıyla oluşturulmuştur.");
            return RedirectToAction("MyCarts");
        }

        public ActionResult EditCart(int? id)
        {
            Check.NotNullOr404(id);
            
            var sepet = Db.b2bSepet
                .Where(x => x.frmYetkili.ytkID == CurrentAccount.ytkID)
                .SingleOrDefault(x => x.Id == id);
            Check.NotNullOr404(sepet);

            var model = new EditCartModel
                            {
                                Id = sepet.Id,
                                Isim = sepet.Ad,
                                Aciklamalar = sepet.Aciklama ?? ""
                            };
            return View(model);
        }

        [HttpPost]
        public ActionResult EditCart(EditCartModel model)
        {
            if (!ModelState.IsValid)
            {
                ShowErrorMessage();
                return EditCart(model.Id);
            }

            var cart = Db.b2bSepet
                .Where(x => x.frmYetkili.ytkID == CurrentAccount.ytkID)
                .Single(x => x.Id == model.Id);

            // Aynı isimle iki sepet olamaz
            var cartWithSameName = Db.b2bSepet
                .Where(x => x.frmYetkili.ytkID == CurrentAccount.ytkID)
                .Where(x => x.Id != cart.Id)
                .SingleOrDefault(x => x.Ad == model.Isim);
            if (cartWithSameName != null)
            {
                ModelState.AddModelError("Isim", "Aynı isimli bir sepetiniz zaten var.");
                ShowErrorMessage();
                return CreateNewCart();
            }

            cart.Ad = model.Isim;
            cart.Aciklama = model.Aciklamalar ?? "";
            Db.SaveChanges();

            ShowSuccessMessage("Sepetiniz başarıyla güncellenmiştir.");
            return RedirectToAction("MyCarts");
        }

        [HttpPost]
        public ActionResult DeleteCart(int id)
        {
            var myCartCount = Db.b2bSepet.Count(x => x.frmYetkili.ytkID == CurrentAccount.ytkID);
            if (myCartCount == 1)
            {
                ShowErrorMessage("Son sepeti silemezseniz.");
                return RedirectToAction("MyCarts");
            }

            var deletingCart = Db.b2bSepet
                .Where(x => x.frmYetkili.ytkID == CurrentAccount.ytkID)
                .Single(x => x.Id == id);

            foreach (var item in deletingCart.b2bSepetAyr.ToList())
            {
                Db.b2bSepetAyr.Remove(item);
            }
            Db.b2bSepet.Remove(deletingCart);

            // Eğer aktif olan sepeti sildiysek, başka bir sepeti aktif yapmalıyız.
            var activeCart = Db.b2bSepet
                .Where(x => x.frmYetkili.ytkID == CurrentAccount.ytkID)
                .Where(x => x.Id != deletingCart.Id)
                .SingleOrDefault(x => x.Aktif);
            if(activeCart == null)
            {
                var newActiveCart = Db.b2bSepet
                    .Where(x => x.Id != deletingCart.Id)
                    .First(x => x.frmYetkili.ytkID == CurrentAccount.ytkID);
                newActiveCart.Aktif = true;
            }

            Db.SaveChanges();
            ShowSuccessMessage("Sepet başarıyla silinmiştir.");
            return RedirectToAction("MyCarts");
        }

        private void SetAllUserCartsAsDisactive()
        {
            var myCarts = Db.b2bSepet
                .Where(x => x.frmYetkili.ytkID == CurrentAccount.ytkID);
            foreach (var myCart in myCarts)
            {
                myCart.Aktif = false;
            }
        }

        #endregion

        #region Cart Actions

        [ChildActionOnly]
        public ActionResult CartMenuElement()
        {
            var aktifSepet = Db.b2bSepet
                .Where(x => x.KullaniciId == CurrentAccount.ytkID)
                .Single(x => x.Aktif);

            var sepetOzeti = from item in aktifSepet.b2bSepetAyr
                             let mrkIndirim = Db.sakMrk_vw
                             .Where(x => x.skTur == CurrentCompany.frmVadeTur)
                                 .Where(x => x.skFrmID == CurrentCompany.frmID)
                                 .FirstOrDefault(x => x.skMrkID == item.urn.urnMrkID)
                             let ekIndirim = Db.fytOzl
                                 .Where(x => x.fStkID == item.urn.stkID)
                                 .Where(x => x.fTip == 4)
                                 //.Where(x => x.fFrmID == 0)
                                 .Where(x => x.fTarih <= DateTime.Today && x.fTarihSon >= DateTime.Today)
                                 .Where(x => x.fTur == 0)
                                 .Where(x => x.onay == 1)
                                 .OrderByDescending(x => x.fTarih)
                                 .ThenByDescending(x => x.fID)
                                 .FirstOrDefault()
                             let kdv = Db.urnKDV.FirstOrDefault(x => x.kdvID == item.urn.KDVs)
                             select new
                                    {
                                        KDVHaricSatisFiyati = item.urn.fiyatS,
                                        Indirim1 = mrkIndirim != null ? mrkIndirim.skY1 : 0,
                                        Indirim2 = mrkIndirim != null ? mrkIndirim.skY2 : 0,
                                        Indirim3 = mrkIndirim != null ? mrkIndirim.skY3 : 0,
                                        Indirim4 = ekIndirim != null ? ekIndirim.fInd1 : 0,
                                        KDVOrani = kdv.kdvYuzde,
                                        Adet = item.Adet,
                                    };

            var model = new CartMenuElementModel
                        {
                            AktifSepetIsmi = aktifSepet.Ad,
                            ToplamUrunCesidi = aktifSepet.b2bSepetAyr.Count(),
                            KDVDahilTutar = sepetOzeti.ToList()
                                .Sum(x =>
                                         {
                                             var indirimler = new[] {x.Indirim1, x.Indirim2, x.Indirim3, x.Indirim4};
                                             var kdvHaricNet = indirimler.Aggregate(x.KDVHaricSatisFiyati,
                                                                                    (fiyat, indirim) =>
                                                                                    fiyat - (fiyat*indirim/100.0m));

                                             var kdv = kdvHaricNet * x.KDVOrani/100.0m;
                                             var kdvDahilToplam = kdvHaricNet + kdv;
                                             return kdvDahilToplam*x.Adet;
                                         }),
                            SonEklenen2Urun = aktifSepet.b2bSepetAyr
                                .OrderByDescending(x => x.Id)
                                .Take(2)
                                .Select(x => new CartMenuElementModel.Urun
                                             {
                                                 UrunIsmi = x.urn.stkAd,
                                             })
                                .ToList()
                        };
             
                
            return PartialView(model);
        }

        [ChildActionOnly]
        public ActionResult CartSidebarElement()
        {
            var aktifSepet = Db.b2bSepet
                .Where(x => x.KullaniciId == CurrentAccount.ytkID)
                .Single(x => x.Aktif);

            var sepetOzeti = from item in aktifSepet.b2bSepetAyr
                             let mrkIndirim = Db.sakMrk_vw
                                 .Where(x => x.skFrmID == CurrentCompany.frmID)
                                 .Where(x => x.skTur == CurrentCompany.frmVadeTur)
                                 .FirstOrDefault(x => x.skMrkID == item.urn.urnMrkID)
                             let ekIndirim = Db.fytOzl
                                 .Where(x => x.fStkID == item.urn.stkID)
                                 .Where(x => x.fTip == 4)
                                 //.Where(x => x.fFrmID == 0)
                                 .Where(x => x.fTarih <= DateTime.Today && x.fTarihSon >= DateTime.Today)
                                 .Where(x => x.fTur == 0)
                                 .Where(x => x.onay == 1)
                                 .OrderByDescending(x => x.fTarih)
                                 .ThenByDescending(x => x.fID)
                                 .FirstOrDefault()
                             let kdv = Db.urnKDV.FirstOrDefault(x => x.kdvID == item.urn.KDVs)
                             select new
                             {
                                 KDVHaricSatisFiyati = item.urn.fiyatS,
                                 Indirim1 = mrkIndirim != null ? mrkIndirim.skY1 : 0,
                                 Indirim2 = mrkIndirim != null ? mrkIndirim.skY2 : 0,
                                 Indirim3 = mrkIndirim != null ? mrkIndirim.skY3 : 0,
                                 Indirim4 = ekIndirim != null ? ekIndirim.fInd1 : 0,
                                 KDVOrani = kdv.kdvYuzde,
                                 Adet = item.Adet,
                             };

            var model = new CartSidebarElementModel
                        {
                            AktifSepetIsmi = aktifSepet.Ad,
                            ToplamUrunCesidi = aktifSepet.b2bSepetAyr.Count(),
                            KDVDahilTutar = sepetOzeti.ToList()
                                .Sum(x =>
                                         {
                                             var indirimler = new[] {x.Indirim1, x.Indirim2, x.Indirim3, x.Indirim4};
                                             var kdvHaricNet = indirimler.Aggregate(x.KDVHaricSatisFiyati,
                                                                                    (fiyat, indirim) =>
                                                                                    fiyat - (fiyat*indirim/100.0m));

                                             var kdv = kdvHaricNet*x.KDVOrani/100.0m;
                                             var kdvDahilToplam = kdvHaricNet + kdv;
                                             return kdvDahilToplam*x.Adet;
                                         }),
                        };


            return PartialView(model);
        }
        
        public ActionResult Cart()
        {
            var activeCart = Db.b2bSepet
                .Where(x => x.KullaniciId == CurrentAccount.ytkID)
                .Single(x => x.Aktif);

            var model = new CartModel
                        {
                            AktifSepetId = activeCart.Id,

                            Sepetlerim = (from cart in Db.b2bSepet
                                          where cart.KullaniciId == CurrentAccount.ytkID
                                          select new CartModel.Sepet
                                                 {
                                                     Id = cart.Id,
                                                     Isim = cart.Ad,
                                                     Aciklama = cart.Aciklama,
                                                 }).ToList(),

                            AktifSepetUrunleri = (from cartItem in Db.b2bSepetAyr
                                                  where cartItem.b2bSepet.KullaniciId == CurrentAccount.ytkID
                                                  where cartItem.b2bSepet.Aktif
                                                  let barkod = Db.urnBarkod_vw.FirstOrDefault(x => x.urnBrkdStkID == cartItem.urn.stkID)
                                                  let mrkIndirim = Db.sakMrk_vw
                                                      .Where(x => x.skFrmID == CurrentCompany.frmID)
                                                      .Where(x => x.skTur == CurrentCompany.frmVadeTur)
                                                      .FirstOrDefault(x => x.skMrkID == cartItem.urn.urnMrkID)
                                                  let ekIndirim = Db.fytOzl
                                                      .Where(x => x.fStkID == cartItem.urn.stkID)
                                                      .Where(x => x.fTip == 4)
                                                      //.Where(x => x.fFrmID == 0)
                                                      .Where(x => x.fTarih <= DateTime.Today && x.fTarihSon >= DateTime.Today)
                                                      .Where(x => x.fTur == 0)
                                                      .Where(x => x.onay == 1)
                                                      .OrderByDescending(x => x.fTarih)
                                                      .ThenByDescending(x => x.fID)
                                                      .FirstOrDefault()
                                                  let kdv = Db.urnKDV.FirstOrDefault(x => x.kdvID == cartItem.urn.KDVs)
                                                  select new CartModel.Urun
                                                         {
                                                             UrunId = cartItem.urn.stkID,
                                                             UrunAdi = cartItem.urn.stkAd,
                                                             Barkod = barkod != null ? barkod.barkod : "",
                                                             StokKodu = cartItem.urn.stkKod,
                                                             KDVHaricSatisFiyati = cartItem.urn.fiyatS,
                                                             Indirim1 = mrkIndirim != null ? mrkIndirim.skY1 : 0,
                                                             Indirim2 = mrkIndirim != null ? mrkIndirim.skY2 : 0,
                                                             Indirim3 = mrkIndirim != null ? mrkIndirim.skY3 : 0,
                                                             Indirim4 = ekIndirim != null ? ekIndirim.fInd1 : 0,
                                                             KDVOrani = kdv.kdvYuzde,
                                                             Adet = cartItem.Adet,
                                                         }).ToList()
                        };

            return View(model);
        }

        [HttpPost]
        public ActionResult ChangeActiveCart(int id)
        {
            // Make every cart is disactive
            SetAllUserCartsAsDisactive();

            var newActiveCart = Db.b2bSepet
                .Where(x => x.frmYetkili.ytkID == CurrentAccount.ytkID)
                .Single(x => x.Id == id);
            newActiveCart.Aktif = true;

            Db.SaveChanges();
            return RedirectToAction("Cart");
        }

        [HttpPost]
        public ActionResult AddToActiveCartAsync(AddToActiveCartModel model)
        {
            var activeCart = Db.b2bSepet
                .Where(x => x.frmYetkili.ytkID == CurrentAccount.ytkID)
                .Single(x => x.Aktif);


            foreach (var cartItem in model.Items)
            {
                var item = activeCart.b2bSepetAyr.SingleOrDefault(x => x.UrunId == cartItem.ProductId);
                if (item == null)
                {
                    activeCart.b2bSepetAyr
                        .Add(new b2bSepetAyr
                                 {
                                     b2bSepet = activeCart,
                                     urn = Db.urn.Single(x => x.stkID == cartItem.ProductId),
                                     Adet = cartItem.Quantity
                                 });
                }
                else
                {
                    item.Adet += cartItem.Quantity;
                }
            }
            
            Db.SaveChanges();
                
            var cartItemPriceList = from cartItem in Db.b2bSepetAyr
                                    where cartItem.b2bSepet.KullaniciId == CurrentAccount.ytkID
                                    where cartItem.b2bSepet.Aktif
                                    let mrkIndirim = Db.sakMrk_vw
                                        .Where(x => x.skFrmID == CurrentCompany.frmID)
                                        .Where(x => x.skTur == CurrentCompany.frmVadeTur)
                                        .FirstOrDefault(x => x.skMrkID == cartItem.urn.urnMrkID)
                                    let ekIndirim = Db.fytOzl
                                        .Where(x => x.fStkID == cartItem.urn.stkID)
                                        .Where(x => x.fTip == 4)
                                        //.Where(x => x.fFrmID == 0)
                                        .Where(x => x.fTarih <= DateTime.Today && x.fTarihSon >= DateTime.Today)
                                        .Where(x => x.fTur == 0)
                                        .Where(x => x.onay == 1)
                                        .OrderByDescending(x => x.fTarih)
                                        .ThenByDescending(x => x.fID)
                                        .FirstOrDefault()
                                    let kdv = Db.urnKDV.FirstOrDefault(x => x.kdvID == cartItem.urn.KDVs)
                                    select new
                                           {
                                               KDVHaricSatisFiyati = cartItem.urn.fiyatS,
                                               Indirim1 = mrkIndirim != null ? mrkIndirim.skY1 : 0,
                                               Indirim2 = mrkIndirim != null ? mrkIndirim.skY2 : 0,
                                               Indirim3 = mrkIndirim != null ? mrkIndirim.skY3 : 0,
                                               Indirim4 = ekIndirim != null ? ekIndirim.fInd1 : 0,
                                               KDVOrani = kdv.kdvYuzde, 
                                               cartItem.Adet,
                                           };

            var toplamSepetTutari = 0.0m;
            foreach(var item in cartItemPriceList)
            {
                var indirimler = new[] { item.Indirim1, item.Indirim2, item.Indirim3, item.Indirim4 };
                var indirimliFiyat = indirimler.Aggregate(item.KDVHaricSatisFiyati, (fiyat, indirim) => fiyat - (fiyat * indirim / 100.0m));
                var kdv = indirimliFiyat * (decimal) item.KDVOrani / 100.0m;
                var tutar = indirimliFiyat + kdv;

                toplamSepetTutari += tutar* item.Adet;
            }
            
            return Json(new
                            {
                                UrunCesidi = activeCart.b2bSepetAyr.Count(),
                                ToplamFiyat = toplamSepetTutari.ToMoneyFormat(),
                                SonIkiUrun = activeCart.b2bSepetAyr
                                    .OrderByDescending(x => x.Id)
                                    .Take(2)
                                    .Select(x => x.urn.stkAd)
                                    .ToList()
                            });
        }

        [HttpPost]
        public ActionResult RemoveFromActiveCart(ICollection<int> ids)
        {
            var activeCart = Db.b2bSepet
                .Where(x => x.frmYetkili.ytkID == CurrentAccount.ytkID)
                .Single(x => x.Aktif);

            var items = activeCart.b2bSepetAyr.Where(x => ids.Contains(x.UrunId)).ToList();
            foreach (var item in items)
            {
                Db.b2bSepetAyr.Remove(item);
            }
            Db.SaveChanges();

            return RedirectToAction("Cart");
        }

        [HttpPost]
        public ActionResult MoveToAnotherCart(int targetCartId, ICollection<int> itemIds)
        {
            var activeCart = Db.b2bSepet
                .Where(x => x.KullaniciId == CurrentAccount.ytkID)
                .Single(x => x.Aktif);

            var targetCart = Db.b2bSepet
                .Where(x => x.KullaniciId == CurrentAccount.ytkID)
                .Single(x => x.Id == targetCartId);

            var movingItems = activeCart.b2bSepetAyr.Where(x => itemIds.Contains(x.UrunId)).ToList();
            foreach (var movingItem in movingItems)
            {
                movingItem.b2bSepet = targetCart;
            }

            Db.SaveChanges();

            ShowSuccessMessage(string.Format("Seçtiğiniz ürünler taşınmıştır", targetCart.Ad));
            return RedirectToAction("Cart");
        }

        [HttpPost]
        public ActionResult UpdateActiveCartItem(UpdateActiveCartItemModel model)
        {
            var activeCart = Db.b2bSepet
                .Where(x => x.frmYetkili.ytkID == CurrentAccount.ytkID)
                .Single(x => x.Aktif);

            var item = activeCart.b2bSepetAyr.Single(x => x.UrunId == model.ProductId);
            item.Adet = model.NewQuantity;
            Db.SaveChanges();

            return RedirectToAction("Cart");
        }

        private decimal DiscountedPriceForProduct(urn urn)
        {
            var price = urn.fiyatS;

            var discount = Db.sakMrk_vw
                .Where(x => x.skFrmID == CurrentAccount.frm.frmID)
                .Where(x => x.skTur == CurrentCompany.frmVadeTur)
                .FirstOrDefault(x => x.skMrkID == urn.urnMrkID);

            if (discount != null)
            {
                price -= discount.skY1 * price / 100.0m;

                if(discount.skY2 != 0.0m)
                {
                    price -= discount.skY2*price/100.0m;
                }

                if(discount.skY3 != 0.0m)
                {
                    price -= discount.skY3*price/100.0m;
                }
            }

            return price;

        }
        
        #endregion

        #region Make Order

        public ActionResult SelectOrderDeliveryAddress()
        {
            PopulateSevkAdresleri();
            return View();
        }

        [HttpPost]
        public ActionResult SelectOrderDeliveryAddress(CreateDeliveryAddressModel newAddress)
        {
            if (!ModelState.IsValid)
            {
                ShowErrorMessage();
                PopulateSevkAdresleri();
                return View(newAddress);
            }

            // Save this address as delivery address
            var frm = new frm
                      {
                          frmTip = 9,
                          frmAd = newAddress.AdresIsmi,
                          frmAd2 = newAddress.AdresIsmi,
                          frmBagID = CurrentAccount.frm.frmID,
                          adres1 = newAddress.Adres1,
                          adres2 = newAddress.Adres2,
                          postaKod = newAddress.PostaKodu,
                          ilce = newAddress.Ilce,
                          il = (short) newAddress.IlId,
                          frmKod = "",
                          tel1 = "",
                          tel2 = "",
                          telFaks = "",
                          telMobil = "",
                          VD = "",
                          VN = "",
                          frmKartNo = "",
                          frmNotlar = "",

                          gKisi = CurrentAccount.ytkID,
                          gTarih = DateTime.Now,
                          kKisi = CurrentAccount.ytkID,
                          kTarih = DateTime.Now,

                          frmCariTarih = DateTime.Today
                      };
            Db.frm.Add(frm);
            Db.SaveChanges();
            return RedirectToAction("OrderSummary", new { addressId = frm.frmID });
        }
        
        public ActionResult OrderSummary(int addressId)
        {
            var activeCart = Db.b2bSepet
                .Where(x => x.frmYetkili.ytkID == CurrentAccount.ytkID)
                .Single(x => x.Aktif);

            var deliveryAddress = Db.frm
                .Single(x => x.frmID == addressId);

            var model = new OrderSummaryModel
                        {
                            AdresId = deliveryAddress.frmID,
                            AdresAdi = deliveryAddress.frmAd,
                            AdresSatiri1 = deliveryAddress.adres1,
                            AdresSatiri2 = deliveryAddress.adres2,
                            AdresPostaKodu = deliveryAddress.postaKod,
                            AdresIlce = deliveryAddress.ilce,
                            AdresIl = Db.frmKent.FirstOrDefault(x => x.alanKodu == deliveryAddress.il).ilAd,

                            AktifSepetId = activeCart.Id,
                            AktifSpetAdi = activeCart.Ad,
                            Urunler = activeCart.b2bSepetAyr
                                .Select(x => new OrderSummaryModel.CartDetail
                                             {
                                                 ProductId = x.UrunId,
                                                 ProductName = x.urn.stkAd,
                                                 Quantity = x.Adet,
                                                 ProductUnitPrice = x.urn.fiyatS
                                             })
                                .ToList()
                        };

            foreach (var item in model.Urunler)
            {
                item.ProductUnitDiscountedPrice = DiscountedPriceForProduct(Db.urn.Single(x => x.stkID == item.ProductId));
                item.TotalPrice = item.ProductUnitDiscountedPrice * item.Quantity;
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult CompleteOrder(int addressId, string sipNotSatiri1, string sipNotSatiri2, string sipNotSatiri3)
        {
            var activeCart = Db.b2bSepet
                .Where(x => x.KullaniciId == CurrentAccount.ytkID)
                .Single(x => x.Aktif);

            var eId = SipEkle(addressId);

            var sira = 1;
            foreach (var cartItem in activeCart.b2bSepetAyr)
            {
                SipSatirEkle(eId, sira++, cartItem.UrunId, cartItem.Adet);
            }
            SipEklendi(eId);

            if (!string.IsNullOrWhiteSpace(sipNotSatiri1) ||
                !string.IsNullOrWhiteSpace(sipNotSatiri2) ||
                !string.IsNullOrWhiteSpace(sipNotSatiri3))
            {
                var not = new sipNot
                          {
                              enID = eId,
                              enNot = sipNotSatiri1.PadRight(33) + sipNotSatiri2.PadRight(33) + sipNotSatiri3.PadRight(33),
                          };
                Db.sipNot.Add(not);
            }

            // aktif sepeti temizleyelim
            foreach (var cartItem in activeCart.b2bSepetAyr.ToList())
            {
                Db.b2bSepetAyr.Remove(cartItem);
            }
            Db.SaveChanges();

            return RedirectToAction("OrderCompleted", new {id = eId});
        }

        public ActionResult OrderCompleted(int? id)
        {
            Check.NotNullOr404(id);

            var sip = Db.sip
                .Where(x => x.eFirma == CurrentCompany.frmID)
                .SingleOrDefault(x => x.eID == id);
            Check.NotNullOr404(sip);

            var model = new OrderCompletedModel
                        {
                            SiparisId = id.Value,
                            SiparisNotu = sip.sipNot != null ? sip.sipNot.enNot : "",

                            SiparisSatirlari = sip.sipAyr
                                .Select(x => new OrderCompletedModel.SiparisSatiri
                                             {
                                                 Sira = x.ehSira,
                                                 UrunId = x.urn.stkID,
                                                 UrunAdi = x.urn.stkAd,
                                                 StokKodu = x.urn.stkKod,
                                                 Adet = x.ehAdet,
                                                 Tutar = x.ehTutar,
                                                 KDVOrani = Db.urnKDV.FirstOrDefault(y => y.kdvID == x.ehKDV).kdvYuzde,
                                             })
                                .ToList(),
                        };
            return View(model);
        }

        private int SipEkle(int addressId)
        {
            var merkezDepo = Db.posMagaza.Single(x => x.mekanMerkezDepo == 1);
            var firmaAdresi = Db.frm
                .Where(x => x.frmID == CurrentCompany.frmID || x.frmBagID == CurrentCompany.frmID)
                .Single(x => x.frmID == addressId);
            
            var result = Db.Database.SqlQuery<int>("EXEC sip_ekle " +
                                                   "@eID = {0}," +
                                                   "@eTip = {1}," +
                                                   "@eNo = {2}," +
                                                   "@frmID = {3}," +
                                                   "@eMekan = {4}," +
                                                   "@neden = {5}," +
                                                   "@eTarih = {6}," +
                                                   "@eTarihS = {7}," +
                                                   "@eTarihV = {8}," +
                                                   "@onay = {9}," +
                                                   "@oKisi = {10}," +
                                                   "@kKisi = {11}," +
                                                   "@eY1 = {12}," +
                                                   "@eY2 = {12}," +
                                                   "@eY3 = {12}," +
                                                   "@eY4 = {12}," +
                                                   "@eY5 = {12}," +
                                                   "@eT6 = {12}," +
                                                   "@eSakTur = {13}," +
                                                   "@eFirmaMkn = {14}," +
                                                   "@eOdm = {15}," +
                                                   "@eKtgr = {16}," +
                                                   "@emir = {17}," +
                                                   "@belgeNot = {18}," +
                                                   "@eNot = {19}," +
                                                   "@eOto = {20}",
                                                   0, 1, string.Format(DateTime.Now.ToString("ddMMHHmm")),
                                                   CurrentCompany.frmID, merkezDepo.mekanID, 0,
                                                   DateTime.Today, DateTime.Today.AddDays(CurrentCompany.frmTeslimGun),
                                                   DateTime.Today.AddDays(CurrentCompany.frmVade),
                                                   1, CurrentAccount.ytkInsID, CurrentAccount.ytkInsID,
                                                   0, CurrentCompany.frmSakTur, firmaAdresi.frmID == CurrentCompany.frmID ? 0 : firmaAdresi.frmID,
                                                   1, 0, 0, "", "", 0);
            return result.Single();
        }

        private void SipSatirEkle(int eId, int sira, int stkId, decimal adet)
        {
            var urn = Db.urn.Single(x => x.stkID == stkId);
            var tutar = urn.fiyatS * adet;
            
            var mrkIndirim = Db.sakMrk_vw
                .Where(x => x.skFrmID == CurrentCompany.frmID)
                .Where(x => x.skTur == CurrentCompany.frmVadeTur)
                .SingleOrDefault(x => x.skMrkID == urn.urnMrkID);

            var ekIndirim = Db.fytOzl
                .Where(x => x.fStkID == urn.stkID)
                .Where(x => x.fTip == 4)
                //.Where(x => x.fFrmID == 0)
                .Where(x => x.fTarih <= DateTime.Today && x.fTarihSon >= DateTime.Today)
                .Where(x => x.fTur == 0)
                .Where(x => x.onay == 1)
                .OrderByDescending(x => x.fTarih)
                .ThenByDescending(x => x.fID)
                .FirstOrDefault();

            var indirimler = new List<decimal>();
            if(mrkIndirim != null)
            {
                indirimler.Add(mrkIndirim.skY1);
                indirimler.Add(mrkIndirim.skY2);
                indirimler.Add(mrkIndirim.skY3);
            }
            if(ekIndirim != null)
            {
                indirimler.Add(ekIndirim.fInd1);    
            }
            var indirimliFiyat = indirimler.Aggregate(tutar, (fiyat, indirim) => fiyat - (fiyat * indirim / 100.0m));
            var toplamIndirim = tutar - indirimliFiyat;

            Db.Database.SqlQuery<dynamic>("EXEC sipSatir_ekle " +
                                          "@ehID = {0}," +
                                          "@ehSira = {1}," +
                                          "@ehStkID = {2}," +
                                          "@ehAdet = {3}," +
                                          "@ehAdetN = {4}," +
                                          "@ehTutar = {5}," +
                                          "@ehIndirim = {6}," +
                                          "@ehKDV = {7}," +
                                          "@ehTutarKDV = {8}," +
                                          "@ehi1 = {9}," +
                                          "@ehi2 = {10}," +
                                          "@ehi3 = {11}," +
                                          "@ehi4 = {12}," +
                                          "@ehi5 = {13}," +
                                          "@ehiT6 = {14}," +
                                          "@ehBirim = {15}",
                                          eId, sira, stkId, adet, adet,
                                          tutar, toplamIndirim, urn.KDVs, 0,
                                          mrkIndirim != null ? mrkIndirim.skY1 : 0,
                                          mrkIndirim != null ? mrkIndirim.skY2 : 0,
                                          mrkIndirim != null ? mrkIndirim.skY3 : 0,
                                          ekIndirim != null ? ekIndirim.fInd1 : 0,
                                          0, 0, urn.urnBrm).ToList();

        }

        private void SipEklendi(int eId)
        {
            Db.Database.SqlQuery<dynamic>("EXEC sip_eklendi @eID = {0}", eId);
        }

        #endregion

        #region DropDown Data
        
        [HttpPost]
        public ActionResult CitiesDropDownData()
        {
            var model = Db.frmKent
                .Select(x => new DropDownListItem
                {
                    Value = x.alanKodu,
                    Text = x.ilAd
                })
                .ToList();

            return Json(model);
        }

        private void PopulateSevkAdresleri()
        {
            var sevkAdresleri = Db.frm
                .Where(x => x.frmTip == 9)
                .Where(x => x.frmBagID == CurrentCompany.frmID)
                .Select(x => new OrderDeliveryAddressModel
                {
                    Id = x.frmID,
                    AdresAdi = x.frmAd,
                    AdresSatiri1 = x.adres1,
                    AdresSatiri2 = x.adres2,
                    PostaKodu = x.postaKod,
                    Ilce = x.ilce,
                    Il = Db.frmKent.FirstOrDefault(y => y.alanKodu == x.il).ilAd,
                });

            if (sevkAdresleri.Any())
            {
                ViewBag.SevkAdresleri = sevkAdresleri.ToList();
            }
            else
            {
                ViewBag.SevkAdresleri = new List<OrderDeliveryAddressModel>();
                ViewBag.SevkAdresleri.Add(new OrderDeliveryAddressModel
                {
                    Id = CurrentCompany.frmID,
                    AdresAdi = CurrentCompany.frmAd,
                    AdresSatiri1 = CurrentCompany.adres1,
                    AdresSatiri2 = CurrentCompany.adres2,
                    PostaKodu = CurrentCompany.postaKod,
                    Ilce = CurrentCompany.ilce,
                    Il = Db.frmKent.FirstOrDefault(y => y.alanKodu == CurrentCompany.il).ilAd
                });
            }
        }

        #endregion
    }
}