﻿namespace DerinSIS.B2B.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using Emails;
    using Helpers;
    using Infrastructure.Auth;
    using Models;
    using ViewModels;

    [B2BAuthorize(AccountType.Supplier | AccountType.Customer, Privilege.YONETIM)]
    public class ManagementController : BaseController
    {
        private readonly Mailer _mailer;
        private readonly SystemSettings _settings;

        public ManagementController(DbEntities db, Mailer mailer, SystemSettings settings) : base(db)
        {
            _mailer = mailer;
            _settings = settings;
        }

        public ActionResult Index()
        {
            return RedirectToAction("CompanyUsers");
        }

        public ActionResult CompanyUsers()
        {
            var admin = CurrentAccount;
            var model = new CompanyUsersModel
                        {
                            AdminSiparisYetkisi = admin.ytkEpostaSiparis != 0,
                            AdminFaturaYetkisi = admin.ytkEpostaFatura != 0,
                            AdminMutabakatYetkisi = admin.ytkEpostaMutabakat != 0,
                            AdminSevkiyatYetkisi = admin.ytkEpostaSevkiyat != 0,

                            CompanyUsers = Db.frmYetkili
                                .Where(x => x.frm.frmID == CurrentCompany.frmID)
                                .Where(x => x.ytkB2B != 0)
                                .Select(x => new CompanyUsersModel.CompanyUser
                                             {
                                                 Id = x.ytkID,
                                                 Ad = x.ytkAd,
                                                 Eposta = x.ytkEposta,
                                                 B2BYoneticisiMi = x.ytkB2B == 3,
                                                 Unvan = x.ytkUnvan,
                                                 
                                                 AktifKullaniciMi = x.ytkB2B != 1,

                                                 SiparisYetkisi = x.ytkEpostaSiparis != 0,
                                                 FaturaYetkisi = x.ytkEpostaFatura != 0,
                                                 SevkiyatYetkisi = x.ytkEpostaSevkiyat != 0,
                                                 MutabakatYetkisi = x.ytkEpostaMutabakat != 0,
                                                 SifresiMevcutMu = !string.IsNullOrEmpty(x.ytkSifre),
                                             })
                                .ToList(),
                        };

            return View(model);
        }

        public ActionResult EditUser(int? id)
        {
            Check.NotNullOr404(id);

            var yetkili = Db.frmYetkili
                .Where(x => x.frm.frmID == CurrentCompany.frmID)
                .Where(x => x.ytkB2B != 3)
                .SingleOrDefault(x => x.ytkID == id);
            Check.NotNullOr404(yetkili);
            Check.CheckOr404(yetkili.ytkB2B != 3);

            var admin = CurrentAccount;

            var model = new EditUserModel
                        {
                            Id = yetkili.ytkID,
                            EPosta = yetkili.ytkEposta,
                            Isim = yetkili.ytkAd,
                            Unvan = yetkili.ytkUnvan,

                            SiparisYetkisi = yetkili.ytkEpostaSiparis != 0,
                            FaturaYetkisi = yetkili.ytkEpostaFatura != 0,
                            SevkiyatYetkisi = yetkili.ytkEpostaSevkiyat != 0,
                            MutabakatYetkisi = yetkili.ytkEpostaMutabakat != 0,
                            AktifKullanici = yetkili.ytkB2B != 1,

                            AdminFaturaYetkisi = admin.ytkEpostaFatura != 0,
                            AdminSiparisYetkisi = admin.ytkEpostaSiparis != 0,
                            AdminSevkiyatYetkisi = admin.ytkEpostaSevkiyat != 0,
                            AdminMutabakatYetkisi = admin.ytkEpostaMutabakat != 0,
                        };

            return View(model);
        }

        [HttpPost]
        public ActionResult EditUser(int? id, EditUserModel model)
        {
            Check.NotNullOr404(id);

            var yetkili = Db.frmYetkili
                .Where(x => x.frm.frmID == CurrentCompany.frmID)
                .SingleOrDefault(x => x.ytkID == id);
            Check.NotNullOr404(yetkili);
            Check.CheckOr404(yetkili.ytkB2B != 3);

            if(!ModelState.IsValid)
            {
                ShowErrorMessage();
                return View(model);
            }

            var admin = CurrentAccount;

            var adminEmailExtension = admin.ytkEposta.Substring(admin.ytkEposta.IndexOf("@"));
            var userEmailExtension = model.EPosta.Substring(model.EPosta.IndexOf("@"));
            if(userEmailExtension.ToLowerInvariant() != adminEmailExtension.ToLowerInvariant())
            {
                ModelState.AddModelError("EPosta", "Kuruma ait bir e-posta hesabı girmelisiniz.");
                ShowErrorMessage();
                return View(model);
            }
            
            yetkili.ytkEposta = model.EPosta;
            yetkili.ytkAd = model.Isim;
            yetkili.ytkUnvan = model.Unvan;

            if (admin.ytkEpostaFatura != 0)
            {
                yetkili.ytkEpostaFatura = (byte)(model.FaturaYetkisi ? 1 : 0);    
            }

            if (admin.ytkEpostaMutabakat != 0)
            {
                yetkili.ytkEpostaMutabakat = (byte)(model.MutabakatYetkisi ? 1 : 0);
            }


            if (admin.ytkEpostaSiparis != 0)
            {
                yetkili.ytkEpostaSiparis = (byte) (model.SiparisYetkisi ? 1 : 0);
            }

            if (admin.ytkEpostaSevkiyat != 0)
            {
                yetkili.ytkEpostaSevkiyat = (byte) (model.SevkiyatYetkisi ? 1 : 0);
            }

            if(yetkili.ytkB2B != 3)
            {
                if(model.AktifKullanici)
                {
                    yetkili.ytkB2B = 2;
                }
                else
                {
                    yetkili.ytkB2B = 1;
                }
            }

            Db.SaveChanges();

            ShowSuccessMessage("Kullanıcı bilgileri güncellenmiştir.");
            return RedirectToAction("CompanyUsers");
        }

        [HttpPost]
        public ActionResult ResetUserPassword(int? id)
        {
            Check.NotNullOr404(id);
            
            //var user = Db.frmYetkili
            //    .Where(x => x.ytkFrm == CurrentCompany.frmID)
            //    .SingleOrDefault(x => x.ytkID == id);
            //Check.NotNullOr404(user);

            //if(string.IsNullOrWhiteSpace(user.ytkEposta))
            //{
            //    ShowErrorMessage("Kullanıcının E-posta adresi mevcut değil. Lütfen önce kullanıcı bilgilerini güncelleyiniz.");
            //    return RedirectToAction("CompanyUsers");
            //}
            //var resetlemeTalebi = new b2bSifreResetlemeTalep
            //                      {
            //                          frmYetkili = user,
            //                          eposta = user.ytkEposta,
            //                          gKisi = CurrentAccount != null ? CurrentAccount.ytkInsID : 0,
            //                          gTarih = DateTime.Now,
            //                          talepDrm = 0,
            //                          Hash = Guid.NewGuid().ToString(),
            //                      };
            //Db.b2bSifreResetlemeTalep.Add(resetlemeTalebi);
            //Db.SaveChanges();

            //// resetleme mailini gönder
            //var mailModel = new ResetPasswordEmailModel
            //{
            //    KullaniciIsmi = user.ytkAd,
            //    ResetUrl = _settings.SiteAdresi + Url.Action("RecoverPasswordComplete", "Account", new { id = resetlemeTalebi.Hash }),
            //};
            //_mailer.SendResetPasswordMail(CurrentAccount, user.ytkEposta.ToLowerInvariant().Trim(), mailModel);

            return RedirectToAction("ResetUserPasswordComplete", new {id});
        }

        public ActionResult ResetUserPasswordComplete(int? id)
        {
            return View();
        }
    }
}