﻿namespace DerinSIS.B2B.Infrastructure.Auth
{
    using System;

    [Flags]
    public enum Privilege
    {
        SIPARIS = 1,
        SEVKIYAT = 2,
        FATURA = 4,
        CARI = 8,
        YONETIM = 16
    }
}