﻿namespace DerinSIS.B2B.Infrastructure.Auth
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;
    using Controllers;
    using Models;

    public class B2BAuthorizeAttribute : AuthorizeAttribute
    {
        public AccountType AccountType { get; set; }
        public Privilege[] Privileges { get; set; }

        public B2BAuthorizeAttribute(AccountType type, params Privilege[] privileges)
        {
            AccountType = type;
            Privileges = privileges;
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext == null)
            {
                throw new ArgumentNullException("filterContext");
            }

            if (OutputCacheAttribute.IsChildActionCacheActive(filterContext))
            {
                // If a child action cache block is active, we need to fail immediately, even if authorization
                // would have succeeded. The reason is that there's no way to hook a callback to rerun 
                // authorization before the fragment is served from the cache, so we can't guarantee that this 
                // filter will be re-run on subsequent requests.
                throw new InvalidOperationException("Cannot use within child action cache");
            }

            if (!AuthorizeCore(filterContext.HttpContext))
            {
                HandleUnauthorizedRequest(filterContext);
                return;
            }

            var baseController = (BaseController) filterContext.Controller;
            var currentAccountType = baseController.CurrentAccountType.Value;

            if(!AuthorizedAccountTypes.Contains(currentAccountType))
            {
                HandleInsufficientPermissionError(filterContext);
                return;
            }

            var accountPrivileges = AccountPriviliges(baseController.CurrentAccount);
            foreach (var requiredPrivilege in Privileges)
            {
                var requiredPrivilegeParts = new List<Privilege>();
                foreach (Privilege privilege in Enum.GetValues(typeof(Privilege)))
                {
                    if((requiredPrivilege & privilege) == privilege)
                    {
                        requiredPrivilegeParts.Add(privilege);
                    }
                }

                if (!accountPrivileges.Intersect(requiredPrivilegeParts).Any())
                {
                    HandleInsufficientPermissionError(filterContext);
                    return;
                }
            }
            
            SetCachePolicy(filterContext);
        }

        protected void HandleInsufficientPermissionError(AuthorizationContext filterContext)
        {
            if(filterContext.IsChildAction)
            {
            }
            else
            {
                filterContext.Result = new RedirectToRouteResult(
                new RouteValueDictionary(
                    new
                    {
                        controller = "Account",
                        action = "Unauthorized",
                    }));    
            }
            
        }

        protected void SetCachePolicy(AuthorizationContext filterContext)
        {
            // ** IMPORTANT **
            // Since we're performing authorization at the action level, the authorization code runs
            // after the output caching module. In the worst case this could allow an authorized user
            // to cause the page to be cached, then an unauthorized user would later be served the
            // cached page. We work around this by telling proxies not to cache the sensitive page,
            // then we hook our custom authorization code into the caching mechanism so that we have
            // the final say on whether a page should be served from the cache.
            HttpCachePolicyBase cachePolicy = filterContext.HttpContext.Response.Cache;
            cachePolicy.SetProxyMaxAge(new TimeSpan(0));
            cachePolicy.AddValidationCallback(CacheValidateHandler, null /* data */);
        }

        private IList<Privilege> AccountPriviliges(frmYetkili account)
        {
            var priviliges = new List<Privilege>();

            if(account.ytkEpostaSiparis != 0)
            {
                priviliges.Add(Privilege.SIPARIS);
            }

            if(account.ytkEpostaSevkiyat != 0)
            {
                priviliges.Add(Privilege.SEVKIYAT);
            }

            if(account.ytkEpostaFatura != 0)
            {
                priviliges.Add(Privilege.FATURA);
            }

            if(account.ytkEpostaMutabakat != 0)
            {
                priviliges.Add(Privilege.CARI);
            }

            if(account.ytkB2B == 3)
            {
                priviliges.Add(Privilege.YONETIM);
            }

            return priviliges;
        }

        private IList<AccountType> AuthorizedAccountTypes
        {
            get
            {
                var authorizedAccountTypes = new List<AccountType>();
                if (CheckAccountTypeAuthorized(AccountType.Customer))
                {
                    authorizedAccountTypes.Add(AccountType.Customer);
                }

                if (CheckAccountTypeAuthorized(AccountType.Supplier))
                {
                    authorizedAccountTypes.Add(AccountType.Supplier);
                }

                return authorizedAccountTypes;
            }
            
        }

        private bool CheckAccountTypeAuthorized(AccountType type)
        {
            return (AccountType & type) == type;
        }

        private void CacheValidateHandler(HttpContext context, object data, ref HttpValidationStatus validationStatus)
        {
            validationStatus = OnCacheAuthorization(new HttpContextWrapper(context));
        } 
    }
}