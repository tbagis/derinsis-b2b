﻿namespace DerinSIS.B2B.Infrastructure.WindsorInstallers
{
    using System.Web.Mvc;
    using Castle.Core;
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;

    public class MvcControllerInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            var controllers = AllTypes
                .FromThisAssembly()
                .BasedOn<IController>()
                .Configure(registration => registration.LifeStyle.Is(LifestyleType.Transient));


            container.Register(controllers);
        }
    }
}