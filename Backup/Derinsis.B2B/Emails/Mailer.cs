﻿namespace DerinSIS.B2B.Emails
{
    using System;
    using System.IO;
    using System.Web;
    using Models;
    using RazorEngine;

    public class Mailer
    {
        private DbEntities _db;

        public Mailer(DbEntities db)
        {
            _db = db;
        }

        public void SendResetPasswordMail(frmYetkili currentAccount, string destination, ResetPasswordEmailModel model)
        {
            var body = MessageBodyFromTemplate("ResetPassword", model);

            //var mail = new b2bMail
            //           {
            //               Mesaj = body,
            //               adres = destination,
            //               belgeTip = 3,
            //               belgeID = 0,
            //               eNo = "",
            //               gKisi = currentAccount != null ? currentAccount.ytkID : 0,
            //               gTarih = DateTime.Now,
            //               kTarih = DateTime.Now,
            //               yDrmE = 0,
            //           };

            //_db.b2bMail.Add(mail);
            _db.SaveChanges();
        }

        private static string MessageBodyFromTemplate(string templateName, dynamic model)
        {
            var templatePath = Path.Combine("~\\Emails\\Templates", templateName + ".cshtml");
            var absoluteTemplatePath = HttpContext.Current.Server.MapPath(templatePath);
            var templateContent = File.OpenText(absoluteTemplatePath).ReadToEnd();
            return Razor.Parse(templateContent, model);
        }
    }
}