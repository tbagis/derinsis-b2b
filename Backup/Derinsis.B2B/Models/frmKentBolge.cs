//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace DerinSIS.B2B.Models
{
    public partial class frmKentBolge
    {
        public frmKentBolge()
        {
            this.frmKent = new HashSet<frmKent>();
        }
    
        public byte bolgeID { get; set; }
        public string bolgeAd { get; set; }
    
        public virtual ICollection<frmKent> frmKent { get; set; }
    }
    
}
