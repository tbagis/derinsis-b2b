//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace DerinSIS.B2B.Models
{
    public partial class posMagaza
    {
        public int mekanID { get; set; }
        public string mekanAd { get; set; }
        public byte mekanTip { get; set; }
        public byte mekanDurum { get; set; }
        public System.DateTime mekanAcilisTarih { get; set; }
        public System.DateTime mekanSatisKontrolTarih { get; set; }
        public System.DateTime mekanKapanisTarih { get; set; }
        public short mekanAlan { get; set; }
        public System.DateTime mekanAktUTopluTarih { get; set; }
        public System.DateTime mekanAktUSonTarih { get; set; }
        public byte mekanAktUSonTip { get; set; }
        public int mekanAktUSonSayi { get; set; }
        public byte mekanOnOfis { get; set; }
        public decimal mekanSevkPay { get; set; }
        public string mekanKod { get; set; }
        public byte mekanMerkezDepo { get; set; }
        public byte mekanNo { get; set; }
        public byte mekanOzetle { get; set; }
        public byte mekanFiyatTur { get; set; }
        public byte mekanStokKontrol { get; set; }
        public decimal mekanCalisanSay { get; set; }
        public decimal mekanKasaSay { get; set; }
        public int mekanGiderMerkez { get; set; }
        public string mekanDizin { get; set; }
        public byte mekanMagazadanGiris { get; set; }
        public byte mekanGrup { get; set; }
        public string mekanKod2 { get; set; }
        public string mekanKod3 { get; set; }
        public int mekanAlanDepo { get; set; }
        public int mekanAlanOfis { get; set; }
        public int mekanAlanOrtak { get; set; }
        public int mekanAlanOtoparkAcik { get; set; }
        public int mekanAlanOtoparkKapali { get; set; }
        public int mekanAlanRapor { get; set; }
        public byte mekanGrup1 { get; set; }
        public byte mekanFirmaGunEtkin { get; set; }
        public string mekanAltMhsKod { get; set; }
        public int mekanRefID { get; set; }
        public byte mekanPosSatisVar { get; set; }
        public string mekanDizin2 { get; set; }
        public byte mekanUrnDurum { get; set; }
        public string mekanEposta { get; set; }
        public string mekanTeraziDizin { get; set; }
        public int mekanAktarimSaat { get; set; }
    
        public virtual posMagazaGrup posMagazaGrup { get; set; }
        public virtual posMagazaGrup1 posMagazaGrup1 { get; set; }
    }
    
}
