﻿
/*
 Ensure that checkbox event fires! http: //stackoverflow.com/a/4870054/225160
*/

$(document).ready(function() {
    $(":checkbox").parent().click(function (evt) {
        if (evt.target.type !== 'checkbox') {
            var $checkbox = $(":checkbox", this);
            $checkbox.attr('checked', !$checkbox.attr('checked'));
            $checkbox.change();
        }
    });
});