﻿
    function loadProductFilter() {
    
        var categories = new Array();
        var brands = new Array();
       

        if ($('#categoryName').val() != '') {
            $.each($('#categoryName').val().split(','), function (index, item) {
                categories.push(item);
            });
        }
        if ($('#brandName').val() != '') {
            $.each($('#brandName').val().split(','), function (index, item) {
                brands.push(item);
            });
        }


        function getSelectedProductContent(category, brand) {
            var content = '<div>';
            if (category.length > 0) {
                content += '<span style="color:firebrick;">Kategoriler</span>';
                $.each(category, function (index, item) {
                    content += '<li>' + item + '</li>';
                });
            }
            if (brand.length > 0) {
                content += '<span style="color:firebrick;">Markalar</span>';
                $.each(brand, function (index, item) {
                    content += '<li>' + item + '</li>';
                });
            }

            content += '</div>';
            return content;
        }
        function getProductTitle() {
            return '<span style="color:firebrick;"> Toplam : ' + $('#pCount').val() + ' ürün </span>';
        }


        if (categories.length > 0 || brands.length > 0) {
            $('#showProductFilter').data('popover', null).popover({
                placement: 'bottom',
                title: getProductTitle(),
                content: getSelectedProductContent(categories, brands),
                delay: { show: 500, hide: 100 }
            });
            $('#showProductFilter').removeClass('btn-info').addClass('btn-warning');
        }

        
    }

    function loadStoreFilter() {
        var cities = new Array();
        var groups = new Array();
        var status = new Array();
      
        if ($('#cityName').val() != '') {
            $.each($('#cityName').val().split(','), function (index, item) {
                cities.push(item);
            });
        }
        if ($('#groupName').val() != '') {
            $.each($('#groupName').val().split(','), function (index, item) {
                groups.push(item);
            });
        }
        if ($('#statueName').val() != '') {
            $.each($('#statueName').val().split(','), function (index, item) {
                status.push(item);
            });
        }
        function getSelectedStoresContent(city, group, statue) {
            var content = '<div>';
            if (city.length > 0) {
                content += '<span style="color:firebrick;">Şehirler</span>';
                $.each(city, function (index, item) {
                    content += '<li>' + item + '</li>';
                });
            }
            if (group.length > 0) {
                content += '<span style="color:firebrick;">Gruplar</span>';
                $.each(group, function (index, item) {
                    content += '<li>' + item + '</li>';
                });
            }
            if (statue.length > 0) {
                content += '<span style="color:firebrick;">Durumlar</span>';
                $.each(statue, function (index, item) {
                    content += '<li>' + item + '</li>';
                });
            }
            content += '</div>';
            return content;
        }

        function getStoresTitle() {
            return '<span style="color:firebrick;"> Toplam : ' + $('#sCount').val() + ' mağaza </span>';
        }

        if (!(cities.length == 0 && groups.length == 0 && status.length == 0)) {

            $('#showStoreFilter').data('popover', null).popover({
                placement: 'bottom',
                title: getStoresTitle(),
                content: getSelectedStoresContent(cities, groups, status),
                delay: { show: 500, hide: 100 }
            });
            $('#showStoreFilter').removeClass('btn-info').addClass('btn-warning');
        }
        
    }
    function naturalSort(a, b) {
        var re = /(^-?[0-9]+(\.?[0-9]*)[df]?e?[0-9]?$|^0x[0-9a-f]+$|[0-9]+)/gi,
            sre = /(^[ ]*|[ ]*$)/g,
            dre = /(^([\w ]+,?[\w ]+)?[\w ]+,?[\w ]+\d+:\d+(:\d+)?[\w ]?|^\d{1,4}[\/\-]\d{1,4}[\/\-]\d{1,4}|^\w+, \w+ \d+, \d{4})/,
            hre = /^0x[0-9a-f]+$/i,
            ore = /^0/,
            // convert all to strings and trim()
            x = a.toString().replace(sre, '') || '',
            y = b.toString().replace(sre, '') || '',
            // chunk/tokenize
            xN = x.replace(re, '\0$1\0').replace(/\0$/, '').replace(/^\0/, '').split('\0'),
            yN = y.replace(re, '\0$1\0').replace(/\0$/, '').replace(/^\0/, '').split('\0'),
            // numeric, hex or date detection
            xD = parseInt(x.match(hre)) || (xN.length != 1 && x.match(dre) && Date.parse(x)),
            yD = parseInt(y.match(hre)) || xD && y.match(dre) && Date.parse(y) || null;
        // first try and sort Hex codes or Dates
        if (yD)
            if (xD < yD) return -1;
            else if (xD > yD) return 1;
        // natural sorting through split numeric strings and default strings
        for (var cLoc = 0, numS = Math.max(xN.length, yN.length) ; cLoc < numS; cLoc++) {
            // find floats not starting with '0', string or 0 if not defined (Clint Priest)
            oFxNcL = !(xN[cLoc] || '').match(ore) && parseFloat(xN[cLoc]) || xN[cLoc] || 0;
            oFyNcL = !(yN[cLoc] || '').match(ore) && parseFloat(yN[cLoc]) || yN[cLoc] || 0;
            // handle numeric vs string comparison - number < string - (Kyle Adams)
            if (isNaN(oFxNcL) !== isNaN(oFyNcL)) return (isNaN(oFxNcL)) ? 1 : -1;
                // rely on string comparison if different types - i.e. '02' < 2 != '02' < '2'
            else if (typeof oFxNcL !== typeof oFyNcL) {
                oFxNcL += '';
                oFyNcL += '';
            }
            if (oFxNcL < oFyNcL) return -1;
            if (oFxNcL > oFyNcL) return 1;
        }
        return 0;
    }