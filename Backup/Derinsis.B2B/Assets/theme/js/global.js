﻿// put form submit buttons into the loading state after submit

$(document).ready(function () {

    $('form').live('submit', function () {
        $(this).find('[type=submit]').button('loading');
    });

    // activate carausels
    $('.carousel').carousel();

    // activate tooltips
    $('[rel=tooltip]').livequery(function() {
        $(this).tooltip();
    });

    $('[rel=popover]').livequery(function () {
        $(this).popover();
    });
    
});
