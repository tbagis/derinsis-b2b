﻿
$(document).ready(function () {

    // initialize modals with data element data-modal
    $('[data-modal]').live('click', function () {
        var t = $(this);

        var modalType = t.data('modal');
        var backdrop = t.data('backdrop');
        var modalElement = undefined;

        if (modalType === "async") {
            // we need to get modal contents from server with AJAX
            modalElement = $('<div class="modal fade" />')
                .html('<div class="modal-body loading"><strong>Loading...</strong></div>');

            var ajaxUrl = t.data('url');
            $.ajax({
                type: 'GET',
                url: ajaxUrl,
                dataType: 'html',
                success: function (response) {
                    modalElement.html(response);
                }
            });
        }
        else if (modalType === "embed") {
            // modal content is embedded into the page.
            var modalElementTemplate = $('.' + t.data('modal-template-class'));
            modalElement = modalElementTemplate.clone();

            // find placeholders in html with format ##variable-name## and
            // replace them with data on the element
            modalElement.html(
                modalElement.html()
                    .replace(/##([A-Za-z0-9_\-]+)##/gi, function (match, $1) { return t.data($1); })
            );
        }

        // initialize and show modal
        modalElement.modal({ backdrop: backdrop, keyboard: true });
        modalElement.bind('hidden', function () { modalElement.remove(); });
        modalElement.modal('show');
        return false;
    });


    // close modal when clicked to the modal-close button
    $('.modal .modal-close').live('click', function () {
        $(this).closest('.modal').modal('hide');
        return false;
    });

});
