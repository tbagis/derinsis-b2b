﻿

$('[data-product-search-toggle]').live('click', function () {
    var t = $(this);

    var filterName = t.data('product-search-toggle');
    var filterValue = t.data('product-search-filter');

    var form = $('.product-search');
    var relatedInput = form.find('input[name=' + filterName + ']');
    relatedInput.val(filterValue);

    form.submit();
});