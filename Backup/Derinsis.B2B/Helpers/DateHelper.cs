﻿using System.Globalization;

namespace DerinSIS.B2B.Helpers
{
    using System;
    using ViewModels;

    public static class DateHelper
    {
        public static void NotNullOrSetDefault(this DocumentDateFilterModel model, DateTime defaultBegin, DateTime defaultEnd)
        {
            if(model.Begin == null && model.End == null)
            {
                model.Begin = defaultBegin;
                model.End = defaultEnd;
            }
            else if(model.Begin == null)
            {
                model.Begin = model.End.Value.AddDays(-30);
            }
            else if(model.End == null)
            {
                model.End = model.Begin.Value.AddDays(30);
            }
        }

        public static DateTime ThisMonthBegin
        {
            get { return DateTime.Today.AddDays(-1*(DateTime.Today.Day - 1)); }
        }

        public static DateTime LastMonthBegin
        {
            get { return LastMonthEnd.AddDays(-1*(LastMonthEnd.Day - 1)); }
        }

        public static DateTime LastMonthEnd
        {
            get { return ThisMonthBegin.AddDays(-1); }
        }

        public static DateTime YearBegin
        {
            get { return DateTime.Today.AddDays(-1*(DateTime.Today.DayOfYear - 1)); }
        }

        public static DateTime ThreeMonthsBack
        {
            get { return DateTime.Today.AddDays(-90); }
        }

        public static DateTime SixMonthsBack
        {
            get { return DateTime.Today.AddDays(-180); }
        }
       
    }
}