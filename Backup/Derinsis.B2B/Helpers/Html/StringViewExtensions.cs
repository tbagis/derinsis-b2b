﻿namespace DerinSIS.B2B.Helpers.Html
{
    using System;

    public static class StringViewExtensions
    {
        public static string SafeSubString(this string str, int index, int length)
        {
            if (str.Length <= index)
                return "";

            var safeLength = Math.Min(length, str.Length - index);
            return str.Substring(index, safeLength);
        }
    }
}