﻿namespace DerinSIS.B2B.Helpers.Paging
{
    using System.Collections.Generic;
    using System.Linq;

    public static class PagingExtensions
    {
        public static IPagedCollection<T> ToPagedList<T>(this IEnumerable<T> source, int page, int itemsPerPage)
        {
            var items = source.ToList();
            var totalItems = items.Count();
            var pagedQuery = items
                .Skip((page - 1)*itemsPerPage)
                .Take(itemsPerPage);

            return new PagedList<T>(pagedQuery, page, itemsPerPage, totalItems);
        }

        public static IPagedCollection<T> ToPagedList<T>(this IQueryable<T> source, int page, int itemsPerPage)
         {
             var totalItems = source.Count();
             var pagedQuery = source
                 .Skip((page - 1)*itemsPerPage)
                 .Take(itemsPerPage);

             return new PagedList<T>(pagedQuery, page, itemsPerPage, totalItems);
         }
    }
}