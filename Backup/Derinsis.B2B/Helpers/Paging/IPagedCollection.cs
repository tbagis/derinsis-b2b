﻿namespace DerinSIS.B2B.Helpers.Paging
{
    using System.Collections.Generic;

    public interface IPagedCollection<T> : ICollection<T>
    {
        int Page { get; }
        int ItemsPerPage { get; }
        int TotalPages { get; }
        int TotalItems { get; }

        bool HasPrevPage();
        bool HasNextPage();

        IEnumerable<int> EnumeratePages();
    }
}