﻿namespace DerinSIS.B2B.Helpers.Paging
{
    using System;
    using System.Collections.Generic;

    public class PagedList<T> : List<T>, IPagedCollection<T>
    {
        public PagedList(IEnumerable<T> collection, int page, int itemsPerPage, int totalItems)
            : base(collection)
        {
            Page = page;
            ItemsPerPage = itemsPerPage;
            TotalItems = totalItems;
        }

        public int Page { get; set; }
        public int ItemsPerPage { get; set; }
        public int TotalItems { get; set; }
        public int TotalPages { get { return (int) Math.Ceiling((decimal) TotalItems / ItemsPerPage); } }

        public bool HasPrevPage()
        {
            return Page > 1;
        }

        public bool HasNextPage()
        {
            return TotalPages > Page;
        }

        public IEnumerable<int> EnumeratePages()
        {
            var enumerateBegin = Math.Max(Page - 2, 1);
            var enumerateEnd = Math.Min(Page + 2, TotalPages);

            if (enumerateBegin == 1)
            {
                // do nothing
            }
            else if (enumerateBegin == 2)
            {
                yield return 1;
            }
            else if (enumerateBegin == 3)
            {
                yield return 1;
                yield return 2;
            }
            else
            {
                yield return 1;
                yield return 2;
                yield return 0; /* means "..." */
            }

            for (var i = enumerateBegin; i <= enumerateEnd; i++)
            {
                yield return i;
            }

            if (TotalPages > enumerateEnd)
            {
                yield return 0; /* means "..." */
            }
        }
    }
}