﻿namespace DerinSIS.B2B.Helpers
{
    using System.Linq;
    using Models;

    public class SystemSettings
    {
        private readonly DbEntities _db;

        public SystemSettings(DbEntities db)
        {
            _db = db;
        }

        public string SiteAdresi
        {
            get
            {
                var url = _db.drn2ayar.Single(x => x.ayarID == 762).ayarDeger;
                if(url.EndsWith("/"))
                {
                    return url.Substring(0, url.Length - 1);
                }
                return url;
            }
        }

        public bool EvrakNotlariGosterilsin
        {
            get { return false; }
        }
    }
}