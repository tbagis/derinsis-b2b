﻿namespace DerinSIS.B2B.Helpers
{
    using System;
    using System.Globalization;

    public static class Parser
    {
         public static DateTime? ParseDate(string dateString)
         {
             DateTime date;
             var isParsed = DateTime.TryParse(dateString, CultureInfo.GetCultureInfo("tr-TR"), DateTimeStyles.None, out date);

             if (isParsed)
             {
                 return date;
             }
             return null;
         }
    }
}