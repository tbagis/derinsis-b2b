﻿namespace DerinSIS.B2B.ViewModels
{
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    #region Product CRUD Models

    public class NewProductModel
    {
        [Required(ErrorMessage = "Barkod alanı gereklidir.")]
        [Display(Name = "Barkod")]
        public string Barkod { get; set; }

        [Required(ErrorMessage = "Ürün Adı alanı gereklidir.")]
        [Display(Name = "Ürün Adı")]
        public string UrunAdi { get; set; }

        [Required(ErrorMessage = "Dağıtıcı Kodu alanı gereklidir.")]
        [Display(Name = "Dağıtıcı Kodu")]
        public string DagiticiKodu { get; set; }

        [Required(ErrorMessage = "KDV Oranı alanı gereklidir.")]
        [Display(Name = "KDV Oranı")]
        public int KDVOrani { get; set; }
    }

    public class EditProductModel
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [Required(ErrorMessage = "Barkod alanı gereklidir.")]
        [Display(Name = "Barkod")]
        public string Barkod { get; set; }

        [Required(ErrorMessage = "Ürün Adı alanı gereklidir.")]
        [Display(Name = "Ürün Adı")]
        public string UrunAdi { get; set; }

        [Required(ErrorMessage = "Dağıtıcı Kodu alanı gereklidir.")]
        [Display(Name = "Dağıtıcı Kodu")]
        public string DagiticiKodu { get; set; }


        [Required(ErrorMessage = "KDV Oranı alanı gereklidir.")]
        [Display(Name = "KDV Oranı")]
        public int KDVOrani { get; set; }
    }

    #endregion
}