﻿namespace DerinSIS.B2B.ViewModels
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    public class CompanyUsersModel
    {
        public bool AdminSiparisYetkisi { get; set; }
        public bool AdminSevkiyatYetkisi { get; set; }
        public bool AdminFaturaYetkisi { get; set; }
        public bool AdminMutabakatYetkisi { get; set; }

        public ICollection<CompanyUser> CompanyUsers { get; set; }
        public class CompanyUser
        {
            public int Id { get; set; }
            public string Eposta { get; set; }
            public string Ad { get; set; }
            public string Unvan { get; set; }
            public bool B2BYoneticisiMi { get; set; }

            public bool AktifKullaniciMi { get; set; }
            public bool SifresiMevcutMu { get; set; }

            public bool SiparisYetkisi { get; set; }
            public bool SevkiyatYetkisi { get; set; }
            public bool FaturaYetkisi { get; set; }
            public bool MutabakatYetkisi { get; set; }
        }
    }

    public class EditUserModel
    {
        [ScaffoldColumn(false)]
        public int Id { get; set; }

        [Required(ErrorMessage = "E-Posta alanı gereklidir.")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Geçersiz E-posta adresi")]
        [Display(Name = "E-Posta")]
        public string EPosta { get; set; }

        [Required(ErrorMessage = "İsim alanı gereklidir.")]
        [Display(Name = "İsim")]
        public string Isim { get; set; }

        [Required(ErrorMessage = "Ünvan alanı gereklidir.")]
        [Display(Name = "Ünvan")]
        public string Unvan { get; set; }

        [Display(Name = "Sipariş Yetkisi")]
        public bool SiparisYetkisi { get; set; }

        [Display(Name = "Sevkiyat Yetkisi")]
        public bool SevkiyatYetkisi { get; set; }

        [Display(Name = "Fatura Yetkisi")]
        public bool FaturaYetkisi { get; set; }

        [Display(Name = "Mutabakat Yetkisi")]
        public bool MutabakatYetkisi { get; set; }

        [Display(Name = "Aktif Kullanıcı Mı?")]
        public bool AktifKullanici { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool AdminSiparisYetkisi { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool AdminSevkiyatYetkisi { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool AdminFaturaYetkisi { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool AdminMutabakatYetkisi { get; set; }
    }
}