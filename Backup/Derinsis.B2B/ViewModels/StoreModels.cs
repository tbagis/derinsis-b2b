﻿
namespace DerinSIS.B2B.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Web.Mvc;
    using Helpers.Paging;

    #region Product List

    public class ProductSearchResultModel
    {
        public int Id { get; set; }
        public string Isim { get; set; }
        public bool ModelMi { get; set; }
    }

    public class ProductListModel
    {
        // Arama Filtereleri
        public string Sorgu { get; set; }
        public int? KategoriIdFiltresi { get; set; }
        public string KategoriFiltreEtiketi { get; set; }
        public int? UreticiIdFiltresi { get; set; }
        public string UreticiFiltreEtiketi { get; set; }
        public int? MarkaIdFiltresi { get; set; }
        public string FiyatFiltresi { get; set; }
        public string FiyatFiltreEtiketi { get; set; }
        public string MarkaFiltreEtiketi { get; set; }
        public bool OzelFiyat { get; set; }


        // Arama Sonuçları
        public IPagedCollection<ProductListItemModel> AramaSonuclari { get; set; }

        public abstract class ProductListItemModel
        {
            public int Id { get; set; }
            public string Isim { get; set; }
            public virtual bool ModelMi { get; set; }
            public string KucukResimUrl { get; set; }
            public string BuyukResimUrl { get; set; }
            public int Rutbe { get; set; }

        }

        public class ProductModel : ProductListItemModel
        {
            public string Barkod { get; set; }
            public string StokKodu { get; set; }
            public decimal KDVHaricFiyat { get; set; }
            public decimal Indirim1 { get; set; }
            public decimal Indirim2 { get; set; }
            public decimal Indirim3 { get; set; }
            public decimal Indirim4 { get; set; }
            public string Birim { get; set; }
            public decimal Kur { get; set; }
            public override bool ModelMi { get { return false; } }
            public int? BagliOlduguModelId { get; set; }
        }

        public class ProductGroupModel : ProductListItemModel
        {
            public IEnumerable<ProductModel> Urunler { get; set; }
            public decimal Indirim1 { get; set; }
            public decimal Indirim2 { get; set; }
            public decimal Indirim3 { get; set; }
            public decimal Indirim4 { get; set; }

            public override bool ModelMi { get { return true; } }
        }
    }


    public class ProductListItemGroupModel
    {
        public int GroupId { get; set; }
        public string Name { get; set; }

        public IEnumerable<GroupItem> Items { get; set; }

        public class GroupItem
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public decimal Fiyat { get; set; }
            public string StokKodu { get; set; }
            public string Barkod { get; set; }
        }
    }
    #endregion

    #region Category & Brand Trees

    public class CategoryTreeModel
    {
        public IList<CategoryItem> Items { get; set; }

        public bool HasChildren(CategoryItem item)
        {
            return Items.Any(x => x.ParentId == item.Id);
        }

        public IList<CategoryItem> GetChildrenFor(CategoryItem item)
        {
            return Items
                .Where(x => x.ParentId == item.Id)
                .OrderBy(x => x.Isim)
                .ToList();
        }
     

        public IList<CategoryItem> GetRootLevelItems()
        {
            return Items
                .Where(x => x.ParentId == null)
                .OrderBy(x => x.Isim)
                .ToList();
        }

        public class CategoryItem
        {
            public int Id { get; set; }
            public int? ParentId { get; set; }
            
            public string Isim { get; set; }
            public int Order { get; set; }
        }
    }

    public class BrandTreeModel
    {
        public IEnumerable<Producer> Producers { get; set; }

        public class Producer
        {
            public int Id { get; set; }
            public string Name { get; set; }

            public IEnumerable<Brand> Brands { get; set; }
        }

        public class Brand
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public int Count { get; set; }
        }
    }
    public class PricesModel
    {
        public ICollection<Price> Prices { get; set; }

        public class Price
        {
            public string Name { get; set; }
            public int Min { get; set; }
            public int Max { get; set; }
        }
    }
    #endregion

    #region Product Detail Pages

    public class ProductDetailModel
    {
        public int Id { get; set; }
        public string UrunKodu { get; set; }
        public string Isim { get; set; }
        public string StokKisaAd { get; set; }

        // Ölçüler
        public string UrunBirimi { get; set; }
        public decimal PaketOlcu { get; set; }
        public string PaketBirimi { get; set; }
        public int KoliOlcu { get; set; }
        public string KoliBirimi { get; set; }
        public int KutuOlcu { get; set; }
        public int PaletOlcu { get; set; }

        // Kategoriler
        public int Kategori0Id { get; set; }
        public int Kategori1Id { get; set; }
        public int Kategori2Id { get; set; }
        public string Kategori0Ad { get; set; }
        public string Kategori1Ad { get; set; }
        public string Kategori2Ad { get; set; }

        // Üretici - Marka - Model
        public int UreticiId { get; set; }
        public string UreticiAd { get; set; }
        public int MarkaId { get; set; }
        public string MarkaAd { get; set; }

        public int ModelId { get; set; }
        public string ModelAd { get; set; }
        

        // Ürün Kodları
        public bool UrunKod1GosterilsinMi { get; set; }
        public bool UrunKod2GosterilsinMi { get; set; }
        public bool UrunKod3GosterilsinMi { get; set; }
        public bool UrunKod4GosterilsinMi { get; set; }
        public bool UrunKod5GosterilsinMi { get; set; }
        public string UrunKod1Baslik { get; set; }
        public string UrunKod2Baslik { get; set; }
        public string UrunKod3Baslik { get; set; }
        public string UrunKod4Baslik { get; set; }
        public string UrunKod5Baslik { get; set; }
        
        public string UrunKod1 { get; set; }
        public string UrunKod2 { get; set; }
        public string UrunKod3 { get; set; }
        public string UrunKod4 { get; set; }
        public string UrunKod5 { get; set; }
        

        // Ürün Özellikleri
        public IEnumerable<BilgiGrubu> BilgiGruplari { get; set; }
        public class BilgiGrubu
        {
            public string GrupAdi { get; set; }
            public IEnumerable<Bilgi> Bilgiler { get; set; }

            public class Bilgi
            {
                public string Ad { get; set; }
                public string Deger { get; set; }
            }
        }

        
        // Stok Bilgileri
        public IEnumerable<StokBilgisi> StokBilgileri { get; set; }
        public class StokBilgisi
        {
            public string MekanAdi { get; set; }
            public decimal Stok { get; set; }
        }

        // Fiyatlar ve İndirimler
        public decimal KDVHaricSatisFiyati { get; set; }
        public string Birim { get; set; }
        public decimal SatisKur { get; set; }
        
        public decimal Indirim1 { get; set; }
        public decimal Indirim2 { get; set; }
        public decimal Indirim3 { get; set; }
        public decimal Indirim4 { get; set; }

        public decimal KDVHaricNet
        {
            get
            {
                var indirimler = new[] {Indirim1, Indirim2, Indirim3, Indirim4};
                return indirimler.Aggregate(KDVHaricSatisFiyati, (fiyat, indirim) => fiyat - (fiyat * indirim / 100.0m));
            }
        }

        public decimal ToplamIndirim { get { return KDVHaricNet - KDVHaricSatisFiyati; } }
        public int KDVOrani { get; set; }
        public decimal KDVTutari { get { return KDVHaricNet * (decimal) KDVOrani / 100.0m; } }
        public decimal KDVDahilToplam { get { return KDVHaricNet + KDVTutari; } }

        public IEnumerable<BarkodBilgisi> BarkodBilgileri { get; set; }
        public class BarkodBilgisi
        {
            public string Barkod { get; set; }
            public int Adet { get; set; }
            public string AdetBirim { get; set; }
        }

        public IEnumerable<UrunSiparis> Siparisler { get; set; }
        public class UrunSiparis
        {
            public int Id { get; set; }
            public string EvrakNo { get; set; }
            public DateTime Tarih { get; set; }
            public DateTime SevkTarihi { get; set; }
            public string HareketTipi { get; set; }
            public string HareketGrubu { get; set; }
            public decimal UrunKoliAdet { get { return UrunToplamAdet / UrunKoliIciAdet; } }
            public decimal UrunKoliIciAdet { get; set; }
            public decimal UrunToplamAdet { get; set; }
            public decimal UrunBirimFiyat { get { return UrunKDVHaricToplam / UrunToplamAdet; } }
            public decimal UrunKDVHaricToplam { get; set; }
            public decimal UrunIndirimTutari { get; set; }
            public decimal UrunKDVHaricNet { get { return UrunKDVHaricToplam - UrunIndirimTutari; } }
            public decimal ToplamKDVHaricNet { get; set; }
            public decimal ToplamKDVDahilTutar { get; set; }
            
        }

        public IEnumerable<UrunIrsaliye> Irsaliyeler { get; set; }
        public class UrunIrsaliye
        {
            public int Id { get; set; }
            public string EvrakNo { get; set; }
            public DateTime Tarih { get; set; }
            public DateTime SevkTarihi { get; set; }
            public string HareketTipi { get; set; }
            public string HareketGrubu { get; set; }
            public decimal UrunKoliAdet { get { return UrunToplamAdet / UrunKoliIciAdet; } }
            public decimal UrunKoliIciAdet { get; set; }
            public decimal UrunToplamAdet { get; set; }
            public decimal UrunBirimFiyat { get { return UrunKDVHaricToplam / UrunToplamAdet; } }
            public decimal UrunKDVHaricToplam { get; set; }
            public decimal UrunIndirim1 { get; set; }
            public decimal UrunIndirim2 { get; set; }
            public decimal UrunIndirim3 { get; set; }
            public decimal UrunIndirim4 { get; set; }
            public decimal UrunIndirim5 { get; set; }
            public decimal UrunIndirimTutari { get; set; }
            public decimal UrunKDVHaricNet { get { return UrunKDVHaricToplam - UrunIndirimTutari; } }
            public decimal ToplamKDVHaricNet { get; set; }
            public decimal ToplamKDVDahilTutar { get; set; }
        }

        public IEnumerable<UrunFatura> Faturalar { get; set; } 
        public class UrunFatura
        {
            public int Id { get; set; }
            public string EvrakNo { get; set; }
            public DateTime Tarih { get; set; }
            public DateTime VadeTarihi { get; set; }
            public string HareketTipi { get; set; }
            public string HareketGrubu { get; set; }
            public decimal UrunKoliAdet { get { return UrunToplamAdet / UrunKoliIciAdet; } }
            public decimal UrunKoliIciAdet { get; set; }
            public decimal UrunToplamAdet { get; set; }
            public decimal UrunBirimFiyat { get { return UrunKDVHaricToplam / UrunToplamAdet; } }
            public decimal UrunKDVHaricToplam { get; set; }
            public decimal UrunIndirim1 { get; set; }
            public decimal UrunIndirim2 { get; set; }
            public decimal UrunIndirim3 { get; set; }
            public decimal UrunIndirim4 { get; set; }
            public decimal UrunIndirim5 { get; set; }
            public decimal UrunIndirimTutari { get; set; }
            public decimal UrunKDVHaricNet { get { return UrunKDVHaricToplam - UrunIndirimTutari; } }
            public decimal ToplamKDVHaricNet { get; set; }
            public decimal ToplamKDVDahilTutar { get; set; }
        }

    }

    public class ProductGroupDetailModel
    {
        public int Id { get; set; }
        public string ModelAdi { get; set; }

        // Üretici - Marka - Model
        public int UreticiId { get; set; }
        public string UreticiAdi { get; set; }
        public int MarkaId { get; set; }
        public string MarkaAdi { get; set; }

        // Kategori
        public int Kategori0Id { get; set; }
        public int Kategori1Id { get; set; }
        public int Kategori2Id { get; set; }
        public string Kategori0Ad { get; set; }
        public string Kategori1Ad { get; set; }
        public string Kategori2Ad { get; set; }

        // Fiyat - İndirim
        public decimal KDVHaricSatisFiyati { get; set; }
        public decimal Indirim1 { get; set; }
        public decimal Indirim2 { get; set; }
        public decimal Indirim3 { get; set; }
        public decimal Indirim4 { get; set; }
        public decimal KDVHaricNet
        {
            get
            {
                var indirimler = new[] { Indirim1, Indirim2, Indirim3, Indirim4 };
                return indirimler.Aggregate(KDVHaricSatisFiyati, (fiyat, indirim) => fiyat - (fiyat * indirim / 100.0m));
            }
        }
        public decimal ToplamIndirim { get { return KDVHaricNet - KDVHaricSatisFiyati; } }
        public int KDVOrani { get; set; }
        public decimal KDVTutari { get { return KDVHaricNet * KDVOrani / 100.0m; } }
        public decimal KDVDahilToplam { get { return KDVHaricNet + KDVTutari; } }

        public ICollection<Product> Products { get; set; }
        public class Product
        {
            public int Id { get; set; }
            public string Isim { get; set; }
            public string StokKodu { get; set; }
            public string Barkod { get; set; }
            public decimal KDVHaricSatisFiyati { get; set; }
            public decimal Indirim1 { get; set; }
            public decimal Indirim2 { get; set; }
            public decimal Indirim3 { get; set; }
            public decimal Indirim4 { get; set; }
            public decimal KDVHaricNet
            {
                get
                {
                    var indirimler = new[] { Indirim1, Indirim2, Indirim3, Indirim4 };
                    return indirimler.Aggregate(KDVHaricSatisFiyati, (fiyat, indirim) => fiyat - (fiyat * indirim / 100.0m));
                }
            }
            public decimal ToplamIndirim { get { return KDVHaricNet - KDVHaricSatisFiyati; } }
            public int KDVOrani { get; set; }
            public decimal KDVTutari { get { return KDVHaricNet * KDVOrani / 100.0m; } }
            public decimal KDVDahilToplam { get { return KDVHaricNet + KDVTutari; } }
        }
    }
    public class ProductPromoDetailModel
    {
        public int Id { get; set; }
        public string Ad { get; set; }
        public string resim { get; set; }

        public ICollection<Product> Products { get; set; }
        public class Product
        {
            public int Id { get; set; }
            public string Isim { get; set; }
            public string StokKodu { get; set; }
            public string Barkod { get; set; }
            public decimal KDVHaricSatisFiyati { get; set; }
            public string Birim { get; set; }
            public decimal Indirim1 { get; set; }
            public decimal Indirim2 { get; set; }
            public decimal Indirim3 { get; set; }
            public decimal Indirim4 { get; set; }
            public decimal KDVHaricNet
            {
                get
                {
                    var indirimler = new[] { Indirim1, Indirim2, Indirim3, Indirim4 };
                    return indirimler.Aggregate(KDVHaricSatisFiyati, (fiyat, indirim) => fiyat - (fiyat * indirim / 100.0m));
                }
            }
            public decimal ToplamIndirim { get { return KDVHaricNet - KDVHaricSatisFiyati; } }
            public int KDVOrani { get; set; }
            public decimal KDVTutari { get { return KDVHaricNet * KDVOrani / 100.0m; } }
            public decimal KDVDahilToplam { get { return KDVHaricNet + KDVTutari; } }
        }
    }
    #endregion

    #region My Carts Models 

    public class MyCartModel
    {
        public int Id { get; set; }
        public string Isim { get; set; }
        public string Aciklamalar { get; set; }
        public bool AktifMi { get; set; }
        public int Adet { get; set; }
    }

    public class CreateNewCartModel
    {
        [Required(ErrorMessage = "Sepet ismi zorunludur.")]
        [Display(Name = "Sepet İsmi")]
        [DataType(DataType.Text)]
        public string Isim { get; set; }

        [Display(Name = "Açıklamalar")]
        [DataType(DataType.MultilineText)]
        public string Aciklamalar { get; set; }
    }

    public class EditCartModel
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }


        [Required(ErrorMessage = "Sepet ismi zorunludur.")]
        [Display(Name = "Sepet İsmi")]
        [DataType(DataType.Text)]
        public string Isim { get; set; }

        [Display(Name = "Açıklamalar")]
        [DataType(DataType.MultilineText)]
        public string Aciklamalar { get; set; }
    }

    #endregion

    #region Cart Models

    public class CartMenuElementModel
    {
        public string AktifSepetIsmi { get; set; }
        public int ToplamUrunCesidi { get; set; }
        public decimal KDVDahilTutar { get; set; }

        public ICollection<Urun> SonEklenen2Urun { get; set; }
        public class Urun
        {
            public string UrunIsmi { get; set; }
        }
    }

    public class CartSidebarElementModel
    {
        public string AktifSepetIsmi { get; set; }
        public int ToplamUrunCesidi { get; set; }
        public decimal KDVDahilTutar { get; set; }
    }

    public class CartModel
    {
        public int AktifSepetId { get; set; }
        public ICollection<Sepet> Sepetlerim { get; set; }
        public class Sepet
        {
            public int Id { get; set; }
            public string Isim { get; set; }
            public string Aciklama { get; set; }
        }


        public IEnumerable<Urun> AktifSepetUrunleri { get; set; }
        public class Urun
        {
            public int UrunId { get; set; }
            public string UrunAdi { get; set; }
            public string Barkod { get; set; }
            public string StokKodu { get; set; }

            public decimal KDVHaricSatisFiyati { get; set; }
            public decimal Indirim1 { get; set; }
            public decimal Indirim2 { get; set; }
            public decimal Indirim3 { get; set; }
            public decimal Indirim4 { get; set; }

            public decimal KDVHaricNet
            {
                get
                {
                    var indirimler = new[] {Indirim1, Indirim2, Indirim3, Indirim4};
                    return indirimler.Aggregate(KDVHaricSatisFiyati, (fiyat, indirim) => fiyat - (fiyat*indirim/100.0m));
                }
            }

            public int KDVOrani { get; set; }
            public decimal KDVTutari
            {
                get { return KDVHaricNet*KDVOrani/100.0m; }
            }

            public decimal KDVDahilToplam
            {
                get { return KDVHaricNet + KDVTutari; }
            }

            public decimal Adet { get; set; }

            public decimal SatirToplami { get { return KDVDahilToplam*Adet; } }
        }
    }

    public class AddToActiveCartModel
    {
        public IEnumerable<CartItem> Items { get; set; }
 
        public class CartItem
        {
            public int ProductId { get; set; }
            public decimal Quantity { get; set; }
        }
    }

    public class UpdateActiveCartItemModel
    {
        public int ProductId { get; set; }
        public decimal NewQuantity { get; set; }
    }

    #endregion

    #region Make Order Models

    public class DeliveryCityModel
    {
        public int AlanKodu { get; set; }
        public string IlAdi { get; set; }
    }

    public class OrderDeliveryAddressModel
    {
        public int Id { get; set; }
        public string AdresAdi { get; set; }
        public string AdresSatiri1 { get; set; }
        public string AdresSatiri2 { get; set; }
        public string PostaKodu { get; set; }
        public string Ilce { get; set; }
        public string Il { get; set; }
    }
    
    public class CreateDeliveryAddressModel
    {
        [ScaffoldColumn(false)]
        public int? Id { get; set; }

        [Required(ErrorMessage = "Adres Başlığı bilgisi gereklidir.")]
        [Display(Name = "Adres Başlığı")]
        public string AdresIsmi { get; set; }

        [Required(ErrorMessage = "Adres 1 bilgisi gereklidir.")]
        [Display(Name = "Adres 1")]
        public string Adres1 { get; set; }

        [Required(ErrorMessage = "Adres 2 bilgisi gereklidir.")]
        [Display(Name = "Adres 2")]
        public string Adres2 { get; set; }

        [Required(ErrorMessage = "Posta Kodu bilgisi gereklidir.")]
        [Display(Name = "Posta Kodu")]
        [StringLength(5, MinimumLength = 5, ErrorMessage = "Posta Kodu 5 karakterden oluşmalıdır.")]
        public string PostaKodu { get; set; }

        [Required(ErrorMessage = "İlçe bilgisi gereklidir.")]
        [Display(Name = "İlçe")]
        public string Ilce { get; set; }

        [Display(Name = "İl")]
        [Required(ErrorMessage = "Il seçilmesi gereklidir.")]
        [UIHint("DropDownList")]
        [AdditionalMetadata("DataController", "Store")]
        [AdditionalMetadata("DataAction", "CitiesDropDownData")]
        public int? IlId { get; set; }
    }

    public class OrderSummaryModel
    {
        public int AktifSepetId { get; set; }
        public string AktifSpetAdi { get; set; }

        public int AdresId { get; set; }
        public string AdresAdi { get; set; }
        public string AdresSatiri1 { get; set; }
        public string AdresSatiri2 { get; set; }
        public string AdresPostaKodu { get; set; }
        public string AdresIlce { get; set; }
        public string AdresIl { get; set; }

        public IEnumerable<CartDetail> Urunler { get; set; } 
        
        public class CartDetail
        {
            public int ProductId { get; set; }
            public string ProductName { get; set; }
            public decimal Quantity { get; set; }
            public decimal ProductUnitPrice { get; set; }
            public decimal ProductUnitDiscountedPrice { get; set; }
            public decimal TotalPrice { get; set; }
        }
    }

    public class OrderCompletedModel
    {
        public int SiparisId { get; set; }
        public string SiparisNotu { get; set; }

        public ICollection<SiparisSatiri> SiparisSatirlari { get; set; }
        public class SiparisSatiri
        {
            public int Sira { get; set; }
            public int UrunId { get; set; }
            public string UrunAdi { get; set; }
            public string StokKodu { get; set; }
            public decimal Adet { get; set; }
            public decimal Tutar { get; set; }
            public int KDVOrani { get; set; }

            public decimal Matrah
            {
                get { return (KDVOrani*Tutar/100m) + Tutar; }
            }
        }

      
    }
    public class PromoModel
    {
        public ICollection<promo> Promolar { get; set; }
        public class promo
        {
            public byte sira { get; set; }
            public string resim { get; set; }
            public string baslik { get; set; }
            public byte etkiTip { get; set; }
            public int etkiId { get; set; }
        }
    }

    #endregion
}