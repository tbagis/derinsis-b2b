﻿using System.ComponentModel.DataAnnotations;

namespace DerinSIS.B2B.ViewModels
{
    using System.Web.Mvc;

    public class LogOnModel
    {
        [Required(ErrorMessage = "E-posta alanı gereklidir.")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Geçersiz E-posta adresi")]
        [Display(Name = "E-posta adresi")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Şifre alanı gereklidir.")]
        [DataType(DataType.Password)]
        [Display(Name = "Şifre")]
        public string Password { get; set; }

        [Display(Name = "Beni hatırla?")]
        [ScaffoldColumn(false)]
        public bool RememberMe { get; set; }
    }

    public class ChangePasswordModel
    {
        [Required(ErrorMessage = "Eski şifre alanı gereklidir.")]
        [DataType(DataType.Password)]
        [Display(Name = "Eski Şifre")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "Yeni şifre alanı gereklidir.")]
        [StringLength(100, ErrorMessage = "{0} alanı en az {2} karakter olmalıdır.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Yeni Şifre")]
        public string NewPassword { get; set; }

        [Required(ErrorMessage = "Yeni şifre onay alanı gereklidir.")]
        [DataType(DataType.Password)]
        [Display(Name = "Yeni Şifre Onayı")]
        [Compare("NewPassword", ErrorMessage = "Şifre ve Şifre Onayı alanları uyuşmuyor.")]
        public string ConfirmPassword { get; set; }
    }

    public class ResetPasswordModel
    {
        [Required(ErrorMessage = "E-posta alanı gereklidir.")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Geçersiz E-posta adresi")]
        [Display(Name = "E-posta adresi")]
        public string Email { get; set; }
    }

    public class ResetPasswordCompleteModel
    {
        [Required(ErrorMessage = "Yeni şifre alanı gereklidir.")]
        [StringLength(100, ErrorMessage = "{0} alanı en az {2} karakter olmalıdır.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Yeni Şifre")]
        public string NewPassword { get; set; }

        [Required(ErrorMessage = "Yeni şifre onay alanı gereklidir.")]
        [DataType(DataType.Password)]
        [Display(Name = "Yeni Şifre Onayı")]
        [Compare("NewPassword", ErrorMessage = "Şifre ve Şifre Onayı alanları uyuşmuyor.")]
        public string ComfirmPassword { get; set; }
    }

    public class MyProfileModel
    {
        public string YetkiliIsmi { get; set; }
        public string YetkiliEPosta { get; set; }

        public string FirmaIsmi { get; set; }

        public string FirmaAdresDetay { get; set; }
        public string FirmaAdresPostaKodu { get; set; }
        public string FirmaAdresIlce { get; set; }
        public string FirmaAdresSehir { get; set; }
        public string FirmaAdresUlke { get; set; }

        public string FirmaTel1 { get; set; }
        public string FirmaTel2 { get; set; }
        public string FirmaTelGsm { get; set; }
        public string FirmaTelFax { get; set; }

        public string FirmaVergiDairesi { get; set; }
        public string FirmaVergiNo { get; set; }
    }
}