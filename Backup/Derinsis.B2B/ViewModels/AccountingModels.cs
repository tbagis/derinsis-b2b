﻿namespace DerinSIS.B2B.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    #region Accounting Dashboard
    
    public class AccountingDashboardModel
    {
        public decimal Borc { get; set; }
        public decimal Alacak { get; set; }
        public decimal Bakiye { get; set; }
        
        public DateTime SistemMutabakatTarihi { get; set; }
        public DateTime FirmaMutabakatTarihi { get; set; }

        public IEnumerable<BankaHesapBilgisi> BankaHesapBilgileri { get; set; }
        public class BankaHesapBilgisi
        {
            public string Banka { get; set; }
            public string Sube { get; set; }
            public string HesapNo { get; set; }
        }

        public IEnumerable<OdenmemisFatura> OdenmemisFaturalar { get; set; }
        public class OdenmemisFatura
        {
            public int Id { get; set; }
            public string EvrakNo { get; set; }
            public DateTime FaturaTarihi { get; set; }
            public DateTime VadeTarihi { get; set; }
            public decimal KalanTutar { get; set; }
        }
    }

    #endregion

    #region Hesap Hareketleri

    public class AccountActivitiesModel
    {
        public DocumentDateFilterModel DateFilter { get; set; }
        public DateTime MutabakatTarihi { get; set; }

        public decimal DevredenBakiye { get; set; }
        public IEnumerable<AccountActivity> Activities { get; set; }

        public class AccountActivity
        {
            public int Id { get; set; }
            public int? IlgiliFaturaId { get; set; }
            public string IlgiliFaturaNo { get; set; }
            public DateTime Tarih { get; set; }
            public string HareketTipi { get; set; }
            public string EvrakNo { get; set; }
            public DateTime VadeTarihi { get; set; }
            public decimal? Borc { get; set; }
            public decimal? Alacak { get; set; }
            public decimal? Bakiye { get; set; }    
        }
    }

    #endregion

    #region Fatura Modelleri

    public class InvoicesModel
    {
        public DocumentDateFilterModel DateFilter { get; set; }
        public int? ProductFilterId { get; set; }
        public string ProductFilterLabel { get; set; }

        public ICollection<DocumentType> AvailableTypes { get; set; }
        public int? TypeFilterId { get; set; }

        public Settings GosterimSecenekleri { get; set; }

        public IEnumerable<Invoice> Invoices { get; set; }
        public class Invoice
        {
            public int Id { get; set; }
            public DateTime Tarih { get; set; }
            public string HareketTipi { get; set; }
            public string EvrakNo { get; set; }
            public InvoiceStatus Durum { get; set; }
            public DateTime VadeTarihi { get; set; }
            public string EvrakNot { get; set; }
            public decimal KDVHaricToplam { get; set; }
            public decimal ToplamIndirim { get; set; }
            public decimal KDVHaricNet { get { return KDVHaricToplam - ToplamIndirim; } }
            public string HareketGrubu { get; set; }
            public string SevkAdresi { get; set; }

            
        }

        public enum InvoiceStatus
        {
            ACIK = 0,
            IPTAL = 2
        }

        public class Settings
        {
            public bool EvrakNotuGosterilsinMi { get; set; }
        }
    }

    public class InvoiceDetailModel
    {
        public int Id { get; set; }
        public string EvrakNo { get; set; }
        public DateTime Tarih { get; set; }
        public DateTime VadeTarihi { get; set; }
        public DateTime SevkTarihi { get; set; }
        public string HareketTipi { get; set; }
        public string EvrakNot { get; set; }
        public string Evrak3SatirNot { get; set; }
        public string HareketGrubu { get; set; }

        public string SevkAdresiAd { get; set; }
        public string SevkAdresi1 { get; set; }
        public string SevkAdresi2 { get; set; }
        public string SevkAdresiPostaKodu { get; set; }
        public string SevkAdresiIlce { get; set; }
        public string SevkAdresiIl { get; set; }
        public string SevkAdresiTel { get; set; }
        public string SevkAdresiFaks { get; set; }

        public InvoiceStatus Durum { get; set; }

        public IEnumerable<InvoiceDetail> EvrakHareketleri { get; set; }

        public IEnumerable<RelatedDelivery> IlgiliIrsaliyeler { get; set; }
        public IEnumerable<RelatedOrder> IlgiliSiparisler { get; set; }

        public Settings GosterimSecenekleri { get; set; }

        public class InvoiceDetail
        {
            public int Id { get; set; }
            public int Sira { get; set; }
            public int UrunId { get; set; }
            public string UrunKod { get; set; }
            public string UrunBarkod { get; set; }
            public string UrunIsmi { get; set; }

            public decimal ToplamAdet { get; set; }
            public decimal KoliIciAdet { get; set; }
            public decimal KoliAdet { get { return Math.Floor(ToplamAdet / KoliIciAdet); } }
            public decimal PaletIciAdet { get; set; }
            public decimal PaletAdet { get { return Math.Floor(ToplamAdet / PaletIciAdet); } }
            public decimal UrunAgirligi { get; set; }
            public decimal ToplamAgirlik { get { return UrunAgirligi * ToplamAdet; } }
            public string AgirlikBirim { get; set; }

            public decimal BirimFiyat { get { return KDVHaricToplam / ToplamAdet; } }
            public decimal KDVHaricToplam { get; set; }
            public decimal Indirim1 { get; set; }
            public decimal Indirim2 { get; set; }
            public decimal Indirim3 { get; set; }
            public decimal Indirim4 { get; set; }
            public decimal Indirim5 { get; set; }
            public decimal IndirimTutari { get; set; }
            public decimal KDVHaricNet { get { return KDVHaricToplam - IndirimTutari; } }
            public int KDVYuzde { get; set; }
            public decimal KDVTutar { get; set; }
            public decimal KDVDahilTutar { get { return KDVHaricNet + KDVTutar; } }
            public bool BedelsizUrunMu { get { return KDVHaricNet == 0; } }
        }

        public class RelatedDelivery
        {
            public int Id { get; set; }
            public string EvrakNo { get; set; }
            public DateTime Tarih { get; set; }
        }

        public class RelatedOrder
        {
            public int Id { get; set; }
            public string EvrakNo { get; set; }
            public DateTime Tarih { get; set; }
        }

        public enum InvoiceStatus
        {
            ACIK = 0,
            FATURALASMIS = 1,
            IPTAL = 2
        }

        public class Settings
        {
            public bool EvrakNotuGosterilsinMi { get; set; }
        }
    }

    #endregion

    #region Mutabakat
    public class ReconsilationModel
    {
        public Reconsilation UnConfirmedReconsilations { get; set; }

        public IList<Reconsilation> Reconsilations { get; set; }

        public class Reconsilation
        {
            public int? ReconsilationId { get; set; }
            public DateTime StartDate { get; set; }
            public decimal? TotalAmount { get; set; }
            public int BillAmount { get; set; }
            public DateTime EndDate { get; set; }
        }
    }

    public class ReconsilationDetailsModel
    {
        public bool Reconsilate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public IEnumerable<Invoice> Invoices { get; set; }
        public class Invoice
        {
            public int Id { get; set; }
            public DateTime Tarih { get; set; }
            public string HareketTipi { get; set; }
            public string EvrakNo { get; set; }
            public InvoiceStatus Durum { get; set; }
            public DateTime VadeTarihi { get; set; }
            public string EvrakNot { get; set; }
            public decimal KDVHaricToplam { get; set; }
            public decimal ToplamIndirim { get; set; }
            public decimal KDVHaricNet { get { return KDVHaricToplam - ToplamIndirim; } }
            public string HareketGrubu { get; set; }
            public string SevkAdresi { get; set; }


        }

        public enum InvoiceStatus
        {
            ACIK = 0,
            IPTAL = 2
        }
        public class Reconsilation
        {
            public int? ReconsilationId { get; set; }
            public DateTime StartDate { get; set; }
            public decimal? TotalAmount { get; set; }
            public int BillAmount { get; set; }
            public DateTime EndDate { get; set; }
        }
    }



    #endregion

   

}