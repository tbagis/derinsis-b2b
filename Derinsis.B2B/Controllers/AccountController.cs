﻿namespace DerinSIS.B2B.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using System.Web.Security;
    using Helpers;
    using Infrastructure.Auth;
    using Emails;
    using Models;
    using ViewModels;

    public class AccountController : BaseController
    {
        private readonly Mailer _mailer;

        public AccountController(DbEntities db, Mailer mailer)
            : base(db)
        {
            _mailer = mailer;
        }

        #region LogOn & LogOut

        public ActionResult LogOn()
        {

            var anaSayfaAd = "";
            var siteAdi = Db.drn2ayar.FirstOrDefault(x => x.ayarID == 762);
            if (siteAdi != null)
            {
                if (siteAdi.ayarDeger != "")
                {
                    anaSayfaAd = siteAdi.ayarDeger;
                }
            }
            ViewBag.logName = anaSayfaAd;
            return View();
        }

        [HttpPost]
        public ActionResult LogOn(LogOnModel model, string returnUrl)
        {
            var anaSayfaAd = "";
            var siteAdi = Db.drn2ayar.FirstOrDefault(x => x.ayarID == 762);
            if (siteAdi != null)
            {
                if (siteAdi.ayarDeger != "")
                {
                    anaSayfaAd = siteAdi.ayarDeger;
                }
            }
            ViewBag.logName = anaSayfaAd;


            var email = model.Email.ToLowerInvariant();

            var sonEk = Db.drn2ayar.FirstOrDefault(x => x.ayarID == 777);
            if (sonEk != null)
            {
                if (sonEk.ayarDeger != "")
                {
                    if (email.IndexOf('@') < 0)
                    {
                        model.Email = model.Email + sonEk.ayarDeger;
                        email = model.Email.ToLowerInvariant();
                    }
                }
            }

            if (!model.sifre_degistir_input)
            {
                model.errorClass = "error";
                if (!ModelState.IsValid)
                {
                    ShowErrorMessage("Hatalı kullanıcı adı ve/veya şifre");
                    return View(model);
                }
            }
            
            var users = Db.frmYetkili.Where(x => x.ytkEposta.ToLower() == email);
            if (users.Count() == 0)
            {
                ModelState.AddModelError("Email", " ");
                ModelState.AddModelError("Password", " ");
                ShowErrorMessage(model.sifre_degistir_input ? "Hatalı kullanıcı adı" : "Hatalı kullanıcı adı ve/veya şifre");
                return View(model);
            }


            if (model.sifre_degistir_input)
            {
                var mailer = new Mailer();
                var user_ = users.First();
                var user_frm = Db.frm.Find(users.First().ytkFrm);
                var sonuc = mailer.SendResetPasswordMail(new ResetPasswordEmailModel
                {
                    KullaniciIsmi = user_.ytkAd,
                    FirmaAdi = user_frm.frmAd,
                    FirmaKodu = user_frm.frmKod,
                    Eposta = model.Email,
                    ResetUrl = ""
                });
                if (sonuc == false)
                {
                    model.errorClass = "error";
                    ShowErrorMessage("Gönderim başarısız!");
                }
                else
                {
                    ShowErrorMessage("Şifre sıfırlama isteğiniz alındı!");
                }
               
                return View();
            }


            bool sifreDogrumu = false;
            if (model.ytkFrm == null)
            {
                foreach (var us in users)
                {
                    if (us.CheckPassword(model.Password))
                    {
                        sifreDogrumu = true;
                        break;
                    }
                }
            }
            else
            {
                sifreDogrumu = true;
            }

            if (sifreDogrumu == false)
            {
                ModelState.AddModelError("Email", " ");
                ModelState.AddModelError("Password", " ");
                ShowErrorMessage("Hatalı kullanıcı adı ve/veya şifre");
                return View(model);
            }

            model.errorClass = "";
            model.ytkSay = 0;

            frmYetkili user = null;

            if (users.Count() > 1)
            {
                var uygunUser = (from us in users
                                 where us.ytkB2B != 0 && us.ytkB2B != 1
                                 where (us.ytkB2bFaturaYetkisi > 0 || us.ytkB2bMutabakatYetkisi > 0 || us.ytkB2bSevkiyatYetkisi > 0 || us.ytkB2bSiparisYetkisi > 0)
                                 where us.frm.frmDurum == 0
                                 where (us.frm.frmTip == 1 || us.frm.frmTip == 0)
                                 where (model.ytkFrm == us.ytkFrm || model.ytkFrm == null)
                                 select new { us });

                if (uygunUser.Count() == 0)
                {
                    user = users.FirstOrDefault();
                }
                else if (uygunUser.Count() == 1)
                {
                    user = uygunUser.FirstOrDefault().us;
                }
                else
                {
                    if (model.ytkFrm == null)
                    {
                        model.ytkSay = uygunUser.Select(x => x.us.frm.frmID).Distinct().Count();
                        ModelState.AddModelError("Email", " ");
                        ModelState.AddModelError("Password", " ");
                        ShowErrorMessage("Hesap seçiniz!");
                        return View(model);
                    }
                    else
                    {
                        user = users.FirstOrDefault(x => x.ytkFrm == model.ytkFrm);
                    }
                }
            }
            else
            {
                user = users.FirstOrDefault();
            }

            model.errorClass = "error";

            if (user.ytkB2B == 0 || user.ytkB2B == 1)
            {
                ModelState.AddModelError("Email", " ");
                ModelState.AddModelError("Password", " ");
                ShowErrorMessage("B2B Sistemine Giriş Yetkiniz Bulunmamaktadır.");
                return View(model);
            }

            if (user.ytkB2bFaturaYetkisi == 0 && user.ytkB2bMutabakatYetkisi == 0 && user.ytkB2bSevkiyatYetkisi == 0 && user.ytkB2bSiparisYetkisi == 0)
            {
                ModelState.AddModelError("Email", " ");
                ModelState.AddModelError("Password", " ");
                ShowErrorMessage("Yetkileriniz belirlenmemiştir. Lütfen firma yetkiliniz ile iletişime geçiniz.");
                return View(model);
            }

            var frmTip = user.frm.frmTip;

            var b2bEtkinlikTipi = b2bEtkinlikTipiGetir();
            //drnayr oku ona göre engelle

            if (b2bEtkinlikTipi == 1)//Satış yönlü etkinlik
            {
                if (frmTip != 1 /* Customers */)
                {
                    ModelState.AddModelError("Email", " ");
                    ModelState.AddModelError("Password", " ");
                    ShowErrorMessage("B2B Sistemi Sadece Müşteriler içindir.");
                    return View(model);
                }
            }
            else if (b2bEtkinlikTipi == 2)//Alış yönlü etkinlik
            {
                if (frmTip != 0 /* Suppliers */ )
                {
                    ModelState.AddModelError("Email", " ");
                    ModelState.AddModelError("Password", " ");
                    ShowErrorMessage("B2B Sistemi Sadece Ürün Sağlayıcılar içindir.");
                    return View(model);
                }
            }
            else
            {
                if (frmTip != 0 /* Suppliers */ && frmTip != 1 /* Customers */)
                {
                    ModelState.AddModelError("Email", " ");
                    ModelState.AddModelError("Password", " ");
                    ShowErrorMessage("B2B Sistemi Sadece Müşteriler ve Ürün Sağlayıcılar içindir.");
                    return View(model);
                }
            }

            var frmDrm = user.frm.frmDurum;
            if (frmDrm != 0)
            {
                ModelState.AddModelError("Email", " ");
                ModelState.AddModelError("Password", " ");
                ShowErrorMessage("B2B sistemine giriş için, firmanız etkin değildir.");
                return View(model);
            }

            // Everything is OK. Log user in
            FormsAuthentication.SetAuthCookie(user.ytkID.ToString(), model.RememberMe);

            // User now logged in; here we must ensure that the user has at least one cart in db
            // otherwise we need to crete one for him.
            var hasCartDefined = Db.b2bSepet.Any(x => x.frmYetkili.ytkID == user.ytkID);
            if (!hasCartDefined)
            {
                var firstCartForUser = new b2bSepet { Ad = "Sepetim", frmYetkili = user, Aktif = true, Aciklama = "" };
                Db.b2bSepet.Add(firstCartForUser);
                Db.SaveChanges();
            }

            if (CheckSafeRedirectUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return RedirectToAction("Index", "Home");
        }

        private void SendResetPasswordMail()
        {
            throw new NotImplementedException();
        }

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult ytkFrmDoldur(string Email)
        {
            var uygunUser = (from us in Db.frmYetkili
                             where us.ytkEposta == Email
                             where us.ytkB2B != 0 && us.ytkB2B != 1
                             where (us.ytkB2bFaturaYetkisi > 0 || us.ytkB2bMutabakatYetkisi > 0 || us.ytkB2bSevkiyatYetkisi > 0 || us.ytkB2bSiparisYetkisi > 0)
                             where us.frm.frmDurum == 0
                             where (us.frm.frmTip == 1 || us.frm.frmTip == 0)
                             select new DropDownListItem()
                             {
                                 Text = us.frm.frmAd,
                                 Value = us.frm.frmID
                             });


            return Json(uygunUser.Distinct().OrderBy(x => x.Text));
        }



        #endregion

        #region Profile

        [B2BAuthorize(AccountType.Supplier | AccountType.Customer)]
        public ActionResult MyProfile()
        {
            var model = new MyProfileModel
            {
                YetkiliIsmi = CurrentAccount.ytkAd,
                YetkiliEPosta = CurrentAccount.ytkEposta,

                FirmaIsmi = CurrentCompany.frmAd,

                FirmaAdresDetay = string.Join(" ", CurrentCompany.adres1, CurrentCompany.adres2),
                FirmaAdresIlce = Db.frmKentIlce.Single(x => x.ilceKodu == CurrentCompany.ilceKod).ilceAdi,
                FirmaAdresSehir = CurrentCompany.frmKent.ilAd,
                FirmaAdresPostaKodu = CurrentCompany.postaKod,
                FirmaAdresUlke = Db.frmUlke.Single(x => x.ulkeID == CurrentCompany.ulke).ulkeAd,

                FirmaTel1 = CurrentCompany.tel1,
                FirmaTel2 = CurrentCompany.tel2,
                FirmaTelGsm = CurrentCompany.telMobil,
                FirmaTelFax = CurrentCompany.telFaks,

                FirmaVergiDairesi = CurrentCompany.VD,
                FirmaVergiNo = CurrentCompany.VN,
            };

            return View(model);
        }

        [ChildActionOnly]
        public ActionResult CurrentCompanyInfo()
        {
            ViewBag.CompanyName = string.Join(" ", new[] { CurrentAccount.frm.frmAd, CurrentAccount.frm.frmAd2 });
            ViewBag.CompanyCode = CurrentAccount.frm.frmKod;
            ViewBag.ContactName = CurrentAccount.ytkAd;
            ViewBag.ContactEmail = CurrentAccount.ytkEposta;
            ViewBag.Efatura = CurrentAccount.frm.etkinEFatura;
            return PartialView();
        }

        #endregion

        #region Change Password

        [B2BAuthorize(AccountType.Supplier | AccountType.Customer)]
        public ActionResult ChangePassword()
        {
            return View();
        }

        [B2BAuthorize(AccountType.Supplier | AccountType.Customer)]
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (!ModelState.IsValid)
            {
                ShowErrorMessage();
                return View();
            }

            // validate old password
            if (!CurrentAccount.CheckPassword(model.OldPassword))
            {
                ModelState.AddModelError("OldPassword", "Eski şifreniz hatalı.");
                ShowErrorMessage();
                return View();
            }

            // set new password and save
            CurrentAccount.SetPassword(model.NewPassword);
            Db.SaveChanges();

            ShowSuccessMessage("Şifreniz başarıyla güncellenmiştir.");

            return RedirectToAction("MyProfile");
        }

        #endregion

        #region Recover Password

        public ActionResult RecoverPasswordComplete(string id)
        {
            var hash = new Guid(id).ToString();
            //var resetlemeTalebi = Db.b2bSifreResetlemeTalep.SingleOrDefault(x => x.Hash == hash);
            //Check.NotNullOr404(resetlemeTalebi);
            //Check.CheckOr404(resetlemeTalebi.talepDrm == 0);
            //Check.CheckOr404(resetlemeTalebi.gTarih > DateTime.Now.AddDays(-3));

            return View();
        }

        [HttpPost]
        public ActionResult RecoverPasswordComplete(string id, ResetPasswordCompleteModel model)
        {
            var hash = new Guid(id).ToString();
            //var resetlemeTalebi = Db.b2bSifreResetlemeTalep.SingleOrDefault(x => x.Hash == hash);
            //Check.NotNullOr404(resetlemeTalebi);
            //Check.CheckOr404(resetlemeTalebi.talepDrm == 0);
            //Check.CheckOr404(resetlemeTalebi.gTarih > DateTime.Now.AddDays(-3));

            //if(!ModelState.IsValid)
            //{
            //    ShowErrorMessage();
            //    return View();
            //}

            //var yetkili = resetlemeTalebi.frmYetkili;
            //yetkili.SetPassword(model.NewPassword);
            Db.SaveChanges();

            return RedirectToAction("RecoverPasswordDone");
        }

        public ActionResult RecoverPasswordDone()
        {
            return View();
        }

        #endregion

        #region Error Pages

        public ActionResult Unauthorized()
        {
            return View();
        }
        #endregion

        #region Utility Functions

        private bool CheckSafeRedirectUrl(string url)
        {
            return Url.IsLocalUrl(url) &&
                   url.Length > 1 &&
                   url.StartsWith("/") &&
                   !url.StartsWith("//") &&
                   !url.StartsWith("/\\");
        }

        private int b2bEtkinlikTipiGetir()
        {
            var ayrDeger = Db.drn2ayar.FirstOrDefault(x => x.ayarID == 772);
            if (ayrDeger != null)
            {
                return Convert.ToInt32(ayrDeger.ayarDeger);
            }
            return 0;
        }

        #endregion
    }
}