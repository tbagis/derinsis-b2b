﻿namespace DerinSIS.B2B.Controllers
{
    using System;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using Helpers;
    using Infrastructure.Auth;
    using Models;
    using ViewModels;
    using System.Net;

    [B2BAuthorize(AccountType.Supplier | AccountType.Customer)]
    public class ThumbnailController : BaseController
    {
        public ThumbnailController(DbEntities db)
            : base(db)
        {
        }

        #region Slide Shows

        [ChildActionOnly]
        public ActionResult ProductSlideShow(int id, int bagliOlduguGrupID = 0)
        {
            var modelfolderName = string.Format("G{0}", bagliOlduguGrupID);
            var modelfolderPath = Path.Combine(ThumbRoot, modelfolderName);
            var modelfolderProductName = string.Format("V{0}", id);

            var model = new ProductSlideShowModel();

            if (bagliOlduguGrupID > 0)
            {
                if (Directory.Exists(modelfolderPath) && Directory.EnumerateFiles(modelfolderPath).Any())
                {
                    model.Images = Directory.EnumerateFiles(modelfolderPath)
                        .Where(x => x.Contains(modelfolderProductName))
                        .Select(Path.GetFileName)
                        .Select(x => Path.Combine(modelfolderName, x))
                        .ToList();
                }
            }

            if (model.Images.Count == 0)
            {
                var folderName = string.Format("U{0}", id);
                var folderPath = Path.Combine(ThumbRoot, folderName);

                if (Directory.Exists(folderPath) && Directory.EnumerateFiles(folderPath).Any())
                {
                    model.Images = Directory.EnumerateFiles(folderPath)
                       .Select(Path.GetFileName)
                       .Select(x => Path.Combine(folderName, x))
                       .ToList();
                }
            }

            return PartialView(model);
        }

        [ChildActionOnly]
        public ActionResult ProductGroupSlideShow(int id)
        {
            var folderName = string.Format("G{0}", id);
            var folderPath = Path.Combine(ThumbRoot, folderName);

            var model = new ProductGroupSlideShowModel();
            if (Directory.Exists(folderPath) && Directory.EnumerateFiles(folderPath).Any())
            {
                model.Images = Directory.EnumerateFiles(folderPath)
                    .Select(Path.GetFileName)
                    .Select(x => Path.Combine(folderName, x))
                    .ToList();
            }

            return PartialView(model);
        }

        #endregion

        #region Thubmanil Image Generators

        public ActionResult ProductImage(int id, int width, int height, int bagliOlduguModelID = 0)
        {
            if (bagliOlduguModelID > 0)
            {
                var ModelklasorAdi = string.Format("G{0}", bagliOlduguModelID);
                var ModeldosyaAdi = string.Format("V{0}_1.jpg", id);

                var modelName = Path.Combine(ModelklasorAdi, ModeldosyaAdi);
                Check.NotNullOr404(modelName);

                var path = Path.Combine(ThumbRoot, modelName);
                if (System.IO.File.Exists(path))
                {
                    return Thumnail(path, width, height);
                }
                else
                {

                    var docName = string.Format("V{0}", id);
                    var folderPath = Path.Combine(ThumbRoot, ModelklasorAdi);

                    var modeli = new ProductSlideShowModel();
                    if (Directory.Exists(folderPath) && Directory.EnumerateFiles(folderPath).Any())
                    {
                        modeli.Images = Directory.EnumerateFiles(folderPath)
                            .Where(y => y.Contains(docName))
                            .Select(Path.GetFileName)
                            .Select(x => Path.Combine(ModelklasorAdi, x))
                            .ToList();

                    }

                    if (modeli.Images.Count() > 0)
                    {
                        var ilkModelName = modeli.Images.FirstOrDefault();
                        var ilkPath = Path.Combine(ThumbRoot, ilkModelName);
                        if (System.IO.File.Exists(ilkPath))
                        {
                            return Thumnail(ilkPath, width, height);
                        }
                    }

                }
            }

            var klasorAdi = string.Format("U{0}", id);
            var dosyaAdi = string.Format("U{0}_1.jpg", id);

            var urnPath = Path.Combine(klasorAdi, dosyaAdi);
            if (System.IO.File.Exists(urnPath))
            {
                return Thumnail(urnPath, width, height);
            }
            else
            {
                var urnIlkdocName = string.Format("U{0}", id);
                var folderUrnIlkPath = Path.Combine(ThumbRoot, klasorAdi);
                var modeli = new ProductSlideShowModel();
                if (Directory.Exists(folderUrnIlkPath) && Directory.EnumerateFiles(folderUrnIlkPath).Any())
                {
                    modeli.Images = Directory.EnumerateFiles(folderUrnIlkPath)
                        .Where(y => y.Contains(urnIlkdocName))
                        .Select(Path.GetFileName)
                        .Select(x => Path.Combine(klasorAdi, x))
                        .ToList();

                }

                if (modeli.Images.Count() > 0)
                {
                    var ilkModelName = modeli.Images.FirstOrDefault();
                    var ilkPath = Path.Combine(ThumbRoot, ilkModelName);
                    if (System.IO.File.Exists(ilkPath))
                    {
                        return Thumnail(ilkPath, width, height);
                    }
                }
            }

            return FitThumb(Path.Combine(klasorAdi, dosyaAdi), width, height);
        }



        public ActionResult ModelImage(int id, int width, int height,bool yoksaUrunden=false)
        {
            var klasorAdi = string.Format("G{0}", id);
            var dosyaAdi = string.Format("G{0}-1.jpg", id);

            var path = Path.Combine(klasorAdi, dosyaAdi);
            if (System.IO.File.Exists(path))
            {
                return Thumnail(path, width, height);
            }
            else
            {
                var docName = string.Format("G{0}-", id);
                var folderPath = Path.Combine(ThumbRoot, klasorAdi);
                
                var modelIlkRsm = new ProductSlideShowModel();
                if (Directory.Exists(folderPath) && Directory.EnumerateFiles(folderPath).Any())
                {
                    modelIlkRsm.Images = Directory.EnumerateFiles(folderPath)
                        .Where(y => y.Contains(docName))
                        .Select(Path.GetFileName)
                        .Select(x => Path.Combine(klasorAdi, x))
                        .ToList();

                }


                if (modelIlkRsm.Images.Count() > 0)
                {
                    var ilkModelName = modelIlkRsm.Images.FirstOrDefault();
                    var ilkPath = Path.Combine(ThumbRoot, ilkModelName);
                    if (System.IO.File.Exists(ilkPath))
                    {
                        return Thumnail(ilkPath, width, height);
                    }
                }
                else
                {
                    if (yoksaUrunden == true)
                    {
                        var modelIlkUrn = new ProductSlideShowModel();
                        if (Directory.Exists(folderPath) && Directory.EnumerateFiles(folderPath).Any())
                        {
                            modelIlkUrn.Images = Directory.EnumerateFiles(folderPath)
                                .Where(y => y.Contains("V"))
                                .Select(Path.GetFileName)
                                .Select(x => Path.Combine(klasorAdi, x))
                                .ToList();

                        }

                        if (modelIlkUrn.Images.Count() > 0)
                        {
                            var ilkModelName = modelIlkUrn.Images.FirstOrDefault();
                            var ilkPath = Path.Combine(ThumbRoot, ilkModelName);
                            if (System.IO.File.Exists(ilkPath))
                            {
                                return Thumnail(ilkPath, width, height);
                            }
                        }
                    }
                }

            }

            return FitThumb(Path.Combine(klasorAdi, dosyaAdi), width, height);
        }

        public ActionResult PlaceHolderThumb(int width, int height)
        {
            return Thumnail(PlaceHolderImagePath(), width, height);
        }

        public ActionResult FitThumb(string name, int width, int height)
        {
            Check.NotNullOr404(name);
            var path = Path.Combine(ThumbRoot, name);

            if (!System.IO.File.Exists(path))
            {
                return Thumnail(PlaceHolderImagePath(), width, height);
            }

            return Thumnail(path, width, height);
        }

        #endregion

        #region Utility Functions

        private ActionResult Thumnail(string path, int width, int height)
        {
            using (var original = Image.FromFile(path))
            using (var resizedImage = ResizeImage(original, width, height))
            using (var stream = new MemoryStream())
            {
                resizedImage.Save(stream, ImageFormat.Png);
                return File(stream.ToArray(), "image/png");
            }
        }

        private Image ResizeImage(Image original, int width, int height)
        {
            var newImage = new Bitmap(width, height);

            var widthRatio = (double)width / original.Width;
            var heightRatio = (double)height / original.Height;
            var aspectRatio = Math.Min(widthRatio, heightRatio);

            var scaledWidth = (int)(original.Width * aspectRatio);
            var scaledHeight = (int)(original.Height * aspectRatio);

            var scaledXOffset = (width - scaledWidth) / 2;
            var scaledYOffset = (height - scaledHeight) / 2;

            using (var graphics = Graphics.FromImage(newImage))
            {
                graphics.SmoothingMode = SmoothingMode.AntiAlias;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
                graphics.DrawImage(original, new Rectangle(scaledXOffset, scaledYOffset, scaledWidth, scaledHeight));
            }

            return newImage;
        }

        private string PlaceHolderImagePath()
        {
            return Server.MapPath("~/Assets/theme/img/img-placeholder.png");
        }

        private string ThumbRoot
        {
            get
            {
                return Db.drn2ayar
                    .Single(x => x.ayarID == 410)
                    .ayarDeger;
            }
        }

      

        #endregion
    }
}