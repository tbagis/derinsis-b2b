﻿namespace DerinSIS.B2B.Controllers
{
    using System.Web.Mvc;
    using Infrastructure.Auth;
    using Models;
    using System;
    using ViewModels;
    using System.Linq;
    using Helpers;
    using Helpers.Html;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Web.Script.Serialization;
    using Helpers;
    using System.Net;
    using System.Data;
    using System.Data.SqlClient;
    using System.IO;
    using System.Web.UI;
    using System.Text;
    using iTextSharp.text.html.simpleparser;
    using iTextSharp.text.pdf;
    using iTextSharp.text;
    using System.Web;
    using System.Drawing;

    [B2BAuthorize(AccountType.Supplier, Privilege.SIPARIS)]
    public class SupplierController : BaseController
    {
        public SupplierController(DbEntities db) : base(db)
        {
        }

        #region Product List

        public ActionResult Index(string q, int? categoryId, string brandId, int? producerId, int? page, string kmp, string price, string priceLabel, string ynUrn, string stogaYeniGirenUrn, string oneCikanUrn, string ordercondition)
        {
            var model = new ProductListModel();

            page = page ?? 1;
            var pageLength = 28;

            short _brandID = -1;
            var _brandGrupID = 0;
            if (brandId != null && brandId != "")
            {
                if (brandId.IndexOf('-') > -1)
                {
                    _brandID = Convert.ToInt16(brandId.Split('-')[0]);
                    _brandGrupID = Convert.ToInt32(brandId.Split('-')[1]);
                }
                else
                {
                    _brandID = Convert.ToInt16(brandId);
                }
            }

            if (!String.IsNullOrWhiteSpace(ordercondition))
                model.siralama = ordercondition;

            var yeniUrunListesi = Db.urnYeniler_vw.Select(x => x.stkID).ToList();
            if (yeniUrunListesi.Count() == 0)
            {
                model.UrunYeniler = -1;
            }
            else
            {
                model.UrunYeniler = (ynUrn == "on") ? 1 : 0;
            }

            var stogaYeniGirenUrnListesi = Db.urnStogaYeniGirenler_vw.Select(x => x.stkID).ToList();
            if (stogaYeniGirenUrnListesi.Count() == 0)
            {
                model.UrunStogaYeniGirenler = -1;
            }
            else
            {
                model.UrunStogaYeniGirenler = (stogaYeniGirenUrn == "on") ? 1 : 0;
            }

            var OneCikanUrnListesi = Db.urnOneCikanlar_vw.Select(x => x.stkID).ToList();
            if (OneCikanUrnListesi.Count() == 0)
            {
                model.UrunOneCikanlar = -1;
            }
            else
            {
                model.UrunOneCikanlar = (oneCikanUrn == "on") ? 1 : 0;
            }

            model.OzelFiyat = kmp == "on" ? true : false;


            string sorguEkKosul = " where 1=1 ";
            if (!String.IsNullOrWhiteSpace(q))
            {
                model.Sorgu = q;
                if (isNumeric(q) && q.Length >= 8)
                {
                    sorguEkKosul += " and alis.stkID in (select (case when urnBrkdAltStkId>0 then urnBrkdAltStkId else urnBrkdStkID end) as urnBrkdStkID from urnBrkd where urnBarkod like '%" + q + "%') ";
                }
                else
                {
                    var qstr = q.Split(' ').Select(x => x.ToLower()).ToArray();
                    if (qstr.Count() > 0)
                    {
                        sorguEkKosul += " and (alis.stkKod like '%" + q + "%' or alis.mrkAd like '%" + q + "%' or ( ";
                        for (int i = 0; i < qstr.Count(); i++)
                        {
                            if (i + 1 == qstr.Count())
                            {
                                sorguEkKosul += " alis.stkAd like '%" + qstr[i] + "%' ";
                            }
                            else
                            {
                                sorguEkKosul += " alis.stkAd like '%" + qstr[i] + "%' and ";
                            }
                        }
                        sorguEkKosul += " )) ";
                    }
                }
            }


            /* Kategori Koşul Filter */
            if (categoryId != null)
            {
                string sorgu = @" select * from b2b.fn_urnAlisKategori_Vw(" + CurrentAccount.frm.frmID + ") where kID=" + categoryId + " ";
                var kategori = Db.Database.SqlQuery<Categories>(sorgu).ToList().Single();

                model.KategoriIdFiltresi = kategori.kID;
                model.KategoriFiltreEtiketi = kategori.kAd;

                sorguEkKosul += " and urnKtgrID=" + kategori.k0 + " ";

                if (kategori.k1 != 0)
                {
                    sorguEkKosul += " and urnKtgrID1=" + kategori.k1 + " ";
                }

                if (kategori.k2 != 0)
                {
                    sorguEkKosul += " and urnKtgrID2=" + kategori.k2 + " ";
                }

                if (kategori.k3 != 0)
                {
                    sorguEkKosul += " and urnKtgrID3=" + kategori.k3 + " ";
                }
            }


            /* Üretici Koşul Filter */
            if (producerId != null)
            {
                var uretici = Db.urnMrkUrt.Single(x => x.urtID == producerId);
                model.UreticiIdFiltresi = uretici.urtID;
                model.UreticiFiltreEtiketi = uretici.urtAd;

                sorguEkKosul += " and urtID=" + uretici.urtID + " ";
            }

            /* Marka Koşul Filter */
            if (_brandID != -1)
            {
                if (_brandGrupID == 1)
                {
                    var markaGrup = Db.urnMrkGrp.Single(x => x.urnMrkGrpID == _brandID);
                    model.MarkaIdFiltresi = brandId;
                    model.MarkaFiltreEtiketi = markaGrup.urnMrkGrpAd;

                    sorguEkKosul += " and urnMrkGrpID=" + markaGrup.urnMrkGrpID + " ";
                }
                else
                {
                    var marka = Db.urnMrk.Single(x => x.mrkID == _brandID);
                    model.MarkaIdFiltresi = brandId;
                    model.MarkaFiltreEtiketi = marka.mrkAd;

                    sorguEkKosul += " and urnMrkID=" + marka.mrkID + " ";
                }
            }

            if (price != null && price != "")
            {
                var esik = price.Split('-');
                int min = int.Parse(esik[0]);
                int max = esik[1] != "" ? int.Parse(esik[1]) : 99999999;
                model.FiyatFiltresi = price;
                model.FiyatFiltreEtiketi = priceLabel;

                sorguEkKosul += " and ((case when odemeKur>0 then odemeKur*fiyatA else fiyatA end)>=" + min + " and (case when odemeKur>0 then odemeKur*fiyatA else fiyatA end)<=" + max + ") ";
            }

            if (model.UrunYeniler > 0)
            {
                sorguEkKosul += " and alis.stkID in (select stkID from b2b.urnYeniler_vw) ";
            }

            if (model.UrunStogaYeniGirenler > 0)
            {
                sorguEkKosul += " and alis.stkID in (select stkID from b2b.urnStogaYeniGirenler_vw) ";
            }

            if (model.UrunOneCikanlar > 0)
            {
                sorguEkKosul += " and alis.stkID in (select stkID from b2b.urnOneCikanlar_vw) ";
            }

            string orderKosul = " order by (case when urnGrpID = 0 then rtb else 0 end),(case when urnGrpID = 0 then stkAd else grpAd end),stkID ";

            var stkGosterimiVar = StkGosterimiVar;
            var brutFiyatGosterimiVar = BrutFiyatGostermiVar;
            model.StokGosterimiVar = stkGosterimiVar;

            if (!String.IsNullOrWhiteSpace(ordercondition))
            {
                switch (ordercondition)
                {
                    case "0-0":
                        orderKosul = " order by (case when urnGrpID = 0 then stkKod else '' end),(case when urnGrpID = 0 then stkAd else grpAd end),stkID ";
                        break;
                    case "0-1":
                        orderKosul = " order by (case when urnGrpID = 0 then stkKod else '' end) desc,(case when urnGrpID = 0 then stkAd else grpAd end),stkID ";
                        break;
                    case "1-0":
                        orderKosul = " order by (case when urnGrpID = 0 then stkAd else grpAd end),stkID ";
                        break;
                    case "1-1":
                        orderKosul = " order by (case when urnGrpID = 0 then stkAd else grpAd end) desc,stkID ";
                        break;
                    case "2-0":
                        orderKosul = " order by (case when urnGrpID = 0 then barkod else '' end),(case when urnGrpID = 0 then stkAd else grpAd end),stkID ";
                        break;
                    case "2-1":
                        orderKosul = " order by (case when urnGrpID = 0 then barkod else '' end) desc,(case when urnGrpID = 0 then stkAd else grpAd end),stkID ";
                        break;
                    case "3-0":
                        orderKosul = " order by (case when urnGrpID = 0 then urnKoli else 1 end),(case when urnGrpID = 0 then stkAd else grpAd end),stkID ";
                        break;
                    case "3-1":
                        orderKosul = " order by (case when urnGrpID = 0 then urnKoli else 1 end) desc,(case when urnGrpID = 0 then stkAd else grpAd end),stkID ";
                        break;
                    case "4-0":
                        orderKosul = " order by (case when urnGrpID = 0 then urnKutu else -1 end),(case when urnGrpID = 0 then stkAd else grpAd end),stkID ";
                        break;
                    case "4-1":
                        orderKosul = " order by (case when urnGrpID = 0 then urnKutu else -1 end) desc,(case when urnGrpID = 0 then stkAd else grpAd end),stkID ";
                        break;
                    case "5-0":
                        orderKosul = " order by (case when urnGrpID = 0 then fiyatA  else 0 end),(case when urnGrpID = 0 then stkAd else grpAd end),stkID ";
                        //orderKosul = " order by (case when urnGrpID = 0 then (fiyatA + (fiyatA * urnKDV.kdvYuzde / 100.00)) else 0 end),(case when urnGrpID = 0 then stkAd else grpAd end),stkID ";
                        break;
                    case "5-1":
                        orderKosul = " order by (case when urnGrpID = 0 then fiyatA else 0 end) desc,(case when urnGrpID = 0 then stkAd else grpAd end),stkID ";
                        //orderKosul = " order by (case when urnGrpID = 0 then (fiyatA + (fiyatA * urnKDV.kdvYuzde / 100.00)) else 0 end) desc,(case when urnGrpID = 0 then stkAd else grpAd end),stkID ";
                        break;
                    case "6-0":
                        if (stkGosterimiVar > 0)
                        {
                            orderKosul = " order by (case when urnGrpID = 0 then gosterilenStok else '' end),(case when urnGrpID = 0 then stkAd else grpAd end),stkID ";
                        }
                        else
                        {
                            orderKosul = " order by (case when urnGrpID = 0 then stkAd else grpAd end),stkID ";
                        }
                        break;
                    case "6-1":
                        if (stkGosterimiVar > 0)
                        {
                            orderKosul = " order by (case when urnGrpID = 0 then gosterilenStok else '' end) desc,(case when urnGrpID = 0 then stkAd else grpAd end),stkID ";
                        }
                        else
                        {
                            orderKosul = " order by (case when urnGrpID = 0 then stkAd else grpAd end),stkID ";
                        }
                        break;
                    case "7-0":
                        orderKosul = " order by (case when urnGrpID = 0 then stkAd else grpAd end),stkID ";
                        break;
                    case "7-1":
                        orderKosul = " order by (case when urnGrpID = 0 then stkAd else grpAd end),stkID ";
                        break;
                }
            }



            string urunSorgu = @"  select  alis.stkID,alis.stkKod,alis.stkAd,alis.mrkAd,alis.grpAd,
            alis.urnKtgrID,alis.urnKtgrID1,alis.urnKtgrID2,alis.urnKtgrID3,
            alis.urtID,alis.urnMrkID,alis.urnGrpID,urnBarkod_vw.barkod,alis.rtb,
            alis.odemeKur,
			alis.fiyatA,
            alis.odemeKisaAd,
            alis.urnMrkGrpID,
			alis.urnKoli,
			alis.urnKutu,
			alis.stok,
			urnKdv.kdvYuzde as urnKdvYuzde,
			alis.gosterilenStok,COUNT(*) OVER() as topSay
	        from b2b.fn_urnAlis_vw(" + CurrentAccount.frm.frmID + @") as alis
	        inner join b2b.urnBarkod_vw as urnBarkod_vw on alis.stkID = urnBarkod_vw.urnBrkdStkID
	        left join urnKdv on kdvID=alis.KDVa 	   
	        " + sorguEkKosul + orderKosul + @" 
            OFFSET " + ((page ?? 1) - 1) + " * " + pageLength + @" ROWS 
            FETCH NEXT " + pageLength + " ROWS ONLY ";
            var query = Db.Database.SqlQuery<SupplierProductListQueryModel>(urunSorgu).ToList().AsQueryable();
            // Arama Sonuç Listesi
            // Sıralı bir şekilde Model ve/veya Ürünler içerir.


            var urunler = (from urn in query
                           where urn.urnGrpID == 0
                           select new ProductListModel.ProductModel
                           {
                               Id = urn.stkID,
                               Isim = urn.stkAd,
                               StokKodu = urn.stkKod,
                               Barkod = urn.barkod != null ? urn.barkod : "",
                               KDVOrani = urn.urnKdvYuzde,
                               KDVHaricFiyat = urn.fiyatA,
                               Indirim1 = 0,
                               Indirim2 = 0,
                               Indirim3 = 0,
                               Indirim4 = 0,
                               Birim = urn.odemeKisaAd,
                               Kur = urn.odemeKur,
                               Rutbe = urn.rtb,
                               urnKoli = urn.urnKoli,
                               urnKutu = urn.urnKutu,
                               stok = urn.stok,
                               stokKontrolu = 0,
                               gosterilenStok = urn.gosterilenStok.ToConvertibleDecimalStokFormat(),
                               stokGosterimi = stkGosterimiVar,
                               brutFiyatGosterimVar = brutFiyatGosterimiVar,
                               KucukResimUrl = Url.Action("ProductImage", "Thumbnail", new { id = urn.stkID, width = 55, height = 55 }),
                               BuyukResimUrl = Url.Action("ProductImage", "Thumbnail", new { id = urn.stkID, width = 185, height = 165 }),
                               BagliOlduguModelId = 0,
                               topSay = urn.topSay
                           })
              .ToList()
              .Cast<ProductListModel.ProductListItemModel>();


            var ayarR = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 771);
            bool grupResmiYoksaUrunden = (ayarR == null) ? false : byte.Parse(ayarR.ayarDeger) > 0;

            // Arama sonuçlarında bulunan modellerin listesi
            var modelurunIdleri = query.Where(x => x.urnGrpID != 0).Select(x => x.urnGrpID);
            var modeller = (from urnGrp in Db.urnGrp.AsEnumerable()
                            where modelurunIdleri.Contains(urnGrp.grpID)
                            let indirim = Db.sakMrk_vw
                                .Where(x => x.skFrmID == CurrentCompany.frmID)
                                .Where(x => x.skTur == CurrentCompany.frmVadeTur)
                                .FirstOrDefault(x => x.skMrkID == urnGrp.grpMrkID)
                            select new ProductListModel.ProductGroupModel
                            {
                                Id = urnGrp.grpID,
                                Isim = urnGrp.grpAd,
                                Indirim1 = 0,
                                Indirim2 = 0,
                                Indirim3 = 0,
                                stokGosterimi = stkGosterimiVar,
                                gosterilenStok = "",
                                KucukResimUrl = Url.Action("ModelImage", "Thumbnail", new { id = urnGrp.grpID, width = 55, height = 55, yoksaUrunden = grupResmiYoksaUrunden }),
                                BuyukResimUrl = Url.Action("ModelImage", "Thumbnail", new { id = urnGrp.grpID, width = 185, height = 165, yoksaUrunden = grupResmiYoksaUrunden }),
                                Urunler = (from urn in query
                                           where urn.urnGrpID == urnGrp.grpID
                                           select new ProductListModel.ProductModel
                                           {
                                               Id = urn.stkID,
                                               Isim = urn.stkAd,
                                               StokKodu = urn.stkKod,
                                               Barkod = urn.barkod != null ? urn.barkod : "",
                                               KDVOrani = urn.urnKdvYuzde,
                                               KDVHaricFiyat = urn.fiyatA,
                                               Indirim1 = 0,
                                               Indirim2 = 0,
                                               Indirim3 = 0,
                                               Indirim4 = 0,
                                               BagliOlduguModelId = urnGrp.grpID,
                                               Birim = urn.odemeKisaAd,
                                               Kur = urn.odemeKur,
                                               Rutbe = urn.rtb,
                                               urnKoli = urn.urnKoli,
                                               urnKutu = urn.urnKutu,
                                               stok = urn.stok,
                                               stokKontrolu = 0,
                                               gosterilenStok = urn.gosterilenStok.ToConvertibleDecimalStokFormat(),
                                               stokGosterimi = stkGosterimiVar,
                                               brutFiyatGosterimVar = brutFiyatGosterimiVar,
                                               KucukResimUrl = Url.Action("ProductImage", "Thumbnail", new { id = urn.stkID, width = 55, height = 55, bagliOlduguModelID = urnGrp.grpID }),
                                               BuyukResimUrl = Url.Action("ProductImage", "Thumbnail", new { id = urn.stkID, width = 185, height = 165 }),
                                               topSay = urn.topSay
                                           })
                            }).ToList();



            // ürünleri ve modelleri, harmanlıyıp tekrar sıralıyoruz
            var itemList = urunler.Union(modeller);
            if (modelurunIdleri.Count() > 0)
            {
                itemList = itemList.OrderBy(x => x.Rutbe).ThenBy(x => x.Isim);
            }

            model.brutFiyatGosterimVar = brutFiyatGosterimiVar;
            model.AramaSonuclari = itemList.ToSqlPagedList(page ?? 1, pageLength, query.Count() > 0 ? query.FirstOrDefault().topSay : 0);
            model.Page = page ?? 1;
            model.PageLength = pageLength;

            GC.Collect();
            var ayar = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 765);
            ViewBag.KategoriYok = ayar == null ? false : byte.Parse(ayar.ayarDeger) > 0;

            return View(model);
        }

        private bool isNumeric(string s)
        {
            try
            {
                decimal a = decimal.Parse(s);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        public JsonResult AutoCompleteArama(string term)
        {

            var qstr = term.Split(' ').Select(x => x.ToLower()).ToArray();
            string ekSorgu = "";
            if (qstr.Length == 1)
            {
                ekSorgu = " stkAd like '%" + qstr[0] + "%' or barkod='" + qstr[0] + "' ";
            }
            else
            {
                if (qstr.Length > 0)
                {
                    ekSorgu += "(";
                }
                foreach (var str in qstr)
                {
                    ekSorgu += "stkAd like '%" + str + "%' or ";
                }
                if (ekSorgu.Length > 1)
                {
                    ekSorgu = ekSorgu.Substring(0, ekSorgu.Length - 3) + " )";
                }
            }

            string urunSorgu = @"  select alis.stkAd
	from b2b.fn_urnAlis_vw(" + CurrentAccount.frm.frmID + @") as alis
	inner join b2b.urnBarkod_vw as urnBarkod_vw on alis.stkID = urnBarkod_vw.urnBrkdStkID " + (ekSorgu != "" ? " where " + ekSorgu : "");

            var query = Db.Database.SqlQuery<SupplierProductListQueryModel>(urunSorgu).ToList().AsQueryable();

            var result = query.Where(x => qstr.All(y => x.stkAd.ToLower().Contains(y))).Select(x => new { VDad = x.stkAd }).Distinct().ToList();
            //var result2 = from s in Db.fn_urnAlis_Vw(CurrentAccount.frm.frmID)
            //               where s.Grade >= Grade
            //               select new
            //               {
            //                   s.Person,
            //                   s.Course.Title
            //               };

            //var result = Db.fn_urnAlis_Vw.(CurrentAccount.frm.frmID);
            //.Where(x =>
            //                qstr.All(y => x.stkAd.ToLower().Contains(y)))
            //                //|| x.stkKod.Contains(q) 
            //                //|| x.mrkAd.Contains(q))
            //                .Select(x => new { VDad = x.stkAd }).Distinct().ToList();



            //var result = (from fvd in Db.urnSatis_vw
            //              where fvd.stkAd.ToLower().Contains(term.ToLower())
            //              select new { VDad = fvd.stkAd }).Distinct().ToList();
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        #endregion

        #region Product Detail Pages

        public ActionResult ProductDetail(int? id, int? kmpid)
        {
            Check.NotNullOr404(id);

            List<int> urunler = new List<int>();
            string urunSorgu = String.Format(@"  select  distinct alis.stkID 
	from b2b.fn_urnAlis_vw(" + CurrentAccount.frm.frmID + @") as alis where stkID={0} ", id);

            int musteriMarkaKosulu = MusteriMarkalarinaAitUrunleriGorsun;
            if (musteriMarkaKosulu > 0)
            {
                urunSorgu = String.Format(@" select  distinct alis.stkID 
	from b2b.fn_urnAlis_vw(" + CurrentAccount.frm.frmID + @") as alis
	inner join sakMrk as skMarka on alis.urnMrkID = skMarka.skMrkID where skMarka.skFrmID=" + CurrentAccount.frm.frmID + " and stkID={0} ", id);
            }
            var donenList = Db.Database.SqlQuery<int>(urunSorgu).ToList().AsQueryable();

            if (donenList.Count() == 0)
            {
                Check.NotNullOr404(null);
            }

            var urn = Db.urn.FirstOrDefault(x => donenList.Contains(x.stkID));
            Check.NotNullOr404(urn);

            var urunKod1 = Db.urnKod1.SingleOrDefault(x => x.kod1ID == urn.kod1ID);
            var urunKod2 = Db.urnkod2.SingleOrDefault(x => x.kod2ID == urn.kod2ID);
            var urunKod3 = Db.urnkod3.SingleOrDefault(x => x.kod3ID == urn.kod3ID);
            var urunKod4 = Db.urnkod4.SingleOrDefault(x => x.kod4ID == urn.kod4ID);
            var urunKod5 = Db.urnkod5.SingleOrDefault(x => x.kod5ID == urn.kod5ID);

            var urunKod1Baslik = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 101);
            var urunKod2Baslik = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 102);
            var urunKod3Baslik = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 103);
            var urunKod4Baslik = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 104);
            var urunKod5Baslik = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 105);

            var urunKod1GosterilsinMi = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 751);
            var urunKod2GosterilsinMi = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 752);
            var urunKod3GosterilsinMi = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 753);
            var urunKod4GosterilsinMi = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 754);
            var urunKod5GosterilsinMi = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 755);

            //var kdvHaricSatisFiyati= Db.urnSatis_vw.Single(x => x.stkID == id).fiyatS;

            var indirim1 = 0m;
            var indirim2 = 0m;
            var indirim3 = 0m;
            var indirim4 = 0m;

            // Ürün kategorisini urnSatis_vw'den sorgulamalıyız

            string urunVWSorgu = @"  select  alis.stkID,alis.stkKod,alis.stkAd,alis.mrkAd,alis.grpAd,
            alis.urnKtgrID,alis.urnKtgrID1,alis.urnKtgrID2,alis.urnKtgrID3,
            alis.urtID,alis.urnMrkID,alis.urnGrpID,urnBarkod_vw.barkod,alis.rtb,
            alis.odemeKur,
			alis.fiyatA,
            alis.odemeKisaAd,
            alis.urnMrkGrpID,
			alis.urnKoli,
			alis.urnKutu,
			alis.stok,
			urnKdv.kdvYuzde as urnKdvYuzde,
			alis.gosterilenStok 
	from b2b.fn_urnAlis_vw(" + CurrentAccount.frm.frmID + @") as alis
	inner join b2b.urnBarkod_vw as urnBarkod_vw on alis.stkID = urnBarkod_vw.urnBrkdStkID
	left join urnKdv on kdvID=alis.KDVa where alis.stkID=" + urn.stkID + " ";
            var urnVw = Db.Database.SqlQuery<SupplierProductListQueryModel>(urunVWSorgu).ToList().FirstOrDefault();

            //var urnVw = Db.urnSatis_vw.Single(x => x.stkID == urn.stkID);
            var kategori1 = Db.urnKtgrAg_vw.SingleOrDefault(x => x.k0 == urnVw.urnKtgrID && x.k1 == 0 && x.k2 == 0);
            var kategori2 = urnVw.urnKtgrID2 != 0
                                ? Db.urnKtgrAg_vw.SingleOrDefault(x => x.k0 == urnVw.urnKtgrID && x.k1 == urnVw.urnKtgrID1 && x.k2 == 0)
                                : null;
            var kategori3 = urnVw.urnKtgrID3 != 0
                                ? Db.urnKtgrAg_vw.SingleOrDefault(x => x.k0 == urnVw.urnKtgrID && x.k1 == urnVw.urnKtgrID1 && x.k2 == urnVw.urnKtgrID2 && x.k3 == 0)
                                : null;

            var posOdeme = Db.posOdeme.FirstOrDefault(x => x.odemeDvzID == urn.fiyatSDvz);
            if (posOdeme == null)
            {
                posOdeme = new posOdeme
                {
                    odemeDvzID = 1,
                    odemeKur = 1m,
                    odemeKisaAd = "TL",
                };
            }

            var stkKontroluVar = StkKontroluVar;
            var stkGosterimiVar = StkGosterimiVar;
            var markaYerineGrup = MarkaYerineGrupGosterimiVar;

            var ayar = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 765);


            bool onaysizSipGoster = onaysizSiparisGoster();

            var model = new ProductDetailModel
            {

                Id = urn.stkID,
                Isim = urn.stkAd,
                UrunKodu = urn.stkKod,
                StokKisaAd = urn.stkAdKisa,

                // Ürün Kodları
                UrunKod1Baslik = urunKod1Baslik != null ? urunKod1Baslik.ayarDeger : "",
                UrunKod2Baslik = urunKod2Baslik != null ? urunKod2Baslik.ayarDeger : "",
                UrunKod3Baslik = urunKod3Baslik != null ? urunKod3Baslik.ayarDeger : "",
                UrunKod4Baslik = urunKod4Baslik != null ? urunKod4Baslik.ayarDeger : "",
                UrunKod5Baslik = urunKod5Baslik != null ? urunKod5Baslik.ayarDeger : "",

                UrunKod1 = urunKod1 != null ? urunKod1.kod1Ad : "",
                UrunKod2 = urunKod2 != null ? urunKod2.kod2Ad : "",
                UrunKod3 = urunKod3 != null ? urunKod3.kod3Ad : "",
                UrunKod4 = urunKod4 != null ? urunKod4.kod4Ad : "",
                UrunKod5 = urunKod5 != null ? urunKod5.kod5Ad : "",

                UrunKod1GosterilsinMi = urunKod1GosterilsinMi != null && urunKod1GosterilsinMi.ayarDeger != "0",
                UrunKod2GosterilsinMi = urunKod2GosterilsinMi != null && urunKod2GosterilsinMi.ayarDeger != "0",
                UrunKod3GosterilsinMi = urunKod3GosterilsinMi != null && urunKod3GosterilsinMi.ayarDeger != "0",
                UrunKod4GosterilsinMi = urunKod4GosterilsinMi != null && urunKod4GosterilsinMi.ayarDeger != "0",
                UrunKod5GosterilsinMi = urunKod5GosterilsinMi != null && urunKod5GosterilsinMi.ayarDeger != "0",

                bagliOlduguGrupIdsi = urn.urnGrpID,
                // Kategoriler
                Kategori0Id = kategori1 != null ? kategori1.kID : 0,
                Kategori1Id = kategori2 != null ? kategori2.kID : 0,
                Kategori2Id = kategori3 != null ? kategori3.kID : 0,
                Kategori0Ad = kategori1 != null ? kategori1.kAd : "",
                Kategori1Ad = kategori2 != null ? kategori2.kAd : "",
                Kategori2Ad = kategori3 != null ? kategori3.kAd : "",


                // Üretici - Marka - Model
                UreticiId = urnVw.urtID,
                UreticiAd = urnVw.urtAd,
                MarkaId = (markaYerineGrup == 0) ? urnVw.urnMrkID + "-0" : urnVw.urnMrkGrpID + "-1",
                MarkaAd = (markaYerineGrup == 0) ? urnVw.mrkAd : urnVw.urnMrkGrpAd,

                stok = urnVw.stok,
                stokKontrolu = stkKontroluVar,
                gosterilenStok = urnVw.gosterilenStok.ToConvertibleDecimalStokFormat(),
                stokGosterimi = stkGosterimiVar,

                kategoriGosterimiYok = ayar == null ? false : byte.Parse(ayar.ayarDeger) > 0,
                // Ürün Özellikleri
                UrunBirimi = urn.urnBrm,
                PaketOlcu = urn.paketOlcu,
                PaketBirimi = urn.paketBrm,
                KoliOlcu = urn.urnKoli,
                KoliBirimi = urn.urnKoliBrm,
                KutuOlcu = urn.urnKutu,
                PaletOlcu = urn.urnPalet,


                // Ürün Bilgileri
                BilgiGruplari = Db.urnBilgiGrp
                                    .Where(x => x.grupB2bGorunsun == 1)
                                    .OrderBy(x => x.grupSira)
                                    .Select(x => new ProductDetailModel.BilgiGrubu
                                    {
                                        GrupAdi = x.grupAd,
                                        Bilgiler = Db.urnBilgi
                                                             .Where(y => y.bVeriID == urn.stkID)
                                                             .Where(y => y.urnBilgiTnm.bilgiGrup == x.grupID)
                                                             .OrderBy(y => y.urnBilgiTnm.BilgiSira)
                                                             .Select(y => new ProductDetailModel.BilgiGrubu.Bilgi
                                                             {
                                                                 Ad = y.urnBilgiTnm.BilgiAd,
                                                                 Deger = y.bDeger
                                                             })
                                    }),


                // Barkod Bilgileri
                BarkodBilgileri = Db.urnBrkd.Where(x => x.urnBrkdAltStkId == urn.stkID || x.urnBrkdStkID == urn.stkID)
                                    .OrderBy(x => x.urnBrkdOnce)
                                    .Select(x => new ProductDetailModel.BarkodBilgisi
                                    {
                                        Barkod = x.urnBarkod,
                                        Adet = x.urnBrkdAdet,
                                        AdetBirim = x.urnBrkdBrm,
                                    }),


                // Stok Bilgileri
                StokBilgileri = Db.stokSon_vw
                                    .Where(x => x.ehstkID == urn.stkID)
                                    .Join(Db.posMagaza, x => x.ehMekan, y => y.mekanID, (stok, mekan) => new { stok, mekan })
                                    .Where(x => x.mekan.mekanMerkezDepo != 2)
                                    .OrderByDescending(x => x.mekan.mekanMerkezDepo)
                                    .Select(x => new ProductDetailModel.StokBilgisi
                                    {
                                        MekanAdi = x.mekan.mekanAd,
                                        Stok = x.stok.stok.Value
                                    }),


                // Fiyat ve İndirim Bilgileri
                KDVHaricSatisFiyati = urnVw.fiyatA,
                Birim = posOdeme.odemeKisaAd,
                SatisKur = posOdeme.odemeKur,
                Indirim1 = indirim1,
                Indirim2 = indirim2,
                Indirim3 = indirim3,
                Indirim4 = indirim4,
                KDVOrani = Db.urnKDV.Single(x => x.kdvID == urn.KDVs).kdvYuzde,


                // Ürün Siparişleri
                Siparisler = Db.sipAyr
                          .Join(Db.sip, x => x.ehID, y => y.eID, (sipA, sipB) => new { sipA, sipB })
                          .Join(Db.sifTip.Where(y => y.hrktSIF == 0), s => s.sipB.eTip, tip => tip.hrktTipID, (s, tip) => new { s, tip })
                          .Join(Db.sipNeden, x => x.s.sipB.Neden, neden => neden.nedenID, (x, neden) => new { x.s, x.tip, neden })
                          .Where(x => x.s.sipA.ehStkID == urn.stkID)
                          .Where(x => x.s.sipB.eTarih >= DateHelper.LastMonthBegin)
                          .Where(x => x.s.sipB.eTarih <= DateTime.Today)
                          .Where(x => x.s.sipB.eFirma == CurrentAccount.frm.frmID)
                          .Where(x => (onaysizSipGoster == true || (onaysizSipGoster == false && x.s.sipB.onay == 1)))
                          .GroupBy(row => new { row.s.sipB.eID, row.s.sipB.eNo, row.s.sipB.eTarih, row.s.sipB.eTarihS, row.tip.hrktTipAd, row.neden.nedenAd })
                          .OrderByDescending(x => x.Key.eTarih)
                          .Select(group => new ProductDetailModel.UrunSiparis
                          {
                              Id = group.Key.eID,
                              EvrakNo = group.Key.eNo,
                              Tarih = group.Key.eTarih,
                              SevkTarihi = group.Key.eTarihS,
                              HareketTipi = group.Key.hrktTipAd,
                              HareketGrubu = group.Key.nedenAd,
                              UrunToplamAdet = group.Sum(s => s.s.sipA.ehAdet),
                              UrunKoliIciAdet = urn.urnKoli,
                              UrunIndirimTutari = group.Sum(s => s.s.sipA.ehIndirim),
                              UrunKDVHaricToplam = group.Sum(s => s.s.sipA.ehTutar),
                              ToplamKDVHaricNet = group.Sum(s => s.s.sipA.ehTutar) - group.Sum(s => s.s.sipA.ehIndirim)
                          }),



                // Ürün İrsaliyeler
                Irsaliyeler = Db.irsAyr
                          .Join(Db.irs, x => x.ehID, y => y.eID, (irsA, irsB) => new { irsA, irsB })
                          .Join(Db.sifTip.Where(y => y.hrktSIF == 1), i => i.irsB.eTip, tip => tip.hrktTipID, (i, tip) => new { i, tip })
                          .Join(Db.sipNeden, x => x.i.irsB.Neden, neden => neden.nedenID, (x, neden) => new { x.i, x.tip, neden })
                          .Where(x => x.i.irsA.ehStkID == urn.stkID)
                          .Where(x => x.i.irsB.eTarih >= DateHelper.LastMonthBegin)
                                    .Where(x => x.i.irsB.eTarih <= DateTime.Today)
                                    .Where(x => x.i.irsB.eFirma == CurrentAccount.frm.frmID)
                                    .GroupBy(row => new { row.i.irsB.eID, row.i.irsB.eNo, row.i.irsB.eTarih, row.i.irsB.eTarihS, row.tip.hrktTipAd, row.neden.nedenAd })
                                    .OrderByDescending(x => x.Key.eTarih)
                                    .Select(group => new ProductDetailModel.UrunIrsaliye
                                    {
                                        Id = group.Key.eID,
                                        EvrakNo = group.Key.eNo,
                                        Tarih = group.Key.eTarih,
                                        SevkTarihi = group.Key.eTarihS,
                                        HareketTipi = group.Key.hrktTipAd,
                                        HareketGrubu = group.Key.nedenAd,
                                        UrunToplamAdet = group.Sum(s => s.i.irsA.ehAdet),
                                        UrunKoliIciAdet = urn.urnKoli,
                                        UrunIndirimTutari = group.Sum(s => s.i.irsA.ehIndirim),
                                        UrunKDVHaricToplam = group.Sum(s => s.i.irsA.ehTutar),
                                        ToplamKDVHaricNet = group.Sum(s => s.i.irsA.ehTutar) - group.Sum(s => s.i.irsA.ehIndirim)
                                    }),



                // Ürün Faturaları
                Faturalar = Db.fatAyr
                          .Join(Db.fat, x => x.ehID, y => y.eID, (fatA, fatB) => new { fatA, fatB })
                          .Join(Db.sifTip.Where(y => y.hrktSIF == 2), f => f.fatB.eTip, tip => tip.hrktTipID, (f, tip) => new { f, tip })
                          .Join(Db.sipNeden, x => x.f.fatB.Neden, neden => neden.nedenID, (x, neden) => new { x.f, x.tip, neden })
                          .Where(x => x.f.fatA.ehStkID == urn.stkID)
                          .Where(x => x.f.fatB.eTarih >= DateHelper.LastMonthBegin)
                                    .Where(x => x.f.fatB.eTarih <= DateTime.Today)
                                    .Where(x => x.f.fatB.eFirma == CurrentAccount.frm.frmID)
                                    .GroupBy(row => new { row.f.fatB.eID, row.f.fatB.eNo, row.f.fatB.eTarih, row.f.fatB.eTarihV, row.tip.hrktTipAd, row.neden.nedenAd })
                                    .OrderByDescending(x => x.Key.eTarih)
                                    .Select(group => new ProductDetailModel.UrunFatura
                                    {
                                        Id = group.Key.eID,
                                        EvrakNo = group.Key.eNo,
                                        Tarih = group.Key.eTarih,
                                        VadeTarihi = group.Key.eTarihV,
                                        HareketTipi = group.Key.hrktTipAd,
                                        HareketGrubu = group.Key.nedenAd,
                                        UrunToplamAdet = group.Sum(s => s.f.fatA.ehAdet),
                                        UrunKoliIciAdet = urn.urnKoli,
                                        UrunIndirimTutari = group.Sum(s => s.f.fatA.ehIndirim),
                                        UrunKDVHaricToplam = group.Sum(s => s.f.fatA.ehTutar),
                                        ToplamKDVHaricNet = group.Sum(s => s.f.fatA.ehTutar) - group.Sum(s => s.f.fatA.ehIndirim)
                                    })
                                    ,
                ControllerName = "Supplier",

            };

            ViewBag.KategoriYok = ayar == null ? false : byte.Parse(ayar.ayarDeger) > 0;

            return View(model);
        }

        public ActionResult ProductGroupDetail(int? id)
        {
            Check.NotNullOr404(id);

            var urunModeli = Db.urnGrp.SingleOrDefault(x => x.grpID == id);
            Check.NotNullOr404(urunModeli);

            var markaYerineGrup = MarkaYerineGrupGosterimiVar;

            int grupVar = 0;
            if (markaYerineGrup > 0)
            {
                grupVar = 1;
            }

            var marka = Db.urnMrk.Single(x => x.mrkID == urunModeli.grpMrkID);
            var markaGrup = Db.urnMrkGrp.Single(x => x.urnMrkGrpID == marka.mrkGrup);

            var uretici = marka.urnMrkUrt;

            var kategori0 = Db.urnKtgrAg_vw.SingleOrDefault(x => x.kID == urunModeli.urnGrpKtgrID);
            var kategori1 = Db.urnKtgrAg_vw.SingleOrDefault(x => x.kID == urunModeli.urnGrpKtgrID1);
            var kategori2 = Db.urnKtgrAg_vw.SingleOrDefault(x => x.kID == urunModeli.urnGrpKtgrID2);

            var markaIndirimi = Db.sakMrk_vw
                    .Where(x => x.skFrmID == CurrentAccount.frm.frmID)
                    .Where(x => x.skTur == CurrentCompany.frmVadeTur)
                    .SingleOrDefault(x => x.skMrkID == marka.mrkID);





            int musteriMarkaKosulu = MusteriMarkalarinaAitUrunleriGorsun;



            string urunVWSorgu = @"  select  alis.stkID,alis.stkKod,alis.stkAd,alis.mrkAd,alis.grpAd,
            alis.urnKtgrID,alis.urnKtgrID1,alis.urnKtgrID2,alis.urnKtgrID3,
            alis.urtID,alis.urnMrkID,alis.urnGrpID,urnBarkod_vw.barkod,alis.rtb,
            alis.odemeKur,
			alis.fiyatA,
            alis.odemeKisaAd,
            alis.urnMrkGrpID,
			alis.urnKoli,
			alis.urnKutu,
			alis.stok,
			urnKdv.kdvYuzde as urnKdvYuzde,
			alis.gosterilenStok 
	from b2b.fn_urnAlis_vw(" + CurrentAccount.frm.frmID + @") as alis
	inner join b2b.urnBarkod_vw as urnBarkod_vw on alis.stkID = urnBarkod_vw.urnBrkdStkID
    " + (musteriMarkaKosulu > 0 ? "inner join sakMrk as skMarka on alis.urnMrkID = skMarka.skMrkID" : "") + @"
	left join urnKdv on kdvID=alis.KDVa where alis.urnGrpID=" + urunModeli.grpID + "  " + (musteriMarkaKosulu > 0 ? " and skMarka = " + CurrentAccount.frm.frmID + "" : "") + " ";
            var urnVw = Db.Database.SqlQuery<SupplierProductListQueryModel>(urunVWSorgu).ToList();

            var enUcuzModelUrunu = urnVw.OrderBy(x => x.fiyatA).First();

            var ekIndirim = Db.fytOzl
                    .Where(x => x.fStkID == enUcuzModelUrunu.stkID)
                    .Where(x => x.fTip == 4)
                    //.Where(x => x.fFrmID == 0)
                    .Where(x => x.fTarih <= DateTime.Today && x.fTarihSon >= DateTime.Today)
                    .Where(x => x.fTur == 0)
                    .Where(x => x.onay == 1)
                    .OrderByDescending(x => x.fTarih)
                    .ThenByDescending(x => x.fID)
                    .FirstOrDefault();

            var stkKontroluVar = StkKontroluVar;
            var stkGosterimiVar = StkGosterimiVar;
            var brutFiyatGosterimiVar = BrutFiyatGostermiVar;

            var ayar = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 765);


            var model = new ProductGroupDetailModel
            {
                Id = urunModeli.grpID,
                ModelAdi = urunModeli.grpAd,

                MarkaId = grupVar == 1 ? markaGrup.urnMrkGrpID : marka.mrkID,
                MarkaAdi = grupVar == 1 ? markaGrup.urnMrkGrpAd : marka.mrkAd,

                Brand_GroupValue = grupVar,

                UreticiId = uretici.urtID,
                UreticiAdi = uretici.urtAd,

                Kategori0Id = kategori0 != null ? kategori0.kID : 0,
                Kategori0Ad = kategori0 != null ? kategori0.kAd : "",
                Kategori1Id = kategori1 != null ? kategori1.kID : 0,
                Kategori1Ad = kategori1 != null ? kategori1.kAd : "",
                Kategori2Id = kategori2 != null ? kategori2.kID : 0,
                Kategori2Ad = kategori2 != null ? kategori2.kAd : "",


                KDVHaricSatisFiyati = enUcuzModelUrunu.fiyatA,
                Indirim1 = markaIndirimi != null ? markaIndirimi.skY1 : 0,
                Indirim2 = markaIndirimi != null ? markaIndirimi.skY2 : 0,
                Indirim3 = markaIndirimi != null ? markaIndirimi.skY3 : 0,
                Indirim4 = ekIndirim != null ? ekIndirim.fInd1 : 0,
                KDVOrani = enUcuzModelUrunu.urnKdvYuzde,

                grupStokKontrolu = stkKontroluVar,
                grupStokGosterimi = stkGosterimiVar,
                brutFiyatGosterimVar = brutFiyatGosterimiVar,

                kategoriGosterimiYok = ayar == null ? false : byte.Parse(ayar.ayarDeger) > 0,

                Products = (from urn in urnVw
                            where urn.urnGrpID == id
                            let urunIndirimi = Db.sakMrk_vw
                            .Where(x => x.skTur == CurrentCompany.frmVadeTur)
                            .FirstOrDefault(x => x.skMrkID == marka.mrkID && x.skFrmID == CurrentCompany.frmID)
                            let ekUrunIndirimi = Db.fytOzl
                                .Where(x => x.fStkID == urn.stkID)
                                .Where(x => x.fTip == 4)
                                //.Where(x => x.fFrmID == 0)
                                .Where(x => x.fTarih <= DateTime.Today && x.fTarihSon >= DateTime.Today)
                                .Where(x => x.fTur == 0)
                                .Where(x => x.onay == 1)
                                .OrderByDescending(x => x.fTarih)
                                .ThenByDescending(x => x.fID)
                                .FirstOrDefault()
                            select new ProductGroupDetailModel.Product
                            {
                                Id = urn.stkID,
                                Isim = urn.stkAd,
                                StokKodu = urn.stkKod,
                                Barkod = urn.barkod,
                                KDVHaricSatisFiyati = urn.fiyatA,
                                Indirim1 = urunIndirimi != null ? urunIndirimi.skY1 : 0,
                                Indirim2 = urunIndirimi != null ? urunIndirimi.skY2 : 0,
                                Indirim3 = urunIndirimi != null ? urunIndirimi.skY3 : 0,
                                Indirim4 = ekUrunIndirimi != null ? ekUrunIndirimi.fInd1 : 0,
                                KDVOrani = urn.urnKdvYuzde,
                                Birim = urn.odemeKisaAd,
                                stok = urn.stok,
                                gosterilenStok = urn.gosterilenStok.ToConvertibleDecimalStokFormat(),
                            })
                                .ToList(),
                ControllerName = "Supplier",
            };

            ViewBag.KategoriYok = ayar == null ? false : byte.Parse(ayar.ayarDeger) > 0;

            return View(model);
        }

        public ActionResult ProductPromoDetail(int? id, string resim)
        {
            Check.NotNullOr404(id);

            var fiyatbaslik = Db.fytB.SingleOrDefault(x => x.feID == id);
            Check.NotNullOr404(fiyatbaslik);


            var stkKontroluVar = StkKontroluVar;
            var stkGosterimiVar = StkGosterimiVar;
            var brutFiyatGosterimVar = BrutFiyatGostermiVar;


            int musteriMarkaKosulu = MusteriMarkalarinaAitUrunleriGorsun;

            string urunVWSorgu = @"  select  alis.stkID,alis.stkKod,alis.stkAd,alis.mrkAd,alis.grpAd,
            alis.urnKtgrID,alis.urnKtgrID1,alis.urnKtgrID2,alis.urnKtgrID3,
            alis.urtID,alis.urnMrkID,alis.urnGrpID,urnBarkod_vw.barkod,alis.rtb,
            alis.odemeKur,
			alis.fiyatA,
            alis.odemeKisaAd,
            alis.urnMrkGrpID,
			alis.urnKoli,
			alis.urnKutu,
			alis.stok,
			urnKdv.kdvYuzde as urnKdvYuzde,
			alis.gosterilenStok 
	from b2b.fn_urnAlis_vw(" + CurrentAccount.frm.frmID + @") as alis
	inner join b2b.urnBarkod_vw as urnBarkod_vw on alis.stkID = urnBarkod_vw.urnBrkdStkID
    " + (musteriMarkaKosulu > 0 ? "inner join sakMrk as skMarka on alis.urnMrkID = skMarka.skMrkID" : "") + @"
	left join urnKdv on kdvID=alis.KDVa  " + (musteriMarkaKosulu > 0 ? " where skMarka = " + CurrentAccount.frm.frmID + "" : "") + " ";
            var urnVw = Db.Database.SqlQuery<SupplierProductListQueryModel>(urunVWSorgu).ToList();



            var model = new ProductPromoDetailModel()
            {
                Id = fiyatbaslik.feID,
                Ad = fiyatbaslik.feNot,
                resim = resim,
                promoStokKontrolu = stkKontroluVar,
                promoStokGosterimi = stkGosterimiVar,
                brutFiyatGosterimVar = brutFiyatGosterimVar,

                Products = (from fytOzl in Db.fytOzl
                            join urn in urnVw on fytOzl.fStkID equals urn.stkID
                            join mainUrn in Db.urn on urn.stkID equals mainUrn.stkID
                            where fytOzl.fhID == id
                            let markaIndirimi = Db.sakMrk_vw
                              .Where(x => x.skFrmID == CurrentCompany.frmID)
                               .Where(x => x.skTur == CurrentCompany.frmVadeTur)
                               .FirstOrDefault(x => x.skMrkID == urn.urnMrkID)
                            select new ProductPromoDetailModel.Product
                            {
                                Id = urn.stkID,
                                Isim = urn.stkAd,
                                StokKodu = urn.stkKod,
                                Barkod = urn.barkod,
                                KDVHaricSatisFiyati = urn.fiyatA,
                                Indirim1 = markaIndirimi != null ? markaIndirimi.skY1 : 0,
                                Indirim2 = markaIndirimi != null ? markaIndirimi.skY2 : 0,
                                Indirim3 = markaIndirimi != null ? markaIndirimi.skY3 : 0,
                                Indirim4 = fytOzl.fInd1,
                                KDVOrani = urn.urnKdvYuzde,
                                Birim = urn.odemeKisaAd,
                                stok = urn.stok,
                                gosterilenStok = urn.gosterilenStok.ToConvertibleDecimalStokFormat(),
                            })
                    .ToList(),
                ControllerName = "Supplier",
            };

            var ayar = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 765);
            ViewBag.KategoriYok = ayar == null ? false : byte.Parse(ayar.ayarDeger) > 0;


            return View(model);
        }

        #endregion

        #region Product List Excel

        private bool isNumericExcel(string s)
        {
            try
            {
                decimal a = decimal.Parse(s);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        #endregion


        #region Promotion & Category & Brand Trees & Prices


        [ChildActionOnly]
        public ActionResult Promotion()
        {
            var model = new PromoModel();


            List<int> Urunler = new List<int>();


            string urunSorgu = @"  select  distinct alis.stkID 
	from b2b.fn_urnAlis_vw(" + CurrentAccount.frm.frmID + @") as alis
    inner join b2bKampanyaResimleri as bk on bk.etkiID=alis.stkID where bk.etkiTip=0 ";

            int musteriMarkaKosulu = MusteriMarkalarinaAitUrunleriGorsun;
            if (musteriMarkaKosulu > 0)
            {
                urunSorgu = @" select  distinct alis.stkID 
	from b2b.fn_urnAlis_vw(" + CurrentAccount.frm.frmID + @") as alis
    inner join b2bKampanyaResimleri as bk on bk.etkiID=alis.stkID
	inner join sakMrk as skMarka on alis.urnMrkID = skMarka.skMrkID where sk.skFrmID=" + CurrentAccount.frm.frmID + " and bk.etkiTip=0  ";
            }
            Urunler = Db.Database.SqlQuery<int>(urunSorgu).ToList();

            var bn = DateTime.Now.Date;
            model.Promolar = (from kr in Db.b2bKampanyaResimleri
                              where kr.durum == true && kr.basTarih <= bn && bn <= kr.bitTarih
                              select new PromoModel.promo
                              {
                                  sira = kr.sira,
                                  baslik = kr.baslik,
                                  resim = kr.resim,
                                  etkiTip = kr.etkiTip,
                                  etkiId = kr.etkiID,
                                  etkiDosya = kr.etkiDosya,
                                  linkAktif = kr.etkiTip == 0 ? (Urunler.Contains(kr.etkiID) ? 1 : 0) : (kr.etkiTip == 3 ? (kr.etkiDosya == "" ? 0 : 1) : 1),
                              }).AsEnumerable().Select(kr => new PromoModel.promo
                              {
                                  sira = kr.sira,
                                  baslik = kr.baslik,
                                  resim = PromotionImage(kr.resim).ToString(),
                                  etkiTip = kr.etkiTip,
                                  etkiId = kr.etkiId,
                                  etkiDosya = kr.etkiDosya,
                                  linkAktif = kr.etkiTip == 0 ? (Urunler.Contains(kr.etkiId) ? 1 : 0) : (kr.etkiTip == 3 ? (kr.etkiDosya == "" ? 0 : 1) : 1),

                              }).ToList();


            return PartialView(model);
        }

        private string PromotionImage(string name)
        {
            if (name != "")
            {
                var absolutePath = Server.MapPath("~/Assets/promoresimler/" + name);
                if (System.IO.File.Exists(absolutePath))
                {
                    return "../../Assets/promoresimler/" + name;
                }
            }
            return "../../Assets/theme/img/notimg.jpg";
        }

        [ChildActionOnly]
        public ActionResult CategoryTree()
        {
            string sorgu = @" select * from b2b.fn_urnAlisKategori_Vw(" + CurrentAccount.frm.frmID + ") ";
            var query = Db.Database.SqlQuery<Categories>(sorgu).ToList();


            var level1Categories = query
                .Where(x => x.k1 == 0)
                .OrderBy(x => x.kAd)
                .Select(x => new CategoryTreeModel.CategoryItem
                {
                    Id = x.kID,
                    Isim = x.kAd,
                    Order = x.k0,
                    ParentId = null,
                });

            // Level 2
            var level2Categories = query
                .Where(x => x.k1 != 0 && x.k2 == 0)
                .OrderBy(x => x.kAd)
                .Select(x => new CategoryTreeModel.CategoryItem
                {
                    Id = x.kID,
                    Isim = x.kAd,
                    Order = x.k1,
                    ParentId = query.FirstOrDefault(y => y.k0 == x.k0 && y.k1 == 0).kID
                });

            // Level 3
            var level3Categories = query
                .Where(x => x.k2 != 0 && x.k3 == 0)
                .OrderBy(x => x.kAd)
                .Select(x => new CategoryTreeModel.CategoryItem
                {
                    Id = x.kID,
                    Isim = x.kAd,
                    Order = x.k2,
                    ParentId = query.FirstOrDefault(y => y.k0 == x.k0 && y.k1 == x.k1 && y.k2 == 0).kID
                });


            var level4Categories = query
             .Where(x => x.k3 != 0)
             .OrderBy(x => x.kAd)
             .Select(x => new CategoryTreeModel.CategoryItem
             {
                 Id = x.kID,
                 Isim = x.kAd,
                 Order = x.k3,
                 ParentId = query.FirstOrDefault(y => y.k0 == x.k0 && y.k1 == x.k1 && y.k2 == x.k2 && y.k3 == 0).kID,
             });




            var allCategories = level1Categories
                .Union(level2Categories).ToList()
                .Union(level3Categories).ToList()
                .Union(level4Categories).ToList();

            var model = new CategoryTreeModel { Items = allCategories };
            return PartialView(model);
        }

        [ChildActionOnly]
        public ActionResult BrandTree(string q, int? categoryId, string price)
        {
            var model = new BrandTreeModel();
            int markaYerineGrup = MarkaYerineGrupGosterimiVar;

            List<int> markaUrunleri = new List<int>();
            int musteriMarkaKosulu = MusteriMarkalarinaAitUrunleriGorsun;

            string urunSorgu = @"  select  alis.stkID,alis.stkKod,alis.stkAd,alis.mrkAd,alis.grpAd,
            alis.urnKtgrID,alis.urnKtgrID1,alis.urnKtgrID2,alis.urnKtgrID3,
            alis.urtID,alis.urtAd,alis.urnMrkID,alis.urnGrpID,urnBarkod_vw.barkod,alis.rtb,
            alis.odemeKur,
			alis.fiyatA,
            alis.odemeKisaAd,
            alis.urnMrkGrpID,
            alis.urnMrkGrpAd,
			alis.urnKoli,
			alis.urnKutu,
			alis.stok,
			alis.gosterilenStok 
	from b2b.fn_urnAlis_vw(" + CurrentAccount.frm.frmID + @") as alis
	inner join b2b.urnBarkod_vw as urnBarkod_vw on alis.stkID = urnBarkod_vw.urnBrkdStkID ";
            var genelUrn = Db.Database.SqlQuery<SupplierProductListQueryModel>(urunSorgu).ToList().AsQueryable();

            if (musteriMarkaKosulu > 0)
            {
                markaUrunleri = genelUrn
                                .Join(Db.sakMrk, x => x.urnMrkID, y => y.skMrkID, (urnAlis, skMarka) => new { urnAlis, skMarka })
                                .Where(x => x.skMarka.skFrmID == CurrentAccount.frm.frmID)
                                .Select(x => x.urnAlis.stkID).Distinct().ToList();
            }

            if (String.IsNullOrWhiteSpace(q))
            {
                var urunList = genelUrn.Where(x => markaUrunleri.Contains(x.stkID) || musteriMarkaKosulu == 0).Distinct();

                if (!string.IsNullOrEmpty(price))
                {
                    var esik = price.Split('-');
                    int min = int.Parse(esik[0]);
                    int max = esik[1] != "" ? int.Parse(esik[1]) : 99999999;
                    urunList = urunList.
                    Where(
                        x3 =>
                        (x3.fiyatA * x3.odemeKur) >= min &&
                        (x3.fiyatA * x3.odemeKur) <= max);
                }

                var ürünUreticileri = urunList
                    .GroupBy(x => new { x.urtID, x.urtAd })
                    .Select(x => new { ureticiIDsi = x.Key.urtID, ureticiAdi = x.Key.urtAd, urunUreticiSay = x.Count() }).ToList();

                IList<BrandData> markaVeri = new List<BrandData>();

                int grupVar = 0;
                if (markaYerineGrup > 0)
                {
                    markaVeri = urunList.GroupBy(x => new { x.urtID, urnMrkID = x.urnMrkGrpID, mrkAd = x.urnMrkGrpAd })
                   .Select(x => new BrandData { UreticiID = x.Key.urtID, MarkaID = x.Key.urnMrkID, markaAd = x.Key.mrkAd, markaCount = x.Count() }).ToList();
                    grupVar = 1;
                }
                else
                {
                    markaVeri = urunList.GroupBy(x => new { x.urtID, x.urnMrkID, x.mrkAd })
                                      .Select(x => new BrandData { UreticiID = x.Key.urtID, MarkaID = x.Key.urnMrkID, markaAd = x.Key.mrkAd, markaCount = x.Count() }).ToList();
                }

                var producersFull = ürünUreticileri
                    .Select(prodcr => new BrandTreeModel.Producer
                    {
                        Id = prodcr.ureticiIDsi,
                        Name = prodcr.ureticiAdi,
                        Brands = markaVeri
                        .Where(x => x.UreticiID == prodcr.ureticiIDsi)
                        .Select(brnd => new BrandTreeModel.Brand
                        {
                            Id = brnd.MarkaID,
                            Name = brnd.markaAd,
                            Count = brnd.markaCount
                        }).OrderBy(x => x.Name)
                    }).OrderBy(x => x.Name);

                model = new BrandTreeModel { Brand_GroupValue = grupVar, Producers = producersFull };
                return PartialView(model);
            }

            var qstr = q.Split(' ').Select(x => x.ToLower()).ToArray();
            var barkodlar = new List<int>();
            if (isNumeric(q) && q.Length >= 8)
            {
                barkodlar = Db.urnBrkd.Where(x => x.urnBarkod.Contains(q)).Select(x => x.urnBrkdAltStkId > 0 ? x.urnBrkdAltStkId : x.urnBrkdStkID).ToList();
            }
            var urunListe = !barkodlar.Any() ?
               genelUrn.Where(x => markaUrunleri.Contains(x.stkID) || musteriMarkaKosulu == 0).Where(x => qstr.All(y => x.stkAd.Contains(y)) ||
                                x.stkKod.Contains(q) ||
                                x.mrkAd.Contains(q))
                : genelUrn.Where(x => markaUrunleri.Contains(x.stkID) || musteriMarkaKosulu == 0).Where(x => barkodlar.Contains(x.stkID));

            if (categoryId != null)
            {
                string sorgu = @" select * from b2b.fn_urnAlisKategori_Vw(" + CurrentAccount.frm.frmID + ") where kID=" + categoryId + " ";
                var kategori = Db.Database.SqlQuery<Categories>(sorgu).ToList().Single();
                //var kategori = Db.fn_urnAlisKategori_Vw(CurrentAccount.frm.frmID).Single(x => x.kID == categoryId);

                urunListe = urunListe.Where(x => x.urnKtgrID == kategori.k0);

                if (kategori.k1 != 0)
                    urunListe = urunListe.Where(x => x.urnKtgrID1 == kategori.k1);

                if (kategori.k2 != 0)
                    urunListe = urunListe.Where(x => x.urnKtgrID2 == kategori.k2);

                if (kategori.k3 != 0)
                    urunListe = urunListe.Where(x => x.urnKtgrID3 == kategori.k3);
            }

            if (!string.IsNullOrEmpty(price))
            {
                var esik = price.Split('-');
                int min = int.Parse(esik[0]);
                int max = esik[1] != "" ? int.Parse(esik[1]) : 99999999;
                urunListe = urunListe.
                    Where(
                        x3 =>
                          (x3.odemeKur == 0 ? x3.fiyatA : x3.fiyatA * x3.odemeKur) >= min &&
                          (x3.odemeKur == 0 ? x3.fiyatA : x3.fiyatA * x3.odemeKur) <= max);
            }

            var ürünUretici = urunListe
                   .GroupBy(x => new { x.urtID, x.urtAd })
                   .Select(x => new { ureticiIDsi = x.Key.urtID, ureticiAdi = x.Key.urtAd, urunUreticiSay = x.Count() }).ToList();



            IList<BrandData> markaVeriDiger = new List<BrandData>();

            var sayisayDiger = 0;
            if (markaYerineGrup > 0)
            {
                markaVeriDiger = urunListe.GroupBy(x => new { x.urtID, urnMrkID = x.urnMrkGrpID, mrkAd = x.urnMrkGrpAd })
               .Select(x => new BrandData { UreticiID = x.Key.urtID, MarkaID = x.Key.urnMrkID, markaAd = x.Key.mrkAd, markaCount = x.Count() }).ToList();
            }
            else
            {
                sayisayDiger = 1;
                markaVeriDiger = urunListe.GroupBy(x => new { x.urtID, x.urnMrkID, x.mrkAd })
                                  .Select(x => new BrandData { UreticiID = x.Key.urtID, MarkaID = x.Key.urnMrkID, markaAd = x.Key.mrkAd, markaCount = x.Count() }).ToList();
            }


            var producers = ürünUretici
                .Select(prodcr => new BrandTreeModel.Producer
                {
                    Id = prodcr.ureticiIDsi,
                    Name = prodcr.ureticiAdi,
                    Brands = markaVeriDiger
                             .Where(x => x.UreticiID == prodcr.ureticiIDsi)
                    .Select(brnd => new BrandTreeModel.Brand
                    {
                        Id = brnd.MarkaID,
                        Name = brnd.markaAd,
                        Count = brnd.markaCount
                    }).OrderBy(x => x.Name)
                }).OrderBy(x => x.Name);


            //var producers = Db.urnMrkUrt
            //    .Where(x => urunListe.Any(y => y.urtID == x.urtID))
            //    .Select(producer => new BrandTreeModel.Producer
            //    {
            //        Id = producer.urtID,
            //        Name = producer.urtAd,
            //        Brands = producer.urnMrk.Where(x => urunListe.Any(y => y.urnMrkID == x.mrkID))
            //                                    .OrderBy(x => x.mrkAd)
            //                                    .Select(brand => new BrandTreeModel.Brand
            //                                    {
            //                                        Id = brand.mrkID,
            //                                        Name = brand.mrkAd,
            //                                        Count = urunListe.Where(x => x.urnMrkID == brand.mrkID).Count()
            //                                    })
            //    });

            model = new BrandTreeModel { Producers = producers };


            return PartialView(model);
        }

        [ChildActionOnly]
        public ActionResult Prices()
        {

            var prices = new Collection<PricesModel.Price>();
            var price = new PricesModel.Price();
            price = new PricesModel.Price { Max = 5, Min = 0, Name = "0-5" };
            prices.Add(price);
            price = new PricesModel.Price { Max = 10, Min = 5, Name = "5-10" };
            prices.Add(price);
            price = new PricesModel.Price { Max = 20, Min = 10, Name = "10-20" };
            prices.Add(price);
            price = new PricesModel.Price { Max = 50, Min = 20, Name = "20-50" };
            prices.Add(price);
            price = new PricesModel.Price { Max = 100, Min = 50, Name = "50-100" };
            prices.Add(price);
            price = new PricesModel.Price { Max = -1, Min = 500, Name = "500-" };
            prices.Add(price);
            var model = new PricesModel { Prices = prices };
            return PartialView(model);
        }

        #endregion



        #region DropDown Data

        [HttpPost]
        public ActionResult CitiesDropDownData()
        {
            var model = Db.frmKent
                .Select(x => new DropDownListItem
                {
                    Value = x.alanKodu,
                    Text = x.ilAd
                })
                .OrderBy(x => x.Text)
                .ToList();

            return Json(model);
        }



        [HttpPost]
        public ActionResult districtDropDownData(int ilId)
        {
            var districts = from frmKentIlce in Db.frmKentIlce
                            where frmKentIlce.ilAlanKodu == ilId
                            select new DropDownListItem
                            {
                                Value = frmKentIlce.ilceKodu,
                                Text = frmKentIlce.ilceAdi
                            };
            return Json(districts.Distinct().OrderBy(x => x.Text));
        }


        #endregion


        private bool onaysizSiparisGoster()
        {
            var OnaysizSiparisGösterimi = Db.drn2ayar.FirstOrDefault(x => x.ayarID == 773);
            bool bekleyenSipGoster = true;
            if (CurrentAccountType == AccountType.Customer)
            {
                if ((OnaysizSiparisGösterimi == null ? 0 : Convert.ToInt32(OnaysizSiparisGösterimi.ayarDeger)) == 3 || (OnaysizSiparisGösterimi == null ? 0 : Convert.ToInt32(OnaysizSiparisGösterimi.ayarDeger)) == 2)
                {
                    bekleyenSipGoster = false;
                }
            }
            else
            {
                if ((OnaysizSiparisGösterimi == null ? 0 : Convert.ToInt32(OnaysizSiparisGösterimi.ayarDeger)) == 3 || (OnaysizSiparisGösterimi == null ? 0 : Convert.ToInt32(OnaysizSiparisGösterimi.ayarDeger)) == 1)
                {
                    bekleyenSipGoster = false;
                }
            }
            return bekleyenSipGoster;
        }



        #region Ürün Liste Raporu
        public ActionResult ProductListReport(string q, int? page)
        {
            var pageLength = 29;

            var ayar = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 765);
            ViewBag.KategoriYok = ayar == null ? false : byte.Parse(ayar.ayarDeger) > 0;

            string aramaKriteri = "";
            if (!String.IsNullOrWhiteSpace(q))
            {
                aramaKriteri = q;
            }

            string sorgu = string.Format(@" exec [b2b].[sp_ProductListReport_Vw] @frmID={0},@page={1},@pageLength={2},@search='{3}' ", CurrentCompany.frmID, page ?? 1, pageLength, aramaKriteri);
            var sipAyrList = Db.Database.SqlQuery<ProductListReportModel.ProductListReport>(sorgu).ToList();

            var model = new ProductListReportModel
            {
                Sorgu = aramaKriteri,
                Page = page ?? 1,
                PageLength = pageLength,
                ProductListReports = sipAyrList.ToSqlPagedList(page ?? 1, pageLength, sipAyrList.Count > 0 ? (sipAyrList.FirstOrDefault().topSay) : 0)
            };

            return View(model);

        }

        public ActionResult ProductListReportExport(string q)
        {
            string aramaKriteri = "";
            if (!String.IsNullOrWhiteSpace(q))
            {
                aramaKriteri = q;
            }
            string sorgu = string.Format(@" exec [b2b].[sp_ProductListReport_Vw] @frmID={0},@page=0,@pageLength=0,@search='{1}' ", CurrentCompany.frmID, aramaKriteri);
            var liste = Db.Database.SqlQuery<ProductListReportExcel>(sorgu).ToList();

            var grid = new System.Web.UI.WebControls.GridView();

            grid.DataSource = liste;

            grid.DataBind();

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=UrunListesiRaporu.xls");
            Response.ContentType = "application/excel";
            StringWriter sw = new StringWriter();
            Encoding.GetEncoding(1254).GetBytes(sw.ToString());
            Response.Charset = "";
            Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1254");
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            grid.RenderControl(htw);

            Response.Write(sw.ToString());

            Response.End();


            return null;
        }

        //public ActionResult ProductListReportExportPDF(string q)
        //{
        //    string aramaKriteri = "";
        //    if (!String.IsNullOrWhiteSpace(q))
        //    {
        //        aramaKriteri = q;
        //    }
        //    string sorgu = string.Format(@" exec [b2b].[sp_ProductListReport_Vw] @frmID={0},@page=0,@pageLength=0,@search='{1}' ", CurrentCompany.frmID, aramaKriteri);
        //    var liste = Db.Database.SqlQuery<ProductListReportExcel>(sorgu).ToList();

        //    if (!liste.Any())
        //    {
        //        return null;
        //    }

        //    var grid = new System.Web.UI.WebControls.GridView();
        //    grid.DataSource = liste;
        //    grid.DataBind();


        //    Response.ContentType = "application/pdf";
        //    Response.AddHeader("content-disposition", "attachment;filename=Vithal_Wadje.pdf");
        //    Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //    StringWriter sw = new StringWriter();
        //    HtmlTextWriter hw = new HtmlTextWriter(sw);
        //    grid.RenderControl(hw);
        //    StringReader sr = new StringReader(sw.ToString());
        //    Document pdfDoc = new Document(PageSize.LETTER_LANDSCAPE.Rotate(), 1, 1, 25, 1);
        //    //Document pdfDoc = new Document(PageSize.A4.Rotate(), 10f, 10f, 10f, 0f);
        //    HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
        //    PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
        //    pdfDoc.Open();
            

        //    htmlparser.Parse(sr);
            

        //    pdfDoc.Close();
        //    Response.Write(pdfDoc);
        //    Response.End();
        //    grid.AllowPaging = true;
        //    grid.DataBind();




        //    //using (StringWriter sw = new StringWriter())
        //    //{
        //    //    using (HtmlTextWriter hw = new HtmlTextWriter(sw))
        //    //    {
        //    //        StringBuilder sb = new StringBuilder();

        //    //        sb.Append("<table width='100%' cellspacing='0' cellpadding='2'>");

        //    //        sb.Append("<thead><tr>");
        //    //        for (int i = 0; i < grid.Rows[0].Cells.Count; i++)
        //    //        {
        //    //            sb.Append("<th>" + ((System.Web.UI.WebControls.DataControlFieldCell)(grid.Rows[0].Cells[i])).ContainingField.ToString() + "</th>");
        //    //        }
        //    //        sb.Append("</tr></thead>");

        //    //        sb.Append("<tbody>");
        //    //        for (int i = 0; i < grid.Rows.Count; i++)
        //    //        {
        //    //            if (i % 2 == 0)
        //    //            {
        //    //                sb.Append("<tr style='background-color: #f5f5f5;'>");
        //    //            }
        //    //            else
        //    //            {
        //    //                sb.Append("<tr>");
        //    //            }
        //    //            for (int j = 0; j < grid.Rows[0].Cells.Count; j++)
        //    //            {
        //    //                sb.Append("<td>" + grid.Rows[i].Cells[j].Text.ToString() + "</td>");
        //    //            }
        //    //            sb.Append("</tr>");
        //    //        }
        //    //        sb.Append("</tbody>");
        //    //        sb.Append("</table>");

        //    //        StringReader sr = new StringReader(sb.ToString());
        //    //        Document pdfDoc = new Document(PageSize.A4.Rotate(), 10f, 10f, 10f, 0f);

        //    //        HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
        //    //        PdfWriter writer = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
        //    //        pdfDoc.Open();
        //    //        htmlparser.Parse(sr);
        //    //        pdfDoc.Close();

        //    //        Response.ContentType = "application/pdf";
        //    //        Response.AddHeader("content-disposition", "attachment;filename=UrunListesiRaporu.pdf");
        //    //        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //    //        Response.Write(pdfDoc);
        //    //        Response.End();
        //    //    }
        //    //}


        //    return null;
        //}

        #endregion


        #region Özel Fiyatlı Ürün Liste Raporu
        public ActionResult PrivateProductPriceListReport(string q, DocumentSingleDateFilterModel dateFilter, string mekanFilter = "", string mekanAdFilter = "", int? page = 1)
        {
            var pageLength = 29;

            var ayar = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 765);
            ViewBag.KategoriYok = ayar == null ? false : byte.Parse(ayar.ayarDeger) > 0;
            ViewData["mekan"] = mekanFilter;
            ViewData["mekanAd"] = mekanAdFilter;

            dateFilter.NotNullOrSetDefault(DateTime.Today);

            if (mekanFilter == "")
            {
                ShowErrorMessage("Mekan seçimi zorunludur!");
                return View(new PrivateProductPriceListReportModel
                {
                    Sorgu = q,
                    Mekanlar = mekanFilter,
                    DateFilter = dateFilter,
                    Page = page ?? 1,
                    PageLength = pageLength,
                    PrivateProductPriceListReports = new List<PrivateProductPriceListReportModel.PrivateProductPriceListReport>()
                });
            }


            string aramaKriteri = "";
            if (!String.IsNullOrWhiteSpace(q))
            {
                aramaKriteri = q;
            }


            DateTime sectarih = DateTime.Now.Date;
            try
            {
                sectarih = Convert.ToDateTime(dateFilter.Begin.Value).Date;
            }
            catch
            {

            }

            string sorgu = string.Format(@" exec [b2b].[sp_PrivatePriceProductListReport_Vw] @frmID={0},@page={1},@pageLength={2},@tarih='{3}',@search='{4}',@mekan='{5}' ", CurrentCompany.frmID, page ?? 1, pageLength, sectarih.ToString("dd.MM.yyyy"), aramaKriteri, mekanFilter);
            var sipAyrList = Db.Database.SqlQuery<PrivateProductPriceListReportModel.PrivateProductPriceListReport>(sorgu).ToList();

            var model = new PrivateProductPriceListReportModel
            {
                Sorgu = aramaKriteri,
                Mekanlar = mekanFilter,
                DateFilter = dateFilter,
                Page = page ?? 1,
                PageLength = pageLength,
                PrivateProductPriceListReports = sipAyrList.ToSqlPagedList(page ?? 1, pageLength, sipAyrList.Count > 0 ? (sipAyrList.FirstOrDefault().topSay) : 0)
            };

            return View(model);

        }

        public ActionResult PrivateProductPriceListReportExport(string q, int mekan, string tarih)
        {
            string aramaKriteri = "";
            if (!String.IsNullOrWhiteSpace(q))
            {
                aramaKriteri = q;
            }

            DateTime sectarih = DateTime.Now.Date;
            try
            {
                sectarih = Convert.ToDateTime(tarih).Date;
            }
            catch
            {

            }

            string sorgu = string.Format(@" exec [b2b].[sp_PrivatePriceProductListReport_Vw] @frmID={0},@page=0,@pageLength=0,@tarih='{1}',@search='{2}',@mekan={3} ", CurrentCompany.frmID, sectarih.ToString("dd.MM.yyyy"), aramaKriteri, mekan);
            var liste = Db.Database.SqlQuery<PrivateProductPriceListReportExcel>(sorgu).ToList();

            var grid = new System.Web.UI.WebControls.GridView();

            grid.DataSource = liste;

            grid.DataBind();

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=ÖzelFiyatliUrunListeRaporu.xls");
            Response.ContentType = "application/excel";
            StringWriter sw = new StringWriter();
            Encoding.GetEncoding(1254).GetBytes(sw.ToString());
            Response.Charset = "";
            Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1254");
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            grid.RenderControl(htw);

            Response.Write(sw.ToString());

            Response.End();


            return null;
        }



        #endregion


        #region Ürün Stok Hareket Raporu
        public ActionResult ProductStockActionReport(DocumentDateFilterModel dateFilter, int? page, string mekanFilter = "", string mekanAdFilter = "")
        {
            var pageLength = 29;

            var ayar = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 765);
            ViewBag.KategoriYok = ayar == null ? false : byte.Parse(ayar.ayarDeger) > 0;
            ViewData["mekan"] = mekanFilter;
            ViewData["mekanAd"] = mekanAdFilter;

            var mekanlar = new List<int>();
            string mekanAd = string.Empty;

            dateFilter.NotNullOrSetDefault(DateHelper.ThisMonthBegin, DateTime.Today);

            string sorgu = string.Format(@" exec [b2b].[sp_ProductStockActionReport_Vw] @frmID={0},@page={1},@pageLength={2},@basTarih='{3}',@bitTarih='{4}',@mekan ='{5}' ", CurrentCompany.frmID, page ?? 1, pageLength, dateFilter.Begin.Value.Date.ToString("dd.MM.yyyy"), dateFilter.End.Value.Date.ToString("dd.MM.yyyy"), mekanFilter);
            var sipAyrList = Db.Database.SqlQuery<ProductStockActionModel.ProductStock>(sorgu).ToList();

            var model = new ProductStockActionModel
            {
                DateFilter = dateFilter,
                Page = page ?? 1,
                PageLength = pageLength,
                Mekanlar = mekanFilter,
                ProductStocks = sipAyrList.ToSqlPagedList(page ?? 1, pageLength, sipAyrList.Count > 0 ? (sipAyrList.FirstOrDefault().topSay) : 0)
            };

            return View(model);

        }

        public ActionResult ProductStockActionReportExport(string mekanFilter = "", string ilkTar = "", string sonTar = "")
        {

            string ilkTarKosul = DateTime.Now.Date.AddDays(-30).ToString("dd.MM.yyyy");
            string sonTarKosul = DateTime.Now.Date.ToString("dd.MM.yyyy");

            if (ilkTar != "")
            {
                try
                {
                    ilkTarKosul = Convert.ToDateTime(ilkTar).Date.ToString("dd.MM.yyyy");
                }
                catch
                {
                }
            }
            if (sonTar != "")
            {
                try
                {
                    sonTarKosul = Convert.ToDateTime(sonTar).Date.ToString("dd.MM.yyyy");
                }
                catch
                {
                }
            }


            string sorgu = string.Format(@" exec [b2b].[sp_ProductStockActionReport_Vw] @frmID={0},@page=0,@pageLength=0,@basTarih='{1}',@bitTarih='{2}',@mekan ='{3}' ", CurrentCompany.frmID, ilkTarKosul, sonTarKosul, mekanFilter);
            var liste = Db.Database.SqlQuery<ProductStockActionReportExcel>(sorgu).ToList();

            var grid = new System.Web.UI.WebControls.GridView();

            grid.DataSource = liste;

            grid.DataBind();

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=UrunStokHareketRaporu.xls");
            Response.ContentType = "application/excel";
            StringWriter sw = new StringWriter();
            Encoding.GetEncoding(1254).GetBytes(sw.ToString());
            Response.Charset = "";
            Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1254");
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            grid.RenderControl(htw);

            Response.Write(sw.ToString());

            Response.End();


            return null;
        }

        #endregion


        #region Ürün Stok Raporu
        public ActionResult ProductStockReport(string q, int? page, string mekanFilter = "", string mekanAdFilter = "")
        {
            var pageLength = 29;

            var ayar = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 765);
            ViewBag.KategoriYok = ayar == null ? false : byte.Parse(ayar.ayarDeger) > 0;
            ViewData["mekan"] = mekanFilter;
            ViewData["mekanAd"] = mekanAdFilter;

            var mekanlar = new List<int>();
            string mekanAd = string.Empty;

            string aramaKriteri = "";
            if (!String.IsNullOrWhiteSpace(q))
            {
                aramaKriteri = q;
            }

            string sorgu = string.Format(@" exec [b2b].[sp_ProductStockReport_Vw] @frmID={0},@page={1},@pageLength={2},@mekan ='{3}',@search='{4}' ", CurrentCompany.frmID, page ?? 1, pageLength, mekanFilter, aramaKriteri);
            var sipAyrList = Db.Database.SqlQuery<ProductStockModel.ProductStock>(sorgu).ToList();

            var topUrun = sipAyrList.Count > 0 ? (sipAyrList.FirstOrDefault().topSay) : 0;
            int topSayfa = (topUrun / pageLength) + (topUrun % pageLength > 0 ? 1 : 0);
            if ((topUrun % pageLength == 0))
            {
                pageLength = pageLength + 1;
            }

            var model = new ProductStockModel
            {
                Sorgu = aramaKriteri,
                Page = page ?? 1,
                PageLength = pageLength,
                Mekanlar = mekanFilter,
                ProductStocks = sipAyrList.ToSqlPagedList(page ?? 1, pageLength, sipAyrList.Count > 0 ? (sipAyrList.FirstOrDefault().topSay) : 0)
            };

            return View(model);

        }

        public ActionResult ProductStockReportExport(string q, string mekanFilter = "")
        {
            string aramaKriteri = "";
            if (!String.IsNullOrWhiteSpace(q))
            {
                aramaKriteri = q;
            }
            string sorgu = string.Format(@" exec [b2b].[sp_ProductStockReport_Vw] @frmID={0},@page=0,@pageLength=0,@mekan ='{1}',@search='{2}' ", CurrentCompany.frmID, mekanFilter, aramaKriteri);
            var liste = Db.Database.SqlQuery<ProductStockReportExcel>(sorgu).ToList();

            var grid = new System.Web.UI.WebControls.GridView();

            grid.DataSource = liste;

            grid.DataBind();

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=UrunStokRaporu.xls");
            Response.ContentType = "application/excel";
            StringWriter sw = new StringWriter();
            Encoding.GetEncoding(1254).GetBytes(sw.ToString());
            Response.Charset = "";
            Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1254");
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            grid.RenderControl(htw);

            Response.Write(sw.ToString());

            Response.End();


            return null;
        }



        #endregion


        #region Alış iade Raporu
        public ActionResult PurchaseReturnReport(DocumentDateFilterModel dateFilter, int? page, string mekanFilter = "", string mekanAdFilter = "")
        {
            var pageLength = 29;

            var ayar = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 765);
            ViewBag.KategoriYok = ayar == null ? false : byte.Parse(ayar.ayarDeger) > 0;
            ViewData["mekan"] = mekanFilter;
            ViewData["mekanAd"] = mekanAdFilter;

            var mekanlar = new List<int>();
            string mekanAd = string.Empty;

            dateFilter.NotNullOrSetDefault(DateHelper.ThisMonthBegin, DateTime.Today);

            string sorgu = string.Format(@" exec [b2b].[sp_PurchaseReturnReport_Vw] @frmID={0},@page={1},@pageLength={2},@basTarih='{3}',@bitTarih='{4}',@mekan ='{5}' ", CurrentCompany.frmID, page ?? 1, pageLength, dateFilter.Begin.Value.Date.ToString("dd.MM.yyyy"), dateFilter.End.Value.Date.ToString("dd.MM.yyyy"), mekanFilter);
            var sipAyrList = Db.Database.SqlQuery<PurchaseReturnModel.PurchaseReturn>(sorgu).ToList();

            var topUrun = sipAyrList.Count > 0 ? (sipAyrList.FirstOrDefault().topSay) : 0;
            int topSayfa = (topUrun / pageLength) + (topUrun % pageLength > 0 ? 1 : 0);
            if ((topUrun % pageLength == 0))
            {
                pageLength = pageLength + 1;
            }

            var model = new PurchaseReturnModel
            {
                DateFilter = dateFilter,
                Page = page ?? 1,
                PageLength = pageLength,
                Mekanlar = mekanFilter,
                PurchaseReturns = sipAyrList.ToSqlPagedList(page ?? 1, pageLength, sipAyrList.Count > 0 ? (sipAyrList.FirstOrDefault().topSay) : 0)
            };

            return View(model);

        }

        public ActionResult PurchaseReturnReportExport(string mekanFilter = "", string ilkTar = "", string sonTar = "")
        {

            string ilkTarKosul = DateTime.Now.Date.AddDays(-30).ToString("dd.MM.yyyy");
            string sonTarKosul = DateTime.Now.Date.ToString("dd.MM.yyyy");

            if (ilkTar != "")
            {
                try
                {
                    ilkTarKosul = Convert.ToDateTime(ilkTar).Date.ToString("dd.MM.yyyy");
                }
                catch
                {
                }
            }
            if (sonTar != "")
            {
                try
                {
                    sonTarKosul = Convert.ToDateTime(sonTar).Date.ToString("dd.MM.yyyy");
                }
                catch
                {
                }
            }


            string sorgu = string.Format(@" exec [b2b].[sp_PurchaseReturnReport_Vw] @frmID={0},@page=0,@pageLength=0,@basTarih='{1}',@bitTarih='{2}',@mekan ='{3}' ", CurrentCompany.frmID, ilkTarKosul, sonTarKosul, mekanFilter);
            var liste = Db.Database.SqlQuery<PurchaseReturnReportExcel>(sorgu).ToList();

            if (liste.Count() > 0)
            {
                var grid = new System.Web.UI.WebControls.GridView();

                grid.DataSource = liste;

                grid.DataBind();

                Response.ClearContent();
                Response.AddHeader("content-disposition", "attachment; filename=AlisIadeRaporu.xls");
                Response.ContentType = "application/excel";
                StringWriter sw = new StringWriter();
                Encoding.GetEncoding(1254).GetBytes(sw.ToString());
                Response.Charset = "";
                Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1254");
                HtmlTextWriter htw = new HtmlTextWriter(sw);

                grid.RenderControl(htw);

                Response.Write(sw.ToString());

                Response.End();
            }

            return null;
        }



        #endregion


        #region Mekansal Stok Raporu Genel
        public ActionResult StoreStockReport(string q, int? page)
        {
            var pageLength = 29;

            var mekanlar = Db.posMagaza.Where(x => x.mekanTip == 0).Select(x => new { x.mekanAd }).ToList();


            string aramaKriteri = "";
            if (!String.IsNullOrWhiteSpace(q))
            {
                aramaKriteri = q;
            }

            string query = string.Format(@"     declare @frmID int={0}
	                                            declare @page int={1}
                                                declare @pageLength int={2}
	
	                                            declare @Kolonlar nvarchar(max)=''
                                                set @Kolonlar=(Select  Substring((Select ',[' + mekanAd +'-STOK]' From posMagaza M
											                                              Where M.mekanTip=0 order by mekanAd For XML Path('')),2,8000) As mekanlarAdet   )
		
                                                declare @KolonlarTutar nvarchar(max)=''
                                                set @KolonlarTutar=(Select  Substring((Select ',[' + mekanAd +'-STOK-TUTAR]' From posMagaza M
											                                              Where M.mekanTip=0 order by mekanAd For XML Path('')),2,8000) As mekanlarTutar   )

                                                declare @SelectKolonlarTutar nvarchar(max)=''
                                                set @SelectKolonlarTutar=(Select  Substring((Select ',sum(isnull([' + mekanAd +'-STOK],0)) as [' + mekanAd +'-STOK],sum(isnull([' + mekanAd +'-STOK-TUTAR],0)) as [' + mekanAd +'-STOK-TUTAR]' From posMagaza M
											                                              Where M.mekanTip=0 order by mekanAd For XML Path('')),2,80000) As mekanlarTutar   )
	
                                              
	                                            declare @sorguTemp1 nvarchar(max)=''
	                                            SET @sorguTemp1 ='
                                                                    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 
                                                                    select topSay,stkID,stkKod ,dagiticiKod,barkod,stkAd,'+@SelectKolonlarTutar+'
					                                               from (
							                                               select * 
							                                               from (
  
											                                            SELECT   urn.stkID,urn.stkKod,urnFrm.urnFrmDagitici as dagiticiKod,barkod,urn.stkAd,SUM(ehAdetN) AS stok
											                                            ,convert(decimal,SUM((ehAdetN)*isnull((sonrakiNet),fiyatA*(1-stki1/100)*(1-stki2/100)*(1-stki3/100))/(case when frmFiyatTur=0  then 1 else (1+CAST(urnKDV.kdvYuzde AS decimal(5, 2))/100) end))) AS stokTutar 
											                                            ,mekanAd+'+char(39)+'-STOK'+char(39)+' as mekanStok,mekanAd+'+char(39)+'-STOK-TUTAR'+char(39)+' as mekanStokTutar 
											                                            ,COUNT(*) OVER() as topSay           
											                                            FROM urn 
                                                                                        INNER JOIN urnBarkod_vw ON urn.stkID = urnBrkdStkID
					                                                                    INNER JOIN urnFrm ON urn.stkFirma = urnFrm.urnFrmFirmaID and urn.stkID = urnFrm.urnFrmStkID
                                                                                        inner join frm on frm.frmID=urn.stkFirma
											                                            inner join irsHrk ON ehStkID=urn.stkID 
											                                            inner hash join posMagaza ON mekanID=ehMekan 
											                                            inner join urnKDV on urn.KDVa=urnKDV.kdvID 
											                                            left hash join fn_fytOzlListe(cast(getdate() as date),1,1) on urn.stkID=fStkID 
											                                            WHERE urnTip = 0 and ehTrhS<=GETDATE()  and urnFrm.urnFrmFirmaID ='+cast(@frmID as nvarchar)+' 
                                                                                        " + (aramaKriteri != "" ? ("and urn.stkAd like '+char(39)+'%" + aramaKriteri + @"%'+char(39)+' ") : "") + @"
                                                                                        AND EXISTS
                                                                                        (
                                                                                            SELECT *
                                                                                            FROM   urnOzt
                                                                                            WHERE  StkID = oztStkID AND oztFrmID IN
                                                                                            (
                                                                                            SELECT frmID
                                                                                            FROM   frm
                                                                                            WHERE  frm.frmID = '+cast(@frmID as nvarchar)+' 
                                                                                            )
                                                                                        ) 
  
											                                            GROUP BY   urn.stkID, urn.stkKod,urn.stkAd,posMagaza.mekanAd ,urnFrm.urnFrmDagitici ,barkod
							                                               ) as anaTable
							                                               PIVOT
							                                               (
								                                              SUM(stok)
								                                              FOR mekanStok in ('+@Kolonlar+')
							                                               ) as PivotTable 
							                                               PIVOT
							                                               (
								                                              SUM(stokTutar)
								                                              FOR mekanStokTutar in ('+@KolonlarTutar+')
							                                               ) as PivotTable2 
					                                              ) as butunTable group by stkID,stkKod,stkAd,topSay, dagiticiKod,barkod order by stkAd,stkKod,stkID, dagiticiKod,barkod
                                                                   OFFSET  ('+cast((@page-1) as nvarchar)+' * '+cast(@pageLength as nvarchar)+') ROWS 
					                                               FETCH NEXT ('+cast(@pageLength as nvarchar)+') ROWS ONLY '


                                               exec sp_executesql @sorguTemp1 ", CurrentCompany.frmID, page ?? 1, pageLength);

            DataTable dt = new DataTable();
            string conBag = System.Configuration.ConfigurationManager.ConnectionStrings["DbEntities"].ConnectionString;
            int baslangic = System.Configuration.ConfigurationManager.ConnectionStrings["DbEntities"].ConnectionString.IndexOf("provider connection string");
            if (baslangic > -1)
            {
                conBag = conBag.Substring(baslangic + 28, conBag.Length - baslangic - 29);
            }
            using (SqlConnection con = new SqlConnection(conBag))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    cmd.Connection = con;
                    con.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                    con.Close();
                }
            }



            var donenSonuc = (from DataRow rw1 in dt.Rows
                              select new GeneralStoreSalesModel.GStoreSale
                              {
                                  stkID = Convert.ToInt32(rw1["stkID"]),
                                  stkKod = rw1["stkKod"].ToString(),
                                  dagiticiKod = rw1["dagiticiKod"].ToString(),
                                  barkod = rw1["barkod"].ToString(),
                                  stkAd = rw1["stkAd"].ToString(),
                                  topSay = Convert.ToInt32(rw1["topSay"]),
                                  satir = rw1,
                              });

            var kBaslik = (from DataColumn cl1 in dt.Columns
                           where cl1.ColumnName != "stkID" && cl1.ColumnName != "stkKod" && cl1.ColumnName != "stkAd" && cl1.ColumnName != "topSay" && cl1.ColumnName != "dagiticiKod" && cl1.ColumnName != "barkod"
                           select new List<string>
                           {
                                cl1.ColumnName
                           });



            var model = new GeneralStoreSalesModel
            {
                Sorgu = aramaKriteri,
                Page = page ?? 1,
                cBaslik = kBaslik,
                sBaslik = kBaslik.Count(),
                GStoreSales = donenSonuc.ToSqlPagedList(page ?? 1, pageLength, donenSonuc.Count() > 0 ? (donenSonuc.FirstOrDefault().topSay) : 0)
            };

            var ayar = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 765);
            ViewBag.KategoriYok = ayar == null ? false : byte.Parse(ayar.ayarDeger) > 0;

            return View(model);

        }

        public ActionResult StoreStockReportExport(string q)
        {
            string aramaKriteri = "";
            if (!String.IsNullOrWhiteSpace(q))
            {
                aramaKriteri = q;
            }

            string query = string.Format(@"     declare @frmID int={0}
	
	                                            declare @Kolonlar nvarchar(max)=''
                                                set @Kolonlar=(Select  Substring((Select ',[' + mekanAd +'-STOK]' From posMagaza M
											                                              Where M.mekanTip=0 order by mekanAd For XML Path('')),2,8000) As mekanlarAdet   )
		
                                                declare @KolonlarTutar nvarchar(max)=''
                                                set @KolonlarTutar=(Select  Substring((Select ',[' + mekanAd +'-STOK-TUTAR]' From posMagaza M
											                                              Where M.mekanTip=0 order by mekanAd For XML Path('')),2,8000) As mekanlarTutar   )

                                                declare @SelectKolonlarTutar nvarchar(max)=''
                                                set @SelectKolonlarTutar=(Select  Substring((Select ',sum(isnull([' + mekanAd +'-STOK],0)) as [' + mekanAd +'-STOK],sum(isnull([' + mekanAd +'-STOK-TUTAR],0)) as [' + mekanAd +'-STOK-TUTAR]' From posMagaza M
											                                              Where M.mekanTip=0 order by mekanAd For XML Path('')),2,80000) As mekanlarTutar   )
	
                                                declare @SelectTopKolonlarStok nvarchar(max)=''
                                                set @SelectTopKolonlarStok=(Select  Substring((Select '+sum(isnull([' + mekanAd +'-STOK],0))' From posMagaza M
	                                                                                      Where M.mekanTip=0 order by mekanAd For XML Path('')),2,80000) As topMekanStok )
	
                                                declare @SelectTopKolonlarTutar nvarchar(max)=''
                                                set @SelectTopKolonlarTutar=(Select  Substring((Select '+sum(isnull([' + mekanAd +'-STOK-TUTAR],0))' From posMagaza M
	                                                                                      Where M.mekanTip=0 order by mekanAd For XML Path('')),2,80000) As topMekanTutar )
	

	                                            declare @sorguTemp1 nvarchar(max)=''
	                                            SET @sorguTemp1 ='
                                                                    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 
                                                                    select stkID,stkKod,dagiticiKod,barkod,stkAd,'+@SelectKolonlarTutar+','+@SelectTopKolonlarStok+' as [TOPLAM STOK],'+@SelectTopKolonlarTutar+' as [TOPLAM TUTAR] 
					                                               from (
							                                               select * 
							                                               from (
											                                            SELECT   urn.stkID,urn.stkKod,urnFrm.urnFrmDagitici as dagiticiKod,barkod,urn.stkAd,SUM(ehAdetN) AS stok
											                                            ,convert(decimal,SUM((ehAdetN)*isnull((sonrakiNet),fiyatA*(1-stki1/100)*(1-stki2/100)*(1-stki3/100))/(case when frmFiyatTur=0  then 1 else (1+CAST(urnKDV.kdvYuzde AS decimal(5, 2))/100) end))) AS stokTutar 
											                                            ,mekanAd+'+char(39)+'-STOK'+char(39)+' as mekanStok,mekanAd+'+char(39)+'-STOK-TUTAR'+char(39)+' as mekanStokTutar 
											                                            FROM urn 
                                                                                        INNER JOIN urnBarkod_vw ON urn.stkID = urnBrkdStkID
					                                                                    INNER JOIN urnFrm ON urn.stkFirma = urnFrm.urnFrmFirmaID and urn.stkID = urnFrm.urnFrmStkID
                                                                                        inner join frm on frm.frmID=urn.stkFirma
											                                            inner join irsHrk ON ehStkID=urn.stkID 
											                                            inner hash join posMagaza ON mekanID=ehMekan 
											                                            inner join urnKDV on urn.KDVa=urnKDV.kdvID 
											                                            left hash join fn_fytOzlListe(cast(getdate() as date),1,1) on urn.stkID=fStkID 
											                                            WHERE urnTip = 0 and ehTrhS<=GETDATE()  and urnFrm.urnFrmFirmaID ='+cast(@frmID as nvarchar)+' 
                                                                                        " + (aramaKriteri != "" ? ("and urn.stkAd like '+char(39)+'%" + aramaKriteri + @"%'+char(39)+' ") : "") + @" 
                                                                                        AND EXISTS
                                                                                        (
                                                                                            SELECT *
                                                                                            FROM   urnOzt
                                                                                            WHERE  StkID = oztStkID AND oztFrmID IN
                                                                                            (
                                                                                            SELECT frmID
                                                                                            FROM   frm
                                                                                            WHERE  frm.frmID = '+cast(@frmID as nvarchar)+' 
                                                                                            )
                                                                                        )  
											                                            GROUP BY   urn.stkID, urn.stkKod,urn.stkAd,posMagaza.mekanAd,urnFrm.urnFrmDagitici,barkod 
							                                               ) as anaTable
							                                               PIVOT
							                                               (
								                                              SUM(stok)
								                                              FOR mekanStok in ('+@Kolonlar+')
							                                               ) as PivotTable 
							                                               PIVOT
							                                               (
								                                              SUM(stokTutar)
								                                              FOR mekanStokTutar in ('+@KolonlarTutar+')
							                                               ) as PivotTable2 
					                                              ) as butunTable group by stkID,stkKod,stkAd,dagiticiKod,barkod order by stkAd,stkKod,stkID,dagiticiKod,barkod '


                                               exec sp_executesql @sorguTemp1 ", CurrentCompany.frmID);

            DataTable dt = new DataTable();
            string conBag = System.Configuration.ConfigurationManager.ConnectionStrings["DbEntities"].ConnectionString;
            int baslangic = System.Configuration.ConfigurationManager.ConnectionStrings["DbEntities"].ConnectionString.IndexOf("provider connection string");
            if (baslangic > -1)
            {
                conBag = conBag.Substring(baslangic + 28, conBag.Length - baslangic - 29);
            }
            using (SqlConnection con = new SqlConnection(conBag))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    cmd.Connection = con;
                    con.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                    con.Close();
                }
            }


            if (dt.Rows.Count > 0)
            {
                var grid = new System.Web.UI.WebControls.GridView();

                grid.DataSource = dt;

                grid.DataBind();

                Response.ClearContent();
                Response.AddHeader("content-disposition", "attachment; filename=MagazaStokRaporu.xls");
                Response.ContentType = "application/excel";
                StringWriter sw = new StringWriter();
                Encoding.GetEncoding(1254).GetBytes(sw.ToString());
                Response.Charset = "";
                Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1254");

                HtmlTextWriter htw = new HtmlTextWriter(sw);








                grid.RenderControl(htw);

                //Response.Write(sw.ToString());
                Response.Write(" <meta http-equiv='Content-Type' content='text/html; charset=windows-1254' />" + sw.ToString());

                Response.End();
            }

            return null;
        }


        #endregion







    }
}