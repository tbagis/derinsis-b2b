﻿using System;
using System.Collections.Generic;

namespace DerinSIS.B2B.Controllers
{
    using System;
    using System.Net;
    using System.Xml.Linq;
    using System.Linq;
    using System.Web.Mvc;
    using Infrastructure.Auth;
    using Models;
    using ViewModels;
    using System.Web;
    using DerinSIS.B2B.Helpers;
    using System.IO;

    public class ServiceController : BaseController
    {
        public ServiceController(DbEntities db)
            : base(db)
        {
        }
        private HttpException HataDondur(string mesaj)
        {
            return new HttpException(401, mesaj);
        }

        private frmYetkili user;

        private bool CheckSafeRedirectUrl(string url)
        {
            return Url.IsLocalUrl(url) &&
                   url.Length > 1 &&
                   url.StartsWith("/") &&
                   !url.StartsWith("//") &&
                   !url.StartsWith("/\\");
        }

        private string ThumbRoot
        {
            get
            {
                return Db.drn2ayar
                    .Single(x => x.ayarID == 410)
                    .ayarDeger;
            }
        }

        public ProductSlideShowModel ProductSlideShow(int id, string ayarDeger)
        {
            var folderName = string.Format("U{0}", id);
            var folderPath = Path.Combine(ayarDeger, folderName);

            var model = new ProductSlideShowModel();
            if (Directory.Exists(folderPath) && Directory.EnumerateFiles(folderPath).Any())
            {
                model.Images = Directory.EnumerateFiles(folderPath)
                    .Select(Path.GetFileName)
                    .Select(x => Path.Combine(folderName, x))
                    .ToList();
            }

            return model;
        }



        public string GetProduct(string email, string sifre)
        {
            var xDoc = new XDocument();
            try
            {
                email = email.ToLowerInvariant();
                user = Db.frmYetkili.SingleOrDefault(x => x.ytkEposta.ToLower() == email);

                if (user == null || !user.CheckPassword(sifre))
                {
                    throw HataDondur("Hatalı kullanıcı adı ve/veya şifre");
                }

                if (user.ytkB2B == 0 || user.ytkB2B == 1)
                {
                    throw HataDondur("B2B Sistemine Giriş Yetkiniz Bulunmamaktadır.");
                }

                if (user.ytkB2bFaturaYetkisi == 0 && user.ytkB2bMutabakatYetkisi == 0 && user.ytkB2bSevkiyatYetkisi == 0 && user.ytkB2bSiparisYetkisi == 0)
                {
                    throw HataDondur("Yetkileriniz belirlenmemiştir. Lütfen firma yetkiliniz ile iletişime geçiniz.");
                }

                var frmTip = user.frm.frmTip;
                if (frmTip != 0 /* Suppliers */ && frmTip != 1 /* Customers */)
                {
                    throw HataDondur("B2B Sistemi Sadece Müşteriler ve Ürün Sağlayıcılar içindir.");
                }

                var _currentCompany = user.frm;

                CheckSafeRedirectUrl("/");

                XDocument doc = new XDocument
                 (
                       new XDeclaration("1.0", "utf-8", "yes"),
                           new XElement("urunler"));


                var stkKontroluVar = StkKontroluVar;

                var query = (
                                             from urnSatis_vw in Db.urnSatis_vw
                                             join urnBarkod_vw in Db.urnBarkod_vw on urnSatis_vw.stkID equals urnBarkod_vw.urnBrkdStkID
                                             let urnKdv = Db.urnKDV.Where(x => x.kdvID == urnSatis_vw.KDVs).FirstOrDefault()
                                             let indirim = Db.sakMrk_vw
                                             .Where(x => x.skFrmID == _currentCompany.frmID)
                                             .Where(x => x.skTur == _currentCompany.frmVadeTur)
                                             .FirstOrDefault(x => x.skMrkID == urnSatis_vw.urnMrkID)
                                             let ekIndirim = Db.fytOzl
                                             .Where(x => x.fStkID == urnSatis_vw.stkID)
                                             .Where(x => x.fTip == 4)
                                             .Where(x => x.fTarih <= DateTime.Today.Date && x.fTarihSon >= DateTime.Today.Date)
                                             .Where(x => x.fTur == 0)
                                             .Where(x => x.onay == 1)
                                             .OrderByDescending(x => x.fTarih)
                                             .ThenByDescending(x => x.fID)
                                             .FirstOrDefault()
                                             let katgr = Db.urnKtgrAg
                                             .Where(x => x.k0 == urnSatis_vw.urnKtgrID)
                                             .Where(x => x.k1 == urnSatis_vw.urnKtgrID1)
                                             .Where(x => x.k2 == urnSatis_vw.urnKtgrID2)
                                             .Where(x => x.k3 == urnSatis_vw.urnKtgrID3)
                                             .FirstOrDefault()
                                             select new ProductListModel.GetProductModel
                                                          {
                                                              Id = urnSatis_vw.stkID,
                                                              StokKodu = urnSatis_vw.stkKod,
                                                              kategori1 = katgr.k0Ad != null ? katgr.k0Ad : "",
                                                              kategori2 = katgr.k1Ad != null ? katgr.k1Ad : "",
                                                              kategori3 = katgr.k2Ad != null ? katgr.k2Ad : "",
                                                              kategori4 = katgr.k3Ad != null ? katgr.k3Ad : "",
                                                              kategoriKod1 = katgr.k0,
                                                              kategoriKod2 = katgr.k1,
                                                              kategoriKod3 = katgr.k2,
                                                              kategoriKod4 = katgr.k3,
                                                              KDVOrani = urnKdv.kdvYuzde,
                                                              KDVHaricFiyat = _currentCompany.frmFiyatTur == 1 ? urnSatis_vw.fiyatS1 : (_currentCompany.frmFiyatTur == 2 ? urnSatis_vw.fiyatS2 : (_currentCompany.frmFiyatTur == 3 ? urnSatis_vw.fiyatS3 : (_currentCompany.frmFiyatTur == 4 ? urnSatis_vw.fiyatS4 : urnSatis_vw.fiyatS))),
                                                              Indirim1 = indirim.skY1 != null ? indirim.skY1 : 0,
                                                              Indirim2 = indirim.skY2 != null ? indirim.skY2 : 0,
                                                              Indirim3 = indirim.skY3 != null ? indirim.skY3 : 0,
                                                              Indirim4 = ekIndirim.fInd1 != null ? ekIndirim.fInd1 : 0,
                                                              Isim = urnSatis_vw.stkAd,
                                                              markaAd = urnSatis_vw.urnMrkGrpAd != "" ? urnSatis_vw.urnMrkGrpAd : urnSatis_vw.mrkAd,
                                                              Birim = urnSatis_vw.odemeKisaAd,
                                                              stok = stkKontroluVar == 0 ? 0 : urnSatis_vw.stok,
                                                              stokKontrolu = stkKontroluVar,
                                                              PSF = urnSatis_vw.PSF
                                                          }
                                         ).ToList();


                //  .ToList().Cast<ProductListModel.ProductListItemModel>();
                string aDegeR = ThumbRoot;
                foreach (var item in query)
                {

                    List<string> GelenBarkodlar = Db.urnBrkd.Where(x => x.urnBrkdStkID == item.Id || x.urnBrkdAltStkId == item.Id)
                                                                                          .OrderBy(x => x.urnBrkdOnce)
                                                                                          .Select(x => x.urnBarkod).ToList();
                    string resim1 = "";
                    string resim2 = "";
                    string resim3 = "";
                    string resim4 = "";
                    string resim5 = "";

                    var Model = ProductSlideShow(item.Id, aDegeR);
                    int sayac = 1;
                    foreach (var image in Model.Images)
                    {
                        if (sayac == 1)
                            resim1 = Url.Action("FitThumb", "Thumbnail", new { name = image, width = 800, height = 600 }, this.Request.Url.Scheme);
                        else if (sayac == 2)
                            resim2 = Url.Action("FitThumb", "Thumbnail", new { name = image, width = 800, height = 600 }, this.Request.Url.Scheme);
                        else if (sayac == 3)
                            resim3 = Url.Action("FitThumb", "Thumbnail", new { name = image, width = 800, height = 600 }, this.Request.Url.Scheme);
                        else if (sayac == 4)
                            resim4 = Url.Action("FitThumb", "Thumbnail", new { name = image, width = 800, height = 600 }, this.Request.Url.Scheme);
                        else if (sayac == 5)
                            resim5 = Url.Action("FitThumb", "Thumbnail", new { name = image, width = 800, height = 600 }, this.Request.Url.Scheme);
                        else
                            break;

                        sayac = sayac + 1;
                    }

                    doc.Root.Add(
                         new XElement("urun",
                             new XElement("stockCode", item.Id),
                             new XElement("Kategori1", item.kategori1),
                             new XElement("Kategori2", item.kategori2),
                             new XElement("Kategori3", item.kategori3),
                             new XElement("Kategori4", item.kategori4),
                             new XElement("KategoriCode", item.kategoriKod1.ToString() + "." + item.kategoriKod2.ToString() + "." + item.kategoriKod3.ToString() + "." + item.kategoriKod4.ToString()),
                             new XElement("Barkod1", GelenBarkodlar.Count > 0 ? GelenBarkodlar[0] : ""),
                             new XElement("Barkod2", GelenBarkodlar.Count > 1 ? GelenBarkodlar[1] : ""),
                             new XElement("Barkod3", GelenBarkodlar.Count > 2 ? GelenBarkodlar[2] : ""),
                             new XElement("Barkod4", GelenBarkodlar.Count > 3 ? GelenBarkodlar[3] : ""),
                             new XElement("Barkod5", GelenBarkodlar.Count > 4 ? GelenBarkodlar[4] : ""),
                             new XElement("Barkod6", GelenBarkodlar.Count > 5 ? GelenBarkodlar[5] : ""),
                             new XElement("KDV", item.KDVOrani.ToString()),
                             new XElement("isim", item.Isim.Replace((char)0x1E, ' ')),
                             new XElement("marka", item.markaAd),
                             new XElement("price1", item.KDVDahilToplam.ToString()),
                             new XElement("birim", item.Birim),
                             new XElement("stock", item.stok.ToString()),
                             new XElement("resim1", resim1),
                             new XElement("resim2", resim2),
                             new XElement("resim3", resim3),
                             new XElement("resim4", resim4),
                             new XElement("resim5", resim5),
                             new XElement("PSF", item.PSF),
                             new XElement("details", ""),
                             new XElement("Aciklama", "")
                             ));

                }

                xDoc = doc;

            }
            catch (Exception ex)
            {
                throw HataDondur("Hatalı kullanıcı adı ve/veya şifre");
            }
            string _xDoc = xDoc.ToString().Replace("&amp;", "&");

            return _xDoc;

        }


    }
}