﻿


namespace DerinSIS.B2B.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Linq;
    using Helpers;
    using Infrastructure.Auth;
    using Models;
    using ViewModels;
    using System.IO;
    using System.Web.UI;
    using System.Data;
    using System.Data.SqlClient;
    using System.Text;
    using System.Xml;
    using System.Xml.Linq;

    [B2BAuthorize(AccountType.Supplier | AccountType.Customer, Privilege.SEVKIYAT)]
    public class SalesController : BaseController
    {
        private readonly SystemSettings _settings;
        private readonly List<int> saleTypes;
        private readonly IEnumerable<int> product;
        private readonly List<int> totalSale;
        private readonly List<int> refundofSale;


        public SalesController(DbEntities db, SystemSettings settings)
            : base(db)
        {
            _settings = settings;
            saleTypes = new List<int>() { 1, 3, 4, 5, 100, 101 };
            product = Db.urnFrm.Where(x => x.urnFrmFirmaID == CurrentCompany.frmID).Select(x => x.urnFrmStkID);
            totalSale = new List<int>() { 1, 4, 100 };
            refundofSale = new List<int>() { 3, 5, 101 };

        }

        //*Onaysizsiparisgöster değerleri*//

        //0-Tümünü göster
        //1-Alış yönlülerini gösterme
        //2-Satış yönlülerini gösterme
        //3-Tümünü gösterme

        #region Dashboard

        public ActionResult Index()
        {
            // Bekleyen Siparişler


            var bekleyenSipGoster = onaysizSiparisGoster();

            var firma = CurrentAccount.frm;
            var bekleyenSiparisler = new List<SalesDashboardModel.BekleyenSiparis>();
            if (bekleyenSipGoster)
            {
                bekleyenSiparisler = Db.sip
                .Where(x => x.frm.frmID == CurrentAccount.frm.frmID)
                .Where(x => x.eDurum == 0) // Açık Siparişler
                .OrderBy(x => x.eTarihS)
                .Select(x => new SalesDashboardModel.BekleyenSiparis
                {
                    Id = x.eID,
                    SiparisTarihi = x.eTarih,
                    SevkTarihi = x.eTarihS,
                    OnayliMi = x.onay == 1,
                })
                .ToList();
            }
            // Yeni eklenen ürünler
            var newProductDaysRange = 30;
            IEnumerable<SalesDashboardModel.YeniUrun> yeniUrunler;
            if (CurrentAccountType == AccountType.Customer)
            {
                var setting = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 760);
                if (setting != null)
                {
                    newProductDaysRange = int.Parse(setting.ayarDeger);
                }
                var newProductRange = DateTime.Today.AddDays(-1 * newProductDaysRange);

                yeniUrunler = Db.urnSatis_vw
                    .Where(x => x.gTarih >= newProductRange)
                    .Select(x => new SalesDashboardModel.YeniUrun
                    {
                        Id = x.stkID,
                        UrunAdi = x.stkAd,
                        UrunFiyati = CurrentCompany.frmFiyatTur == 1 ? x.fiyatS1 : (CurrentCompany.frmFiyatTur == 2 ? x.fiyatS2 : (CurrentCompany.frmFiyatTur == 3 ? x.fiyatS3 : (CurrentCompany.frmFiyatTur == 4 ? x.fiyatS4 : x.fiyatS))),
                        EklenmeTarihi = x.gTarih
                    });
            }
            else
            {
                var setting = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 761);
                if (setting != null)
                {
                    newProductDaysRange = int.Parse(setting.ayarDeger);
                }
                var newProductRange = DateTime.Today.AddDays(-1 * newProductDaysRange);

                yeniUrunler = Db.urnAlis_vw
                    .Where(x => x.urnFrmFirmaID == CurrentCompany.frmID)
                    .Where(x => x.gTarih >= newProductRange)
                    .Select(x => new SalesDashboardModel.YeniUrun
                    {
                        Id = x.stkID,
                        UrunAdi = x.stkAd,
                        UrunFiyati = x.fiyatA,
                        EklenmeTarihi = x.gTarih,
                    });
            }


            // Sipariş Günleri
            var siparisGunleri = new List<SalesDashboardModel.SiparisGunu>
                                     {
                                         new SalesDashboardModel.SiparisGunu
                                             {
                                                 HaftaninGunu = 1,
                                                 GunIsmi = "Pazartesi",
                                                 SiparisVerilebilirMi = firma.frmSipGun1 > 0
                                             },
                                         new SalesDashboardModel.SiparisGunu
                                             {
                                                 HaftaninGunu = 2,
                                                 GunIsmi = "Salı",
                                                 SiparisVerilebilirMi = firma.frmSipGun2 > 0
                                             },
                                         new SalesDashboardModel.SiparisGunu
                                             {
                                                 HaftaninGunu = 3,
                                                 GunIsmi = "Çarşamba",
                                                 SiparisVerilebilirMi = firma.frmSipGun3 > 0,
                                             },
                                         new SalesDashboardModel.SiparisGunu
                                             {
                                                 HaftaninGunu = 4,
                                                 GunIsmi = "Perşembe",
                                                 SiparisVerilebilirMi = firma.frmSipGun4 > 0
                                             },
                                         new SalesDashboardModel.SiparisGunu
                                             {
                                                 HaftaninGunu = 5,
                                                 GunIsmi = "Cuma",
                                                 SiparisVerilebilirMi = firma.frmSipGun5 > 0
                                             },
                                         new SalesDashboardModel.SiparisGunu
                                             {
                                                 HaftaninGunu = 6,
                                                 GunIsmi = "Cumartesi",
                                                 SiparisVerilebilirMi = firma.frmSipGun6 > 0,
                                             },
                                         new SalesDashboardModel.SiparisGunu
                                             {
                                                 HaftaninGunu = 7,
                                                 GunIsmi = "Pazar",
                                                 SiparisVerilebilirMi = firma.frmSipGun7 > 0
                                             },
                                     };

            var model = new SalesDashboardModel
            {
                ControllerName = CurrentAccountType.Value == AccountType.Supplier ? "Supplier" : "Store",
                BekleyenSiparisler = bekleyenSiparisler,
                YeniUrunler = yeniUrunler,
                SiparisGunleri = siparisGunleri,
                acikSipGoster = bekleyenSipGoster
            };

            return View(model);
        }

        #endregion

        #region Orders

        public ActionResult Orders(DocumentDateFilterModel dateFilter, int? productFilter, int? documentTypeFilter)
        {

            var onaysizSipGoster = onaysizSiparisGoster();

            dateFilter.NotNullOrSetDefault(DateHelper.ThisMonthBegin, DateTime.Today);

            var orders =
                (
                    from sip in Db.sip
                    join frm in Db.frm on sip.eFirma equals frm.frmID
                    join sifTip in Db.sifTip on sip.eTip equals sifTip.hrktTipID
                    join sipNeden in Db.sipNeden on sip.Neden equals sipNeden.nedenID
                    join posMagaza in Db.posMagaza on sip.eMekan equals posMagaza.mekanID
                    join firmaMekan in Db.frm on sip.eFirmaMkn equals firmaMekan.frmID
                    /* Only Orders for siftip */
                    where sifTip.hrktSIF == 0
                    where documentTypeFilter == null || sifTip.hrktTipID == documentTypeFilter
                    where frm.frmID == CurrentAccount.frm.frmID
                    where sip.eTarih >= dateFilter.Begin && sip.eTarih <= dateFilter.End
                    where productFilter == null || sip.sipAyr.Select(x => x.ehStkID).Contains(productFilter.Value)
                    where (onaysizSipGoster == true || (onaysizSipGoster == false && sip.onay == 1))
                    orderby sip.eTarih descending, sip.eID descending
                    select new OrdersModel.Order
                    {
                        Id = sip.eID,
                        Tarih = sip.eTarih,
                        EvrakNo = sip.eNo,
                        HareketTipi = sifTip.hrktTipAd,
                        Durum = (OrdersModel.OrderStatus)sip.eDurum,
                        OnayliMi = sip.onay == 1,
                        VadeTarihi = sip.eTarihV,
                        EvrakNot = sip.eNot,
                        KDVHaricToplam = sip.sipAyr.Sum(x => (decimal?)x.ehTutar) ?? 0m,
                        ToplamIndirim = sip.sipAyr.Sum(x => (decimal?)x.ehIndirim) ?? 0m,
                        HareketGrubu = sipNeden.nedenAd,
                        SevkAdresi = frm.frmTip == 1 ? (sip.eFirmaMkn == 0 ? "" : firmaMekan.frmAd) : posMagaza.mekanAd
                    }
                )
                    .ToList();

            return View(new OrdersModel
            {
                Orders = orders,
                DateFilter = dateFilter,
                ProductFilterId = productFilter,
                ProductFilterLabel = productFilter.HasValue ? Db.urn.Single(x => x.stkID == productFilter).stkAd : "",
                GosterimSecenekleri =
                                    new OrdersModel.Settings
                                    {
                                        EvrakNotuGosterilsinMi = _settings.EvrakNotlariGosterilsin
                                    }
            });
        }

        public ActionResult OrderDetail(int? id)
        {
            Check.NotNullOr404(id);

            var onaysizSipGoster = onaysizSiparisGoster();

            var sip = Db.sip.FirstOrDefault(x => x.eID == id && (onaysizSipGoster == true || (onaysizSipGoster == false && x.onay == 1)));
            Check.NotNullOr404(sip);

            var frm = sip.frm;
            // Başka firmalara ait kayıt gösterilmemeli
            Check.CheckOr404(frm.frmID == CurrentAccount.frm.frmID);

            var sifTip = Db.sifTip.Where(x => x.hrktSIF == 0).Single(x => x.hrktTipID == sip.eTip);
            var sipNeden = Db.sipNeden.Single(x => x.nedenID == sip.Neden);

            var sevkAdresiAd = "";
            frm sevkAdresi;
            if (frm.frmTip == 1)
            {
                // sevk adresi ismini firma mekandan alıyoruz.
                var firmaMekan = sip.eFirmaMkn == 0 ? sip.frm : Db.frm.Single(x => x.frmID == sip.eFirmaMkn);
                sevkAdresiAd = firmaMekan.frmAd;
                sevkAdresi = firmaMekan;
            }
            else
            {
                // sevk adresi ismini posMagaza'dan alıyoruz    
                var posMagaza = Db.posMagaza.Single(x => x.mekanID == sip.eMekan);
                sevkAdresiAd = posMagaza.mekanAd;
                sevkAdresi = Db.frm.Single(x => x.frmID == sip.eMekan);
            }

            string gKisisi = "";
            var gKisiVeri = Db.frmYetkili.Where(x => x.ytkInsID == sip.gKisi).ToList();
            if (gKisiVeri.Count() > 0)
            {
                gKisisi = gKisiVeri.Select(x => new { kisiAd = x.ytkAd + " (" + x.ytkEposta + ")" }).FirstOrDefault().kisiAd;
            }
            else
            {
                gKisisi = Db.drn1.Where(x => x.insID == sip.gKisi).Select(x => new { kisiAd = x.insAd + " " + x.insSoyad }).FirstOrDefault().kisiAd;
            }

            string kKisisi = "";
            var kKisiVeri = Db.frmYetkili.Where(x => x.ytkInsID == sip.kKisi).ToList();
            if (kKisiVeri.Count() > 0)
            {
                kKisisi = kKisiVeri.Select(x => new { kisiAd = x.ytkAd + " (" + x.ytkEposta + ")" }).FirstOrDefault().kisiAd;
            }
            else
            {
                kKisisi = Db.drn1.Where(x => x.insID == sip.kKisi).Select(x => new { kisiAd = x.insAd + " " + x.insSoyad }).FirstOrDefault().kisiAd;
            }



            string sipAyrSorgu = @" select ehID as Id,ehSira as Sira,ehStkID as UrunId,stkAd as UrunIsmi,stkKod as UrunKod,isnull(barkod,'') as UrunBarkod
                                  ,ehAdet as ToplamAdet,cast(urnKoli as decimal(18,2)) as KoliIciAdet,cast(urnPalet as decimal(18,2)) as PaletIciAdet,paketOlcu as UrunAgirligi,paketBrm as AgirlikBirim,ehAdetN as KalanAdet,ehDurum
                                  ,ehi1 as Indirim1,ehi2 as Indirim2,ehi3 as Indirim3,ehi4 as Indirim4,ehi5 as Indirim5,ehIndirim as IndirimTutari,ehTutar as KDVHaricToplam,cast(kdvYuzde as int) as KDVYuzde  from sipAyr 
                                  inner join urn on stkID=ehStkID
                                  inner join urnKDV on kdvID=ehKdv
                                  left join urnBarkod_vw on ehSTkID=urnBrkdStkID where ehID=" + sip.eID + " ";
            var sipAyrList = Db.Database.SqlQuery<OrderDetailModel.OrderDetail>(sipAyrSorgu).ToList();

            var sevkAdresiilceAdi = "";
            var sevkAdresiilAdi = "";
            if (sevkAdresi != null)
            {
                var ilces = Db.frmKentIlce.SingleOrDefault(x => x.ilceKodu == sevkAdresi.ilceKod);
                if (ilces != null)
                    sevkAdresiilceAdi = ilces.ilceAdi;
                var ils = Db.frmKent.SingleOrDefault(x => x.alanKodu == sevkAdresi.il);
                if (ils != null)
                    sevkAdresiilAdi = ils.ilAd;
            }
                
               
            var model = new OrderDetailModel
            {
                Id = sip.eID,
                EvrakNo = sip.eNo,
                Tarih = sip.eTarih,
                SevkTarihi = sip.eTarihS,
                HareketTipi = sifTip.hrktTipAd,
                EvrakNot = sip.eNot,
                Evrak3SatirNot = sip.sipNot != null ? sip.sipNot.enNot : "",
                VadeTarihi = sip.eTarihV,
                HareketGrubu = sipNeden.nedenAd,
                Durum = (OrderDetailModel.OrderStatus)sip.eDurum,
                Onaylimi = sip.onay == 1,
                gKisiAd = gKisisi,
                gTarih = sip.gTarih,
                kKisiAd = kKisisi,
                kTarih = sip.kTarih,
                SevkAdresiAd = sevkAdresiAd,
                SevkAdresi1 = sevkAdresi == null ? "" : sevkAdresi.adres1,
                SevkAdresi2 = sevkAdresi == null ? "" : sevkAdresi.adres2,
                SevkAdresiIlce = sevkAdresiilceAdi,
                SevkAdresiIl = sevkAdresiilAdi,
                SevkAdresiPostaKodu = sevkAdresi == null ? "" : sevkAdresi.postaKod,
                SevkAdresiTel = sevkAdresi == null ? "" : sevkAdresi.tel1,
                SevkAdresiFaks = sevkAdresi == null ? "" : sevkAdresi.telFaks,
                ControllerName = CurrentAccountType.Value == AccountType.Supplier ? "Supplier" : "Store",
                // Sipariş Hareketleri (Ayrıntılar)
                EvrakHareketleri = sipAyrList,
                // İlişkili İrsaliyeler
                IlgiliIrsaliyeler = Db.irsAyr
                                    .Where(x => x.ehSipID == sip.eID)
                                    .Select(x => x.irs)
                                    .Distinct()
                                    .Select(x => new OrderDetailModel.RelatedDelivery
                                    {
                                        Id = x.eID,
                                        Tarih = x.eTarih,
                                        EvrakNo = x.eNo
                                    }),

                // İlişkili Faturalar
                IlgiliFaturalar = Db.fatAyr
                                    .Where(x => x.ehSipID == sip.eID)
                                    .Select(x => x.fat)
                                    .Distinct()
                                    .Select(x => new OrderDetailModel.RelatedInvoice
                                    {
                                        Id = x.eID,
                                        Tarih = x.eTarih,
                                        EvrakNo = x.eNo
                                    }),

                // Gösterim Seçenekleri
                GosterimSecenekleri =
                                    new OrderDetailModel.Settings
                                    {
                                        EvrakNotuGosterilsinMi = _settings.EvrakNotlariGosterilsin,
                                    }
            };


            return View(model);
        }

        public ActionResult OrderDetailExportClientsListToExcel(int sipIdsi)
        {
            var grid = new System.Web.UI.WebControls.GridView();

            grid.DataSource = (from x in Db.sipAyr
                               where x.ehID == sipIdsi
                               let barkod = Db.urnBarkod_vw.FirstOrDefault(y => y.urnBrkdStkID == x.ehStkID)
                               let kdv = Db.urnKDV.FirstOrDefault(z => z.kdvID == x.ehKDV)
                               select new //OrderDetailModel.OrderDetail
                               {
                                   // Id = x.ehID,
                                   // Sira = x.ehSira,
                                   UrunId = x.urn.stkID,
                                   UrunIsmi = x.urn.stkAd,
                                   UrunKod = x.urn.stkKod,
                                   UrunBarkod = barkod != null ? barkod.barkod : "",
                                   KoliAdet = Math.Floor(x.ehAdet / x.urn.urnKoli),
                                   KoliIciAdet = x.urn.urnKoli,
                                   ToplamAdet = x.ehAdet,
                                   // PaletIciAdet = x.urn.urnPalet,
                                   //UrunAgirligi = x.urn.paketOlcu,
                                   //AgirlikBirim = x.urn.paketBrm,
                                   KalanAdet = x.ehAdetN,
                                   BirimFiyat = x.ehTutar / x.ehAdet,
                                   KDVHaricToplam = x.ehTutar,
                                   IndirimTutari = x.ehIndirim,
                                   KDVHaricNet = x.ehTutar - x.ehIndirim,

                                   // Durum = (OrderDetailModel.OrderDetailStatus)x.ehDurum,
                                   // Indirim1 = x.ehi1,
                                   // Indirim2 = x.ehi2,
                                   // Indirim3 = x.ehi3,
                                   // Indirim4 = x.ehi4,
                                   // Indirim5 = x.ehi5,

                                   KDVYuzde = (kdv != null ? kdv.kdvYuzde : 0),
                                   KDVDahilTutar = (x.ehTutar - x.ehIndirim) * (1 + ((kdv != null ? kdv.kdvYuzde : 0) / 100m))
                                   // KDVTutar = x.ehTutarKDV,
                               }).ToList();

            grid.DataBind();

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=Exported_OrderDetail.xls");
            Response.ContentType = "application/excel";
            StringWriter sw = new StringWriter();
            Encoding.GetEncoding(1254).GetBytes(sw.ToString());
            Response.Charset = "";
            Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1254");
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            grid.RenderControl(htw);

            Response.Write(sw.ToString());

            Response.End();

            return null;
        }

        [HttpPost]
        public ActionResult AddToCopyCartAsync(int sipID)
        {

            var activeCart = Db.b2bSepet
                .Where(x => x.frmYetkili.ytkID == CurrentAccount.ytkID)
                .Single(x => x.Aktif);

            if (activeCart == null)
            {
                return Json("Aktif sepetiniz bulunmamaktadır.");
            }

            var sipAyrListe = Db.sipAyr
                .Where(x => x.ehID == sipID)
                .Select(x => new { stokID = x.ehStkID, Adeti = x.ehAdet });

            foreach (var cartItem in sipAyrListe)
            {
                var item = activeCart.b2bSepetAyr.SingleOrDefault(x => x.UrunId == cartItem.stokID);
                if (item == null)
                {
                    activeCart.b2bSepetAyr
                        .Add(new b2bSepetAyr
                        {
                            b2bSepet = activeCart,
                            urn = Db.urn.Single(x => x.stkID == cartItem.stokID),
                            Adet = cartItem.Adeti
                        });
                }
                else
                {
                    item.Adet += cartItem.Adeti;
                }
            }

            Db.SaveChanges();
            return Json("");


        }


        #endregion

        #region Deliveries

        public ActionResult Deliveries(DocumentDateFilterModel dateFilter, int? productFilter, int? documentTypeFilter)
        {
            dateFilter.NotNullOrSetDefault(DateHelper.ThisMonthBegin, DateTime.Today);

            var deliveries =
                (
                    from irs in Db.irs
                    join frm in Db.frm on irs.eFirma equals frm.frmID
                    join sifTip in Db.sifTip on irs.eTip equals sifTip.hrktTipID
                    join sipNeden in Db.sipNeden on irs.Neden equals sipNeden.nedenID
                    join posMagaza in Db.posMagaza on irs.eMekan equals posMagaza.mekanID
                    join firmaMekan in Db.frm on irs.eFirmaMkn equals firmaMekan.frmID
                    /* Only Orders for siftip */
                    where sifTip.hrktSIF == 1
                    where documentTypeFilter == null || sifTip.hrktTipID == documentTypeFilter
                    where frm.frmID == CurrentAccount.frm.frmID
                    where irs.eTarih >= dateFilter.Begin && irs.eTarih <= dateFilter.End
                    where productFilter == null || irs.irsAyr.Select(x => x.ehStkID).Contains(productFilter.Value)
                    orderby irs.eTarih descending, irs.eID descending
                    select new DeliveriesModel.Delivery
                    {
                        Id = irs.eID,
                        Tarih = irs.eTarih,
                        EvrakNo = irs.eNo,
                        HareketTipi = sifTip.hrktTipAd,
                        Durum = (DeliveriesModel.DeliveryStatus)irs.eDurum,
                        VadeTarihi = irs.eTarihV,
                        EvrakNot = irs.eNot,
                        KDVHaricToplam = irs.irsAyr.Sum(x => x.ehTutar),
                        ToplamIndirim = irs.irsAyr.Sum(x => x.ehIndirim),
                        HareketGrubu = sipNeden.nedenAd,
                        SevkAdresi = frm.frmTip == 1 ? (irs.eFirmaMkn == 0 ? "" : firmaMekan.frmAd) : posMagaza.mekanAd
                    }
                )
                    .ToList();

            return View(new DeliveriesModel
            {
                Deliveries = deliveries,
                DateFilter = dateFilter,
                ProductFilterId = productFilter,
                ProductFilterLabel = productFilter.HasValue ? Db.urn.Single(x => x.stkID == productFilter).stkAd : "",
                GosterimSecenekleri =
                    new DeliveriesModel.Settings
                    {
                        EvrakNotuGosterilsinMi = _settings.EvrakNotlariGosterilsin
                    }
            });
        }

        public ActionResult DeliveryDetail(int? id)
        {
            Check.NotNullOr404(id);

            var irs = Db.irs.SingleOrDefault(x => x.eID == id);
            Check.NotNullOr404(irs);

            var frm = irs.frm;
            // Başka firmalara ait kayıt gösterilmemeli
            Check.CheckOr404(frm.frmID == CurrentAccount.frm.frmID);

            var sifTip = Db.sifTip.Where(x => x.hrktSIF == 1).Single(x => x.hrktTipID == irs.eTip);
            var sipNeden = Db.sipNeden.Single(x => x.nedenID == irs.Neden);

            var sevkAdresiAd = "";
            frm sevkAdresi;
            if (frm.frmTip == 1)
            {
                // sevk adresi ismini firma mekandan alıyoruz.
                var firmaMekan = irs.eFirmaMkn == 0 ? irs.frm : Db.frm.Single(x => x.frmID == irs.eFirmaMkn);
                sevkAdresiAd = firmaMekan.frmAd;
                sevkAdresi = firmaMekan;
            }
            else
            {
                // sevk adresi ismini posMagaza'dan alıyoruz    
                var posMagaza = Db.posMagaza.Single(x => x.mekanID == irs.eMekan);
                sevkAdresiAd = posMagaza.mekanAd;
                sevkAdresi = Db.frm.Single(x => x.frmID == irs.eMekan);
            }

            string irsAyrSorgu = @" select ehID as Id,ehSira as Sira,ehStkID as UrunId,stkAd as UrunIsmi,stkKod as UrunKod,isnull(barkod,'') as UrunBarkod
                                  ,ehAdet as ToplamAdet,cast(urnKoli as decimal(18,2)) as KoliIciAdet,cast(urnPalet as decimal(18,2)) as PaletIciAdet,paketOlcu as UrunAgirligi,paketBrm as AgirlikBirim,ehAdetN as KalanAdet
                                  ,ehi1 as Indirim1,ehi2 as Indirim2,ehi3 as Indirim3,ehi4 as Indirim4,ehi5 as Indirim5,ehIndirim as IndirimTutari,ehTutar as KDVHaricToplam,cast(kdvYuzde as int) as KDVYuzde,ehTutarKDV as KDVTutar  from irsAyr 
                                  inner join urn on stkID=ehStkID
                                  inner join urnKDV on kdvID=ehKdv
                                  left join urnBarkod_vw on ehSTkID=urnBrkdStkID where ehID=" + irs.eID + " ";
            var irsAyrList = Db.Database.SqlQuery<DeliveryDetailModel.DeliveryDetail>(irsAyrSorgu).ToList();


            var model = new DeliveryDetailModel
            {
                Id = irs.eID,
                EvrakNo = irs.eNo,
                Tarih = irs.eTarih,
                SevkTarihi = irs.eTarihS,
                HareketTipi = sifTip.hrktTipAd,
                EvrakNot = irs.eNot,
                Evrak3SatirNot = irs.irsNot != null ? irs.irsNot.enNot : "",
                VadeTarihi = irs.eTarihV,
                HareketGrubu = sipNeden.nedenAd,
                Durum = (DeliveryDetailModel.DeliveryStatus)irs.eDurum,

                SevkAdresiAd = sevkAdresiAd,
                SevkAdresi1 = sevkAdresi.adres1,
                SevkAdresi2 = sevkAdresi.adres2,
                SevkAdresiIlce = Db.frmKentIlce.Single(x => x.ilceKodu == sevkAdresi.ilceKod).ilceAdi,
                SevkAdresiIl = Db.frmKent.Single(x => x.alanKodu == sevkAdresi.il).ilAd,
                SevkAdresiPostaKodu = sevkAdresi.postaKod,
                SevkAdresiTel = sevkAdresi.tel1,
                SevkAdresiFaks = sevkAdresi.telFaks,

                ControllerName = CurrentAccountType.Value == AccountType.Supplier ? "Supplier" : "Store",
                // Sipariş Hareketleri (Ayrıntılar)
                EvrakHareketleri = irsAyrList,

                // Gösterim Seçenekleri
                GosterimSecenekleri =
                    new DeliveryDetailModel.Settings
                    {
                        EvrakNotuGosterilsinMi = _settings.EvrakNotlariGosterilsin,
                    }
            };

            model.IlgiliSiparisler = (from irsAyr in Db.irsAyr
                                      where irsAyr.ehID == id
                                      join sip in Db.sip on irsAyr.ehSipID equals sip.eID
                                      select new DeliveryDetailModel.RelatedOrder
                                      {
                                          Id = sip.eID,
                                          Tarih = sip.eTarih,
                                          EvrakNo = sip.eNo
                                      }).Distinct().ToList();

            model.IlgiliFaturalar = (from fatAyr in Db.fatAyr
                                     join irs1 in Db.irs on fatAyr.ehIrsID equals irs1.eID
                                     where irs1.eID == id
                                     select new DeliveryDetailModel.RelatedInvoice
                                     {
                                         Id = fatAyr.ehID,
                                         Tarih = fatAyr.fat.eTarih,
                                         EvrakNo = fatAyr.fat.eNo
                                     }).Distinct().ToList();



            return View(model);
        }

        #endregion

        #region Sale MyDiscounts

        public ActionResult MyDiscounts()
        {
            var discounts = Db.sakMrk_vw
                .Where(x => x.skFrmID == CurrentAccount.frm.frmID)
                .Where(x => x.skTur == CurrentCompany.frmVadeTur)
                .Select(x => new MyDiscountModel
                {
                    MarkaId = x.skMrkID,
                    MarkaIsmi = Db.urnMrk.FirstOrDefault(y => y.mrkID == x.skMrkID).mrkAd,
                    Indirim1 = x.skY1,
                    Indirim2 = x.skY2,
                    Indirim3 = x.skY3,
                })
                .OrderBy(x => x.MarkaIsmi)
                .ToList();

            return View(discounts);
        }

        #endregion




        #region Kategori Bazlı Satış Raporu


        public ActionResult SalesPerCategoryReport(DocumentDateFilterModel dateFilter, string cityFilter = "", string groupFilter = "", string statueFilter = "", string categoryFilter = "", string brandFilter = "", string storeCount = "", string productCount = "")
        {

            dateFilter.NotNullOrSetDefault(DateHelper.LastMonthBegin, DateHelper.LastMonthEnd);
            var stores = (from irsAyr in Db.irsAyr
                          join irs in Db.irs on irsAyr.ehID equals irs.eID
                          join urn in Db.urn on irsAyr.ehStkID equals urn.stkID
                          join posMagaza in Db.posMagaza on irs.eMekan equals posMagaza.mekanID
                          join urnKtgrAg in Db.urnKtgrAg on urn.urnKtgrID equals urnKtgrAg.k0
                          where irs.eFirma == CurrentCompany.frmID
                          where urnKtgrAg.k1 == 0
                          where irs.eTarih >= dateFilter.Begin && irs.eTarih <= dateFilter.End
                          where product.Contains(irsAyr.ehStkID)
                          where saleTypes.Contains(irs.eTip) == false
                          select new { irs, irsAyr, urn, posMagaza, urnKtgrAg });


            ViewData["storeCount"] = storeCount;
            ViewData["productCount"] = productCount;
            ViewData["group"] = groupFilter;
            ViewData["city"] = cityFilter;
            ViewData["brand"] = brandFilter;
            ViewData["category"] = categoryFilter;
            ViewData["statue"] = statueFilter;

            var category = new List<int>();
            var brand = new List<int>();
            var city = new List<int>();
            var groups = new List<int>();
            var status = new List<int>();
            var statusList = new List<string>();
            string categoryName = string.Empty, brandName = string.Empty,
           cityName = string.Empty, groupName = string.Empty, statusName = string.Empty;

            if (categoryFilter != "")
            {
                category.AddRange(categoryFilter.Split(',').Select(item => Convert.ToInt32(item)));
                categoryName = string.Join(",", Db.urnKtgrAg.Where(x => category.Contains(x.kID)).Select(x => x.kAd));

                stores = stores.Where(x => category.Contains(x.urnKtgrAg.kID));
            }
            if (brandFilter != "")
            {

                brand.AddRange(brandFilter.Split(',').Select(item => Convert.ToInt32(item)));
                brandName = string.Join(",", Db.urnMrk.Where(x => brand.Contains(x.mrkID)).Select(x => x.mrkAd));

                stores = stores.Where(x => brand.Contains(x.urn.urnMrkID));
            }
            if (cityFilter != "")
            {

                city.AddRange(cityFilter.Split(',').Select(item => Convert.ToInt32(item)));
                cityName = string.Join(",", Db.frmKent.Where(x => city.Contains(x.alanKodu)).Select(x => x.ilAd));

                stores = from store in stores
                         join frm in Db.frm on store.irs.eMekan equals frm.frmID
                         where city.Contains(frm.il)
                         select store;

            }
            if (groupFilter != "")
            {
                groups.AddRange(groupFilter.Split(',').Select(item => Convert.ToInt32(item)));
                groupName = string.Join(",", Db.posMagazaGrup.Where(x => groups.Contains(x.mekanGrupID)).Select(x => x.mekanGrupAd));
                stores = stores.Where(x => groups.Contains(x.posMagaza.mekanGrup));

            }
            if (statueFilter != "")
            {
                status.AddRange(statueFilter.Split(',').Select(item => Convert.ToInt32(item)));
                if (status.Contains(0)) { statusList.Add("Açık"); }
                if (status.Contains(1)) { statusList.Add("Kapalı"); }
                if (status.Contains(2)) { statusList.Add("Açılmamış"); }

                statusName = string.Join(",", statusList);

                if (status.Count == 1)
                {
                    stores = !status.Contains(2) ?
                        (status.Contains(0) ? stores.Where(x => x.posMagaza.mekanDurum == 0)
                        : stores.Where(x => x.posMagaza.mekanDurum != 0))
                        : stores.Where(x => x.posMagaza.mekanDurum == 0 && x.posMagaza.mekanAcilisTarih > DateTime.Now);
                }
                if (status.Count == 2)
                {
                    if (!status.Contains(1))
                    {
                        stores = stores.Where(x => x.posMagaza.mekanDurum == 0);
                    }
                    if (!status.Contains(0))
                    {
                        stores = stores.Where(x => (x.posMagaza.mekanDurum == 0
                                       && x.posMagaza.mekanAcilisTarih > DateTime.Now)
                                       || x.posMagaza.mekanDurum != 0);
                    }
                }
            }
            var model = new SalesPerCategoryModel
            {
                Reports = (from store in stores
                           group store.irsAyr by new { store.urnKtgrAg.kID } into g
                           select new SalesPerCategoryModel.SalesPerCategory
                           {
                               CategoryName = Db.urnKtgrAg.FirstOrDefault(x => x.kID == g.Key.kID).kAd,
                               TotalSaleCount = g.Count(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
                    (-1) * g.Where(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehAdet) : 0,

                               RefundofSaleCount = g.Count(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
                       g.Where(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehAdet) : 0,


                               TotalSale = g.Count(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
    g.Where(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehTutar - x.ehIndirim + x.ehTutarKDV) : 0,

                               RefundofSale = g.Count(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
    g.Where(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehTutar - x.ehIndirim + x.ehTutarKDV) : 0

                           }
                                   ).ToList(),

                DateFilter = dateFilter,
                Categories = categoryName,
                Brands = brandName,
                ProductCount = productCount,
                Cities = cityName,
                Groups = groupName,
                Status = statusName,
                StoreCount = storeCount

            };

            return View(model);
        }

        #endregion

        #region Marka Bazlı Satış Raporu

        public ActionResult SalesPerBrandReport(DocumentDateFilterModel dateFilter, string cityFilter = "", string groupFilter = "", string statueFilter = "", string categoryFilter = "", string brandFilter = "", string storeCount = "", string productCount = "")
        {
            dateFilter.NotNullOrSetDefault(DateHelper.LastMonthBegin, DateHelper.LastMonthEnd);

            var stores = (from irsAyr in Db.irsAyr
                          join irs in Db.irs on irsAyr.ehID equals irs.eID
                          join urn in Db.urn on irsAyr.ehStkID equals urn.stkID
                          join posMagaza in Db.posMagaza on irs.eMekan equals posMagaza.mekanID
                          where irs.eTarih >= dateFilter.Begin && irs.eTarih <= dateFilter.End
                          where product.Contains(irsAyr.ehStkID)
                          where saleTypes.Contains(irs.eTip) == false
                          where irs.eFirma == CurrentCompany.frmID
                          select new { irs, irsAyr, urn, posMagaza });

            ViewData["storeCount"] = storeCount;
            ViewData["productCount"] = productCount;
            ViewData["group"] = groupFilter;
            ViewData["city"] = cityFilter;
            ViewData["brand"] = brandFilter;
            ViewData["category"] = categoryFilter;
            ViewData["statue"] = statueFilter;

            var category = new List<int>();
            var brand = new List<int>();
            var city = new List<int>();
            var groups = new List<int>();
            var status = new List<int>();
            var statusList = new List<string>();
            string categoryName = string.Empty, brandName = string.Empty,
            cityName = string.Empty, groupName = string.Empty, statusName = string.Empty;

            if (categoryFilter != "")
            {
                category.AddRange(categoryFilter.Split(',').Select(item => Convert.ToInt32(item)));

                categoryName = string.Join(",", Db.urnKtgrAg.Where(x => category.Contains(x.kID)).Select(x => x.kAd));

                stores = (from store in stores
                          join urnKtgrAg in Db.urnKtgrAg on store.urn.urnKtgrID equals urnKtgrAg.k0
                          where urnKtgrAg.k1 == 0
                          where category.Contains(urnKtgrAg.kID)
                          select store);
                //stores = stores.Where(x => x.urnKtgrAg.k1 == 0 && category.Contains(x.urnKtgrAg.kID));
            }
            if (brandFilter != "")
            {

                brand.AddRange(brandFilter.Split(',').Select(item => Convert.ToInt32(item)));
                brandName = string.Join(",", Db.urnMrk.Where(x => brand.Contains(x.mrkID)).Select(x => x.mrkAd));

                stores = stores.Where(x => brand.Contains(x.urn.urnMrkID));
            }
            if (cityFilter != "")
            {

                city.AddRange(cityFilter.Split(',').Select(item => Convert.ToInt32(item)));
                cityName = string.Join(",", Db.frmKent.Where(x => city.Contains(x.alanKodu)).Select(x => x.ilAd));

                stores = from store in stores
                         join frm in Db.frm on store.irs.eMekan equals frm.frmID
                         where city.Contains(frm.il)
                         select store;
                //stores = stores.Where(x => city.Contains(x.frm.il));
            }
            if (groupFilter != "")
            {
                groups.AddRange(groupFilter.Split(',').Select(item => Convert.ToInt32(item)));
                groupName = string.Join(",", Db.posMagazaGrup.Where(x => groups.Contains(x.mekanGrupID)).Select(x => x.mekanGrupAd));
                stores = stores.Where(x => groups.Contains(x.posMagaza.mekanGrup));

            }
            if (statueFilter != "")
            {
                status.AddRange(statueFilter.Split(',').Select(item => Convert.ToInt32(item)));
                if (status.Contains(0)) { statusList.Add("Açık"); }
                if (status.Contains(1)) { statusList.Add("Kapalı"); }
                if (status.Contains(2)) { statusList.Add("Açılmamış"); }

                statusName = string.Join(",", statusList);

                if (status.Count == 1)
                {
                    stores = !status.Contains(2) ?
                        (status.Contains(0) ? stores.Where(x => x.posMagaza.mekanDurum == 0)
                        : stores.Where(x => x.posMagaza.mekanDurum != 0))
                        : stores.Where(x => x.posMagaza.mekanDurum == 0 && x.posMagaza.mekanAcilisTarih > DateTime.Now);
                }
                if (status.Count == 2)
                {
                    if (!status.Contains(1))
                    {
                        stores = stores.Where(x => x.posMagaza.mekanDurum == 0);
                    }
                    if (!status.Contains(0))
                    {
                        stores = stores.Where(x => (x.posMagaza.mekanDurum == 0
                                       && x.posMagaza.mekanAcilisTarih > DateTime.Now)
                                       || x.posMagaza.mekanDurum != 0);
                    }
                }
            }


            var model = new SalesPerBrandModel
            {
                Reports = (from store in stores
                           group store.irsAyr by new { store.urn.urnMrkID } into g
                           select new SalesPerBrandModel.SalesPerBrand
                           {
                               BrandName = Db.urnMrk.FirstOrDefault(x => x.mrkID == g.Key.urnMrkID).mrkAd,
                               TotalSaleCount = g.Count(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
                         (-1) * g.Where(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehAdet) : 0,

                               RefundofSaleCount = g.Count(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
                  g.Where(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehAdet) : 0,


                               TotalSale = g.Count(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
    g.Where(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehTutar - x.ehIndirim + x.ehTutarKDV) : 0,

                               RefundofSale = g.Count(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
    g.Where(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehTutar - x.ehIndirim + x.ehTutarKDV) : 0
                           }
                     ).ToList(),
                DateFilter = dateFilter,
                Categories = categoryName,
                Brands = brandName,
                ProductCount = productCount,
                Cities = cityName,
                Groups = groupName,
                Status = statusName,
                StoreCount = storeCount
            };

            return View(model);
        }
        #endregion

        #region Aylık Satış Raporu

        public ActionResult SalesPerMonthReport(DocumentDateFilterModel dateFilter, string cityFilter = "", string groupFilter = "", string statueFilter = "", string categoryFilter = "", string brandFilter = "", string storeCount = "", string productCount = "")
        {
            dateFilter.NotNullOrSetDefault(DateHelper.LastMonthBegin, DateHelper.LastMonthEnd);

            var stores = (from irsAyr in Db.irsAyr
                          join irs in Db.irs on irsAyr.ehID equals irs.eID
                          join urn in Db.urn on irsAyr.ehStkID equals urn.stkID
                          join posMagaza in Db.posMagaza on irs.eMekan equals posMagaza.mekanID
                          where irs.eFirma == CurrentCompany.frmID
                          where irs.eTarih >= dateFilter.Begin && irs.eTarih <= dateFilter.End
                          where product.Contains(irsAyr.ehStkID)
                          where saleTypes.Contains(irs.eTip) == false
                          select new { irs, irsAyr, urn, posMagaza });

            ViewData["storeCount"] = storeCount;
            ViewData["productCount"] = productCount;
            ViewData["group"] = groupFilter;
            ViewData["city"] = cityFilter;
            ViewData["brand"] = brandFilter;
            ViewData["category"] = categoryFilter;
            ViewData["statue"] = statueFilter;

            var category = new List<int>();
            var brand = new List<int>();
            var city = new List<int>();
            var groups = new List<int>();
            var status = new List<int>();
            var statusList = new List<string>();
            string categoryName = string.Empty, brandName = string.Empty,
            cityName = string.Empty, groupName = string.Empty, statusName = string.Empty;

            if (categoryFilter != "")
            {
                category.AddRange(categoryFilter.Split(',').Select(item => Convert.ToInt32(item)));

                categoryName = string.Join(",", Db.urnKtgrAg.Where(x => category.Contains(x.kID)).Select(x => x.kAd));

                stores = (from store in stores
                          join urnKtgrAg in Db.urnKtgrAg on store.urn.urnKtgrID equals urnKtgrAg.k0
                          where urnKtgrAg.k1 == 0
                          where category.Contains(urnKtgrAg.kID)
                          select store);
                //stores = stores.Where(x => x.urnKtgrAg.k1 == 0 && category.Contains(x.urnKtgrAg.kID));
            }
            if (brandFilter != "")
            {

                brand.AddRange(brandFilter.Split(',').Select(item => Convert.ToInt32(item)));
                brandName = string.Join(",", Db.urnMrk.Where(x => brand.Contains(x.mrkID)).Select(x => x.mrkAd));

                stores = stores.Where(x => brand.Contains(x.urn.urnMrkID));
            }
            if (cityFilter != "")
            {

                city.AddRange(cityFilter.Split(',').Select(item => Convert.ToInt32(item)));
                cityName = string.Join(",", Db.frmKent.Where(x => city.Contains(x.alanKodu)).Select(x => x.ilAd));

                stores = from store in stores
                         join frm in Db.frm on store.irs.eMekan equals frm.frmID
                         where city.Contains(frm.il)
                         select store;
                //stores = stores.Where(x => city.Contains(x.frm.il));
            }
            if (groupFilter != "")
            {
                groups.AddRange(groupFilter.Split(',').Select(item => Convert.ToInt32(item)));
                groupName = string.Join(",", Db.posMagazaGrup.Where(x => groups.Contains(x.mekanGrupID)).Select(x => x.mekanGrupAd));
                stores = stores.Where(x => groups.Contains(x.posMagaza.mekanGrup));

            }
            if (statueFilter != "")
            {
                status.AddRange(statueFilter.Split(',').Select(item => Convert.ToInt32(item)));
                if (status.Contains(0)) { statusList.Add("Açık"); }
                if (status.Contains(1)) { statusList.Add("Kapalı"); }
                if (status.Contains(2)) { statusList.Add("Açılmamış"); }

                statusName = string.Join(",", statusList);

                if (status.Count == 1)
                {
                    stores = !status.Contains(2) ?
                        (status.Contains(0) ? stores.Where(x => x.posMagaza.mekanDurum == 0)
                        : stores.Where(x => x.posMagaza.mekanDurum != 0))
                        : stores.Where(x => x.posMagaza.mekanDurum == 0 && x.posMagaza.mekanAcilisTarih > DateTime.Now);
                }
                if (status.Count == 2)
                {
                    if (!status.Contains(1))
                    {
                        stores = stores.Where(x => x.posMagaza.mekanDurum == 0);
                    }
                    if (!status.Contains(0))
                    {
                        stores = stores.Where(x => (x.posMagaza.mekanDurum == 0
                                       && x.posMagaza.mekanAcilisTarih > DateTime.Now)
                                       || x.posMagaza.mekanDurum != 0);
                    }
                }
            }


            var model = new SalesPerMonthModel
            {
                Reports = (from store in stores
                           group store.irsAyr by new { store.irs.eTarih.Year, store.irs.eTarih.Month } into g
                           select new SalesPerMonthModel.SalesPerMonth
                           {
                               Year = g.Key.Year,
                               Month = g.Key.Month,
                               TotalSaleCount = g.Count(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
                         (-1) * g.Where(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehAdet) : 0,

                               RefundofSaleCount = g.Count(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
                          g.Where(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehAdet) : 0,


                               TotalSale = g.Count(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
    g.Where(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehTutar - x.ehIndirim + x.ehTutarKDV) : 0,

                               RefundofSale = g.Count(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
    g.Where(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehTutar - x.ehIndirim + x.ehTutarKDV) : 0
                           }
                     ).ToList(),
                DateFilter = dateFilter,
                Categories = categoryName,
                Brands = brandName,
                ProductCount = productCount,
                Cities = cityName,
                Groups = groupName,
                Status = statusName,
                StoreCount = storeCount
            };

            return View(model);
        }
        #endregion

        #region Haftalık Satış Raporu

        public ActionResult SalesPerWeekReport(DocumentDateFilterModel dateFilter)
        {

            dateFilter.NotNullOrSetDefault(DateHelper.YearBegin, DateTime.Today);

            var model = new SalesPerWeekModel
            {
                Reports = (from irs in Db.irs
                           join irsAyr in Db.irsAyr on irs.eID equals irsAyr.ehID
                           where irs.eFirma == CurrentCompany.frmID
                           where irs.eTarih >= dateFilter.Begin && irs.eTarih <= dateFilter.End

                           group irsAyr by new { irs.eTarih.Year, irs.eTarih.Month } into g
                           orderby g.Key.Month
                           select new SalesPerWeekModel.SalesPerWeek
                           {
                               Year = g.Key.Year,
                               Month = g.Key.Month


                           }
                     ).ToList(),
                DateFilter = dateFilter
            };

            return View(model);
        }


        #endregion

        #region Bölgesel Satış Raporu

        public ActionResult SalesPerRegionReport(DocumentDateFilterModel dateFilter, string cityFilter = "", string groupFilter = "", string statueFilter = "", string categoryFilter = "", string brandFilter = "", string storeCount = "", string productCount = "")
        {

            dateFilter.NotNullOrSetDefault(DateHelper.YearBegin, DateTime.Today);
            var stores = (from irsAyr in Db.irsAyr
                          join irs in Db.irs on irsAyr.ehID equals irs.eID
                          join urn in Db.urn on irsAyr.ehStkID equals urn.stkID
                          join posMagaza in Db.posMagaza on irs.eMekan equals posMagaza.mekanID
                          join frm in Db.frm on irs.eMekan equals frm.frmID
                          join frmKent in Db.frmKent on frm.il equals frmKent.alanKodu
                          where irs.eTarih >= dateFilter.Begin && irs.eTarih <= dateFilter.End
                          where product.Contains(irsAyr.ehStkID)
                          where saleTypes.Contains(irs.eTip)
                          select new { irs, irsAyr, urn, posMagaza, frm, frmKent });

            ViewData["storeCount"] = storeCount;
            ViewData["productCount"] = productCount;
            ViewData["group"] = groupFilter;
            ViewData["city"] = cityFilter;
            ViewData["brand"] = brandFilter;
            ViewData["category"] = categoryFilter;
            ViewData["statue"] = statueFilter;

            var category = new List<int>();
            var brand = new List<int>();
            var city = new List<int>();
            var groups = new List<int>();
            var status = new List<int>();
            var statusList = new List<string>();
            string categoryName = string.Empty, brandName = string.Empty,
            cityName = string.Empty, groupName = string.Empty, statusName = string.Empty;

            if (categoryFilter != "")
            {
                category.AddRange(categoryFilter.Split(',').Select(item => Convert.ToInt32(item)));

                categoryName = string.Join(",", Db.urnKtgrAg.Where(x => category.Contains(x.kID)).Select(x => x.kAd));

                stores = (from store in stores
                          join urnKtgrAg in Db.urnKtgrAg on store.urn.urnKtgrID equals urnKtgrAg.k0
                          where urnKtgrAg.k1 == 0
                          where category.Contains(urnKtgrAg.kID)
                          select store);
                //stores = stores.Where(x => x.urnKtgrAg.k1 == 0 && category.Contains(x.urnKtgrAg.kID));
            }
            if (brandFilter != "")
            {

                brand.AddRange(brandFilter.Split(',').Select(item => Convert.ToInt32(item)));
                brandName = string.Join(",", Db.urnMrk.Where(x => brand.Contains(x.mrkID)).Select(x => x.mrkAd));

                stores = stores.Where(x => brand.Contains(x.urn.urnMrkID));
            }
            if (cityFilter != "")
            {

                city.AddRange(cityFilter.Split(',').Select(item => Convert.ToInt32(item)));
                cityName = string.Join(",", Db.frmKent.Where(x => city.Contains(x.alanKodu)).Select(x => x.ilAd));

                stores = stores.Where(x => city.Contains(x.frm.il));
            }
            if (groupFilter != "")
            {
                groups.AddRange(groupFilter.Split(',').Select(item => Convert.ToInt32(item)));
                groupName = string.Join(",", Db.posMagazaGrup.Where(x => groups.Contains(x.mekanGrupID)).Select(x => x.mekanGrupAd));
                stores = stores.Where(x => groups.Contains(x.posMagaza.mekanGrup));

            }
            if (statueFilter != "")
            {
                status.AddRange(statueFilter.Split(',').Select(item => Convert.ToInt32(item)));
                if (status.Contains(0)) { statusList.Add("Açık"); }
                if (status.Contains(1)) { statusList.Add("Kapalı"); }
                if (status.Contains(2)) { statusList.Add("Açılmamış"); }

                statusName = string.Join(",", statusList);

                if (status.Count == 1)
                {
                    stores = !status.Contains(2) ?
                        (status.Contains(0) ? stores.Where(x => x.posMagaza.mekanDurum == 0)
                        : stores.Where(x => x.posMagaza.mekanDurum != 0))
                        : stores.Where(x => x.posMagaza.mekanDurum == 0 && x.posMagaza.mekanAcilisTarih > DateTime.Now);
                }
                if (status.Count == 2)
                {
                    if (!status.Contains(1))
                    {
                        stores = stores.Where(x => x.posMagaza.mekanDurum == 0);
                    }
                    if (!status.Contains(0))
                    {
                        stores = stores.Where(x => (x.posMagaza.mekanDurum == 0
                                       && x.posMagaza.mekanAcilisTarih > DateTime.Now)
                                       || x.posMagaza.mekanDurum != 0);
                    }
                }
            }
            var model = new SalesPerRegionModel
            {
                Reports = (from store in stores
                           group store.irsAyr by new { store.frmKent.ilBolge } into g
                           select new SalesPerRegionModel.SalesPerRegion
                           {
                               Region = Db.frmKentBolge.FirstOrDefault(x => x.bolgeID == g.Key.ilBolge).bolgeAd,

                               TotalSaleCount = g.Count(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
             (-1) * g.Where(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehAdet) : 0,

                               RefundofSaleCount = g.Count(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
                   g.Where(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehAdet) : 0,


                               TotalSale = g.Count(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
    g.Where(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehTutar - x.ehIndirim + x.ehTutarKDV) : 0,

                               RefundofSale = g.Count(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
    g.Where(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehTutar - x.ehIndirim + x.ehTutarKDV) : 0

                           }
                     ).ToList(),
                DateFilter = dateFilter,
                Categories = categoryName,
                Brands = brandName,
                ProductCount = productCount,
                Cities = cityName,
                Groups = groupName,
                Status = statusName,
                StoreCount = storeCount
            };

            return View(model);
        }


        #endregion

        #region İl Bazlı Satış Raporu

        public ActionResult SalesPerCityReport(DocumentDateFilterModel dateFilter, string cityFilter = "", string groupFilter = "", string statueFilter = "", string categoryFilter = "", string brandFilter = "", string storeCount = "", string productCount = "")
        {

            dateFilter.NotNullOrSetDefault(DateHelper.YearBegin, DateTime.Today);
            var stores = (from irsAyr in Db.irsAyr
                          join irs in Db.irs on irsAyr.ehID equals irs.eID
                          join urn in Db.urn on irsAyr.ehStkID equals urn.stkID
                          join posMagaza in Db.posMagaza on irs.eMekan equals posMagaza.mekanID
                          join frm in Db.frm on irs.eMekan equals frm.frmID
                          where irs.eTarih >= dateFilter.Begin && irs.eTarih <= dateFilter.End
                          where product.Contains(irsAyr.ehStkID)
                          where saleTypes.Contains(irs.eTip)
                          select new { irs, irsAyr, urn, posMagaza, frm });

            ViewData["storeCount"] = storeCount;
            ViewData["productCount"] = productCount;
            ViewData["group"] = groupFilter;
            ViewData["city"] = cityFilter;
            ViewData["brand"] = brandFilter;
            ViewData["category"] = categoryFilter;
            ViewData["statue"] = statueFilter;

            var category = new List<int>();
            var brand = new List<int>();
            var city = new List<int>();
            var groups = new List<int>();
            var status = new List<int>();
            var statusList = new List<string>();
            string categoryName = string.Empty, brandName = string.Empty,
            cityName = string.Empty, groupName = string.Empty, statusName = string.Empty;

            if (categoryFilter != "")
            {
                category.AddRange(categoryFilter.Split(',').Select(item => Convert.ToInt32(item)));

                categoryName = string.Join(",", Db.urnKtgrAg.Where(x => category.Contains(x.kID)).Select(x => x.kAd));

                stores = (from store in stores
                          join urnKtgrAg in Db.urnKtgrAg on store.urn.urnKtgrID equals urnKtgrAg.k0
                          where urnKtgrAg.k1 == 0
                          where category.Contains(urnKtgrAg.kID)
                          select store);
                //stores = stores.Where(x => x.urnKtgrAg.k1 == 0 && category.Contains(x.urnKtgrAg.kID));
            }
            if (brandFilter != "")
            {

                brand.AddRange(brandFilter.Split(',').Select(item => Convert.ToInt32(item)));
                brandName = string.Join(",", Db.urnMrk.Where(x => brand.Contains(x.mrkID)).Select(x => x.mrkAd));

                stores = stores.Where(x => brand.Contains(x.urn.urnMrkID));
            }
            if (cityFilter != "")
            {

                city.AddRange(cityFilter.Split(',').Select(item => Convert.ToInt32(item)));
                cityName = string.Join(",", Db.frmKent.Where(x => city.Contains(x.alanKodu)).Select(x => x.ilAd));

                stores = stores.Where(x => city.Contains(x.frm.il));
            }
            if (groupFilter != "")
            {
                groups.AddRange(groupFilter.Split(',').Select(item => Convert.ToInt32(item)));
                groupName = string.Join(",", Db.posMagazaGrup.Where(x => groups.Contains(x.mekanGrupID)).Select(x => x.mekanGrupAd));
                stores = stores.Where(x => groups.Contains(x.posMagaza.mekanGrup));

            }
            if (statueFilter != "")
            {
                status.AddRange(statueFilter.Split(',').Select(item => Convert.ToInt32(item)));
                if (status.Contains(0)) { statusList.Add("Açık"); }
                if (status.Contains(1)) { statusList.Add("Kapalı"); }
                if (status.Contains(2)) { statusList.Add("Açılmamış"); }

                statusName = string.Join(",", statusList);

                if (status.Count == 1)
                {
                    stores = !status.Contains(2) ?
                        (status.Contains(0) ? stores.Where(x => x.posMagaza.mekanDurum == 0)
                        : stores.Where(x => x.posMagaza.mekanDurum != 0))
                        : stores.Where(x => x.posMagaza.mekanDurum == 0 && x.posMagaza.mekanAcilisTarih > DateTime.Now);
                }
                if (status.Count == 2)
                {
                    if (!status.Contains(1))
                    {
                        stores = stores.Where(x => x.posMagaza.mekanDurum == 0);
                    }
                    if (!status.Contains(0))
                    {
                        stores = stores.Where(x => (x.posMagaza.mekanDurum == 0
                                       && x.posMagaza.mekanAcilisTarih > DateTime.Now)
                                       || x.posMagaza.mekanDurum != 0);
                    }
                }
            }
            var model = new SalesPerRegionModel
            {
                Reports = (from store in stores
                           group store.irsAyr by new { store.frm.il } into g
                           select new SalesPerRegionModel.SalesPerRegion
                           {
                               Region = Db.frmKent.FirstOrDefault(x => x.alanKodu == g.Key.il).ilAd,

                               TotalSaleCount = g.Count(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
                    (-1) * g.Where(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehAdet) : 0,

                               RefundofSaleCount = g.Count(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
                      g.Where(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehAdet) : 0,


                               TotalSale = g.Count(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
    g.Where(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehTutar - x.ehIndirim + x.ehTutarKDV) : 0,

                               RefundofSale = g.Count(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
    g.Where(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehTutar - x.ehIndirim + x.ehTutarKDV) : 0

                           }
                     ).ToList(),
                DateFilter = dateFilter,
                Categories = categoryName,
                Brands = brandName,
                ProductCount = productCount,
                Cities = cityName,
                Groups = groupName,
                Status = statusName,
                StoreCount = storeCount
            };

            return View(model);
        }



        #endregion

        #region Grup Bazlı Satış Raporu

        public ActionResult SalesPerGroupReport(DocumentDateFilterModel dateFilter, string cityFilter = "", string groupFilter = "", string statueFilter = "", string categoryFilter = "", string brandFilter = "", string storeCount = "", string productCount = "")
        {

            dateFilter.NotNullOrSetDefault(DateHelper.LastMonthBegin, DateHelper.LastMonthEnd);

            var stores = (from irsAyr in Db.irsAyr
                          join irs in Db.irs on irsAyr.ehID equals irs.eID
                          join urn in Db.urn on irsAyr.ehStkID equals urn.stkID
                          join posMagaza in Db.posMagaza on irs.eMekan equals posMagaza.mekanID
                          where irs.eTarih >= dateFilter.Begin && irs.eTarih <= dateFilter.End
                          where product.Contains(irsAyr.ehStkID)
                          where saleTypes.Contains(irs.eTip)
                          select new { irs, irsAyr, urn, posMagaza });

            ViewData["storeCount"] = storeCount;
            ViewData["productCount"] = productCount;
            ViewData["group"] = groupFilter;
            ViewData["city"] = cityFilter;
            ViewData["brand"] = brandFilter;
            ViewData["category"] = categoryFilter;
            ViewData["statue"] = statueFilter;

            var category = new List<int>();
            var brand = new List<int>();
            var city = new List<int>();
            var groups = new List<int>();
            var status = new List<int>();
            var statusList = new List<string>();
            string categoryName = string.Empty, brandName = string.Empty,
            cityName = string.Empty, groupName = string.Empty, statusName = string.Empty;

            if (categoryFilter != "")
            {
                category.AddRange(categoryFilter.Split(',').Select(item => Convert.ToInt32(item)));

                categoryName = string.Join(",", Db.urnKtgrAg.Where(x => category.Contains(x.kID)).Select(x => x.kAd));

                stores = (from store in stores
                          join urnKtgrAg in Db.urnKtgrAg on store.urn.urnKtgrID equals urnKtgrAg.k0
                          where urnKtgrAg.k1 == 0
                          where category.Contains(urnKtgrAg.kID)
                          select store);
                //stores = stores.Where(x => x.urnKtgrAg.k1 == 0 && category.Contains(x.urnKtgrAg.kID));
            }
            if (brandFilter != "")
            {

                brand.AddRange(brandFilter.Split(',').Select(item => Convert.ToInt32(item)));
                brandName = string.Join(",", Db.urnMrk.Where(x => brand.Contains(x.mrkID)).Select(x => x.mrkAd));

                stores = stores.Where(x => brand.Contains(x.urn.urnMrkID));
            }
            if (cityFilter != "")
            {

                city.AddRange(cityFilter.Split(',').Select(item => Convert.ToInt32(item)));
                cityName = string.Join(",", Db.frmKent.Where(x => city.Contains(x.alanKodu)).Select(x => x.ilAd));

                stores = from store in stores
                         join frm in Db.frm on store.irs.eMekan equals frm.frmID
                         where city.Contains(frm.il)
                         select store;
                //stores = stores.Where(x => city.Contains(x.frm.il));
            }
            if (groupFilter != "")
            {
                groups.AddRange(groupFilter.Split(',').Select(item => Convert.ToInt32(item)));
                groupName = string.Join(",", Db.posMagazaGrup.Where(x => groups.Contains(x.mekanGrupID)).Select(x => x.mekanGrupAd));
                stores = stores.Where(x => groups.Contains(x.posMagaza.mekanGrup));

            }
            if (statueFilter != "")
            {
                status.AddRange(statueFilter.Split(',').Select(item => Convert.ToInt32(item)));
                if (status.Contains(0)) { statusList.Add("Açık"); }
                if (status.Contains(1)) { statusList.Add("Kapalı"); }
                if (status.Contains(2)) { statusList.Add("Açılmamış"); }

                statusName = string.Join(",", statusList);

                if (status.Count == 1)
                {
                    stores = !status.Contains(2) ?
                        (status.Contains(0) ? stores.Where(x => x.posMagaza.mekanDurum == 0)
                        : stores.Where(x => x.posMagaza.mekanDurum != 0))
                        : stores.Where(x => x.posMagaza.mekanDurum == 0 && x.posMagaza.mekanAcilisTarih > DateTime.Now);
                }
                if (status.Count == 2)
                {
                    if (!status.Contains(1))
                    {
                        stores = stores.Where(x => x.posMagaza.mekanDurum == 0);
                    }
                    if (!status.Contains(0))
                    {
                        stores = stores.Where(x => (x.posMagaza.mekanDurum == 0
                                       && x.posMagaza.mekanAcilisTarih > DateTime.Now)
                                       || x.posMagaza.mekanDurum != 0);
                    }
                }
            }
            var model = new SalesPerGroupModel
            {
                Reports = from store in stores
                          group store.irsAyr by new { store.posMagaza.mekanGrup } into g
                          select new SalesPerGroupModel.SalesPerGroup
                          {
                              Group = Db.posMagazaGrup.FirstOrDefault(x => x.mekanGrupID == g.Key.mekanGrup).mekanGrupAd,

                              TotalSaleCount = g.Count(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
              (-1) * g.Where(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehAdet) : 0,

                              RefundofSaleCount = g.Count(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
              g.Where(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehAdet) : 0,

                              TotalSale = g.Count(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
    g.Where(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehTutar - x.ehIndirim + x.ehTutarKDV) : 0,

                              RefundofSale = g.Count(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
    g.Where(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehTutar - x.ehIndirim + x.ehTutarKDV) : 0
                          },
                DateFilter = dateFilter,
                Categories = categoryName,
                Brands = brandName,
                ProductCount = productCount,
                Cities = cityName,
                Groups = groupName,
                Status = statusName,
                StoreCount = storeCount
            };

            return View(model);
        }


        #endregion

        #region Boyut Bazlı Satış Raporu

        public ActionResult SalesPerSizeReport(DocumentDateFilterModel dateFilter, string cityFilter = "", string groupFilter = "", string statueFilter = "", string categoryFilter = "", string brandFilter = "", string storeCount = "", string productCount = "")
        {

            dateFilter.NotNullOrSetDefault(DateHelper.LastMonthBegin, DateHelper.LastMonthEnd);

            var stores = (from irsAyr in Db.irsAyr
                          join irs in Db.irs on irsAyr.ehID equals irs.eID
                          join urn in Db.urn on irsAyr.ehStkID equals urn.stkID
                          join posMagaza in Db.posMagaza on irs.eMekan equals posMagaza.mekanID
                          where irs.eTarih >= dateFilter.Begin && irs.eTarih <= dateFilter.End
                          where product.Contains(irsAyr.ehStkID)
                          where saleTypes.Contains(irs.eTip)
                          select new { irs, irsAyr, urn, posMagaza });

            ViewData["storeCount"] = storeCount;
            ViewData["productCount"] = productCount;
            ViewData["group"] = groupFilter;
            ViewData["city"] = cityFilter;
            ViewData["brand"] = brandFilter;
            ViewData["category"] = categoryFilter;
            ViewData["statue"] = statueFilter;

            var category = new List<int>();
            var brand = new List<int>();
            var city = new List<int>();
            var groups = new List<int>();
            var status = new List<int>();
            var statusList = new List<string>();
            string categoryName = string.Empty, brandName = string.Empty,
            cityName = string.Empty, groupName = string.Empty, statusName = string.Empty;

            if (categoryFilter != "")
            {
                category.AddRange(categoryFilter.Split(',').Select(item => Convert.ToInt32(item)));

                categoryName = string.Join(",", Db.urnKtgrAg.Where(x => category.Contains(x.kID)).Select(x => x.kAd));

                stores = (from store in stores
                          join urnKtgrAg in Db.urnKtgrAg on store.urn.urnKtgrID equals urnKtgrAg.k0
                          where urnKtgrAg.k1 == 0
                          where category.Contains(urnKtgrAg.kID)
                          select store);
                //stores = stores.Where(x => x.urnKtgrAg.k1 == 0 && category.Contains(x.urnKtgrAg.kID));
            }
            if (brandFilter != "")
            {

                brand.AddRange(brandFilter.Split(',').Select(item => Convert.ToInt32(item)));
                brandName = string.Join(",", Db.urnMrk.Where(x => brand.Contains(x.mrkID)).Select(x => x.mrkAd));

                stores = stores.Where(x => brand.Contains(x.urn.urnMrkID));
            }
            if (cityFilter != "")
            {

                city.AddRange(cityFilter.Split(',').Select(item => Convert.ToInt32(item)));
                cityName = string.Join(",", Db.frmKent.Where(x => city.Contains(x.alanKodu)).Select(x => x.ilAd));

                stores = from store in stores
                         join frm in Db.frm on store.irs.eMekan equals frm.frmID
                         where city.Contains(frm.il)
                         select store;
                //stores = stores.Where(x => city.Contains(x.frm.il));
            }
            if (groupFilter != "")
            {
                groups.AddRange(groupFilter.Split(',').Select(item => Convert.ToInt32(item)));
                groupName = string.Join(",", Db.posMagazaGrup.Where(x => groups.Contains(x.mekanGrupID)).Select(x => x.mekanGrupAd));
                stores = stores.Where(x => groups.Contains(x.posMagaza.mekanGrup));

            }
            if (statueFilter != "")
            {
                status.AddRange(statueFilter.Split(',').Select(item => Convert.ToInt32(item)));
                if (status.Contains(0)) { statusList.Add("Açık"); }
                if (status.Contains(1)) { statusList.Add("Kapalı"); }
                if (status.Contains(2)) { statusList.Add("Açılmamış"); }

                statusName = string.Join(",", statusList);

                if (status.Count == 1)
                {
                    stores = !status.Contains(2) ?
                        (status.Contains(0) ? stores.Where(x => x.posMagaza.mekanDurum == 0)
                        : stores.Where(x => x.posMagaza.mekanDurum != 0))
                        : stores.Where(x => x.posMagaza.mekanDurum == 0 && x.posMagaza.mekanAcilisTarih > DateTime.Now);
                }
                if (status.Count == 2)
                {
                    if (!status.Contains(1))
                    {
                        stores = stores.Where(x => x.posMagaza.mekanDurum == 0);
                    }
                    if (!status.Contains(0))
                    {
                        stores = stores.Where(x => (x.posMagaza.mekanDurum == 0
                                       && x.posMagaza.mekanAcilisTarih > DateTime.Now)
                                       || x.posMagaza.mekanDurum != 0);
                    }
                }
            }
            var model = new SalesPerSizeModel
            {
                Reports = from store in stores
                          group store.irsAyr by new { store.posMagaza.mekanGrup1 } into g
                          select new SalesPerSizeModel.SalesPerSize
                          {
                              Size = Db.posMagazaGrup1.FirstOrDefault(x => x.mekanGrup1ID == g.Key.mekanGrup1).mekanGrup1Ad,

                              TotalSaleCount = g.Count(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
                   (-1) * g.Where(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehAdet) : 0,

                              RefundofSaleCount = g.Count(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
                    g.Where(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehAdet) : 0,

                              TotalSale = g.Count(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
    g.Where(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehTutar - x.ehIndirim + x.ehTutarKDV) : 0,

                              RefundofSale = g.Count(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
    g.Where(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehTutar - x.ehIndirim + x.ehTutarKDV) : 0
                          },
                DateFilter = dateFilter,
                Categories = categoryName,
                Brands = brandName,
                ProductCount = productCount,
                Cities = cityName,
                Groups = groupName,
                Status = statusName,
                StoreCount = storeCount
            };

            return View(model);
        }


        #endregion

        #region Rapor Filtesi
        public ActionResult LoadStoreFilter()
        {

            var model = new ReportFilterModel
            {
                OpenStoreCount =
                                        (from irs in Db.irs
                                         join irsAyr in Db.irsAyr on irs.eID equals irsAyr.ehID
                                         join posMagaza in Db.posMagaza on irs.eMekan equals posMagaza.mekanID
                                         where irs.eFirma == CurrentCompany.frmID
                                         where posMagaza.mekanDurum == 0
                                         where posMagaza.mekanAcilisTarih < DateTime.Now
                                         where product.Contains(irsAyr.ehStkID)
                                         where saleTypes.Contains(irs.eTip)
                                         group posMagaza by new { posMagaza.mekanAd } into g
                                         select g).Count(),
                PreparedStoreCount =
                                         (from irs in Db.irs
                                          join irsAyr in Db.irsAyr on irs.eID equals irsAyr.ehID
                                          join posMagaza in Db.posMagaza on irs.eMekan equals posMagaza.mekanID
                                          where irs.eFirma == CurrentCompany.frmID
                                          where posMagaza.mekanDurum == 0
                                          where posMagaza.mekanAcilisTarih > DateTime.Now
                                          where product.Contains(irsAyr.ehStkID)
                                          where saleTypes.Contains(irs.eTip)
                                          group posMagaza by new { posMagaza.mekanAd } into g
                                          select g).Count(),
                ClosedStoreCount =
                                         (from irs in Db.irs
                                          join irsAyr in Db.irsAyr on irs.eID equals irsAyr.ehID
                                          join posMagaza in Db.posMagaza on irs.eMekan equals posMagaza.mekanID
                                          where irs.eFirma == CurrentCompany.frmID
                                          where posMagaza.mekanDurum != 0
                                          where product.Contains(irsAyr.ehStkID)
                                          where saleTypes.Contains(irs.eTip)
                                          group posMagaza by new { posMagaza.mekanAd } into g
                                          select g).Count(),
                Cities =
                                        from irs in Db.irs
                                        join irsAyr in Db.irsAyr on irs.eID equals irsAyr.ehID
                                        join posMagaza in Db.posMagaza on irs.eMekan equals posMagaza.mekanID
                                        join frm in Db.frm on irs.eMekan equals frm.frmID
                                        join frmKent in Db.frmKent on frm.il equals frmKent.alanKodu
                                        where irs.eFirma == CurrentCompany.frmID
                                        group new { posMagaza, irs, irsAyr } by
                                        new { frmKent.alanKodu, frmKent.ilAd, frmKent.ilBolge } into g
                                        select new ReportFilterModel.City
                                        {
                                            RegionId = g.Key.ilBolge,
                                            CityId = g.Key.alanKodu,
                                            CityName = g.Key.ilAd,
                                            StoreCount = g.Where(x => product.Contains(x.irsAyr.ehStkID))
                                                                         .Where(x => saleTypes.Contains(x.irs.eTip))
                                                                         .GroupBy(x => x.posMagaza.mekanAd).Count()

                                        },
                Regions =
                                        from irs in Db.irs
                                        join irsAyr in Db.irsAyr on irs.eID equals irsAyr.ehID
                                        join posMagaza in Db.posMagaza on irs.eMekan equals posMagaza.mekanID
                                        join frm in Db.frm on irs.eMekan equals frm.frmID
                                        join frmKent in Db.frmKent on frm.il equals frmKent.alanKodu
                                        join frmKentBolge in Db.frmKentBolge on frmKent.ilBolge
                                        equals frmKentBolge.bolgeID
                                        where irs.eFirma == CurrentCompany.frmID
                                        group new { posMagaza, irs, irsAyr } by new { frmKentBolge.bolgeAd, frmKentBolge.bolgeID } into g
                                        select new ReportFilterModel.Region
                                        {
                                            RegionId = g.Key.bolgeID,
                                            RegionName = g.Key.bolgeAd,
                                            StoreCount = g.Where(x => product.Contains(x.irsAyr.ehStkID))
                                                                         .Where(x => saleTypes.Contains(x.irs.eTip))
                                                                         .GroupBy(x => x.posMagaza.mekanAd).Count()


                                        },
                Groups =
                                     from irs in Db.irs
                                     join irsAyr in Db.irsAyr on irs.eID equals irsAyr.ehID
                                     join posMagaza in Db.posMagaza on irs.eMekan equals posMagaza.mekanID
                                     join posMagazaGrup in Db.posMagazaGrup on posMagaza.mekanGrup
                                     equals posMagazaGrup.mekanGrupID
                                     where irs.eFirma == CurrentCompany.frmID
                                     group new { posMagaza, irs, irsAyr } by
                                     new { posMagazaGrup.mekanGrupAd, posMagazaGrup.mekanGrupID } into g
                                     select new ReportFilterModel.Group
                                     {
                                         GroupId = g.Key.mekanGrupID,
                                         GroupName = g.Key.mekanGrupAd,
                                         StoreCount = g.Where(x => product.Contains(x.irsAyr.ehStkID))
                                                                         .Where(x => saleTypes.Contains(x.irs.eTip))
                                                                         .GroupBy(x => x.posMagaza.mekanAd).Count()
                                     },

            };

            return Json(model);
        }
        public ActionResult LoadProductFilter()
        {
            var model = new ReportFilterModel
            {
                TotalCount = (from irsAyr in Db.irsAyr
                              join irs in Db.irs on irsAyr.ehID equals irs.eID
                              join urn in Db.urn on irsAyr.ehStkID equals urn.stkID
                              join urnKtgrAg in Db.urnKtgrAg on urn.urnKtgrID equals urnKtgrAg.k0
                              where urnKtgrAg.k1 == 0
                              where irs.eFirma == CurrentCompany.frmID
                              where product.Contains(irsAyr.ehStkID)
                              where saleTypes.Contains(irs.eTip)
                              group urn by urn.stkID into g
                              select g).Count(),

                Categories = from irsAyr in Db.irsAyr
                             join irs in Db.irs on irsAyr.ehID equals irs.eID
                             join urn in Db.urn on irsAyr.ehStkID equals urn.stkID
                             join urnKtgrAg in Db.urnKtgrAg on urn.urnKtgrID equals urnKtgrAg.k0
                             where urnKtgrAg.k1 == 0
                             where irs.eFirma == CurrentCompany.frmID
                             group new { urn, irs, irsAyr } by new { urnKtgrAg.kID, urnKtgrAg.kAd, urnKtgrAg.k0 } into g
                             select new ReportFilterModel.Category
                             {
                                 CategoryId = g.Key.kID,
                                 CategoryName = g.Key.kAd,
                                 ProductCount = g.Where(x => product.Contains(x.irsAyr.ehStkID))
                                                   .Where(x => saleTypes.Contains(x.irs.eTip))
                                                   .GroupBy(x => x.urn.stkID).Count()
                             },

                Producers = from urn in Db.urn
                            join irsAyr in Db.irsAyr on urn.stkID equals irsAyr.ehStkID
                            join irs in Db.irs on irsAyr.ehID equals irs.eID
                            join urnMrk in Db.urnMrk on urn.urnMrkID equals urnMrk.mrkID
                            join urnMrkUrt in Db.urnMrkUrt on urnMrk.mrkUretici equals urnMrkUrt.urtID
                            //join urnKtgrAg in Db.urnKtgrAg on urn.urnKtgrID equals urnKtgrAg.k0
                            //where urnKtgrAg.k1 == 0
                            where irs.eFirma == CurrentCompany.frmID
                            group new { urn, irs, irsAyr } by new { urnMrkUrt.urtID, urnMrkUrt.urtAd } into g
                            select new ReportFilterModel.Producer
                            {
                                ProducerId = g.Key.urtID,
                                ProducerName = g.Key.urtAd,
                                ProductCount = g.Where(x => product.Contains(x.irsAyr.ehStkID))
                                                  .Where(x => saleTypes.Contains(x.irs.eTip))
                                                  .GroupBy(x => x.urn.stkID).Count()
                            },
                Brands = from urn in Db.urn
                         join irsAyr in Db.irsAyr on urn.stkID equals irsAyr.ehStkID
                         join irs in Db.irs on irsAyr.ehID equals irs.eID
                         join urnMrk in Db.urnMrk on urn.urnMrkID equals urnMrk.mrkID
                         //join urnKtgrAg in Db.urnKtgrAg on urn.urnKtgrID equals urnKtgrAg.k0
                         //where urnKtgrAg.k1 == 0
                         where irs.eFirma == CurrentCompany.frmID
                         group new { urn, irs, irsAyr } by new { urnMrk.mrkID, urnMrk.mrkAd, urnMrk.mrkUretici } into g
                         select new ReportFilterModel.Brand
                         {
                             ProducerId = g.Key.mrkUretici,
                             BrandId = g.Key.mrkID,
                             BrandName = g.Key.mrkAd,
                             ProductCount = g.Where(x => product.Contains(x.irsAyr.ehStkID))
                                            .Where(x => saleTypes.Contains(x.irs.eTip))
                                            .GroupBy(x => x.urn.stkID).Count()
                         }


            };

            return Json(model);
        }
        public ActionResult GetStoresCount(IList<int> cities, IList<int> groups, IList<int> status)
        {
            var stores = (from irs in Db.irs
                          join irsAyr in Db.irsAyr on irs.eID equals irsAyr.ehID
                          join posMagaza in Db.posMagaza on irs.eMekan equals posMagaza.mekanID
                          join frm in Db.frm on irs.eMekan equals frm.frmID
                          join frmKent in Db.frmKent on frm.il equals frmKent.alanKodu
                          join posMagazaGrup in Db.posMagazaGrup on posMagaza.mekanGrup equals posMagazaGrup.mekanGrupID
                          where irs.eFirma == CurrentCompany.frmID
                          where product.Contains(irsAyr.ehStkID)
                          where saleTypes.Contains(irs.eTip)
                          select new { posMagaza, frmKent });


            if (cities != null)
            {
                stores = stores.Where(x => cities.Contains(x.frmKent.alanKodu));
            }
            if (groups != null)
            {
                stores = stores.Where(x => groups.Contains(x.posMagaza.mekanGrup));
            }
            if (status != null)
            {
                if (status.Count == 1)
                {
                    stores = !status.Contains(2) ?
                        (status.Contains(0) ? stores.Where(x => x.posMagaza.mekanDurum == 0)
                        : stores.Where(x => x.posMagaza.mekanDurum != 0))
                        : stores.Where(x => x.posMagaza.mekanDurum == 0 && x.posMagaza.mekanAcilisTarih > DateTime.Now);
                }
                if (status.Count == 2)
                {
                    if (!status.Contains(1))
                    {
                        stores = stores.Where(x => x.posMagaza.mekanDurum == 0);
                    }
                    if (!status.Contains(0))
                    {
                        stores = stores.Where(x => (x.posMagaza.mekanDurum == 0
                                       && x.posMagaza.mekanAcilisTarih > DateTime.Now)
                                       || x.posMagaza.mekanDurum != 0);
                    }
                }


            }


            return Json(stores.GroupBy(x => x.posMagaza.mekanAd).Count());
        }
        public ActionResult GetProductsCount(IList<int> categories, List<int> brands)
        {
            var products =
                  from irsAyr in Db.irsAyr
                  join irs in Db.irs on irsAyr.ehID equals irs.eID
                  join urn in Db.urn on irsAyr.ehStkID equals urn.stkID
                  join urnKtgrAg in Db.urnKtgrAg on urn.urnKtgrID equals urnKtgrAg.k0
                  //where urnKtgrAg.k1 == 0
                  where product.Contains(irsAyr.ehStkID)
                  where saleTypes.Contains(irs.eTip)
                  select new { urn, urnKtgrAg };

            if (categories != null)
            {
                products = products.Where(x => x.urnKtgrAg.k1 == 0 && categories.Contains(x.urnKtgrAg.kID));

            }
            if (brands != null)
            {
                products = products.Where(x => brands.Contains(x.urn.urnMrkID));
            }
            return Json(products.GroupBy(x => x.urn.stkID).Count());
        }


        public ActionResult magazaFiltre()
        {
            var model = new mekanFiltreModel
            {
                Mekanlar = (from m in Db.posMagaza
                            where m.mekanTip == 0 && m.mekanDurum == 0
                            select new mekanFiltreModel.Mekan
                            {
                                MekanID = m.mekanID,
                                MekanAd = m.mekanAd
                            }).OrderBy(x => x.MekanAd)
            };

            return Json(model);
        }
        #endregion



        private bool onaysizSiparisGoster()
        {
            var OnaysizSiparisGösterimi = Db.drn2ayar.FirstOrDefault(x => x.ayarID == 773);
            bool bekleyenSipGoster = true;
            if (CurrentAccountType == AccountType.Customer)
            {
                if ((OnaysizSiparisGösterimi == null ? 0 : Convert.ToInt32(OnaysizSiparisGösterimi.ayarDeger)) == 3 || (OnaysizSiparisGösterimi == null ? 0 : Convert.ToInt32(OnaysizSiparisGösterimi.ayarDeger)) == 2)
                {
                    bekleyenSipGoster = false;
                }
            }
            else
            {
                if ((OnaysizSiparisGösterimi == null ? 0 : Convert.ToInt32(OnaysizSiparisGösterimi.ayarDeger)) == 3 || (OnaysizSiparisGösterimi == null ? 0 : Convert.ToInt32(OnaysizSiparisGösterimi.ayarDeger)) == 1)
                {
                    bekleyenSipGoster = false;
                }
            }
            return bekleyenSipGoster;
        }





        #region Bakiye Sipariş Raporu
        public ActionResult RemainderOrderReport(string q, DocumentDateFilterModel dateFilter, int? page)
        {
            var pageLength = 29;

            dateFilter.NotNullOrSetDefault(DateHelper.ThisMonthBegin, DateTime.Today);

            string aramaKriteri = "";
            if (!String.IsNullOrWhiteSpace(q))
            {
                aramaKriteri = q;
            }



            string sorgu = string.Format(@" exec [b2b].[sp_RemainderOrderReport_Vw] @frmID={0},@page={1},@pageLength={2},@basTarih='{3}',@bitTarih='{4}',@search='{5}' ", CurrentCompany.frmID, page ?? 1, pageLength, dateFilter.Begin.Value.Date.ToString("dd.MM.yyyy"), dateFilter.End.Value.Date.ToString("dd.MM.yyyy"), aramaKriteri);
            var sipAyrList = Db.Database.SqlQuery<RemainderOrderModel.RemainderOrder>(sorgu).ToList();

            var model = new RemainderOrderModel
            {
                Sorgu = aramaKriteri,
                DateFilter = dateFilter,
                Page = page ?? 1,
                PageLength = pageLength,
                RemainderOrders = sipAyrList.ToSqlPagedList(page ?? 1, pageLength, sipAyrList.Count > 0 ? (sipAyrList.FirstOrDefault().topSay) : 0)
            };

            return View(model);

        }

        public ActionResult RemainderOrderReporExport(string q, string ilkTar = "", string sonTar = "")
        {

            string aramaKriteri = "";
            if (!String.IsNullOrWhiteSpace(q))
            {
                aramaKriteri = q;
            }

            string ilkTarKosul = DateTime.Now.Date.AddDays(-30).ToString("dd.MM.yyyy");
            string sonTarKosul = DateTime.Now.Date.ToString("dd.MM.yyyy");

            if (ilkTar != "")
            {
                try
                {
                    ilkTarKosul = Convert.ToDateTime(ilkTar).Date.ToString("dd.MM.yyyy");
                }
                catch
                {
                }
            }
            if (sonTar != "")
            {
                try
                {
                    sonTarKosul = Convert.ToDateTime(sonTar).Date.ToString("dd.MM.yyyy");
                }
                catch
                {
                }
            }


            string sorgu = string.Format(@" exec [b2b].[sp_RemainderOrderReport_Vw] @frmID={0},@page=0,@pageLength=0,@basTarih='{1}',@bitTarih='{2}',@search='{3}' ", CurrentCompany.frmID, ilkTarKosul, sonTarKosul, aramaKriteri);
            var liste = Db.Database.SqlQuery<RemainderOrderReportExcel>(sorgu).ToList();

            var grid = new System.Web.UI.WebControls.GridView();

            grid.DataSource = liste;

            grid.DataBind();

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=BakiyeSiparisRaporu.xls");
            Response.ContentType = "application/excel";
            StringWriter sw = new StringWriter();
            Encoding.GetEncoding(1254).GetBytes(sw.ToString());
            Response.Charset = "";
            Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1254");
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            grid.RenderControl(htw);

            Response.Write(sw.ToString());

            Response.End();


            return null;
        }

        #endregion

        #region Ürün Bazlı Sipariş Raporu
        public ActionResult ProductBasedOrderReport(string q, DocumentDateFilterModel dateFilter, int? page)
        {
            var stkKontroluVar = StkKontroluVar;
            var stkGosterimiVar = StkGosterimiVar;

            var pageLength = 29;

            string aramaKriteri = "";
            string ekKosul = "";

            if (!String.IsNullOrWhiteSpace(q))
            {
                aramaKriteri = q;
                ekKosul = " and (urn.stkAd like '%" + aramaKriteri + "%') ";
            }

            dateFilter.NotNullOrSetDefault(DateHelper.ThisMonthBegin, DateTime.Today);

            if (dateFilter.Begin != null && dateFilter.End != null)
            {
                var dt1 = dateFilter.Begin.Value;
                var dt2 = dateFilter.End.Value;
                ekKosul += " AND (sip_vw.eTarih  BETWEEN '" + dt1.Date.ToString("dd.MM.yyyy") + "' AND '" + dt2.Date.ToString("dd.MM.yyyy") + @"') ";
            }


            string ekSelect = " ,cast(0 as Decimal) as stok,'' as gosterilenStok  ";
            string ekJoin = "";
            if (AccountType.Customer == CurrentAccountType.Value)
            {
                ekJoin = " left hash join Db.urnSatis_vw as us on us.stkID=urn.stkID ";
                ekSelect = " ,us.stok as stok,us.gosterilenStok as gosterilenStok  ";
            }




            dateFilter.NotNullOrSetDefault(DateHelper.ThisMonthBegin, DateTime.Today);
            string sorgu = @" SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 
SELECT  eID,eNo,ehNot,sifTip.hrktTipAd,urn.stkID, urn.stkKod, urn.stkAd,barkod, eTarih , eTarihS ,mekanID,mekanAd,ehAdet as siparisAdet
,ehAdetN as kalanAdet, ehAdet - ehAdetN as sevkAdet,(ehTutar-ehIndirim)/ehAdet*ehAdetN AS kalanTutar, urnFrm.urnFrmDagitici as dagiticiKod,COUNT(*) OVER() as topSay " + ekSelect + @" 
FROM sip_vw
INNER JOIN urn ON stkID=sip_vw.ehStkID
INNER JOIN frm ON frm.frmID=eFirma
INNER HASH JOIN posMagaza on mekanID=eMekan
INNER HASH JOIN sifTip ON sip_vw.eTip= sifTip.hrktTipID AND hrktSIF=0
left hash JOIN urnFrm ON sip_vw.eFirma = urnFrm.urnFrmFirmaID and urn.stkID = urnFrm.urnFrmStkID
left hash join urnBarkod_vw on urnBrkdStkID=urn.stkID
" + ekJoin + @"
WHERE  frm.frmID=" + CurrentCompany.frmID + ekKosul + @" AND sip_vw.eDurum!=2 AND sip_vw.ehDurum=0  and onay=1 
ORDER BY eTarih,eID,eNo ";
            string sorguPage = @"
OFFSET " + ((page ?? 1) - 1) + " * " + pageLength + @" ROWS 
FETCH NEXT " + pageLength + " ROWS ONLY ";

            var sipAyrList = Db.Database.SqlQuery<ProductBasedOrderModel.ProductBasedOrder>(sorgu + sorguPage).ToList();


            decimal toplamAdet = 0;
            decimal toplamKalan = 0;
            decimal toplamSevk = 0;
            decimal toplamTutar = 0;

            var topUrun = sipAyrList.Count > 0 ? (sipAyrList.FirstOrDefault().topSay) : 0;
            int topSayfa = (topUrun / pageLength) + (topUrun % pageLength > 0 ? 1 : 0);
            if (topSayfa == (page ?? 1))
            {
                var sipAyrListeTop = Db.Database.SqlQuery<ProductBasedOrderModel.ProductBasedOrder>(sorgu).ToList();
                if (sipAyrListeTop.Any())
                {
                    toplamAdet = sipAyrListeTop.Sum(x => x.siparisAdet);
                    toplamKalan = sipAyrListeTop.Sum(x => x.kalanAdet);
                    toplamSevk = sipAyrListeTop.Sum(x => x.sevkAdet);
                    toplamTutar = sipAyrListeTop.Sum(x => x.kalanTutar);
                }
            }


            var model = new ProductBasedOrderModel
            {
                topAdet = toplamAdet,
                topKalan = toplamKalan,
                topSevk = toplamSevk,
                topTutar = toplamTutar,
                Sorgu = aramaKriteri,
                DateFilter = dateFilter,
                stokGosterimi = (AccountType.Customer == CurrentAccountType.Value) ? stkGosterimiVar : 0,
                stokKontrolu = stkKontroluVar,
                Page = page ?? 1,
                PageLength = pageLength,
                ProductBasedOrders = sipAyrList.ToSqlPagedList(page ?? 1, pageLength, sipAyrList.Count > 0 ? (sipAyrList.FirstOrDefault().topSay) : 0),
                musterimi = (AccountType.Customer == CurrentAccountType.Value) ? 1 : 0
            };

            return View(model);

        }

        public ActionResult ProductBasedOrderReportExport(string q, string ilkTar = "", string sonTar = "")
        {

            string aramaKriteri = "";
            string ekKosul = "";
            if (!String.IsNullOrWhiteSpace(q))
            {
                aramaKriteri = q;
                ekKosul = " and (urn.stkAd like '%" + aramaKriteri + "%') ";
            }

            string ilkTarKosul = DateTime.Now.Date.AddDays(-30).ToString("dd.MM.yyyy");
            string sonTarKosul = DateTime.Now.Date.ToString("dd.MM.yyyy");

            if (ilkTar != "")
            {
                try
                {
                    ilkTarKosul = Convert.ToDateTime(ilkTar).Date.ToString("dd.MM.yyyy");
                }
                catch
                {
                }
            }
            if (sonTar != "")
            {
                try
                {
                    sonTarKosul = Convert.ToDateTime(sonTar).Date.ToString("dd.MM.yyyy");
                }
                catch
                {
                }
            }



            ekKosul += " AND (sip_vw.eTarih  BETWEEN '" + ilkTarKosul + "' AND '" + sonTarKosul + @"') ";


            string sorgu = @" SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 
SELECT  eID,eNo,sifTip.hrktTipAd, eTarih , eTarihS ,mekanID,mekanAd,ehNot,urn.stkID, urn.stkKod, urn.stkAd,barkod,ehAdet as siparisAdet
,ehAdetN as kalanAdet, ehAdet - ehAdetN as sevkAdet,(ehTutar-ehIndirim)/ehAdet*ehAdetN AS kalanTutar, urnFrm.urnFrmDagitici as dagiticiKod ,ehNot
FROM sip_vw
INNER JOIN urn ON stkID=sip_vw.ehStkID
INNER JOIN frm ON frm.frmID=eFirma
INNER HASH JOIN posMagaza on mekanID=eMekan
INNER HASH JOIN sifTip ON sip_vw.eTip= sifTip.hrktTipID AND hrktSIF=0
left hash JOIN urnFrm ON sip_vw.eFirma = urnFrm.urnFrmFirmaID and urn.stkID = urnFrm.urnFrmStkID
left hash join urnBarkod_vw on urnBrkdStkID=urn.stkID
WHERE  frm.frmID=" + CurrentCompany.frmID + ekKosul + @" AND sip_vw.eDurum!=2 AND sip_vw.ehDurum=0  and onay=1 
ORDER BY eTarih,eID,eNo ";

            var liste = Db.Database.SqlQuery<ProductBasedOrderReportExcel>(sorgu).ToList();


            var grid = new System.Web.UI.WebControls.GridView();

            grid.DataSource = liste;

            grid.DataBind();

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=UrunDetayBazliSiparisRaporu.xls");
            Response.ContentType = "application/excel";
            StringWriter sw = new StringWriter();
            Encoding.GetEncoding(1254).GetBytes(sw.ToString());
            Response.Charset = "";
            Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1254");
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            grid.RenderControl(htw);

            Response.Write(sw.ToString());

            Response.End();


            return null;
        }


        public ActionResult ProductBasedOrderReportXml(string q, string ilkTar = "", string sonTar = "")
        {
            string aramaKriteri = "";
            string ekKosul = "";
            if (!String.IsNullOrWhiteSpace(q))
            {
                aramaKriteri = q;
                ekKosul = " and (urn.stkAd like '%" + aramaKriteri + "%') ";
            }

            string ilkTarKosul = DateTime.Now.Date.AddDays(-30).ToString("dd.MM.yyyy");
            string sonTarKosul = DateTime.Now.Date.ToString("dd.MM.yyyy");

            if (ilkTar != "")
            {
                try
                {
                    ilkTarKosul = Convert.ToDateTime(ilkTar).Date.ToString("dd.MM.yyyy");
                }
                catch
                {
                }
            }
            if (sonTar != "")
            {
                try
                {
                    sonTarKosul = Convert.ToDateTime(sonTar).Date.ToString("dd.MM.yyyy");
                }
                catch
                {
                }
            }



            ekKosul += " AND (sip_vw.eTarih  BETWEEN '" + ilkTarKosul + "' AND '" + sonTarKosul + @"') ";


            string sorgu = @" SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 
SELECT  eID,eNo,sifTip.hrktTipAd, eTarih , eTarihS ,mekanID,mekanAd,ehNot,urn.stkID, urn.stkKod, urn.stkAd,barkod,ehAdet as siparisAdet
,ehAdetN as kalanAdet, ehAdet - ehAdetN as teslimAdet,(ehTutar-ehIndirim)/ehAdet*ehAdetN AS ehTutar, urnFrm.urnFrmDagitici as dagiticiKod ,ehNot
FROM sip_vw
INNER JOIN urn ON stkID=sip_vw.ehStkID
INNER JOIN frm ON frm.frmID=eFirma
INNER HASH JOIN posMagaza on mekanID=eMekan
INNER HASH JOIN sifTip ON sip_vw.eTip= sifTip.hrktTipID AND hrktSIF=0
left hash JOIN urnFrm ON sip_vw.eFirma = urnFrm.urnFrmFirmaID and urn.stkID = urnFrm.urnFrmStkID
left hash join urnBarkod_vw on urnBrkdStkID=urn.stkID
WHERE  frm.frmID=" + CurrentCompany.frmID + ekKosul + @" AND sip_vw.eDurum!=2 AND sip_vw.ehDurum=0  and onay=1 
ORDER BY eTarih,eID,eNo ";

            var liste = Db.Database.SqlQuery<ProductBasedOrderReportExcel>(sorgu).ToList();


            var xmlDocument = new XmlDocument();
            var xDoc = new XDocument();


            XDocument doc = new XDocument
             (
                   new XDeclaration("1.0", "utf-8", "yes"),
                       new XElement("UrunDetayBazliBakiyeSiparisRaporu"));



            foreach (var item in liste)
            {
                doc.Root.Add(
                     new XElement("Satir",
                         new XElement("SiparisNo", item.eNo),
                         new XElement("StokKod", item.stkKod),
                         new XElement("Aciklama", item.ehNot),
                         new XElement("UrunAdi", item.stkAd),
                         new XElement("Tarih", item.eTarih.ToString("dd.MM.yyyy")),
                         new XElement("GirisYeri", item.mekanAd),
                         new XElement("Sevk", item.eTarihS.ToString("dd.MM.yyyy")),
                         new XElement("Miktar", item.siparisAdet),
                         new XElement("SiparisToplamTutar", decimal.Round(item.kalanTutar, 2)),
                         new XElement("TeslimMiktar", item.sevkAdet),
                         new XElement("Kalan", item.kalanAdet),
                         new XElement("dagiticiKod", item.dagiticiKod),
                         new XElement("mekanKod", item.mekanID)));

            }


            using (var xmlReader = doc.CreateReader())
            {
                xmlDocument.Load(xmlReader);
            }
            XmlDeclaration xmldecl;
            xmldecl = xmlDocument.CreateXmlDeclaration("1.0", "windows-1254", "yes");
            XmlElement root = xmlDocument.DocumentElement;
            xmlDocument.InsertBefore(xmldecl, root);


            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename = urunDetayBazliSiparis.xml");
            Response.ContentType = "text/xml";

            var serializer = new System.Xml.Serialization.XmlSerializer(xmlDocument.GetType());
            serializer.Serialize(Response.OutputStream, xmlDocument);

            return null;

        }
        #endregion

        #region Ürün Satış Raporu
        public ActionResult ProductSalesReport(string q, DocumentDateFilterModel dateFilter, int? page, string mekanFilter = "", string mekanAdFilter = "")
        {
            var pageLength = 29;

            ViewData["mekan"] = mekanFilter;
            ViewData["mekanAd"] = mekanAdFilter;

            var mekanlar = new List<int>();
            string mekanAd = string.Empty;

            dateFilter.NotNullOrSetDefault(DateHelper.ThisMonthBegin, DateTime.Today);

            string aramaKriteri = "";
            if (!String.IsNullOrWhiteSpace(q))
            {
                aramaKriteri = q;
            }

            string sorgu = string.Format(@" exec [b2b].[sp_ProductSalesReport_Vw] @frmID={0},@page={1},@pageLength={2},@basTarih='{3}',@bitTarih='{4}',@mekan ='{5}',@search='{6}' ", CurrentCompany.frmID, page ?? 1, pageLength, dateFilter.Begin.Value.Date.ToString("dd.MM.yyyy"), dateFilter.End.Value.Date.ToString("dd.MM.yyyy"), mekanFilter, aramaKriteri);
            var sipAyrList = Db.Database.SqlQuery<ProductSalesModel.ProductSale>(sorgu).ToList();

            var topUrun = sipAyrList.Count > 0 ? (sipAyrList.FirstOrDefault().topSay) : 0;
            int topSayfa = (topUrun / pageLength) + (topUrun % pageLength > 0 ? 1 : 0);
            if ((topUrun % pageLength == 0))
            {
                pageLength = pageLength + 1;
            }

            var model = new ProductSalesModel
            {
                Sorgu = aramaKriteri,
                DateFilter = dateFilter,
                Page = page ?? 1,
                PageLength = pageLength,
                Mekanlar = mekanFilter,
                ProductSales = sipAyrList.ToSqlPagedList(page ?? 1, pageLength, sipAyrList.Count > 0 ? (sipAyrList.FirstOrDefault().topSay) : 0)
            };

            return View(model);

        }

        public ActionResult ProductSalesReportExport(string q, string mekanFilter = "", string ilkTar = "", string sonTar = "")
        {

            string aramaKriteri = "";
            if (!String.IsNullOrWhiteSpace(q))
            {
                aramaKriteri = q;
            }

            string ilkTarKosul = DateTime.Now.Date.AddDays(-30).ToString("dd.MM.yyyy");
            string sonTarKosul = DateTime.Now.Date.ToString("dd.MM.yyyy");

            if (ilkTar != "")
            {
                try
                {
                    ilkTarKosul = Convert.ToDateTime(ilkTar).Date.ToString("dd.MM.yyyy");
                }
                catch
                {
                }
            }
            if (sonTar != "")
            {
                try
                {
                    sonTarKosul = Convert.ToDateTime(sonTar).Date.ToString("dd.MM.yyyy");
                }
                catch
                {
                }
            }


            string sorgu = string.Format(@" exec [b2b].[sp_ProductSalesReport_Vw] @frmID={0},@page=0,@pageLength=0,@basTarih='{1}',@bitTarih='{2}',@mekan ='{3}',@search='{4}' ", CurrentCompany.frmID, ilkTarKosul, sonTarKosul, mekanFilter, aramaKriteri);
            var liste = Db.Database.SqlQuery<ProductSaleReportExcel>(sorgu).ToList();

            var grid = new System.Web.UI.WebControls.GridView();

            grid.DataSource = liste;

            grid.DataBind();

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=UrunSatisRaporu.xls");
            Response.ContentType = "application/excel";
            StringWriter sw = new StringWriter();
            Encoding.GetEncoding(1254).GetBytes(sw.ToString());
            Response.Charset = "";
            Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1254");
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            grid.RenderControl(htw);

            Response.Write(sw.ToString());

            Response.End();


            return null;
        }

        #endregion

        #region Tarihsel Satış Raporu
        public ActionResult HistoricalSalesReport(DocumentDateFilterModel dateFilter, int? page)
        {
            var pageLength = 29;

            dateFilter.NotNullOrSetDefault(DateHelper.ThisMonthBegin, DateTime.Today);

            string sorgu = string.Format(@" exec [b2b].[sp_HistoricalSalesReport_Vw] @frmID={0},@page={1},@pageLength={2},@basTarih='{3}',@bitTarih='{4}' ", CurrentCompany.frmID, page ?? 1, pageLength, dateFilter.Begin.Value.Date.ToString("dd.MM.yyyy"), dateFilter.End.Value.Date.ToString("dd.MM.yyyy"));

            Db.Database.CommandTimeout = 300;
            var AyrList = Db.Database.SqlQuery<HistoricalSalesModel.HistoricalSale>(sorgu).ToList();

            var model = new HistoricalSalesModel
            {
                DateFilter = dateFilter,
                Page = page ?? 1,
                PageLength = pageLength,
                HistoricalSales = AyrList.ToSqlPagedList(page ?? 1, pageLength, AyrList.Count > 0 ? (AyrList.FirstOrDefault().topSay) : 0)
            };

            return View(model);

        }

        public ActionResult HistoricalSalesReportExport(string ilkTar = "", string sonTar = "")
        {
            string ilkTarKosul = DateTime.Now.Date.AddDays(-30).ToString("dd.MM.yyyy");
            string sonTarKosul = DateTime.Now.Date.ToString("dd.MM.yyyy");

            if (ilkTar != "")
            {
                try
                {
                    ilkTarKosul = Convert.ToDateTime(ilkTar).Date.ToString("dd.MM.yyyy");
                }
                catch
                {
                }
            }
            if (sonTar != "")
            {
                try
                {
                    sonTarKosul = Convert.ToDateTime(sonTar).Date.ToString("dd.MM.yyyy");
                }
                catch
                {
                }
            }


            string sorgu = string.Format(@" exec [b2b].[sp_HistoricalSalesReport_Vw] @frmID={0},@page=0,@pageLength=0,@basTarih='{1}',@bitTarih='{2}' ", CurrentCompany.frmID, ilkTarKosul, sonTarKosul);
            var liste = Db.Database.SqlQuery<HistoricalSaleReportExcel>(sorgu).ToList();

            var grid = new System.Web.UI.WebControls.GridView();

            grid.DataSource = liste;

            grid.DataBind();

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=TarihselSatisRaporu.xls");
            Response.ContentType = "application/excel";
            StringWriter sw = new StringWriter();
            Encoding.GetEncoding(1254).GetBytes(sw.ToString());
            Response.Charset = "";
            Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1254");

            HtmlTextWriter htw = new HtmlTextWriter(sw);

            grid.RenderControl(htw);

            Response.Write(sw.ToString());

            Response.End();


            return null;
        }

        #endregion

        #region Mekansal Satış Raporu
        public ActionResult StoreSalesReport(DocumentDateFilterModel dateFilter, int? page)
        {
            var pageLength = 29;

            dateFilter.NotNullOrSetDefault(DateHelper.ThisMonthBegin, DateTime.Today);

            string sorgu = string.Format(@" exec [b2b].[sp_StoreSalesReport_Vw] @frmID={0},@page={1},@pageLength={2},@basTarih='{3}',@bitTarih='{4}' ", CurrentCompany.frmID, page ?? 1, pageLength, dateFilter.Begin.Value.Date.ToString("dd.MM.yyyy"), dateFilter.End.Value.Date.ToString("dd.MM.yyyy"));
            var AyrList = Db.Database.SqlQuery<StoreSalesModel.StoreSale>(sorgu).ToList();


            decimal toplamAdet = 0;
            decimal toplamTutar = 0;
            var topUrun = AyrList.Count > 0 ? (AyrList.FirstOrDefault().topSay) : 0;
            int topSayfa = (topUrun / pageLength) + (topUrun % pageLength > 0 ? 1 : 0);
            if (topSayfa == (page ?? 1))
            {
                string sorguTop = string.Format(@" exec [b2b].[sp_StoreSalesReport_Vw] @frmID={0},@page=0,@pageLength=0,@basTarih='{1}',@bitTarih='{2}' ", CurrentCompany.frmID, dateFilter.Begin.Value.Date.ToString("dd.MM.yyyy"), dateFilter.End.Value.Date.ToString("dd.MM.yyyy"));
                var listeTop = Db.Database.SqlQuery<StoreSaleReportExcel>(sorguTop).ToList();
                if (listeTop.Any())
                {
                    toplamAdet = listeTop.Sum(x => x.adet);
                    toplamTutar = listeTop.Sum(x => x.tutar);
                }
            }

            var model = new StoreSalesModel
            {
                DateFilter = dateFilter,
                Page = page ?? 1,
                topAdet = toplamAdet,
                topTutar = toplamTutar,
                StoreSales = AyrList.ToSqlPagedList(page ?? 1, pageLength, AyrList.Count > 0 ? (AyrList.FirstOrDefault().topSay) : 0)
            };

            return View(model);

        }

        public ActionResult StoreSalesReportExport(string ilkTar = "", string sonTar = "")
        {
            string ilkTarKosul = DateTime.Now.Date.AddDays(-30).ToString("dd.MM.yyyy");
            string sonTarKosul = DateTime.Now.Date.ToString("dd.MM.yyyy");

            if (ilkTar != "")
            {
                try
                {
                    ilkTarKosul = Convert.ToDateTime(ilkTar).Date.ToString("dd.MM.yyyy");
                }
                catch
                {
                }
            }
            if (sonTar != "")
            {
                try
                {
                    sonTarKosul = Convert.ToDateTime(sonTar).Date.ToString("dd.MM.yyyy");
                }
                catch
                {
                }
            }


            string sorgu = string.Format(@" exec [b2b].[sp_StoreSalesReport_Vw] @frmID={0},@page=0,@pageLength=0,@basTarih='{1}',@bitTarih='{2}' ", CurrentCompany.frmID, ilkTarKosul, sonTarKosul);
            var liste = Db.Database.SqlQuery<StoreSaleReportExcel>(sorgu).ToList();

            var grid = new System.Web.UI.WebControls.GridView();

            grid.DataSource = liste;

            grid.DataBind();

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=MagazaDagilimliSatisRaporu.xls");
            Response.ContentType = "application/excel";
            StringWriter sw = new StringWriter();
            Encoding.GetEncoding(1254).GetBytes(sw.ToString());
            Response.Charset = "";
            Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1254");
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            grid.RenderControl(htw);

            Response.Write(sw.ToString());

            Response.End();


            return null;
        }

        #endregion

        #region Mekansal Satış Raporu Genel
        public ActionResult GeneralStoreSalesReport(string q, DocumentDateFilterModel dateFilter, int? page)
        {
            var pageLength = 29;

            dateFilter.NotNullOrSetDefault(DateHelper.ThisMonthBegin, DateTime.Today);


            var mekanlar = Db.posMagaza.Where(x => x.mekanTip == 0).Select(x => new { x.mekanAd }).ToList();

            //foreach (var a in mekanlar)
            //{
            //    CreateAutoImplementedProperty(builder, a.mekanAd + "-ADET", typeof(decimal));
            //    CreateAutoImplementedProperty(builder, a.mekanAd + "-TUTAR", typeof(decimal));
            //}


            string aramaKriteri = "";
            if (!String.IsNullOrWhiteSpace(q))
            {
                aramaKriteri = q;
            }

            string query = string.Format(@" declare @frmID int={0}
	declare @basTarih nvarchar(max)='{1}'
	declare @bitTarih nvarchar(max)='{2}'
    declare @page int={3}
    declare @pageLength int={4}
	
	declare @Kolonlar nvarchar(max)=''
    set @Kolonlar=(Select  Substring((Select ',[' + mekanAd +'-ADET]' From posMagaza M
											  Where M.mekanTip=0 order by mekanAd For XML Path('')),2,8000) As mekanlarAdet   )
		
    declare @KolonlarTutar nvarchar(max)=''
    set @KolonlarTutar=(Select  Substring((Select ',[' + mekanAd +'-TUTAR]' From posMagaza M
											  Where M.mekanTip=0 order by mekanAd For XML Path('')),2,8000) As mekanlarTutar   )

    declare @SelectKolonlarTutar nvarchar(max)=''
    set @SelectKolonlarTutar=(Select  Substring((Select ',sum(isnull([' + mekanAd +'-ADET],0)) as [' + mekanAd +'-ADET],sum(isnull([' + mekanAd +'-TUTAR],0)) as [' + mekanAd +'-TUTAR]' From posMagaza M
											  Where M.mekanTip=0 order by mekanAd For XML Path('')),2,80000) As mekanlarTutar   )
	
	declare @sorguTemp1 nvarchar(max)=''
	SET @sorguTemp1 =' 
                        SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 
                        select topSay,stkID,stkKod,dagiticiKod,barkod,stkAd,'+@SelectKolonlarTutar+' 
					   from (
							   select * 
							   from (
											SELECT   urn.stkID,urn.stkKod,urnFrm.urnFrmDagitici as dagiticiKod,barkod,urn.stkAd,ABS(SUM(ehAdet)) AS ehAdet
											,SUM(CASE WHEN eTip IN  (3,5,101) THEN -1*(ehTutar+ehTutarKDV-ehIndirim) ELSE ehTutar+ehTutarKDV-ehIndirim END) AS ehTutar
											,mekanAd+'+char(39)+'-ADET'+char(39)+' as mekanAdet,mekanAd+'+char(39)+'-TUTAR'+char(39)+' as mekanTutar 
											, COUNT(*) OVER() as topSay           
											FROM irs_vw
											INNER JOIN urn ON stkID = ehStkID
											INNER JOIN urnBarkod_vw ON urn.stkID = urnBrkdStkID
					                        INNER JOIN urnFrm ON urn.stkFirma = urnFrm.urnFrmFirmaID and urn.stkID = urnFrm.urnFrmStkID
											inner hash join posMagaza on mekanID=eMekan
											WHERE  (eTip IN (100,101)  or (eTip in (1,4,3,5) and eDurum=1))
											and (eTarih between '+char(39)+@basTarih+char(39)+' and '+char(39)+@bitTarih+char(39)+') 
                                            " + (aramaKriteri != "" ? ("and urn.stkAd like '+char(39)+'%" + aramaKriteri + @"%'+char(39)+' ") : "") + @"
											AND urnFrm.urnFrmFirmaID='+cast(@frmID as nvarchar)+'  									
											GROUP BY urn.stkKod, urn.stkAd, urn.stkID,mekanAd,urnFrm.urnFrmDagitici,barkod
							   ) as anaTable
							   PIVOT
							   (
								  SUM(ehAdet)
								  FOR mekanAdet in ('+@Kolonlar+')
							   ) as PivotTable 
							   PIVOT
							   (
								  SUM(ehTutar)
								  FOR mekanTutar in ('+@KolonlarTutar+')
							   ) as PivotTable2 
					  ) as butunTable group by stkID,stkKod,stkAd,topSay,dagiticiKod,barkod order by stkAd,stkKod,stkID,dagiticiKod,barkod
                       OFFSET  ('+cast((@page-1) as nvarchar)+' * '+cast(@pageLength as nvarchar)+') ROWS 
					   FETCH NEXT ('+cast(@pageLength as nvarchar)+') ROWS ONLY '


   exec sp_executesql @sorguTemp1 ", CurrentCompany.frmID, dateFilter.Begin.Value.Date.ToString("dd.MM.yyyy"), dateFilter.End.Value.Date.ToString("dd.MM.yyyy"), page ?? 1, pageLength);

            DataTable dt = new DataTable();
            string conBag = System.Configuration.ConfigurationManager.ConnectionStrings["DbEntities"].ConnectionString;
            int baslangic = System.Configuration.ConfigurationManager.ConnectionStrings["DbEntities"].ConnectionString.IndexOf("provider connection string");
            if (baslangic > -1)
            {
                conBag = conBag.Substring(baslangic + 28, conBag.Length - baslangic - 29);
            }
            using (SqlConnection con = new SqlConnection(conBag))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    cmd.Connection = con;
                    con.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                    con.Close();
                }
            }



            var donenSonuc = (from DataRow rw1 in dt.Rows
                              select new GeneralStoreSalesModel.GStoreSale
                              {
                                  stkID = Convert.ToInt32(rw1["stkID"]),
                                  stkKod = rw1["stkKod"].ToString(),
                                  dagiticiKod = rw1["dagiticiKod"].ToString(),
                                  barkod = rw1["barkod"].ToString(),
                                  stkAd = rw1["stkAd"].ToString(),
                                  topSay = Convert.ToInt32(rw1["topSay"]),
                                  satir = rw1,
                              });

            var kBaslik = (from DataColumn cl1 in dt.Columns
                           where cl1.ColumnName != "stkID" && cl1.ColumnName != "stkKod" && cl1.ColumnName != "stkAd" && cl1.ColumnName != "topSay" && cl1.ColumnName != "dagiticiKod" && cl1.ColumnName != "barkod"
                           select new List<string>
                           {
                                cl1.ColumnName
                           });



            var model = new GeneralStoreSalesModel
            {
                Sorgu = aramaKriteri,
                DateFilter = dateFilter,
                Page = page ?? 1,
                cBaslik = kBaslik,
                sBaslik = kBaslik.Count(),
                GStoreSales = donenSonuc.ToSqlPagedList(page ?? 1, pageLength, donenSonuc.Count() > 0 ? (donenSonuc.FirstOrDefault().topSay) : 0)
            };

            return View(model);

        }

        public ActionResult GeneralStoreSalesReportExport(string q, string ilkTar = "", string sonTar = "")
        {
            string aramaKriteri = "";
            if (!String.IsNullOrWhiteSpace(q))
            {
                aramaKriteri = q;
            }

            string ilkTarKosul = DateTime.Now.Date.AddDays(-30).ToString("dd.MM.yyyy");
            string sonTarKosul = DateTime.Now.Date.ToString("dd.MM.yyyy");

            if (ilkTar != "")
            {
                try
                {
                    ilkTarKosul = Convert.ToDateTime(ilkTar).Date.ToString("dd.MM.yyyy");
                }
                catch
                {
                }
            }
            if (sonTar != "")
            {
                try
                {
                    sonTarKosul = Convert.ToDateTime(sonTar).Date.ToString("dd.MM.yyyy");
                }
                catch
                {
                }
            }

            string query = string.Format(@" declare @frmID int={0}
	                                        declare @basTarih nvarchar(max)='{1}'
	                                        declare @bitTarih nvarchar(max)='{2}'
	
	                                        declare @Kolonlar nvarchar(max)=''
                                            set @Kolonlar=(Select  Substring((Select ',[' + mekanAd +'-ADET]' From posMagaza M
											                                          Where M.mekanTip=0 order by mekanAd For XML Path('')),2,8000) As mekanlarAdet   )
		
                                            declare @KolonlarTutar nvarchar(max)=''
                                            set @KolonlarTutar=(Select  Substring((Select ',[' + mekanAd +'-TUTAR]' From posMagaza M
											                                          Where M.mekanTip=0 order by mekanAd For XML Path('')),2,8000) As mekanlarTutar   )

                                            declare @SelectKolonlarTutar nvarchar(max)=''
                                            set @SelectKolonlarTutar=(Select  Substring((Select ',sum(isnull([' + mekanAd +'-ADET],0)) as [' + mekanAd +'-ADET],sum(isnull([' + mekanAd +'-TUTAR],0)) as [' + mekanAd +'-TUTAR]' From posMagaza M
											                                          Where M.mekanTip=0 order by mekanAd For XML Path('')),2,80000) As mekanlarTutar   )
	
	                                        declare @sorguTemp1 nvarchar(max)=''
	                                        SET @sorguTemp1 =' 
                                                                SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 
                                                                select topSay,stkID,stkKod,dagiticiKod,barkod,stkAd,'+@SelectKolonlarTutar+' 
					                                           from (
							                                           select * 
							                                           from (
											                                        SELECT   urn.stkID,urn.stkKod,urnFrm.urnFrmDagitici as dagiticiKod,barkod,urn.stkAd,ABS(SUM(ehAdet)) AS ehAdet
											                                        ,SUM(CASE WHEN eTip IN  (3,5,101) THEN -1*(ehTutar+ehTutarKDV-ehIndirim) ELSE ehTutar+ehTutarKDV-ehIndirim END) AS ehTutar
											                                        ,mekanAd+'+char(39)+'-ADET'+char(39)+' as mekanAdet,mekanAd+'+char(39)+'-TUTAR'+char(39)+' as mekanTutar 
											                                        , COUNT(*) OVER() as topSay           
											                                        FROM irs_vw
											                                        INNER JOIN urn ON stkID = ehStkID
											                                        INNER JOIN urnBarkod_vw ON urn.stkID = urnBrkdStkID
					                                                                INNER JOIN urnFrm ON urn.stkFirma = urnFrm.urnFrmFirmaID and urn.stkID = urnFrm.urnFrmStkID
											                                        inner hash join posMagaza on mekanID=eMekan
											                                        WHERE  (eTip IN (100,101)  or (eTip in (1,4,3,5) and eDurum=1))
											                                        and (eTarih between '+char(39)+@basTarih+char(39)+' and '+char(39)+@bitTarih+char(39)+') 
                                                                                    " + (aramaKriteri != "" ? ("and urn.stkAd like '+char(39)+'%" + aramaKriteri + @"%'+char(39)+' ") : "") + @"
											                                        AND urnFrm.urnFrmFirmaID='+cast(@frmID as nvarchar)+'  									
											                                        GROUP BY urn.stkKod, urn.stkAd, urn.stkID,mekanAd,urnFrm.urnFrmDagitici,barkod
							                                           ) as anaTable
							                                           PIVOT
							                                           (
								                                          SUM(ehAdet)
								                                          FOR mekanAdet in ('+@Kolonlar+')
							                                           ) as PivotTable 
							                                           PIVOT
							                                           (
								                                          SUM(ehTutar)
								                                          FOR mekanTutar in ('+@KolonlarTutar+')
							                                           ) as PivotTable2 
					                                          ) as butunTable group by stkID,stkKod,stkAd,topSay,dagiticiKod,barkod order by stkAd,stkKod,stkID,dagiticiKod,barkod  '


                                           exec sp_executesql @sorguTemp1 ", CurrentCompany.frmID, ilkTarKosul, sonTarKosul);


            DataTable dt = new DataTable();
            string conBag = System.Configuration.ConfigurationManager.ConnectionStrings["DbEntities"].ConnectionString;
            int baslangic = System.Configuration.ConfigurationManager.ConnectionStrings["DbEntities"].ConnectionString.IndexOf("provider connection string");
            if (baslangic > -1)
            {
                conBag = conBag.Substring(baslangic + 28, conBag.Length - baslangic - 29);
            }
            using (SqlConnection con = new SqlConnection(conBag))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    cmd.Connection = con;
                    con.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                    con.Close();
                }
            }


            if (dt.Rows.Count > 0)
            {
                var grid = new System.Web.UI.WebControls.GridView();

                grid.DataSource = dt;

                grid.DataBind();

                Response.ClearContent();
                Response.AddHeader("content-disposition", "attachment; filename=UrunMagazaSatisRaporu.xls");
                Response.ContentType = "application/excel";
                StringWriter sw = new StringWriter();
                Encoding.GetEncoding(1254).GetBytes(sw.ToString());
                Response.Charset = "";
                Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1254");
                HtmlTextWriter htw = new HtmlTextWriter(sw);

                grid.RenderControl(htw);

                Response.Write(sw.ToString());

                Response.End();
            }

            return null;
        }

        #endregion





    }
}



