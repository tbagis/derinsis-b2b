﻿using System.Collections.ObjectModel;
using System.Data;

namespace DerinSIS.B2B.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using Helpers;
    using Helpers.Html;
    using Helpers;
    using Infrastructure.Auth;
    using Models;
    using ViewModels;
    using System.Web.Script.Serialization;
    using System.Data.Entity.Validation;
    using System.Net;
    using System.Net.Mail;

    [B2BAuthorize(AccountType.Customer, Privilege.SIPARIS)]
    public class StoreController : BaseController
    {
        public StoreController(DbEntities db)
            : base(db)
        {
        }

        #region Product List

        public ActionResult Index(string q, int? categoryId, string brandId, int? producerId, int? page, string kmp, string price, string priceLabel, string ynUrn, string stogaYeniGirenUrn, string oneCikanUrn, string ordercondition)
        {
            var model = new ProductListModel();

            page = page ?? 1;
            var pageLength = 28;

            int _brandID = -1;
            var _brandGrupID = 0;
            if (brandId != null && brandId != "")
            {
                if (brandId.IndexOf('-') > -1)
                {
                    _brandID = Convert.ToInt32(brandId.Split('-')[0]);
                    _brandGrupID = Convert.ToInt32(brandId.Split('-')[1]);
                }
                else
                {
                    _brandID = Convert.ToInt32(brandId);
                }
            }


            if (!String.IsNullOrWhiteSpace(ordercondition))
                model.siralama = ordercondition;


            var yeniUrunListesi = Db.urnYeniler_vw.Select(x => x.stkID).ToList();
            if (yeniUrunListesi.Count() == 0)
            {
                model.UrunYeniler = -1;
            }
            else
            {
                model.UrunYeniler = (ynUrn == "on") ? 1 : 0;
            }

            var stogaYeniGirenUrnListesi = Db.urnStogaYeniGirenler_vw.Select(x => x.stkID).ToList();
            if (stogaYeniGirenUrnListesi.Count() == 0)
            {
                model.UrunStogaYeniGirenler = -1;
            }
            else
            {
                model.UrunStogaYeniGirenler = (stogaYeniGirenUrn == "on") ? 1 : 0;
            }

            var OneCikanUrnListesi = Db.urnOneCikanlar_vw.Select(x => x.stkID).ToList();
            if (OneCikanUrnListesi.Count() == 0)
            {
                model.UrunOneCikanlar = -1;
            }
            else
            {
                model.UrunOneCikanlar = (oneCikanUrn == "on") ? 1 : 0;
            }

            model.OzelFiyat = kmp == "on" ? true : false;

            string sorguEkKosul = " where 1=1 ";
            if (!String.IsNullOrWhiteSpace(q))
            {
                model.Sorgu = q;
                if (isNumeric(q) && q.Length >= 8)
                {
                    sorguEkKosul += " and urnSatis_vw.stkID in (select (case when urnBrkdAltStkId>0 then urnBrkdAltStkId else urnBrkdStkID end) as urnBrkdStkID from urnBrkd where urnBarkod like '%" + q + "%') ";
                }
                else
                {
                    var qstr = q.Split(' ').Select(x => x.ToLower()).ToArray();
                    if (qstr.Count() > 0)
                    {
                        sorguEkKosul += " and urnSatis_vw.stkID in (select stkID from b2b.urnSatis_vw where stkKod like '%" + q + "%' or mrkAd like '%" + q + "%' or ( ";
                        for (int i = 0; i < qstr.Count(); i++)
                        {
                            if (i + 1 == qstr.Count())
                            {
                                sorguEkKosul += " stkAd like '%" + qstr[i] + "%' ";
                            }
                            else
                            {
                                sorguEkKosul += " stkAd like '%" + qstr[i] + "%' and ";
                            }
                        }
                        sorguEkKosul += " )) ";
                    }
                }
            }

            /* Category Filter */
            if (categoryId != null)
            {
                var kategori = Db.urnKtgrAg_vw.Single(x => x.kID == categoryId);
                model.KategoriIdFiltresi = kategori.kID;
                model.KategoriFiltreEtiketi = kategori.kAd;


                sorguEkKosul += " and urnKtgrID=" + kategori.k0 + " ";

                if (kategori.k1 != 0)
                {
                    sorguEkKosul += " and urnKtgrID1=" + kategori.k1 + " ";
                }

                if (kategori.k2 != 0)
                {
                    sorguEkKosul += " and urnKtgrID2=" + kategori.k2 + " ";
                }

                if (kategori.k3 != 0)
                {
                    sorguEkKosul += " and urnKtgrID3=" + kategori.k3 + " ";
                }
            }

            /* Producer Filter */
            if (producerId != null)
            {
                var uretici = Db.urnMrkUrt.Single(x => x.urtID == producerId);
                model.UreticiIdFiltresi = uretici.urtID;
                model.UreticiFiltreEtiketi = uretici.urtAd;

                sorguEkKosul += " and urtID=" + uretici.urtID + " ";
            }


            /* Brand Filter */
            if (_brandID != -1)
            {
                if (_brandGrupID == 1)
                {
                    var markaGrup = Db.urnMrkGrp.Single(x => x.urnMrkGrpID == _brandID);
                    model.MarkaIdFiltresi = brandId;
                    model.MarkaFiltreEtiketi = markaGrup.urnMrkGrpAd;

                    sorguEkKosul += " and urnMrkGrpID=" + markaGrup.urnMrkGrpID + " ";
                }
                else
                {
                    var marka = Db.urnMrk.Single(x => x.mrkID == _brandID);
                    model.MarkaIdFiltresi = brandId;
                    model.MarkaFiltreEtiketi = marka.mrkAd;

                    sorguEkKosul += " and urnMrkID=" + marka.mrkID + " ";
                }
            }

            if (model.OzelFiyat)
            {
                var ekIndirim = Db.fytOzl
                    .Where(x => x.fTip == 4)
                    //.Where(x => x.fFrmID == 0)
                    .Where(x => x.fTarih <= DateTime.Today.Date && x.fTarihSon >= DateTime.Today.Date)
                    .Where(x => x.fTur == 0)
                    .Where(x => x.onay == 1);

                sorguEkKosul += " and urnSatis_vw.stkID in( select fStkID from fytOzl where fTip=4 and fTur=0 and onay=1 and cast(fTarih as date) <='" + DateTime.Today.Date.ToString("dd.MM.yyy") + "' and '" + DateTime.Today.Date.ToString("dd.MM.yyy") + "'<=cast(fTarihSon as date) ) ";
            }

            if (price != null && price != "")
            {
                var esik = price.Split('-');
                int min = int.Parse(esik[0]);
                int max = esik[1] != "" ? int.Parse(esik[1]) : 99999999;
                model.FiyatFiltresi = price;
                model.FiyatFiltreEtiketi = priceLabel;

                sorguEkKosul += " and ((case when odemeKur>0 then odemeKur*fiyatS else fiyatS end)>=" + min + " and (case when odemeKur>0 then odemeKur*fiyatS else fiyatS end)<=" + max + ") ";
            }

            if (model.UrunYeniler > 0)
            {
                sorguEkKosul += " and urnSatis_vw.stkID in (select stkID from b2b.urnYeniler_vw) ";
            }

            if (model.UrunStogaYeniGirenler > 0)
            {
                sorguEkKosul += " and urnSatis_vw.stkID in (select stkID from b2b.urnStogaYeniGirenler_vw) ";
            }

            if (model.UrunOneCikanlar > 0)
            {
                sorguEkKosul += " and urnSatis_vw.stkID in (select stkID from b2b.urnOneCikanlar_vw) ";
            }

            //List<int> markaUrunleri = new List<int>();
            string musteriMarkaKosulEkJoin = "";
            string musteriMarkaKosulEkWhere = "";
            int musteriMarkaKosulu = MusteriMarkalarinaAitUrunleriGorsun;
            if (musteriMarkaKosulu > 0)
            {
                musteriMarkaKosulEkJoin = " inner join sakMrk on sakMrk.skMrkID=urnSatis_vw.urnMrkID ";
                musteriMarkaKosulEkWhere = " and sakMrk.skFrmID=" + CurrentAccount.frm.frmID + " ";
            }

            string orderKosul = " order by (case when urnGrpID = 0 then rtb else 0 end),(case when urnGrpID = 0 then stkAd else grpAd end),stkID ";

            var stkKontroluVar = StkKontroluVar;
            var stkGosterimiVar = StkGosterimiVar;
            var brutFiyatGosterimiVar = BrutFiyatGostermiVar;
            model.StokGosterimiVar = stkGosterimiVar;

            if (!String.IsNullOrWhiteSpace(ordercondition))
            {
                switch (ordercondition)
                {
                    case "0-0":
                        orderKosul = " order by (case when urnGrpID = 0 then stkKod else '' end),(case when urnGrpID = 0 then stkAd else grpAd end),stkID ";
                        break;
                    case "0-1":
                        orderKosul = " order by (case when urnGrpID = 0 then stkKod else '' end) desc,(case when urnGrpID = 0 then stkAd else grpAd end),stkID ";
                        break;
                    case "1-0":
                        orderKosul = " order by (case when urnGrpID = 0 then stkAd else grpAd end),stkID ";
                        break;
                    case "1-1":
                        orderKosul = " order by (case when urnGrpID = 0 then stkAd else grpAd end) desc,stkID ";
                        break;
                    case "2-0":
                        orderKosul = " order by (case when urnGrpID = 0 then barkod else '' end),(case when urnGrpID = 0 then stkAd else grpAd end),stkID ";
                        break;
                    case "2-1":
                        orderKosul = " order by (case when urnGrpID = 0 then barkod else '' end) desc,(case when urnGrpID = 0 then stkAd else grpAd end),stkID ";
                        break;
                    case "3-0":
                        orderKosul = " order by (case when urnGrpID = 0 then urnKoli else 1 end),(case when urnGrpID = 0 then stkAd else grpAd end),stkID ";
                        break;
                    case "3-1":
                        orderKosul = " order by (case when urnGrpID = 0 then urnKoli else 1 end) desc,(case when urnGrpID = 0 then stkAd else grpAd end),stkID ";
                        break;
                    case "4-0":
                        orderKosul = " order by (case when urnGrpID = 0 then urnKutu else -1 end),(case when urnGrpID = 0 then stkAd else grpAd end),stkID ";
                        break;
                    case "4-1":
                        orderKosul = " order by (case when urnGrpID = 0 then urnKutu else -1 end) desc,(case when urnGrpID = 0 then stkAd else grpAd end),stkID ";
                        break;
                    case "5-0":
                        orderKosul = " order by (case when urnGrpID = 0 then (indirim1+indirim2+indirim3) else 0 end),(case when urnGrpID = 0 then stkAd else grpAd end),stkID ";
                        break;
                    case "5-1":
                        orderKosul = " order by (case when urnGrpID = 0 then (indirim1+indirim2+indirim3) else 0 end) desc,(case when urnGrpID = 0 then stkAd else grpAd end),stkID ";
                        break;
                    case "6-0":
                        orderKosul = " order by (case when urnGrpID = 0 then indirimek else 0 end),(case when urnGrpID = 0 then stkAd else grpAd end),stkID ";
                        break;
                    case "6-1":
                        orderKosul = " order by (case when urnGrpID = 0 then indirimek else 0 end) desc,(case when urnGrpID = 0 then stkAd else grpAd end),stkID ";
                        break;
                    case "7-0":
                        orderKosul = " order by (case when urnGrpID = 0 then (fiyatS + (fiyatS * urnKDV.kdvYuzde / 100.00)) else 0 end),(case when urnGrpID = 0 then stkAd else grpAd end),stkID ";
                        break;
                    case "7-1":
                        orderKosul = " order by (case when urnGrpID = 0 then (fiyatS + (fiyatS * urnKDV.kdvYuzde / 100.00)) else 0 end) desc,(case when urnGrpID = 0 then stkAd else grpAd end),stkID ";
                        break;
                    case "8-0":
                        if (stkGosterimiVar > 0)
                        {
                            orderKosul = " order by (case when urnGrpID = 0 then gosterilenStok else '' end),(case when urnGrpID = 0 then stkAd else grpAd end),stkID ";
                        }
                        else
                        {
                            orderKosul = " order by (case when urnGrpID = 0 then stok else 0 end),(case when urnGrpID = 0 then stkAd else grpAd end),stkID ";
                        }
                        break;
                    case "8-1":
                        if (stkGosterimiVar > 0)
                        {
                            orderKosul = " order by (case when urnGrpID = 0 then gosterilenStok else '' end) desc,(case when urnGrpID = 0 then stkAd else grpAd end),stkID ";
                        }
                        else
                        {
                            orderKosul = " order by (case when urnGrpID = 0 then stok else 0 end) desc,(case when urnGrpID = 0 then stkAd else grpAd end),stkID ";
                        }
                        break;
                    case "9-0":
                        orderKosul = " order by (case when urnGrpID = 0 then stok else 0 end),(case when urnGrpID = 0 then stkAd else grpAd end),stkID ";
                        break;
                    case "9-1":
                        orderKosul = " order by (case when urnGrpID = 0 then stok else 0 end) desc,(case when urnGrpID = 0 then stkAd else grpAd end),stkID ";
                        break;
                }
            }


            string urunSorgu = @"  select  urnSatis_vw.stkID,urnSatis_vw.stkKod,urnSatis_vw.stkAd,urnSatis_vw.mrkAd,urnSatis_vw.grpAd,
            urnSatis_vw.urnKtgrID,urnSatis_vw.urnKtgrID1,urnSatis_vw.urnKtgrID2,urnSatis_vw.urnKtgrID3,
            urnSatis_vw.urtID,urnSatis_vw.urnMrkID,urnSatis_vw.urnGrpID,urnBarkod_vw.barkod,urnSatis_vw.rtb,
            urnSatis_vw.odemeKur,
			case " + CurrentCompany.frmFiyatTur + @" when 1 then urnSatis_vw.fiyatS1 when 2 then urnSatis_vw.fiyatS2 when 3 then urnSatis_vw.fiyatS3 when 4then  urnSatis_vw.fiyatS4 else urnSatis_vw.fiyatS end as fiyatS,
            urnSatis_vw.odemeKisaAd,
            case when indirim.skY1 is not null then indirim.skY1 else 0 end as indirim1,
            case when indirim.skY2 is not null then indirim.skY2 else 0 end as indirim2,
            case when indirim.skY3 is not null then indirim.skY3 else 0 end as indirim3,
            case when ekIndirim.fInd1 is not null then ekIndirim.fInd1 else 0 end as indirimek,
			urnSatis_vw.urnMrkGrpID,
			urnSatis_vw.urnKoli,
			urnSatis_vw.urnKutu,
			urnSatis_vw.stok,
			urnKdv.kdvYuzde as urnKdvYuzde,
			urnSatis_vw.gosterilenStok,COUNT(*) OVER() as topSay
	        from b2b.urnSatis_vw as urnSatis_vw
	        inner join b2b.urnBarkod_vw as urnBarkod_vw on urnSatis_vw.stkID = urnBarkod_vw.urnBrkdStkID
	        left join urnKdv on kdvID=urnSatis_vw.KDVs 	   
	        left join (select skY1,skY2,skY3,skMrkID from sakMrk_vw
	                   where skFrmID=" + CurrentAccount.frm.frmID + " and skTur=" + CurrentCompany.frmVadeTur + @"
			           ) as indirim on skMrkID=urnSatis_vw.urnMrkID 
	        left join ( select fInd1,fStkID from (
          	            select fInd1,fStkID,ROW_NUMBER() over (partition by fStkID Order by fTarih desc,fID desc) as sira  
				        from fytOzl 
	                    where fTip=4 and fTarih<=cast(getdate() as date) and fTarihSon>=cast(getdate() as date) and fTur=0 and onay=1
                       ) as t where sira=1) as ekIndirim on ekIndirim.fStkID=urnSatis_vw.stkID " + musteriMarkaKosulEkJoin + " where 1=1 " + musteriMarkaKosulEkWhere + sorguEkKosul + orderKosul + @"
            OFFSET " + ((page ?? 1) - 1) + " * " + pageLength + @" ROWS 
            FETCH NEXT " + pageLength + " ROWS ONLY ";
            var query = Db.Database.SqlQuery<ProductListQueryModel>(urunSorgu).ToList().AsQueryable();
            // Arama Sonuç Listesi
            // Sıralı bir şekilde Model ve/veya Ürünler içerir.


            var urunler = (from urn in query
                           where urn.urnGrpID == 0
                           select new ProductListModel.ProductModel
                           {
                               Id = urn.stkID,
                               Isim = urn.stkAd,
                               StokKodu = urn.stkKod,
                               Barkod = urn.barkod != null ? urn.barkod : "",
                               KDVOrani = urn.urnKdvYuzde,
                               KDVHaricFiyat = urn.fiyatS,
                               Indirim1 = urn.indirim1,
                               Indirim2 = urn.indirim2,
                               Indirim3 = urn.indirim3,
                               Indirim4 = urn.indirimek,
                               Birim = urn.odemeKisaAd,
                               Kur = urn.odemeKur,
                               Rutbe = urn.rtb,
                               urnKoli = urn.urnKoli,
                               urnKutu = urn.urnKutu,
                               stok = urn.stok,
                               stokKontrolu = stkKontroluVar,
                               gosterilenStok = urn.gosterilenStok,
                               stokGosterimi = stkGosterimiVar,
                               brutFiyatGosterimVar = brutFiyatGosterimiVar,
                               KucukResimUrl = Url.Action("ProductImage", "Thumbnail", new { id = urn.stkID, width = 55, height = 55 }),
                               BuyukResimUrl = Url.Action("ProductImage", "Thumbnail", new { id = urn.stkID, width = 185, height = 165 }),
                               BagliOlduguModelId = 0,
                               topSay = urn.topSay
                           }).OrderBy(x => x.Rutbe).ThenBy(x => x.Isim)
              .ToList()
              .Cast<ProductListModel.ProductListItemModel>();


            var ayarR = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 771);
            bool grupResmiYoksaUrunden = (ayarR == null) ? false : byte.Parse(ayarR.ayarDeger) > 0;

            // Arama sonuçlarında bulunan modellerin listesi
            var modelurunIdleri = query.Where(x => x.urnGrpID != 0).Select(x => x.urnGrpID);
            var modeller = (from urnGrp in Db.urnGrp.AsEnumerable()
                            where modelurunIdleri.Contains(urnGrp.grpID)
                            let indirim = Db.sakMrk_vw
                                .Where(x => x.skFrmID == CurrentCompany.frmID)
                                .Where(x => x.skTur == CurrentCompany.frmVadeTur)
                                .FirstOrDefault(x => x.skMrkID == urnGrp.grpMrkID)
                            select new ProductListModel.ProductGroupModel
                            {
                                Id = urnGrp.grpID,
                                Isim = urnGrp.grpAd,
                                Indirim1 = indirim != null ? indirim.skY1 : 0,
                                Indirim2 = indirim != null ? indirim.skY2 : 0,
                                Indirim3 = indirim != null ? indirim.skY3 : 0,
                                stokGosterimi = stkGosterimiVar,
                                gosterilenStok = "",
                                KucukResimUrl = Url.Action("ModelImage", "Thumbnail", new { id = urnGrp.grpID, width = 55, height = 55, yoksaUrunden = grupResmiYoksaUrunden }),
                                BuyukResimUrl = Url.Action("ModelImage", "Thumbnail", new { id = urnGrp.grpID, width = 185, height = 165, yoksaUrunden = grupResmiYoksaUrunden }),
                                Urunler = (from urn in query
                                           where urn.urnGrpID == urnGrp.grpID
                                           select new ProductListModel.ProductModel
                                           {
                                               Id = urn.stkID,
                                               Isim = urn.stkAd,
                                               StokKodu = urn.stkKod,
                                               Barkod = urn.barkod != null ? urn.barkod : "",
                                               KDVOrani = urn.urnKdvYuzde,
                                               KDVHaricFiyat = urn.fiyatS,
                                               Indirim1 = urn.indirim1,
                                               Indirim2 = urn.indirim2,
                                               Indirim3 = urn.indirim3,
                                               Indirim4 = urn.indirimek,
                                               BagliOlduguModelId = urnGrp.grpID,
                                               Birim = urn.odemeKisaAd,
                                               Kur = urn.odemeKur,
                                               Rutbe = urn.rtb,
                                               urnKoli = urn.urnKoli,
                                               urnKutu = urn.urnKutu,
                                               stok = urn.stok,
                                               stokKontrolu = stkKontroluVar,
                                               gosterilenStok = urn.gosterilenStok,
                                               stokGosterimi = stkGosterimiVar,
                                               brutFiyatGosterimVar = brutFiyatGosterimiVar,
                                               KucukResimUrl = Url.Action("ProductImage", "Thumbnail", new { id = urn.stkID, width = 55, height = 55, bagliOlduguModelID = urnGrp.grpID }),
                                               BuyukResimUrl = Url.Action("ProductImage", "Thumbnail", new { id = urn.stkID, width = 185, height = 165 }),
                                               topSay = urn.topSay
                                           }).OrderBy(x => x.Rutbe).ThenBy(x => x.Isim)
                            }).ToList();



            // ürünleri ve modelleri, harmanlıyıp tekrar sıralıyoruz
            var itemList = urunler.Union(modeller);
            if (modelurunIdleri.Count() > 0)
            {
                itemList = itemList.OrderBy(x => x.Rutbe).ThenBy(x => x.Isim);
            }


            model.AramaSonuclari = itemList.ToSqlPagedList(page ?? 1, pageLength, query.Count() > 0 ? query.FirstOrDefault().topSay : 0);
            model.Page = page ?? 1;
            model.PageLength = pageLength;
            GC.Collect();
            var ayar = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 765);
            ViewBag.KategoriYok = ayar == null ? false : byte.Parse(ayar.ayarDeger) > 0;

            return View(model);
        }

        private bool isNumeric(string s)
        {
            try
            {
                decimal a = decimal.Parse(s);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        public JsonResult AutoCompleteArama(string term)
        {

            var qstr = term.Split(' ').Select(x => x.ToLower()).ToArray();

            var result = Db.urnSatis_vw.Where(x =>
                            qstr.All(y => x.stkAd.ToLower().Contains(y)))
                            //|| x.stkKod.Contains(q) 
                            //|| x.mrkAd.Contains(q))
                            .Select(x => new { VDad = x.stkAd }).Distinct().ToList();



            //var result = (from fvd in Db.urnSatis_vw
            //              where fvd.stkAd.ToLower().Contains(term.ToLower())
            //              select new { VDad = fvd.stkAd }).Distinct().ToList();
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        #endregion


        #region Product List Excel

        public ActionResult TransferExcel(string q, int? categoryId, int? brandId, int? producerId, int? page, string kmp, string price, string priceLabel, string excellistesi)
        {
            page = page ?? 1;
            var pageLength = 5000;

            var exceller = new Collection<ProductListModelExcel.ExListe>();
            var excel = new ProductListModelExcel.ExListe();
            var excellerHatali = new Collection<ProductListModelExcel.ExListeHatali>();
            var excelHatali = new ProductListModelExcel.ExListeHatali();
            string[] gelenSatirlar = new string[] { };
            string[] kolonAyir = new string[] { };

            if (excellistesi != null)
            {
                gelenSatirlar = excellistesi.Split((char)13);
                for (int i = 0; i < gelenSatirlar.Count(); i++)
                {
                    kolonAyir = gelenSatirlar[i].Split((char)9);
                    if (kolonAyir.Count() == 2)
                    {
                        try
                        {
                            decimal AdetTut = decimal.Parse(kolonAyir[1].Replace((char)10, ' ').Replace(" ", ""));
                            if (AdetTut > 0)
                            {
                                excel = new ProductListModelExcel.ExListe { Barkod = kolonAyir[0].Replace((char)10, ' ').Replace(" ", ""), Adet = AdetTut };
                                exceller.Add(excel);
                            }
                        }
                        catch
                        {
                            excelHatali = new ProductListModelExcel.ExListeHatali { Barkod = kolonAyir[0].Replace((char)10, ' ').Replace(" ", ""), Adet = kolonAyir[1].Replace((char)10, ' ').Replace(" ", "") };
                            excellerHatali.Add(excelHatali);
                        }



                        //excels = new ProductListModelExcel.ExcelListe { Barkod = excellistesi.ToString().Split(' ')[0].Split('\t')[0], Adet = excellistesi.ToString().Split(' ')[0].Split('\t')[1] };
                        //excels.Add(excel);
                    }
                    else
                    {
                        if (kolonAyir.Count() < 2)
                        {
                            excelHatali = new ProductListModelExcel.ExListeHatali { Barkod = kolonAyir[0].Replace((char)10, ' ').Replace(" ", ""), Adet = "" };
                            excellerHatali.Add(excelHatali);
                        }
                        else
                        {
                            excelHatali = new ProductListModelExcel.ExListeHatali { Barkod = kolonAyir[0].Replace((char)10, ' ').Replace(" ", ""), Adet = kolonAyir[1].Replace((char)10, ' ').Replace(" ", "") };
                            excellerHatali.Add(excelHatali);
                        }
                    }
                }
            }

            List<int> markaUrunleri = new List<int>();
            int musteriMarkaKosulu = MusteriMarkalarinaAitUrunleriGorsun;

            if (musteriMarkaKosulu > 0)
            {
                markaUrunleri = Db.urnSatis_vw
                                .Join(Db.sakMrk, x => x.urnMrkID, y => y.skMrkID, (urnSatis, skMarka) => new { urnSatis, skMarka })
                                .Where(x => x.skMarka.skFrmID == CurrentAccount.frm.frmID)
                                .Select(x => x.urnSatis.stkID).Distinct().ToList();
            }

            var model = new ProductListModelExcel();

            var query = (from excelListesi in exceller
                         join brkd in Db.urnBrkd on excelListesi.Barkod equals brkd.urnBarkod
                         join urnSatis_vw in Db.urnSatis_vw on (brkd.urnBrkdAltStkId > 0 ? brkd.urnBrkdAltStkId : brkd.urnBrkdStkID) equals urnSatis_vw.stkID
                         where musteriMarkaKosulu == 0 || markaUrunleri.Contains(urnSatis_vw.stkID)
                         select new
                         {
                             urnSatis_vw.stkID,
                             urnSatis_vw.stkKod,
                             urnSatis_vw.stkAd,
                             urnSatis_vw.mrkAd,
                             urnSatis_vw.grpAd,
                             urnSatis_vw.urnKtgrID,
                             urnSatis_vw.urnKtgrID1,
                             urnSatis_vw.urnKtgrID2,
                             urnSatis_vw.urnKtgrID3,
                             urnSatis_vw.urtID,
                             urnSatis_vw.urnMrkID,
                             urnSatis_vw.urnGrpID,
                             brkd.urnBarkod,
                             urnSatis_vw.rtb,
                             excelListesi.Adet,
                         }
                        )
                .AsQueryable();



            if (!string.IsNullOrWhiteSpace(q))
            {

                model.Sorgu = q;
                if (isNumericExcel(q) && q.Length >= 8)
                {
                    var barkodlar = Db.urnBrkd.Where(x => x.urnBarkod.Contains(q)).Select(x => x.urnBrkdAltStkId > 0 ? x.urnBrkdAltStkId : x.urnBrkdStkID).ToList();
                    query = query.Where(x => barkodlar.Contains(x.stkID));
                }
                else
                {
                    var qstr = q.Split(' ').Select(x => x.ToLower()).ToArray();
                    query = query.Where(x =>
                                        qstr.All(y => x.stkAd.Contains(y)) ||
                                        x.stkKod.Contains(q) ||
                                        x.mrkAd.Contains(q) ||
                                        x.grpAd.Contains(q));
                }
            }

            // Arama Sonuç Listesi
            // Sıralı bir şekilde Model ve/veya Ürünler içerir.
            var searchResultList = query
                .GroupBy(x => new { Id = x.stkID, Rutbe = x.rtb, Isim = x.stkAd, stkKodu = x.stkKod })
                .OrderBy(x => x.Key.Rutbe).ThenBy(x => x.Key.Isim)
                .Select(y => new { Id = y.Key.Id, Rutbe = y.Key.Rutbe, UrnAdeti = y.Sum(x => x.Adet), Isim = y.Key.Isim, stkKodu = y.Key.stkKodu })
                .ToList();

            var urunIdleri = searchResultList.Select(x => new { stkIDsi = x.Id, stkAdeti = x.UrnAdeti, stkAdi = x.Isim, stkRtb = x.Rutbe, stkKodd = x.stkKodu }).ToList();
            // Arama sonuçlarında bulunanan ürünlerin listesi
            var urunler = (from excelListesi in urunIdleri
                           let barkod = Db.urnBarkod_vw.FirstOrDefault(x => x.urnBrkdStkID == excelListesi.stkIDsi)
                           select new ProductListModelExcel.ProductModelExcel
                           {
                               Id = excelListesi.stkIDsi,
                               Isim = excelListesi.stkAdi,
                               StokKodu = excelListesi.stkKodd,
                               Barkod = barkod != null ? barkod.barkod : "",
                               Rutbe = excelListesi.stkRtb,
                               Adeti = excelListesi.stkAdeti,
                           })
                .ToList()
                .Cast<ProductListModelExcel.ProductListItemModelExcel>();

            var itemList = urunler.OrderBy(x => x.Rutbe).ThenBy(x => x.Isim);

            model.AramaSonuclariExcel = itemList.ToSqlPagedList(page, pageLength, itemList.Count());
            //model.AramaSonuclariExcel = new PagedList<ProductListModelExcel.ProductListItemModelExcel>(
            //    itemList,
            //    searchResultList.Page,
            //    searchResultList.ItemsPerPage,
            //    searchResultList.TotalItems);

            var ayar = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 765);
            ViewBag.KategoriYok = ayar == null ? false : byte.Parse(ayar.ayarDeger) > 0;

            return View(model);
        }


        public ActionResult TransferExcelYukle(string JSonModel)
        {
            var Model = new JavaScriptSerializer().Deserialize<ICollection<DerinSIS.B2B.ViewModels.ProductListModelExcel.ExListe>>(JSonModel);


            var aktifSepetId = Db.b2bSepet.
                  Where(x => x.KullaniciId == CurrentAccount.ytkID).
                  First(x => x.Aktif).Id;

            var aktifSepetAyr = Db.b2bSepet.
                Where(x => x.Id == aktifSepetId).
                SelectMany(x => x.b2bSepetAyr);

            int strvar = 0;
            foreach (DerinSIS.B2B.ViewModels.ProductListModelExcel.ExListe model in Model)
            {
                var varmi = aktifSepetAyr.Any(x => x.UrunId == model.Id);
                var urun = new b2bSepetAyr();
                if (varmi)
                    urun = aktifSepetAyr.SingleOrDefault(x => x.UrunId == model.Id);
                else
                {
                    urun.SepetId = aktifSepetId;
                    urun.UrunId = model.Id;

                }

                urun.Adet += model.Adet;

                if (!varmi)
                    Db.b2bSepetAyr.Add(urun);
                strvar = 1;

            }

            Db.SaveChanges();
            if (strvar == 1)
                return RedirectToAction("Cart");
            else
                return RedirectToAction("TransferExcel");
        }

        private bool isNumericExcel(string s)
        {
            try
            {
                decimal a = decimal.Parse(s);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        #endregion


        #region Category & Brand Trees & Prices

        [ChildActionOnly]
        public ActionResult CategoryTree()
        {
            // Level 1
            var level1Categories = Db.urnKtgrAg_vw
                .Where(x => x.k1 == 0)
                .OrderBy(x => x.kAd)
                .Select(x => new CategoryTreeModel.CategoryItem
                {
                    Id = x.kID,
                    Isim = x.kAd,
                    Order = x.k0,
                    ParentId = null,
                });

            // Level 2
            var level2Categories = Db.urnKtgrAg_vw
                .Where(x => x.k1 != 0 && x.k2 == 0)
                .OrderBy(x => x.kAd)
                .Select(x => new CategoryTreeModel.CategoryItem
                {
                    Id = x.kID,
                    Isim = x.kAd,
                    Order = x.k1,
                    ParentId = Db.urnKtgrAg_vw.FirstOrDefault(y => y.k0 == x.k0 && y.k1 == 0).kID
                });

            // Level 3
            var level3Categories = Db.urnKtgrAg_vw
                .Where(x => x.k2 != 0 && x.k3 == 0)
                .OrderBy(x => x.kAd)
                .Select(x => new CategoryTreeModel.CategoryItem
                {
                    Id = x.kID,
                    Isim = x.kAd,
                    Order = x.k2,
                    ParentId = Db.urnKtgrAg_vw.FirstOrDefault(y => y.k0 == x.k0 && y.k1 == x.k1 && y.k2 == 0).kID
                });
            var level4Categories = Db.urnKtgrAg_vw
             .Where(x => x.k3 != 0)
             .OrderBy(x => x.kAd)
             .Select(x => new CategoryTreeModel.CategoryItem
             {
                 Id = x.kID,
                 Isim = x.kAd,
                 Order = x.k3,
                 ParentId = Db.urnKtgrAg_vw.FirstOrDefault(y => y.k0 == x.k0 && y.k1 == x.k1 && y.k2 == x.k2 && y.k3 == 0).kID,
             });

            var allCategories = level1Categories
                .Union(level2Categories)
                .Union(level3Categories)
                .Union(level4Categories)
                .ToList();

            var model = new CategoryTreeModel { Items = allCategories };
            return PartialView(model);
        }
        [ChildActionOnly]
        public ActionResult Promotion()
        {
            var model = new PromoModel();

            List<int> Urunler = new List<int>();
            int musteriMarkaKosulu = MusteriMarkalarinaAitUrunleriGorsun;
            if (musteriMarkaKosulu > 0)
            {
                Urunler = (from kampRes in Db.b2bKampanyaResimleri
                           join urn in Db.urnSatis_vw on kampRes.etkiID equals urn.stkID
                           join skMarka in Db.sakMrk on urn.urnMrkID equals skMarka.skMrkID
                           where kampRes.etkiTip == 0
                           where skMarka.skFrmID == CurrentAccount.frm.frmID
                           select (urn.stkID)).Distinct().ToList();
            }
            else
            {
                Urunler = (from kampRes in Db.b2bKampanyaResimleri
                           join urn in Db.urnSatis_vw on kampRes.etkiID equals urn.stkID
                           where kampRes.etkiTip == 0
                           select (urn.stkID)).Distinct().ToList();
            }

            var bn = DateTime.Now.Date;
            model.Promolar = (from kr in Db.b2bKampanyaResimleri
                              where kr.durum == true && kr.basTarih <= bn && bn <= kr.bitTarih
                              select new PromoModel.promo
                              {
                                  sira = kr.sira,
                                  baslik = kr.baslik,
                                  resim = kr.resim,
                                  etkiTip = kr.etkiTip,
                                  etkiId = kr.etkiID,
                                  etkiDosya = kr.etkiDosya,
                                  linkAktif = kr.etkiTip == 0 ? (Urunler.Contains(kr.etkiID) ? 1 : 0) : (kr.etkiTip == 3 ? (kr.etkiDosya == "" ? 0 : 1) : 1),
                              }).AsEnumerable().Select(kr => new PromoModel.promo
                              {
                                  sira = kr.sira,
                                  baslik = kr.baslik,
                                  resim = PromotionImage(kr.resim).ToString(),
                                  etkiTip = kr.etkiTip,
                                  etkiId = kr.etkiId,
                                  etkiDosya = kr.etkiDosya,
                                  linkAktif = kr.etkiTip == 0 ? (Urunler.Contains(kr.etkiId) ? 1 : 0) : (kr.etkiTip == 3 ? (kr.etkiDosya == "" ? 0 : 1) : 1),

                              }).ToList();


            return PartialView(model);
        }
        [ChildActionOnly]

        private string PromotionImage(string name)
        {
            if (name != "")
            {
                var absolutePath = Server.MapPath("~/Assets/promoresimler/" + name);
                if (System.IO.File.Exists(absolutePath))
                {
                    return "../../Assets/promoresimler/" + name;
                }
            }
            return "../../Assets/theme/img/notimg.jpg";
        }

        public ActionResult BrandTree(string q, int? categoryId, string price)
        {
            var model = new BrandTreeModel();
            int markaYerineGrup = MarkaYerineGrupGosterimiVar;

            List<int> markaUrunleri = new List<int>();
            int musteriMarkaKosulu = MusteriMarkalarinaAitUrunleriGorsun;

            if (musteriMarkaKosulu > 0)
            {
                markaUrunleri = Db.urnSatis_vw
                                .Join(Db.sakMrk, x => x.urnMrkID, y => y.skMrkID, (urnSatis, skMarka) => new { urnSatis, skMarka })
                                .Where(x => x.skMarka.skFrmID == CurrentAccount.frm.frmID)
                                .Select(x => x.urnSatis.stkID).Distinct().ToList();
            }

            if (String.IsNullOrWhiteSpace(q))
            {
                var urnBrkdStkIDleri = Db.urnBarkod_vw.Select(x => x.urnBrkdStkID).Distinct();
                var urunList = Db.urnSatis_vw.Where(x => markaUrunleri.Contains(x.stkID) || musteriMarkaKosulu == 0).Where(x => urnBrkdStkIDleri.Contains(x.stkID)).Distinct();

                if (!string.IsNullOrEmpty(price))
                {
                    var esik = price.Split('-');
                    int min = int.Parse(esik[0]);
                    int max = esik[1] != "" ? int.Parse(esik[1]) : 99999999;
                    urunList = urunList.
                    Where(
                        x3 =>
                        (x3.odemeKur == 0 ? CurrentCompany.frmFiyatTur == 1 ? x3.fiyatS1 : (CurrentCompany.frmFiyatTur == 2 ? x3.fiyatS2 : (CurrentCompany.frmFiyatTur == 3 ? x3.fiyatS3 : (CurrentCompany.frmFiyatTur == 4 ? x3.fiyatS4 : x3.fiyatS))) : CurrentCompany.frmFiyatTur == 1 ? x3.fiyatS1 : (CurrentCompany.frmFiyatTur == 2 ? x3.fiyatS2 : (CurrentCompany.frmFiyatTur == 3 ? x3.fiyatS3 : (CurrentCompany.frmFiyatTur == 4 ? x3.fiyatS4 : x3.fiyatS))) * x3.odemeKur) >= min &&
                        (x3.odemeKur == 0 ? CurrentCompany.frmFiyatTur == 1 ? x3.fiyatS1 : (CurrentCompany.frmFiyatTur == 2 ? x3.fiyatS2 : (CurrentCompany.frmFiyatTur == 3 ? x3.fiyatS3 : (CurrentCompany.frmFiyatTur == 4 ? x3.fiyatS4 : x3.fiyatS))) : CurrentCompany.frmFiyatTur == 1 ? x3.fiyatS1 : (CurrentCompany.frmFiyatTur == 2 ? x3.fiyatS2 : (CurrentCompany.frmFiyatTur == 3 ? x3.fiyatS3 : (CurrentCompany.frmFiyatTur == 4 ? x3.fiyatS4 : x3.fiyatS))) * x3.odemeKur) <= max);
                }

                var ürünUreticileri = urunList
                    .GroupBy(x => new { x.urtID, x.urtAd })
                    .Select(x => new { ureticiIDsi = x.Key.urtID, ureticiAdi = x.Key.urtAd, urunUreticiSay = x.Count() }).ToList();

                IList<BrandData> markaVeri = new List<BrandData>();

                int grupVar = 0;
                if (markaYerineGrup > 0)
                {
                    markaVeri = urunList.GroupBy(x => new { x.urtID, urnMrkID = x.urnMrkGrpID, mrkAd = x.urnMrkGrpAd })
                   .Select(x => new BrandData { UreticiID = x.Key.urtID, MarkaID = x.Key.urnMrkID, markaAd = x.Key.mrkAd, markaCount = x.Count() }).ToList();
                    grupVar = 1;
                }
                else
                {
                    markaVeri = urunList.GroupBy(x => new { x.urtID, x.urnMrkID, x.mrkAd })
                                      .Select(x => new BrandData { UreticiID = x.Key.urtID, MarkaID = x.Key.urnMrkID, markaAd = x.Key.mrkAd, markaCount = x.Count() }).ToList();
                }

                var producersFull = ürünUreticileri
                    .Select(prodcr => new BrandTreeModel.Producer
                    {
                        Id = prodcr.ureticiIDsi,
                        Name = prodcr.ureticiAdi,
                        Brands = markaVeri
                        .Where(x => x.UreticiID == prodcr.ureticiIDsi)
                        .Select(brnd => new BrandTreeModel.Brand
                        {
                            Id = brnd.MarkaID,
                            Name = brnd.markaAd,
                            Count = brnd.markaCount
                        }).OrderBy(x => x.Name)
                    }).OrderBy(x => x.Name);

                model = new BrandTreeModel { Brand_GroupValue = grupVar, Producers = producersFull };
                return PartialView(model);
            }

            var qstr = q.Split(' ').Select(x => x.ToLower()).ToArray();
            var barkodlar = new List<int>();
            if (isNumeric(q) && q.Length >= 8)
            {
                barkodlar = Db.urnBrkd.Where(x => x.urnBarkod.Contains(q)).Select(x => x.urnBrkdAltStkId > 0 ? x.urnBrkdAltStkId : x.urnBrkdStkID).ToList();
            }
            var urunListe = !barkodlar.Any() ?
                Db.urnSatis_vw.Where(x => markaUrunleri.Contains(x.stkID) || musteriMarkaKosulu == 0).Where(x => qstr.All(y => x.stkAd.Contains(y)) ||
                                x.stkKod.Contains(q) ||
                                x.mrkAd.Contains(q))
                : Db.urnSatis_vw.Where(x => markaUrunleri.Contains(x.stkID) || musteriMarkaKosulu == 0).Where(x => barkodlar.Contains(x.stkID));

            if (categoryId != null)
            {
                var kategori = Db.urnKtgrAg_vw.Single(x => x.kID == categoryId);

                urunListe = urunListe.Where(x => x.urnKtgrID == kategori.k0);

                if (kategori.k1 != 0)
                    urunListe = urunListe.Where(x => x.urnKtgrID1 == kategori.k1);

                if (kategori.k2 != 0)
                    urunListe = urunListe.Where(x => x.urnKtgrID2 == kategori.k2);

                if (kategori.k3 != 0)
                    urunListe = urunListe.Where(x => x.urnKtgrID3 == kategori.k3);
            }

            if (!string.IsNullOrEmpty(price))
            {
                var esik = price.Split('-');
                int min = int.Parse(esik[0]);
                int max = esik[1] != "" ? int.Parse(esik[1]) : 99999999;
                urunListe = urunListe.
                    Where(
                        x3 =>
                        (x3.odemeKur == 0 ? CurrentCompany.frmFiyatTur == 1 ? x3.fiyatS1 : (CurrentCompany.frmFiyatTur == 2 ? x3.fiyatS2 : (CurrentCompany.frmFiyatTur == 3 ? x3.fiyatS3 : (CurrentCompany.frmFiyatTur == 4 ? x3.fiyatS4 : x3.fiyatS))) : CurrentCompany.frmFiyatTur == 1 ? x3.fiyatS1 : (CurrentCompany.frmFiyatTur == 2 ? x3.fiyatS2 : (CurrentCompany.frmFiyatTur == 3 ? x3.fiyatS3 : (CurrentCompany.frmFiyatTur == 4 ? x3.fiyatS4 : x3.fiyatS))) * x3.odemeKur) >= min &&
                        (x3.odemeKur == 0 ? CurrentCompany.frmFiyatTur == 1 ? x3.fiyatS1 : (CurrentCompany.frmFiyatTur == 2 ? x3.fiyatS2 : (CurrentCompany.frmFiyatTur == 3 ? x3.fiyatS3 : (CurrentCompany.frmFiyatTur == 4 ? x3.fiyatS4 : x3.fiyatS))) : CurrentCompany.frmFiyatTur == 1 ? x3.fiyatS1 : (CurrentCompany.frmFiyatTur == 2 ? x3.fiyatS2 : (CurrentCompany.frmFiyatTur == 3 ? x3.fiyatS3 : (CurrentCompany.frmFiyatTur == 4 ? x3.fiyatS4 : x3.fiyatS))) * x3.odemeKur) <= max);

            }

            var ürünUretici = urunListe
                   .GroupBy(x => new { x.urtID, x.urtAd })
                   .Select(x => new { ureticiIDsi = x.Key.urtID, ureticiAdi = x.Key.urtAd, urunUreticiSay = x.Count() }).ToList();



            IList<BrandData> markaVeriDiger = new List<BrandData>();

            var sayisayDiger = 0;
            if (markaYerineGrup > 0)
            {
                markaVeriDiger = urunListe.GroupBy(x => new { x.urtID, urnMrkID = x.urnMrkGrpID, mrkAd = x.urnMrkGrpAd })
               .Select(x => new BrandData { UreticiID = x.Key.urtID, MarkaID = x.Key.urnMrkID, markaAd = x.Key.mrkAd, markaCount = x.Count() }).ToList();
            }
            else
            {
                sayisayDiger = 1;
                markaVeriDiger = urunListe.GroupBy(x => new { x.urtID, x.urnMrkID, x.mrkAd })
                                  .Select(x => new BrandData { UreticiID = x.Key.urtID, MarkaID = x.Key.urnMrkID, markaAd = x.Key.mrkAd, markaCount = x.Count() }).ToList();
            }


            var producers = ürünUretici
                .Select(prodcr => new BrandTreeModel.Producer
                {
                    Id = prodcr.ureticiIDsi,
                    Name = prodcr.ureticiAdi,
                    Brands = markaVeriDiger
                             .Where(x => x.UreticiID == prodcr.ureticiIDsi)
                    .Select(brnd => new BrandTreeModel.Brand
                    {
                        Id = brnd.MarkaID,
                        Name = brnd.markaAd,
                        Count = brnd.markaCount
                    }).OrderBy(x => x.Name)
                }).OrderBy(x => x.Name);


            //var producers = Db.urnMrkUrt
            //    .Where(x => urunListe.Any(y => y.urtID == x.urtID))
            //    .Select(producer => new BrandTreeModel.Producer
            //    {
            //        Id = producer.urtID,
            //        Name = producer.urtAd,
            //        Brands = producer.urnMrk.Where(x => urunListe.Any(y => y.urnMrkID == x.mrkID))
            //                                    .OrderBy(x => x.mrkAd)
            //                                    .Select(brand => new BrandTreeModel.Brand
            //                                    {
            //                                        Id = brand.mrkID,
            //                                        Name = brand.mrkAd,
            //                                        Count = urunListe.Where(x => x.urnMrkID == brand.mrkID).Count()
            //                                    })
            //    });

            model = new BrandTreeModel { Producers = producers };


            return PartialView(model);
        }

        [ChildActionOnly]
        public ActionResult Prices()
        {

            var prices = new Collection<PricesModel.Price>();
            var price = new PricesModel.Price();
            price = new PricesModel.Price { Max = 5, Min = 0, Name = "0-5" };
            prices.Add(price);
            price = new PricesModel.Price { Max = 10, Min = 5, Name = "5-10" };
            prices.Add(price);
            price = new PricesModel.Price { Max = 20, Min = 10, Name = "10-20" };
            prices.Add(price);
            price = new PricesModel.Price { Max = 50, Min = 20, Name = "20-50" };
            prices.Add(price);
            price = new PricesModel.Price { Max = 100, Min = 50, Name = "50-100" };
            prices.Add(price);
            price = new PricesModel.Price { Max = -1, Min = 500, Name = "500-" };
            prices.Add(price);
            var model = new PricesModel { Prices = prices };
            return PartialView(model);
        }

        #endregion


        #region My Carts

        public ActionResult MyCarts()
        {
            var myCarts = Db.b2bSepet
                .Where(x => x.frmYetkili.ytkID == CurrentAccount.ytkID)
                .Select(x => new MyCartModel
                {
                    Id = x.Id,
                    Isim = x.Ad,
                    Aciklamalar = x.Aciklama,
                    AktifMi = x.Aktif,
                    Adet = x.b2bSepetAyr.Count()
                })
                .ToList();

            var ayar = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 765);
            ViewBag.KategoriYok = ayar == null ? false : byte.Parse(ayar.ayarDeger) > 0;

            return View(myCarts);
        }

        [HttpPost]
        public ActionResult MakeCartActive(int id)
        {
            // Make every cart is disactive
            SetAllUserCartsAsDisactive();

            var newActiveCart = Db.b2bSepet
                .Where(x => x.frmYetkili.ytkID == CurrentAccount.ytkID)
                .Single(x => x.Id == id);
            newActiveCart.Aktif = true;

            Db.SaveChanges();
            return RedirectToAction("MyCarts");
        }

        [HttpPost]
        public ActionResult ChangeActiveCartAsync(int cartId)
        {
            // Make every cart is disactive
            SetAllUserCartsAsDisactive();

            var newActiveCart = Db.b2bSepet
                .Where(x => x.frmYetkili.ytkID == CurrentAccount.ytkID)
                .Single(x => x.Id == cartId);
            newActiveCart.Aktif = true;

            Db.SaveChanges();
            return Json(new
            {
                Result = true,
                CartName = newActiveCart.Ad,
                CartDistinctProductCount = newActiveCart.b2bSepetAyr.Count()
            });
        }

        public ActionResult CreateNewCart()
        {
            var ayar = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 765);
            ViewBag.KategoriYok = ayar == null ? false : byte.Parse(ayar.ayarDeger) > 0;

            return View();
        }

        [HttpPost]
        public ActionResult CreateNewCart(CreateNewCartModel model)
        {
            if (!ModelState.IsValid)
            {
                ShowErrorMessage();
                return CreateNewCart();
            }

            // Aynı isimle iki sepet olamaz
            var cartWithSameName = Db.b2bSepet
                .Where(x => x.frmYetkili.ytkID == CurrentAccount.ytkID)
                .SingleOrDefault(x => x.Ad == model.Isim);
            if (cartWithSameName != null)
            {
                ModelState.AddModelError("Isim", "Aynı isimli bir sepetiniz zaten var.");
                ShowErrorMessage();
                return CreateNewCart();
            }

            var newCart = new b2bSepet
            {
                Aktif = false,
                frmYetkili = CurrentAccount,
                Ad = model.Isim,
                Aciklama = model.Aciklamalar ?? ""
            };
            Db.b2bSepet.Add(newCart);
            Db.SaveChanges();

            ShowSuccessMessage("Sepetiniz başarıyla oluşturulmuştur.");
            return RedirectToAction("MyCarts");
        }

        public ActionResult EditCart(int? id)
        {
            Check.NotNullOr404(id);

            var sepet = Db.b2bSepet
                .Where(x => x.frmYetkili.ytkID == CurrentAccount.ytkID)
                .SingleOrDefault(x => x.Id == id);
            Check.NotNullOr404(sepet);

            var model = new EditCartModel
            {
                Id = sepet.Id,
                Isim = sepet.Ad,
                Aciklamalar = sepet.Aciklama ?? ""
            };

            var ayar = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 765);
            ViewBag.KategoriYok = ayar == null ? false : byte.Parse(ayar.ayarDeger) > 0;

            return View(model);
        }

        [HttpPost]
        public ActionResult EditCart(EditCartModel model)
        {
            if (!ModelState.IsValid)
            {
                ShowErrorMessage();
                return EditCart(model.Id);
            }

            var cart = Db.b2bSepet
                .Where(x => x.frmYetkili.ytkID == CurrentAccount.ytkID)
                .Single(x => x.Id == model.Id);

            // Aynı isimle iki sepet olamaz
            var cartWithSameName = Db.b2bSepet
                .Where(x => x.frmYetkili.ytkID == CurrentAccount.ytkID)
                .Where(x => x.Id != cart.Id)
                .SingleOrDefault(x => x.Ad == model.Isim);
            if (cartWithSameName != null)
            {
                ModelState.AddModelError("Isim", "Aynı isimli bir sepetiniz zaten var.");
                ShowErrorMessage();
                return CreateNewCart();
            }

            cart.Ad = model.Isim;
            cart.Aciklama = model.Aciklamalar ?? "";
            Db.SaveChanges();

            ShowSuccessMessage("Sepetiniz başarıyla güncellenmiştir.");
            return RedirectToAction("MyCarts");
        }

        [HttpPost]
        public ActionResult DeleteCart(int id)
        {
            var myCartCount = Db.b2bSepet.Count(x => x.frmYetkili.ytkID == CurrentAccount.ytkID);
            if (myCartCount == 1)
            {
                ShowErrorMessage("Son sepeti silemezseniz.");
                return RedirectToAction("MyCarts");
            }

            var deletingCart = Db.b2bSepet
                .Where(x => x.frmYetkili.ytkID == CurrentAccount.ytkID)
                .Single(x => x.Id == id);

            foreach (var item in deletingCart.b2bSepetAyr.ToList())
            {
                Db.b2bSepetAyr.Remove(item);
            }
            Db.b2bSepet.Remove(deletingCart);

            // Eğer aktif olan sepeti sildiysek, başka bir sepeti aktif yapmalıyız.
            var activeCart = Db.b2bSepet
                .Where(x => x.frmYetkili.ytkID == CurrentAccount.ytkID)
                .Where(x => x.Id != deletingCart.Id)
                .SingleOrDefault(x => x.Aktif);
            if (activeCart == null)
            {
                var newActiveCart = Db.b2bSepet
                    .Where(x => x.Id != deletingCart.Id)
                    .First(x => x.frmYetkili.ytkID == CurrentAccount.ytkID);
                newActiveCart.Aktif = true;
            }

            Db.SaveChanges();
            ShowSuccessMessage("Sepet başarıyla silinmiştir.");
            return RedirectToAction("MyCarts");
        }

        private void SetAllUserCartsAsDisactive()
        {
            var myCarts = Db.b2bSepet
                .Where(x => x.frmYetkili.ytkID == CurrentAccount.ytkID);
            foreach (var myCart in myCarts)
            {
                myCart.Aktif = false;
            }
        }

        #endregion

        #region Product Detail Pages

        public ActionResult ProductDetail(int? id, int? kmpid)
        {
            Check.NotNullOr404(id);

            List<int> markaUrunleri = new List<int>();
            int musteriMarkaKosulu = MusteriMarkalarinaAitUrunleriGorsun;

            if (musteriMarkaKosulu > 0)
            {
                markaUrunleri = Db.urnSatis_vw
                                .Join(Db.sakMrk, x => x.urnMrkID, y => y.skMrkID, (urnSatis, skMarka) => new { urnSatis, skMarka })
                                .Where(x => x.skMarka.skFrmID == CurrentAccount.frm.frmID)
                                .Select(x => x.urnSatis.stkID).Distinct().ToList();
            }
            var urnSorgu = Db.urn.Join(Db.urnSatis_vw, x => x.stkID, y => y.stkID, (urnT, _urnVw) => new { urnT, _urnVw }).Where(x => markaUrunleri.Contains(x.urnT.stkID) || musteriMarkaKosulu == 0).Where(x => x.urnT.stkID == id);
            if (urnSorgu.Count() == 0)
            {
                Check.NotNullOr404(null);
            }

            var urn = urnSorgu.FirstOrDefault().urnT;
            Check.NotNullOr404(urn);

            var urunKod1 = Db.urnKod1.SingleOrDefault(x => x.kod1ID == urn.kod1ID);
            var urunKod2 = Db.urnkod2.SingleOrDefault(x => x.kod2ID == urn.kod2ID);
            var urunKod3 = Db.urnkod3.SingleOrDefault(x => x.kod3ID == urn.kod3ID);
            var urunKod4 = Db.urnkod4.SingleOrDefault(x => x.kod4ID == urn.kod4ID);
            var urunKod5 = Db.urnkod5.SingleOrDefault(x => x.kod5ID == urn.kod5ID);

            var urunKod1Baslik = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 101);
            var urunKod2Baslik = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 102);
            var urunKod3Baslik = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 103);
            var urunKod4Baslik = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 104);
            var urunKod5Baslik = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 105);

            var urunKod1GosterilsinMi = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 751);
            var urunKod2GosterilsinMi = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 752);
            var urunKod3GosterilsinMi = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 753);
            var urunKod4GosterilsinMi = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 754);
            var urunKod5GosterilsinMi = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 755);

            //var kdvHaricSatisFiyati= Db.urnSatis_vw.Single(x => x.stkID == id).fiyatS;

            var indirim1 = 0m;
            var indirim2 = 0m;
            var indirim3 = 0m;
            var indirim4 = 0m;
            if (CurrentAccount.frm.frmTip == 1)
            {
                var mrkIndirim = Db.sakMrk_vw
                    .Where(x => x.skFrmID == CurrentAccount.frm.frmID)
                    .Where(x => x.skTur == CurrentCompany.frmVadeTur)
                    .SingleOrDefault(x => x.skMrkID == urn.urnMrkID);

                if (mrkIndirim != null)
                {
                    indirim1 = mrkIndirim.skY1;
                    indirim2 = mrkIndirim.skY2;
                    indirim3 = mrkIndirim.skY3;
                }

                if (kmpid == null)
                {
                    var ekIndirim = Db.fytOzl
                        .Where(x => x.fStkID == urn.stkID)
                        .Where(x => x.fTip == 4)
                        .Where(x => x.fTarih <= DateTime.Today && x.fTarihSon >= DateTime.Today)
                        .Where(x => x.fTur == 0)
                        .Where(x => x.onay == 1)
                        .OrderByDescending(x => x.fTarih)
                        .ThenByDescending(x => x.fID)
                        .FirstOrDefault();

                    if (ekIndirim != null)
                    {
                        indirim4 = ekIndirim.fInd1;
                    }
                }
                else
                {
                    var ekIndirim = Db.fytOzl
                        .Where(x => x.fhID == kmpid)
                        .Where(x => x.fStkID == urn.stkID)
                        .FirstOrDefault();

                    if (ekIndirim != null)
                    {
                        indirim4 = ekIndirim.fInd1;
                    }
                }
            }


            // Ürün kategorisini urnSatis_vw'den sorgulamalıyız
            var urnVw = Db.urnSatis_vw.Single(x => x.stkID == urn.stkID);
            var kategori1 = Db.urnKtgrAg_vw.SingleOrDefault(x => x.k0 == urnVw.urnKtgrID && x.k1 == 0 && x.k2 == 0);
            var kategori2 = urnVw.urnKtgrID2 != 0
                                ? Db.urnKtgrAg_vw.SingleOrDefault(x => x.k0 == urnVw.urnKtgrID && x.k1 == urnVw.urnKtgrID1 && x.k2 == 0)
                                : null;
            var kategori3 = urnVw.urnKtgrID3 != 0
                                ? Db.urnKtgrAg_vw.SingleOrDefault(x => x.k0 == urnVw.urnKtgrID && x.k1 == urnVw.urnKtgrID1 && x.k2 == urnVw.urnKtgrID2 && x.k3 == 0)
                                : null;

            var posOdeme = Db.posOdeme.FirstOrDefault(x => x.odemeDvzID == urn.fiyatSDvz);
            if (posOdeme == null)
            {
                posOdeme = new posOdeme
                {
                    odemeDvzID = 1,
                    odemeKur = 1m,
                    odemeKisaAd = "TL",
                };
            }

            var stkKontroluVar = StkKontroluVar;
            var stkGosterimiVar = StkGosterimiVar;
            var markaYerineGrup = MarkaYerineGrupGosterimiVar;

            var ayar = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 765);

            bool onaysizSipGoster = onaysizSiparisGoster();

            var model = new ProductDetailModel
            {

                Id = urn.stkID,
                Isim = urn.stkAd,
                UrunKodu = urn.stkKod,
                StokKisaAd = urn.stkAdKisa,

                // Ürün Kodları
                UrunKod1Baslik = urunKod1Baslik != null ? urunKod1Baslik.ayarDeger : "",
                UrunKod2Baslik = urunKod2Baslik != null ? urunKod2Baslik.ayarDeger : "",
                UrunKod3Baslik = urunKod3Baslik != null ? urunKod3Baslik.ayarDeger : "",
                UrunKod4Baslik = urunKod4Baslik != null ? urunKod4Baslik.ayarDeger : "",
                UrunKod5Baslik = urunKod5Baslik != null ? urunKod5Baslik.ayarDeger : "",

                UrunKod1 = urunKod1 != null ? urunKod1.kod1Ad : "",
                UrunKod2 = urunKod2 != null ? urunKod2.kod2Ad : "",
                UrunKod3 = urunKod3 != null ? urunKod3.kod3Ad : "",
                UrunKod4 = urunKod4 != null ? urunKod4.kod4Ad : "",
                UrunKod5 = urunKod5 != null ? urunKod5.kod5Ad : "",

                UrunKod1GosterilsinMi = urunKod1GosterilsinMi != null && urunKod1GosterilsinMi.ayarDeger != "0",
                UrunKod2GosterilsinMi = urunKod2GosterilsinMi != null && urunKod2GosterilsinMi.ayarDeger != "0",
                UrunKod3GosterilsinMi = urunKod3GosterilsinMi != null && urunKod3GosterilsinMi.ayarDeger != "0",
                UrunKod4GosterilsinMi = urunKod4GosterilsinMi != null && urunKod4GosterilsinMi.ayarDeger != "0",
                UrunKod5GosterilsinMi = urunKod5GosterilsinMi != null && urunKod5GosterilsinMi.ayarDeger != "0",

                bagliOlduguGrupIdsi = urn.urnGrpID,
                // Kategoriler
                Kategori0Id = kategori1 != null ? kategori1.kID : 0,
                Kategori1Id = kategori2 != null ? kategori2.kID : 0,
                Kategori2Id = kategori3 != null ? kategori3.kID : 0,
                Kategori0Ad = kategori1 != null ? kategori1.kAd : "",
                Kategori1Ad = kategori2 != null ? kategori2.kAd : "",
                Kategori2Ad = kategori3 != null ? kategori3.kAd : "",


                // Üretici - Marka - Model
                UreticiId = urnVw.urtID,
                UreticiAd = urnVw.urtAd,
                MarkaId = (markaYerineGrup == 0) ? urnVw.urnMrkID + "-0" : urnVw.urnMrkGrpID + "-1",
                MarkaAd = (markaYerineGrup == 0) ? urnVw.mrkAd : urnVw.urnMrkGrpAd,

                stok = urnVw.stok,
                stokKontrolu = stkKontroluVar,
                gosterilenStok = urnVw.gosterilenStok,
                stokGosterimi = stkGosterimiVar,

                kategoriGosterimiYok = ayar == null ? false : byte.Parse(ayar.ayarDeger) > 0,
                // Ürün Özellikleri
                UrunBirimi = urn.urnBrm,
                PaketOlcu = urn.paketOlcu,
                PaketBirimi = urn.paketBrm,
                KoliOlcu = urn.urnKoli,
                KoliBirimi = urn.urnKoliBrm,
                KutuOlcu = urn.urnKutu,
                PaletOlcu = urn.urnPalet,


                // Ürün Bilgileri
                BilgiGruplari = Db.urnBilgiGrp
                                    .Where(x => x.grupB2bGorunsun == 1)
                                    .OrderBy(x => x.grupSira)
                                    .Select(x => new ProductDetailModel.BilgiGrubu
                                    {
                                        GrupAdi = x.grupAd,
                                        Bilgiler = Db.urnBilgi
                                                             .Where(y => y.bVeriID == urn.stkID)
                                                             .Where(y => y.urnBilgiTnm.bilgiGrup == x.grupID)
                                                             .OrderBy(y => y.urnBilgiTnm.BilgiSira)
                                                             .Select(y => new ProductDetailModel.BilgiGrubu.Bilgi
                                                             {
                                                                 Ad = y.urnBilgiTnm.BilgiAd,
                                                                 Deger = y.bDeger
                                                             })
                                    }),


                // Barkod Bilgileri
                BarkodBilgileri = Db.urnBrkd.Where(x => x.urnBrkdAltStkId == urn.stkID || x.urnBrkdStkID == urn.stkID)
                                    .OrderBy(x => x.urnBrkdOnce)
                                    .Select(x => new ProductDetailModel.BarkodBilgisi
                                    {
                                        Barkod = x.urnBarkod,
                                        Adet = x.urnBrkdAdet,
                                        AdetBirim = x.urnBrkdBrm,
                                    }),


                // Stok Bilgileri
                StokBilgileri = Db.stokSon_vw
                                    .Where(x => x.ehstkID == urn.stkID)
                                    .Join(Db.posMagaza, x => x.ehMekan, y => y.mekanID, (stok, mekan) => new { stok, mekan })
                                    .Where(x => x.mekan.mekanMerkezDepo != 2)
                                    .OrderByDescending(x => x.mekan.mekanMerkezDepo)
                                    .Select(x => new ProductDetailModel.StokBilgisi
                                    {
                                        MekanAdi = x.mekan.mekanAd,
                                        Stok = x.stok.stok.Value
                                    }),


                // Fiyat ve İndirim Bilgileri
                KDVHaricSatisFiyati = CurrentCompany.frmFiyatTur == 1 ? urnVw.fiyatS1 : (CurrentCompany.frmFiyatTur == 2 ? urnVw.fiyatS2 : (CurrentCompany.frmFiyatTur == 3 ? urnVw.fiyatS3 : (CurrentCompany.frmFiyatTur == 4 ? urnVw.fiyatS4 : urnVw.fiyatS))),
                Birim = posOdeme.odemeKisaAd,
                SatisKur = posOdeme.odemeKur,
                Indirim1 = indirim1,
                Indirim2 = indirim2,
                Indirim3 = indirim3,
                Indirim4 = indirim4,
                KDVOrani = Db.urnKDV.Single(x => x.kdvID == urn.KDVs).kdvYuzde,


                // Ürün Siparişleri
                Siparisler = Db.sipAyr
                          .Join(Db.sip, x => x.ehID, y => y.eID, (sipA, sipB) => new { sipA, sipB })
                          .Join(Db.sifTip.Where(y => y.hrktSIF == 0), s => s.sipB.eTip, tip => tip.hrktTipID, (s, tip) => new { s, tip })
                          .Join(Db.sipNeden, x => x.s.sipB.Neden, neden => neden.nedenID, (x, neden) => new { x.s, x.tip, neden })
                          .Where(x => x.s.sipA.ehStkID == urn.stkID)
                          .Where(x => x.s.sipB.eTarih >= DateHelper.LastMonthBegin)
                          .Where(x => x.s.sipB.eTarih <= DateTime.Today)
                          .Where(x => x.s.sipB.eFirma == CurrentAccount.frm.frmID)
                          .Where(x => (onaysizSipGoster == true || (onaysizSipGoster == false && x.s.sipB.onay == 1)))
                          .GroupBy(row => new { row.s.sipB.eID, row.s.sipB.eNo, row.s.sipB.eTarih, row.s.sipB.eTarihS, row.tip.hrktTipAd, row.neden.nedenAd })
                          .OrderByDescending(x => x.Key.eTarih)
                          .Select(group => new ProductDetailModel.UrunSiparis
                          {
                              Id = group.Key.eID,
                              EvrakNo = group.Key.eNo,
                              Tarih = group.Key.eTarih,
                              SevkTarihi = group.Key.eTarihS,
                              HareketTipi = group.Key.hrktTipAd,
                              HareketGrubu = group.Key.nedenAd,
                              UrunToplamAdet = group.Sum(s => s.s.sipA.ehAdet),
                              UrunKoliIciAdet = urn.urnKoli,
                              UrunIndirimTutari = group.Sum(s => s.s.sipA.ehIndirim),
                              UrunKDVHaricToplam = group.Sum(s => s.s.sipA.ehTutar),
                              ToplamKDVHaricNet = group.Sum(s => s.s.sipA.ehTutar) - group.Sum(s => s.s.sipA.ehIndirim)
                          }),



                // Ürün İrsaliyeler
                Irsaliyeler = Db.irsAyr
                          .Join(Db.irs, x => x.ehID, y => y.eID, (irsA, irsB) => new { irsA, irsB })
                          .Join(Db.sifTip.Where(y => y.hrktSIF == 1), i => i.irsB.eTip, tip => tip.hrktTipID, (i, tip) => new { i, tip })
                          .Join(Db.sipNeden, x => x.i.irsB.Neden, neden => neden.nedenID, (x, neden) => new { x.i, x.tip, neden })
                          .Where(x => x.i.irsA.ehStkID == urn.stkID)
                          .Where(x => x.i.irsB.eTarih >= DateHelper.LastMonthBegin)
                                    .Where(x => x.i.irsB.eTarih <= DateTime.Today)
                                    .Where(x => x.i.irsB.eFirma == CurrentAccount.frm.frmID)
                                    .GroupBy(row => new { row.i.irsB.eID, row.i.irsB.eNo, row.i.irsB.eTarih, row.i.irsB.eTarihS, row.tip.hrktTipAd, row.neden.nedenAd })
                                    .OrderByDescending(x => x.Key.eTarih)
                                    .Select(group => new ProductDetailModel.UrunIrsaliye
                                    {
                                        Id = group.Key.eID,
                                        EvrakNo = group.Key.eNo,
                                        Tarih = group.Key.eTarih,
                                        SevkTarihi = group.Key.eTarihS,
                                        HareketTipi = group.Key.hrktTipAd,
                                        HareketGrubu = group.Key.nedenAd,
                                        UrunToplamAdet = group.Sum(s => s.i.irsA.ehAdet),
                                        UrunKoliIciAdet = urn.urnKoli,
                                        UrunIndirimTutari = group.Sum(s => s.i.irsA.ehIndirim),
                                        UrunKDVHaricToplam = group.Sum(s => s.i.irsA.ehTutar),
                                        ToplamKDVHaricNet = group.Sum(s => s.i.irsA.ehTutar) - group.Sum(s => s.i.irsA.ehIndirim)
                                    }),



                // Ürün Faturaları
                Faturalar = Db.fatAyr
                          .Join(Db.fat, x => x.ehID, y => y.eID, (fatA, fatB) => new { fatA, fatB })
                          .Join(Db.sifTip.Where(y => y.hrktSIF == 2), f => f.fatB.eTip, tip => tip.hrktTipID, (f, tip) => new { f, tip })
                          .Join(Db.sipNeden, x => x.f.fatB.Neden, neden => neden.nedenID, (x, neden) => new { x.f, x.tip, neden })
                          .Where(x => x.f.fatA.ehStkID == urn.stkID)
                          .Where(x => x.f.fatB.eTarih >= DateHelper.LastMonthBegin)
                                    .Where(x => x.f.fatB.eTarih <= DateTime.Today)
                                    .Where(x => x.f.fatB.eFirma == CurrentAccount.frm.frmID)
                                    .GroupBy(row => new { row.f.fatB.eID, row.f.fatB.eNo, row.f.fatB.eTarih, row.f.fatB.eTarihV, row.tip.hrktTipAd, row.neden.nedenAd })
                                    .OrderByDescending(x => x.Key.eTarih)
                                    .Select(group => new ProductDetailModel.UrunFatura
                                    {
                                        Id = group.Key.eID,
                                        EvrakNo = group.Key.eNo,
                                        Tarih = group.Key.eTarih,
                                        VadeTarihi = group.Key.eTarihV,
                                        HareketTipi = group.Key.hrktTipAd,
                                        HareketGrubu = group.Key.nedenAd,
                                        UrunToplamAdet = group.Sum(s => s.f.fatA.ehAdet),
                                        UrunKoliIciAdet = urn.urnKoli,
                                        UrunIndirimTutari = group.Sum(s => s.f.fatA.ehIndirim),
                                        UrunKDVHaricToplam = group.Sum(s => s.f.fatA.ehTutar),
                                        ToplamKDVHaricNet = group.Sum(s => s.f.fatA.ehTutar) - group.Sum(s => s.f.fatA.ehIndirim)
                                    })
                                    ,
                ControllerName = "Store"
            };

            ViewBag.KategoriYok = ayar == null ? false : byte.Parse(ayar.ayarDeger) > 0;

            return View(model);
        }

        public ActionResult ProductGroupDetail(int? id)
        {
            Check.NotNullOr404(id);

            var urunModeli = Db.urnGrp.SingleOrDefault(x => x.grpID == id);
            Check.NotNullOr404(urunModeli);

            var markaYerineGrup = MarkaYerineGrupGosterimiVar;

            int grupVar = 0;
            if (markaYerineGrup > 0)
            {
                grupVar = 1;
            }

            var marka = Db.urnMrk.Single(x => x.mrkID == urunModeli.grpMrkID);
            var markaGrup = Db.urnMrkGrp.Single(x => x.urnMrkGrpID == marka.mrkGrup);

            var uretici = marka.urnMrkUrt;

            var kategori0 = Db.urnKtgrAg_vw.SingleOrDefault(x => x.kID == urunModeli.urnGrpKtgrID);
            var kategori1 = Db.urnKtgrAg_vw.SingleOrDefault(x => x.kID == urunModeli.urnGrpKtgrID1);
            var kategori2 = Db.urnKtgrAg_vw.SingleOrDefault(x => x.kID == urunModeli.urnGrpKtgrID2);

            var markaIndirimi = Db.sakMrk_vw
                    .Where(x => x.skFrmID == CurrentAccount.frm.frmID)
                    .Where(x => x.skTur == CurrentCompany.frmVadeTur)
                    .SingleOrDefault(x => x.skMrkID == marka.mrkID);

            List<int> markaUrunleri = new List<int>();
            int musteriMarkaKosulu = MusteriMarkalarinaAitUrunleriGorsun;

            if (musteriMarkaKosulu > 0)
            {
                markaUrunleri = Db.urnSatis_vw
                                .Join(Db.sakMrk, x => x.urnMrkID, y => y.skMrkID, (urnSatis, skMarka) => new { urnSatis, skMarka })
                                .Where(x => x.skMarka.skFrmID == CurrentAccount.frm.frmID)
                                .Select(x => x.urnSatis.stkID).Distinct().ToList();
            }

            var enUcuzModelUrunuVw = Db.urnSatis_vw
                .Where(x => x.urnGrpID == urunModeli.grpID)
                .Where(x => markaUrunleri.Contains(x.stkID) || musteriMarkaKosulu == 0);
            if (enUcuzModelUrunuVw.Count() == 0)
            {
                Check.NotNullOr404(null);
            }

            var enUcuzModelUrunu = enUcuzModelUrunuVw.OrderBy(x => CurrentCompany.frmFiyatTur == 1 ? x.fiyatS1 : (CurrentCompany.frmFiyatTur == 2 ? x.fiyatS2 : (CurrentCompany.frmFiyatTur == 3 ? x.fiyatS3 : (CurrentCompany.frmFiyatTur == 4 ? x.fiyatS4 : x.fiyatS)))).First();

            var ekIndirim = Db.fytOzl
                    .Where(x => x.fStkID == enUcuzModelUrunu.stkID)
                    .Where(x => x.fTip == 4)
                    //.Where(x => x.fFrmID == 0)
                    .Where(x => x.fTarih <= DateTime.Today && x.fTarihSon >= DateTime.Today)
                    .Where(x => x.fTur == 0)
                    .Where(x => x.onay == 1)
                    .OrderByDescending(x => x.fTarih)
                    .ThenByDescending(x => x.fID)
                    .FirstOrDefault();

            var stkKontroluVar = StkKontroluVar;
            var stkGosterimiVar = StkGosterimiVar;
            var brutFiyatGosterimiVar = BrutFiyatGostermiVar;

            var ayar = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 765);


            var model = new ProductGroupDetailModel
            {
                Id = urunModeli.grpID,
                ModelAdi = urunModeli.grpAd,

                MarkaId = grupVar == 1 ? markaGrup.urnMrkGrpID : marka.mrkID,
                MarkaAdi = grupVar == 1 ? markaGrup.urnMrkGrpAd : marka.mrkAd,

                Brand_GroupValue = grupVar,

                UreticiId = uretici.urtID,
                UreticiAdi = uretici.urtAd,

                Kategori0Id = kategori0 != null ? kategori0.kID : 0,
                Kategori0Ad = kategori0 != null ? kategori0.kAd : "",
                Kategori1Id = kategori1 != null ? kategori1.kID : 0,
                Kategori1Ad = kategori1 != null ? kategori1.kAd : "",
                Kategori2Id = kategori2 != null ? kategori2.kID : 0,
                Kategori2Ad = kategori2 != null ? kategori2.kAd : "",


                KDVHaricSatisFiyati = CurrentCompany.frmFiyatTur == 1 ? enUcuzModelUrunu.fiyatS1 : (CurrentCompany.frmFiyatTur == 2 ? enUcuzModelUrunu.fiyatS2 : (CurrentCompany.frmFiyatTur == 3 ? enUcuzModelUrunu.fiyatS3 : (CurrentCompany.frmFiyatTur == 4 ? enUcuzModelUrunu.fiyatS4 : enUcuzModelUrunu.fiyatS))),
                Indirim1 = markaIndirimi != null ? markaIndirimi.skY1 : 0,
                Indirim2 = markaIndirimi != null ? markaIndirimi.skY2 : 0,
                Indirim3 = markaIndirimi != null ? markaIndirimi.skY3 : 0,
                Indirim4 = ekIndirim != null ? ekIndirim.fInd1 : 0,
                KDVOrani = Db.urnKDV.Single(x => x.kdvID == enUcuzModelUrunu.KDVs).kdvYuzde,

                grupStokKontrolu = stkKontroluVar,
                grupStokGosterimi = stkGosterimiVar,
                brutFiyatGosterimVar = brutFiyatGosterimiVar,

                kategoriGosterimiYok = ayar == null ? false : byte.Parse(ayar.ayarDeger) > 0,

                Products = (from urn in Db.urnSatis_vw
                            where urn.urnGrpID == id
                            where markaUrunleri.Contains(urn.stkID) || musteriMarkaKosulu == 0
                            let urunIndirimi = Db.sakMrk_vw
                            .Where(x => x.skTur == CurrentCompany.frmVadeTur)
                            .FirstOrDefault(x => x.skMrkID == marka.mrkID && x.skFrmID == CurrentCompany.frmID)
                            let ekUrunIndirimi = Db.fytOzl
                                .Where(x => x.fStkID == urn.stkID)
                                .Where(x => x.fTip == 4)
                                //.Where(x => x.fFrmID == 0)
                                .Where(x => x.fTarih <= DateTime.Today && x.fTarihSon >= DateTime.Today)
                                .Where(x => x.fTur == 0)
                                .Where(x => x.onay == 1)
                                .OrderByDescending(x => x.fTarih)
                                .ThenByDescending(x => x.fID)
                                .FirstOrDefault()
                            let kdv = Db.urnKDV.FirstOrDefault(x => x.kdvID == urn.KDVs)
                            let barkod = Db.urnBarkod_vw.FirstOrDefault(x => x.urnBrkdStkID == urn.stkID)
                            select new ProductGroupDetailModel.Product
                            {
                                Id = urn.stkID,
                                Isim = urn.stkAd,
                                StokKodu = urn.stkKod,
                                Barkod = barkod != null ? barkod.barkod : "",
                                KDVHaricSatisFiyati = CurrentCompany.frmFiyatTur == 1 ? urn.fiyatS1 : (CurrentCompany.frmFiyatTur == 2 ? urn.fiyatS2 : (CurrentCompany.frmFiyatTur == 3 ? urn.fiyatS3 : (CurrentCompany.frmFiyatTur == 4 ? urn.fiyatS4 : urn.fiyatS))),
                                Indirim1 = urunIndirimi != null ? urunIndirimi.skY1 : 0,
                                Indirim2 = urunIndirimi != null ? urunIndirimi.skY2 : 0,
                                Indirim3 = urunIndirimi != null ? urunIndirimi.skY3 : 0,
                                Indirim4 = ekUrunIndirimi != null ? ekUrunIndirimi.fInd1 : 0,
                                KDVOrani = kdv.kdvYuzde,
                                Birim = urn.odemeKisaAd,
                                stok = urn.stok,
                                gosterilenStok = urn.gosterilenStok,
                            })
                                .ToList(),
                ControllerName = "Store",
            };

            ViewBag.KategoriYok = ayar == null ? false : byte.Parse(ayar.ayarDeger) > 0;

            return View(model);
        }

        public ActionResult ProductPromoDetail(int? id, string resim)
        {
            Check.NotNullOr404(id);

            var fiyatbaslik = Db.fytB.SingleOrDefault(x => x.feID == id);
            Check.NotNullOr404(fiyatbaslik);


            var stkKontroluVar = StkKontroluVar;
            var stkGosterimiVar = StkGosterimiVar;
            var brutFiyatGosterimVar = BrutFiyatGostermiVar;

            List<int> markaUrunleri = new List<int>();
            int musteriMarkaKosulu = MusteriMarkalarinaAitUrunleriGorsun;

            if (musteriMarkaKosulu > 0)
            {
                markaUrunleri = Db.urnSatis_vw
                                .Join(Db.sakMrk, x => x.urnMrkID, y => y.skMrkID, (urnSatis, skMarka) => new { urnSatis, skMarka })
                                .Where(x => x.skMarka.skFrmID == CurrentAccount.frm.frmID)
                                .Select(x => x.urnSatis.stkID).Distinct().ToList();
            }


            var model = new ProductPromoDetailModel()
            {
                Id = fiyatbaslik.feID,
                Ad = fiyatbaslik.feNot,
                resim = resim,
                promoStokKontrolu = stkKontroluVar,
                promoStokGosterimi = stkGosterimiVar,
                brutFiyatGosterimVar = brutFiyatGosterimVar,

                Products = (from fytOzl in Db.fytOzl
                            join urn in Db.urnSatis_vw on fytOzl.fStkID equals urn.stkID
                            join mainUrn in Db.urn on urn.stkID equals mainUrn.stkID
                            where fytOzl.fhID == id
                            where markaUrunleri.Contains(urn.stkID) || musteriMarkaKosulu == 0
                            let markaIndirimi = Db.sakMrk_vw
                              .Where(x => x.skFrmID == CurrentCompany.frmID)
                               .Where(x => x.skTur == CurrentCompany.frmVadeTur)
                               .FirstOrDefault(x => x.skMrkID == urn.urnMrkID)
                            let kdv = Db.urnKDV.FirstOrDefault(x => x.kdvID == urn.KDVs)
                            let barkod = Db.urnBarkod_vw.FirstOrDefault(x => x.urnBrkdStkID == fytOzl.fStkID)
                            select new ProductPromoDetailModel.Product
                            {
                                Id = urn.stkID,
                                Isim = urn.stkAd,
                                StokKodu = urn.stkKod,
                                Barkod = barkod != null ? barkod.barkod : "",
                                KDVHaricSatisFiyati = CurrentCompany.frmFiyatTur == 1 ? urn.fiyatS1 : (CurrentCompany.frmFiyatTur == 2 ? urn.fiyatS2 : (CurrentCompany.frmFiyatTur == 3 ? urn.fiyatS3 : (CurrentCompany.frmFiyatTur == 4 ? urn.fiyatS4 : urn.fiyatS))),
                                Indirim1 = markaIndirimi != null ? markaIndirimi.skY1 : 0,
                                Indirim2 = markaIndirimi != null ? markaIndirimi.skY2 : 0,
                                Indirim3 = markaIndirimi != null ? markaIndirimi.skY3 : 0,
                                Indirim4 = fytOzl.fInd1,
                                KDVOrani = kdv.kdvYuzde,
                                Birim = urn.odemeKisaAd,
                                stok = urn.stok,
                                gosterilenStok = urn.gosterilenStok,
                            })
                    .ToList(),
                ControllerName = "Store",
            };

            var ayar = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 765);
            ViewBag.KategoriYok = ayar == null ? false : byte.Parse(ayar.ayarDeger) > 0;


            return View(model);
        }

        #endregion


        #region Cart Actions

        [ChildActionOnly]
        public ActionResult CartMenuElement()
        {
            var aktifSepet = Db.b2bSepet
                .Where(x => x.KullaniciId == CurrentAccount.ytkID)
                .Single(x => x.Aktif);

            string markaUrunleriEkSorgu = " ";
            int musteriMarkaKosulu = MusteriMarkalarinaAitUrunleriGorsun;

            if (musteriMarkaKosulu > 0)
            {
                markaUrunleriEkSorgu = " or urn.urnMrkID not in (select skMrkID from sakMrk where skFrmID=" + CurrentAccount.frm.frmID + ") ";
            }

            string silinenUrunler = "";
            var pasifOlanUrunler = @" select sa.Id as sepetAyrID,sa.UrunId as urunID,isnull(stkAd,'') as urunAd,Adet from b2bSepetAyr as sa
inner join b2bSepet as s on s.Id=sa.SepetId 
left join urn on stkID=sa.UrunId
where (sa.UrunId not in (select stkID from b2b.urnSatis_vw) " + markaUrunleriEkSorgu + " ) and s.Id=" + aktifSepet.Id + " ";

            var query2 = Db.Database.SqlQuery<PasiveProductListModel>(pasifOlanUrunler).ToList();
            if (query2.Any())
            {
                string silSorgu = "";
                silinenUrunler = @"<table class='table table-striped'><tr><th class='header'>Stok ID</th><th class='header'>Stok Adı</th><th class='header'>Adet</th></tr><tbody>";


                foreach (var item in query2)
                {
                    silinenUrunler += "<tr><td>" + item.urunID + "</td><td>" + item.urunAd + "</td><td>" + item.Adet + "</td></tr>";
                    silSorgu += item.sepetAyrID + ",";
                }
                if (silSorgu.Length > 0)
                {
                    silinenUrunler = silinenUrunler + "</tbody></table>";
                    silSorgu = silSorgu.Substring(0, silSorgu.Length - 1);
                    silSorgu = " delete b2bSepetAyr where Id in(" + silSorgu + ") ";
                    Db.Database.ExecuteSqlCommand(silSorgu);
                }
            }

            var urunIdleri = aktifSepet.b2bSepetAyr.Select(x => x.UrunId).Distinct().ToList();
            var urunler = Db.urnSatis_vw.Where(x => urunIdleri.Contains(x.stkID)).Select(x => new
            {
                StkID = x.stkID,
                Fiyat = CurrentCompany.frmFiyatTur == 1 ? x.fiyatS1 : (CurrentCompany.frmFiyatTur == 2 ? x.fiyatS2 : (CurrentCompany.frmFiyatTur == 3 ? x.fiyatS3 : (CurrentCompany.frmFiyatTur == 4 ? x.fiyatS4 : x.fiyatS))),
                DovizAdi = x.odemeKisaAd

            });

            var sepetOzeti = from item in aktifSepet.b2bSepetAyr
                             let mrkIndirim = Db.sakMrk_vw
                                 .Where(x => x.skFrmID == CurrentCompany.frmID)
                                 .Where(x => x.skTur == CurrentCompany.frmVadeTur)
                                 .FirstOrDefault(x => x.skMrkID == item.urn.urnMrkID)
                             let ekIndirim = Db.fytOzl
                                 .Where(x => x.fStkID == item.UrunId)
                                 .Where(x => x.fTip == 4)
                                 .Where(x => x.fTarih <= DateTime.Today && x.fTarihSon >= DateTime.Today)
                                 .Where(x => x.fTur == 0)
                                 .Where(x => x.onay == 1)
                                 .OrderByDescending(x => x.fTarih)
                                 .ThenByDescending(x => x.fID)
                                 .FirstOrDefault()
                             let kdv = Db.urnKDV.FirstOrDefault(x => x.kdvID == item.urn.KDVs)
                             select new
                             {
                                 KDVHaricSatisFiyati = urunler.Single(x => x.StkID == item.UrunId).Fiyat,
                                 Indirim1 = mrkIndirim != null ? mrkIndirim.skY1 : 0,
                                 Indirim2 = mrkIndirim != null ? mrkIndirim.skY2 : 0,
                                 Indirim3 = mrkIndirim != null ? mrkIndirim.skY3 : 0,
                                 Indirim4 = ekIndirim != null ? ekIndirim.fInd1 : 0,
                                 KDVOrani = kdv.kdvYuzde,
                                 Adet = item.Adet,
                                 DvzBirim = urunler.Single(x => x.StkID == item.UrunId).DovizAdi,
                             };

            var grupluSepetOzeti = sepetOzeti
              .GroupBy(ac => new
              {
                  ac.DvzBirim
              })
              .Select(ac => new
              {
                  birim = ac.Key.DvzBirim,
                  nettutar = ac.Sum(x =>
                  {
                      var indirimler = new[] { x.Indirim1, x.Indirim2, x.Indirim3, x.Indirim4 };
                      var kdvHaricNet = indirimler.Aggregate(x.KDVHaricSatisFiyati,
                                                             (fiyat, indirim) =>
                                                             fiyat - (fiyat * indirim / 100.0m));

                      var kdv = kdvHaricNet * x.KDVOrani / 100.0m;
                      var kdvDahilToplam = kdvHaricNet + kdv;
                      return kdvDahilToplam * x.Adet;
                  })
              }).ToList();

            string tutarDegeri = "";
            foreach (var cartItem in grupluSepetOzeti)
            {
                tutarDegeri += cartItem.nettutar.ToMoneyFormat().Replace(".", "").Replace(",", ".") + " " + cartItem.birim + " ";
            }

            var model = new CartMenuElementModel
            {
                AktifSepetIsmi = aktifSepet.Ad,
                ToplamUrunCesidi = aktifSepet.b2bSepetAyr.Count(),
                KDVDahilTutar = tutarDegeri,
                silinenUrn = WebUtility.HtmlEncode(silinenUrunler),
                SonEklenen2Urun = aktifSepet.b2bSepetAyr
                          .OrderByDescending(x => x.Id)
                          .Take(2)
                          .Select(x => new CartMenuElementModel.Urun
                          {
                              UrunIsmi = x.urn.stkAd,
                          })
                          .ToList()
            };

            //  asdasd
            return PartialView(model);
        }

        [ChildActionOnly]
        public ActionResult CartSidebarElement()
        {
            var aktifSepet = Db.b2bSepet
                .Where(x => x.KullaniciId == CurrentAccount.ytkID)
                .Single(x => x.Aktif);

            var urunIdleri = aktifSepet.b2bSepetAyr.Select(x => x.UrunId).Distinct().ToList();
            var urunler = Db.urnSatis_vw.Where(x => urunIdleri.Contains(x.stkID)).Select(x => new
            {
                StkID = x.stkID,
                Fiyat = CurrentCompany.frmFiyatTur == 1 ? x.fiyatS1 : (CurrentCompany.frmFiyatTur == 2 ? x.fiyatS2 : (CurrentCompany.frmFiyatTur == 3 ? x.fiyatS3 : (CurrentCompany.frmFiyatTur == 4 ? x.fiyatS4 : x.fiyatS))),
                DovizAdi = x.odemeKisaAd

            });
            var sepetOzeti = from item in aktifSepet.b2bSepetAyr
                             let mrkIndirim = Db.sakMrk_vw
                                 .Where(x => x.skFrmID == CurrentCompany.frmID)
                                 .Where(x => x.skTur == CurrentCompany.frmVadeTur)
                                 .FirstOrDefault(x => x.skMrkID == item.urn.urnMrkID)
                             let ekIndirim = Db.fytOzl
                                 .Where(x => x.fStkID == item.UrunId)
                                 .Where(x => x.fTip == 4)
                                 .Where(x => x.fTarih <= DateTime.Today && x.fTarihSon >= DateTime.Today)
                                 .Where(x => x.fTur == 0)
                                 .Where(x => x.onay == 1)
                                 .OrderByDescending(x => x.fTarih)
                                 .ThenByDescending(x => x.fID)
                                 .FirstOrDefault()
                             let kdv = Db.urnKDV.FirstOrDefault(x => x.kdvID == item.urn.KDVs)
                             select new
                             {
                                 KDVHaricSatisFiyati = urunler.Single(x => x.StkID == item.UrunId).Fiyat,
                                 Indirim1 = mrkIndirim != null ? mrkIndirim.skY1 : 0,
                                 Indirim2 = mrkIndirim != null ? mrkIndirim.skY2 : 0,
                                 Indirim3 = mrkIndirim != null ? mrkIndirim.skY3 : 0,
                                 Indirim4 = ekIndirim != null ? ekIndirim.fInd1 : 0,
                                 KDVOrani = kdv.kdvYuzde,
                                 Adet = item.Adet,
                                 DvzBirim = urunler.Single(x => x.StkID == item.UrunId).DovizAdi,
                             };

            var grupluSepetOzeti = sepetOzeti
                .GroupBy(ac => new
                {
                    ac.DvzBirim
                })
                .Select(ac => new
                {
                    birim = ac.Key.DvzBirim,
                    nettutar = ac.Sum(x =>
                    {
                        var indirimler = new[] { x.Indirim1, x.Indirim2, x.Indirim3, x.Indirim4 };
                        var kdvHaricNet = indirimler.Aggregate(x.KDVHaricSatisFiyati,
                                                               (fiyat, indirim) =>
                                                               fiyat - (fiyat * indirim / 100.0m));

                        return kdvHaricNet * x.Adet;
                    }),
                    kdvtutar = ac.Sum(x =>
                    {
                        var indirimler = new[] { x.Indirim1, x.Indirim2, x.Indirim3, x.Indirim4 };
                        var kdvHaricNet = indirimler.Aggregate(x.KDVHaricSatisFiyati,
                                                               (fiyat, indirim) =>
                                                               fiyat - (fiyat * indirim / 100.0m));

                        var kdv = kdvHaricNet * x.KDVOrani / 100.0m;
                        return kdv * x.Adet;
                    })
                }).ToList();

            string[,] tutarDegeri = new string[grupluSepetOzeti.Count, 4];
            int sayac = 0;
            foreach (var cartItem in grupluSepetOzeti)
            {
                tutarDegeri[sayac, 0] = cartItem.birim;
                tutarDegeri[sayac, 1] = cartItem.nettutar.ToMoneyFormat().Replace(".", "").Replace(",", ".");
                tutarDegeri[sayac, 2] = cartItem.kdvtutar.ToMoneyFormat().Replace(".", "").Replace(",", ".");
                tutarDegeri[sayac, 3] = (cartItem.kdvtutar + cartItem.nettutar).ToMoneyFormat().Replace(".", "").Replace(",", ".");
                sayac = sayac + 1;
            }

            var model = new CartSidebarElementModel
            {
                AktifSepetIsmi = aktifSepet.Ad,
                ToplamUrunCesidi = aktifSepet.b2bSepetAyr.Count(),
                TutarBilgisi = tutarDegeri,
                //KDVDahilTutar
            };

            return PartialView(model);
        }

        public ActionResult Cart(CartModel _model)
        {
            var activeCart = Db.b2bSepet
                .Where(x => x.KullaniciId == CurrentAccount.ytkID)
                .Single(x => x.Aktif);

            var stkKontroluVar = StkKontroluVar;
            var model = new CartModel
            {
                mesajVer = _model.mesajVer == null ? 0 : _model.mesajVer,
                AktifSepetId = activeCart.Id,
                modelStokKontrolu = stkKontroluVar,
                Sepetlerim = (from cart in Db.b2bSepet
                              where cart.KullaniciId == CurrentAccount.ytkID
                              select new CartModel.Sepet
                              {
                                  Id = cart.Id,
                                  Isim = cart.Ad,
                                  Aciklama = cart.Aciklama,
                              }).ToList(),

                AktifSepetUrunleri = (from cartItem in Db.b2bSepetAyr
                                      join Satis_vw in Db.urnSatis_vw on cartItem.UrunId equals Satis_vw.stkID
                                      where cartItem.b2bSepet.KullaniciId == CurrentAccount.ytkID
                                      where cartItem.b2bSepet.Aktif
                                      let barkod = Db.urnBarkod_vw.FirstOrDefault(x => x.urnBrkdStkID == cartItem.urn.stkID)
                                      let mrkIndirim = Db.sakMrk_vw
                                          .Where(x => x.skFrmID == CurrentCompany.frmID)
                                          .Where(x => x.skTur == CurrentCompany.frmVadeTur)
                                          .FirstOrDefault(x => x.skMrkID == cartItem.urn.urnMrkID)
                                      let ekIndirim = Db.fytOzl
                                          .Where(x => x.fStkID == cartItem.urn.stkID)
                                          .Where(x => x.fTip == 4)
                                          //.Where(x => x.fFrmID == 0)
                                          .Where(x => x.fTarih <= DateTime.Today && x.fTarihSon >= DateTime.Today)
                                          .Where(x => x.fTur == 0)
                                          .Where(x => x.onay == 1)
                                          .OrderByDescending(x => x.fTarih)
                                          .ThenByDescending(x => x.fID)
                                          .FirstOrDefault()
                                      let kdv = Db.urnKDV.FirstOrDefault(x => x.kdvID == cartItem.urn.KDVs)
                                      select new CartModel.Urun
                                      {
                                          UrunId = cartItem.urn.stkID,
                                          UrunAdi = cartItem.urn.stkAd,
                                          Barkod = barkod != null ? barkod.barkod : "",
                                          StokKodu = cartItem.urn.stkKod,
                                          KDVHaricSatisFiyati = CurrentCompany.frmFiyatTur == 1 ? Satis_vw.fiyatS1 : (CurrentCompany.frmFiyatTur == 2 ? Satis_vw.fiyatS2 : (CurrentCompany.frmFiyatTur == 3 ? Satis_vw.fiyatS3 : (CurrentCompany.frmFiyatTur == 4 ? Satis_vw.fiyatS4 : Satis_vw.fiyatS))),
                                          Indirim1 = mrkIndirim != null ? mrkIndirim.skY1 : 0,
                                          Indirim2 = mrkIndirim != null ? mrkIndirim.skY2 : 0,
                                          Indirim3 = mrkIndirim != null ? mrkIndirim.skY3 : 0,
                                          Indirim4 = ekIndirim != null ? ekIndirim.fInd1 : 0,
                                          KDVOrani = kdv.kdvYuzde,
                                          Adet = cartItem.Adet,
                                          DvzBirim = Satis_vw.odemeKisaAd,
                                          Stok = Satis_vw.stok,
                                      }).ToList(),

            };

            var ayar = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 765);
            ViewBag.KategoriYok = ayar == null ? false : byte.Parse(ayar.ayarDeger) > 0;

            return View(model);
        }

        [HttpPost]
        public ActionResult ChangeActiveCart(int id)
        {
            // Make every cart is disactive
            SetAllUserCartsAsDisactive();

            var newActiveCart = Db.b2bSepet
                .Where(x => x.frmYetkili.ytkID == CurrentAccount.ytkID)
                .Single(x => x.Id == id);
            newActiveCart.Aktif = true;

            Db.SaveChanges();
            return RedirectToAction("Cart");
            //  return View();
        }

        [HttpPost]
        public ActionResult AddToActiveCartAsync(AddToActiveCartModel model)
        {
            var activeCart = Db.b2bSepet
                .Where(x => x.frmYetkili.ytkID == CurrentAccount.ytkID)
                .Single(x => x.Aktif);


            foreach (var cartItem in model.Items)
            {
                var item = activeCart.b2bSepetAyr.SingleOrDefault(x => x.UrunId == cartItem.ProductId);
                if (item == null)
                {
                    activeCart.b2bSepetAyr
                        .Add(new b2bSepetAyr
                        {
                            b2bSepet = activeCart,
                            urn = Db.urn.Single(x => x.stkID == cartItem.ProductId),
                            Adet = cartItem.Quantity
                        });
                }
                else
                {
                    item.Adet += cartItem.Quantity;
                }
            }

            Db.SaveChanges();

            var cartItemPriceList = from cartItem in Db.b2bSepetAyr
                                    join urnSatis_vw in Db.urnSatis_vw on cartItem.UrunId equals urnSatis_vw.stkID
                                    where cartItem.b2bSepet.KullaniciId == CurrentAccount.ytkID
                                    where cartItem.b2bSepet.Aktif
                                    let mrkIndirim = Db.sakMrk_vw
                                        .Where(x => x.skFrmID == CurrentCompany.frmID)
                                        .Where(x => x.skTur == CurrentCompany.frmVadeTur)
                                        .FirstOrDefault(x => x.skMrkID == cartItem.urn.urnMrkID)
                                    let ekIndirim = Db.fytOzl
                                        .Where(x => x.fStkID == cartItem.urn.stkID)
                                        .Where(x => x.fTip == 4)
                                        //.Where(x => x.fFrmID == 0)
                                        .Where(x => x.fTarih <= DateTime.Today && x.fTarihSon >= DateTime.Today)
                                        .Where(x => x.fTur == 0)
                                        .Where(x => x.onay == 1)
                                        .OrderByDescending(x => x.fTarih)
                                        .ThenByDescending(x => x.fID)
                                        .FirstOrDefault()
                                    let kdv = Db.urnKDV.FirstOrDefault(x => x.kdvID == cartItem.urn.KDVs)
                                    select new
                                    {
                                        KDVHaricSatisFiyati = CurrentCompany.frmFiyatTur == 1 ? urnSatis_vw.fiyatS1 : (CurrentCompany.frmFiyatTur == 2 ? urnSatis_vw.fiyatS2 : (CurrentCompany.frmFiyatTur == 3 ? urnSatis_vw.fiyatS3 : (CurrentCompany.frmFiyatTur == 4 ? urnSatis_vw.fiyatS4 : urnSatis_vw.fiyatS))),
                                        Indirim1 = mrkIndirim != null ? mrkIndirim.skY1 : 0,
                                        Indirim2 = mrkIndirim != null ? mrkIndirim.skY2 : 0,
                                        Indirim3 = mrkIndirim != null ? mrkIndirim.skY3 : 0,
                                        Indirim4 = ekIndirim != null ? ekIndirim.fInd1 : 0,
                                        KDVOrani = kdv.kdvYuzde,
                                        cartItem.Adet,
                                    };

            var toplamSepetTutari = 0.0m;
            foreach (var item in cartItemPriceList)
            {
                var indirimler = new[] { item.Indirim1, item.Indirim2, item.Indirim3, item.Indirim4 };
                var indirimliFiyat = indirimler.Aggregate(item.KDVHaricSatisFiyati, (fiyat, indirim) => fiyat - (fiyat * indirim / 100.0m));
                var kdv = indirimliFiyat * (decimal)item.KDVOrani / 100.0m;
                var tutar = indirimliFiyat + kdv;

                toplamSepetTutari += tutar * item.Adet;
            }

            return Json(new
            {
                UrunCesidi = activeCart.b2bSepetAyr.Count(),
                ToplamFiyat = toplamSepetTutari.ToMoneyFormat().Replace(".", "").Replace(",", "."),
                SonIkiUrun = activeCart.b2bSepetAyr
                                    .OrderByDescending(x => x.Id)
                                    .Take(2)
                                    .Select(x => x.urn.stkAd)
                                    .ToList()
            });
        }

        [HttpPost]
        public ActionResult RemoveFromActiveCart(ICollection<int> ids)
        {
            var activeCart = Db.b2bSepet
                .Where(x => x.frmYetkili.ytkID == CurrentAccount.ytkID)
                .Single(x => x.Aktif);

            var items = activeCart.b2bSepetAyr.Where(x => ids.Contains(x.UrunId)).ToList();
            foreach (var item in items)
            {
                Db.b2bSepetAyr.Remove(item);
            }
            Db.SaveChanges();

            return RedirectToAction("Cart");
        }

        [HttpPost]
        public ActionResult MoveToAnotherCart(int targetCartId, ICollection<int> itemIds)
        {
            var activeCart = Db.b2bSepet
                .Where(x => x.KullaniciId == CurrentAccount.ytkID)
                .Single(x => x.Aktif);

            var targetCart = Db.b2bSepet
                .Where(x => x.KullaniciId == CurrentAccount.ytkID)
                .Single(x => x.Id == targetCartId);

            var movingItems = activeCart.b2bSepetAyr.Where(x => itemIds.Contains(x.UrunId)).ToList();
            foreach (var movingItem in movingItems)
            {
                movingItem.b2bSepet = targetCart;
            }

            Db.SaveChanges();

            ShowSuccessMessage(string.Format("Seçtiğiniz ürünler taşınmıştır", targetCart.Ad));
            return RedirectToAction("Cart");
        }

        [HttpPost]
        public ActionResult UpdateActiveCartItem(UpdateActiveCartItemModel model)
        {
            var activeCart = Db.b2bSepet
                .Where(x => x.frmYetkili.ytkID == CurrentAccount.ytkID)
                .Single(x => x.Aktif);

            var item = activeCart.b2bSepetAyr.Single(x => x.UrunId == model.ProductId);
            item.Adet = model.NewQuantity;
            Db.SaveChanges();

            return RedirectToAction("Cart");
        }

        private decimal DiscountedPriceForProduct(urn urn)
        {
            var price = Db.urnSatis_vw.Where(x => x.stkID == urn.stkID).Select(x => CurrentCompany.frmFiyatTur == 1 ? x.fiyatS1 : (CurrentCompany.frmFiyatTur == 2 ? x.fiyatS2 : (CurrentCompany.frmFiyatTur == 3 ? x.fiyatS3 : (CurrentCompany.frmFiyatTur == 4 ? x.fiyatS4 : x.fiyatS)))).FirstOrDefault();

            //urn.fiyatS;

            var discount = Db.sakMrk_vw
                .Where(x => x.skFrmID == CurrentAccount.frm.frmID)
                .Where(x => x.skTur == CurrentCompany.frmVadeTur)
                .FirstOrDefault(x => x.skMrkID == urn.urnMrkID);

            if (discount != null)
            {
                price -= discount.skY1 * price / 100.0m;

                if (discount.skY2 != 0.0m)
                {
                    price -= discount.skY2 * price / 100.0m;
                }

                if (discount.skY3 != 0.0m)
                {
                    price -= discount.skY3 * price / 100.0m;
                }
            }

            return price;

        }


        [HttpPost]
        public ActionResult CartMenuElementRefresh()
        {
            var activeCart = Db.b2bSepet
                .Where(x => x.frmYetkili.ytkID == CurrentAccount.ytkID)
                .Single(x => x.Aktif);

            var cartItemPriceList = from cartItem in Db.b2bSepetAyr
                                    join urnSatis_vw in Db.urnSatis_vw on cartItem.UrunId equals urnSatis_vw.stkID
                                    where cartItem.b2bSepet.KullaniciId == CurrentAccount.ytkID
                                    where cartItem.b2bSepet.Aktif
                                    let mrkIndirim = Db.sakMrk_vw
                                        .Where(x => x.skFrmID == CurrentCompany.frmID)
                                        .Where(x => x.skTur == CurrentCompany.frmVadeTur)
                                        .FirstOrDefault(x => x.skMrkID == cartItem.urn.urnMrkID)
                                    let ekIndirim = Db.fytOzl
                                        .Where(x => x.fStkID == cartItem.urn.stkID)
                                        .Where(x => x.fTip == 4)
                                        //.Where(x => x.fFrmID == 0)
                                        .Where(x => x.fTarih <= DateTime.Today && x.fTarihSon >= DateTime.Today)
                                        .Where(x => x.fTur == 0)
                                        .Where(x => x.onay == 1)
                                        .OrderByDescending(x => x.fTarih)
                                        .ThenByDescending(x => x.fID)
                                        .FirstOrDefault()
                                    let kdv = Db.urnKDV.FirstOrDefault(x => x.kdvID == cartItem.urn.KDVs)
                                    select new
                                    {
                                        KDVHaricSatisFiyati = CurrentCompany.frmFiyatTur == 1 ? urnSatis_vw.fiyatS1 : (CurrentCompany.frmFiyatTur == 2 ? urnSatis_vw.fiyatS2 : (CurrentCompany.frmFiyatTur == 3 ? urnSatis_vw.fiyatS3 : (CurrentCompany.frmFiyatTur == 4 ? urnSatis_vw.fiyatS4 : urnSatis_vw.fiyatS))),
                                        Indirim1 = mrkIndirim != null ? mrkIndirim.skY1 : 0,
                                        Indirim2 = mrkIndirim != null ? mrkIndirim.skY2 : 0,
                                        Indirim3 = mrkIndirim != null ? mrkIndirim.skY3 : 0,
                                        Indirim4 = ekIndirim != null ? ekIndirim.fInd1 : 0,
                                        KDVOrani = kdv.kdvYuzde,
                                        cartItem.Adet,
                                    };

            var toplamSepetTutari = 0.0m;
            foreach (var item in cartItemPriceList)
            {
                var indirimler = new[] { item.Indirim1, item.Indirim2, item.Indirim3, item.Indirim4 };
                var indirimliFiyat = indirimler.Aggregate(item.KDVHaricSatisFiyati, (fiyat, indirim) => fiyat - (fiyat * indirim / 100.0m));
                var kdv = indirimliFiyat * (decimal)item.KDVOrani / 100.0m;
                var tutar = indirimliFiyat + kdv;

                toplamSepetTutari += tutar * item.Adet;
            }

            return Json(new
            {
                UrunCesidi = activeCart.b2bSepetAyr.Count(),
                ToplamFiyat = toplamSepetTutari.ToMoneyFormat().Replace(".", "").Replace(",", "."),
                SonIkiUrun = activeCart.b2bSepetAyr
                                    .OrderByDescending(x => x.Id)
                                    .Take(2)
                                    .Select(x => x.urn.stkAd)
                                    .ToList()
            });
        }

        #endregion

        #region Make Order

        public ActionResult SelectOrderDeliveryAddress(string tutar)
        {

            if (CurrentCompany.etkinVGSiparis > 0)
            {
                Db.Database.SqlQuery<dynamic>("exec car_isle @frmID= {0}", CurrentCompany.frmID);
                var bakiyeVD = Db.Database.SqlQuery<decimal>("select Gecen from fn_bakiyeVDGGort(getdate()) where Gecen<0 and cKod={0}", CurrentCompany.frmID).ToList();
                if (bakiyeVD.Count > 0)
                {
                    var model = new CartModel
                    {
                        mesajVer = bakiyeVD[0]
                    };
                    return RedirectToAction("Cart", model);
                }
            }

            if (CurrentCompany.etkinSiparis > 0)
            {
                var model = new CartModel
                {
                    mesajVer = 2
                };
                return RedirectToAction("Cart", model);
            }

            Decimal? bakiye = 0;
            if (Db.bakiye_vw.Where(m => m.cKod == CurrentCompany.frmID).Any())
            {
                bakiye = Db.bakiye_vw.SingleOrDefault(m => m.cKod == CurrentCompany.frmID).bakiye;
            }
            Decimal bakiyeLimit = CurrentCompany.frmBakiyeLimit;

            var acikSiparisToplami =
                Db.Database.SqlQuery<decimal>(
                    "Select IsNull(Sum(ehTutar-ehIndirim+ehTutarKdv),0) From sip_vw Where eTip IN (1,4) And eDurum=0 And ehDurum=0 And ehAdetN>0 and eFirma={0}",
                    CurrentCompany.frmID).First();

            if (bakiyeLimit > 0)
            {
                if (-1 * bakiye + Convert.ToDecimal(tutar) + acikSiparisToplami > bakiyeLimit)
                {
                    var model = new CartModel
                    {
                        mesajVer = 1
                    };
                    return RedirectToAction("Cart", model);
                }
            }

            var stkKontroluVar = StkKontroluVar;

            if (stkKontroluVar > 0)
            {
                var activeCart = Db.b2bSepet
                                    .Join(Db.b2bSepetAyr, x => x.Id, y => y.SepetId, (spt, sptAyr) => new { spt, sptAyr })
                                    .Join(Db.urnSatis_vw, x => x.sptAyr.UrunId, y => y.stkID, (sptOnceki, urnVw) => new { sptOnceki, urnVw })
                              .Where(x => x.sptOnceki.spt.KullaniciId == CurrentAccount.ytkID)
                              .Where(x => x.sptOnceki.sptAyr.Adet > x.urnVw.stok)
                              .Where(x => x.sptOnceki.spt.Aktif).Select(x => new { ID = x.sptOnceki.spt.Id }).Distinct().ToList();

                if (activeCart.Count > 0)
                {
                    return RedirectToAction("Cart");
                }
            }

            PopulateSevkAdresleri();

            var ayar = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 765);
            ViewBag.KategoriYok = ayar == null ? false : byte.Parse(ayar.ayarDeger) > 0;
            ViewBag.FatSevkID = CurrentCompany.frmID;
            return View();
        }

        [HttpPost]
        public ActionResult SelectOrderDeliveryAddress(CreateDeliveryAddressModel newAddress)
        {
            if (!ModelState.IsValid)
            {
                ShowErrorMessage();
                PopulateSevkAdresleri();

                var ayar = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 765);
                ViewBag.KategoriYok = ayar == null ? false : byte.Parse(ayar.ayarDeger) > 0;
                ViewBag.FatSevkID = CurrentCompany.frmID;
                return View(newAddress);
            }

            // Save this address as delivery address
            var frm = new frm
            {
                frmTip = 9,
                frmAd = newAddress.AdresIsmi,
                frmAd2 = newAddress.AdresIsmi,
                frmBagID = CurrentAccount.frm.frmID,
                adres1 = newAddress.Adres1,
                adres2 = newAddress.Adres2,
                postaKod = (newAddress.PostaKodu == null) ? "" : newAddress.PostaKodu,
                ilce = "",
                ilceKod = newAddress.ilceSecim,
                il = (short)newAddress.IlId,
                frmKod = "",
                tel1 = "",
                tel2 = "",
                telFaks = "",
                telMobil = "",
                VD = newAddress.VD == null ? "" : newAddress.VD,
                VN = newAddress.VN == null ? "" : newAddress.VN,
                frmKartNo = "",
                frmNotlar = "",

                gKisi = CurrentAccount.ytkID,
                gTarih = DateTime.Now,
                kKisi = CurrentAccount.ytkID,
                kTarih = DateTime.Now,

                frmCariTarih = DateTime.Today,

                adres3 = "",
                frmMersisNo = "",
                frmEposta = "",
                EfaturaEtiket = "",
                frmNACE = "",
                frmUlke = Db.frmUlke.Single(x => x.ulkeID == CurrentCompany.ulke)

            };
            Db.frm.Add(frm);
            Db.SaveChanges();
            frm.frmKod = frm.frmID.ToString();
            Db.SaveChanges();
            return RedirectToAction("OrderSummary", new { addressId = frm.frmID });
        }

        public ActionResult OrderSummary(int addressId)
        {
            var activeCart = Db.b2bSepet
                .Where(x => x.frmYetkili.ytkID == CurrentAccount.ytkID)
                .Single(x => x.Aktif);

            var deliveryAddress = Db.frm
                .Single(x => x.frmID == addressId);

            var model = new OrderSummaryModel
            {
                AdresId = deliveryAddress.frmID,
                AdresAdi = deliveryAddress.frmAd,
                AdresSatiri1 = deliveryAddress.adres1,
                AdresSatiri2 = deliveryAddress.adres2,
                AdresPostaKodu = deliveryAddress.postaKod,
                AdresIlce = Db.frmKentIlce.FirstOrDefault(x => x.ilceKodu == deliveryAddress.ilceKod).ilceAdi,
                AdresIl = Db.frmKent.FirstOrDefault(x => x.alanKodu == deliveryAddress.il).ilAd,

                AktifSepetId = activeCart.Id,
                AktifSpetAdi = activeCart.Ad,
                Urunler = (from cartItem in activeCart.b2bSepetAyr
                           let dvzBrmAd = Db.posOdeme.FirstOrDefault(x => x.odemeDvzID == cartItem.urn.fiyatSDvz)
                           let urnKdv = Db.urnKDV.Where(x => x.kdvID == cartItem.urn.KDVs).FirstOrDefault()
                           select new OrderSummaryModel.CartDetail
                           {
                               ProductId = cartItem.UrunId,
                               ProductName = cartItem.urn.stkAd,
                               Quantity = cartItem.Adet,
                               ProductUnitPrice = Db.urnSatis_vw.Where(x => x.stkID == cartItem.UrunId).Select(x => CurrentCompany.frmFiyatTur == 1 ? x.fiyatS1 : (CurrentCompany.frmFiyatTur == 2 ? x.fiyatS2 : (CurrentCompany.frmFiyatTur == 3 ? x.fiyatS3 : (CurrentCompany.frmFiyatTur == 4 ? x.fiyatS4 : x.fiyatS)))).FirstOrDefault(),
                               DvzBirim = dvzBrmAd.odemeKisaAd,
                               KDVOrani = urnKdv.kdvYuzde
                           }).ToList(),
            };

            foreach (var item in model.Urunler)
            {
                decimal netTutar = DiscountedPriceForProduct(Db.urn.Single(x => x.stkID == item.ProductId));
                item.ProductUnitDiscountedPrice = netTutar;
                item.KDVTutari = netTutar * item.KDVOrani / 100.0m;
                item.TotalPrice = (item.ProductUnitDiscountedPrice + item.KDVTutari) * item.Quantity;
            }

            var ayar = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 765);
            ViewBag.KategoriYok = ayar == null ? false : byte.Parse(ayar.ayarDeger) > 0;

            return View(model);
        }

        [HttpPost]
        public ActionResult CompleteOrder(int addressId, string sipNotSatiri1, string sipNotSatiri2, string sipNotSatiri3)
        {
            var activeCart = Db.b2bSepet
                .Where(x => x.KullaniciId == CurrentAccount.ytkID)
                .Single(x => x.Aktif);

            var eId = SipEkle(addressId);

            var sira = 1;
            foreach (var cartItem in activeCart.b2bSepetAyr)
            {
                SipSatirEkle(eId, sira++, cartItem.UrunId, cartItem.Adet);
            }
            SipEklendi(eId);

            if (!string.IsNullOrWhiteSpace(sipNotSatiri1) ||
                !string.IsNullOrWhiteSpace(sipNotSatiri2) ||
                !string.IsNullOrWhiteSpace(sipNotSatiri3))
            {
                var not = new sipNot
                {
                    enID = eId,
                    enNot = sipNotSatiri1.PadRight(33) + sipNotSatiri2.PadRight(33) + sipNotSatiri3.PadRight(33),
                };
                Db.sipNot.Add(not);
            }

            // aktif sepeti temizleyelim
            foreach (var cartItem in activeCart.b2bSepetAyr.ToList())
            {
                Db.b2bSepetAyr.Remove(cartItem);
            }
            Db.SaveChanges();

            MailSend(eId);


            return RedirectToAction("OrderCompleted", new { id = eId });
        }

        private void MailSend(int eID)
        {
            try
            {
                var musteriMailListesi = new List<string>();
                if (CurrentAccount.ytkSiparisMailBildirimi > 0)
                {
                    musteriMailListesi.Add(CurrentAccount.ytkEposta);
                }
                musteriMailListesi.AddRange((from fy in Db.frmYetkili
                                             where fy.ytkFrm == CurrentCompany.frmID
                                             where fy.ytkID != CurrentAccount.ytkID
                                             where fy.ytkSiparisMailBildirimi > 0 && fy.ytkEposta != ""
                                             select (
                                                      fy.ytkEposta
                                                    )).ToList());

                var musteriMailTo = string.Join(";", musteriMailListesi);

                if (musteriMailListesi.Any())
                {
                    var musteriMailSorgu = " exec [b2b].[sipMailGonder] @eID=" + eID + ",@mailTo='" + musteriMailTo + "',@saglayici=0  ";
                    Db.Database.ExecuteSqlCommand(musteriMailSorgu);
                }

                var saglayiciMaili = Db.drn2ayar.Where(x => x.ayarID == 769).ToList();
                if (saglayiciMaili.Any())
                {
                    if (saglayiciMaili.SingleOrDefault().ayarDeger.ToString().Replace(" ", "") != "")
                    {
                        var saglayiciMailSorgu = " exec [b2b].[sipMailGonder] @eID=" + eID + ",@mailTo='" + saglayiciMaili.SingleOrDefault().ayarDeger.ToString() + "',@saglayici=1  ";
                        Db.Database.ExecuteSqlCommand(saglayiciMailSorgu);
                    }
                }

            }
            catch
            {
            }
        }

        public ActionResult OrderCompleted(int? id)
        {
            Check.NotNullOr404(id);

            var sip = Db.sip
                .Where(x => x.eFirma == CurrentCompany.frmID)
                .SingleOrDefault(x => x.eID == id);
            Check.NotNullOr404(sip);

            var model = new OrderCompletedModel
            {
                SiparisId = id.Value,
                SiparisNotu = sip.sipNot != null ? sip.sipNot.enNot : "",

                SiparisSatirlari = sip.sipAyr
                                .Select(x => new OrderCompletedModel.SiparisSatiri
                                {
                                    Sira = x.ehSira,
                                    UrunId = x.urn.stkID,
                                    UrunAdi = x.urn.stkAd,
                                    StokKodu = x.urn.stkKod,
                                    Adet = x.ehAdet,
                                    Matrah = x.ehTutar - x.ehIndirim,
                                    Tutar = x.ehTutar - x.ehIndirim + x.ehTutarKDV,
                                })
                                .ToList(),
            };

            var ayar = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 765);
            ViewBag.KategoriYok = ayar == null ? false : byte.Parse(ayar.ayarDeger) > 0;

            return View(model);
        }

        private int SipEkle(int addressId)
        {
            int eTip = 1;
            int mekanTip = 1;
            int mekanID = Db.posMagaza.Single(x => x.mekanMerkezDepo == 1).mekanID;


            var siparisMekani = Db.drn2ayar.Where(x => x.ayarID == 770).ToList();
            if (siparisMekani.Any())
            {
                int ayarDgr = Convert.ToInt32(siparisMekani.FirstOrDefault().ayarDeger);
                if (ayarDgr > 0)
                {
                    var yeniMekan = Db.posMagaza.Where(x => x.mekanID == ayarDgr && (x.mekanTip == 0 || x.mekanTip == 1)).ToList();
                    if (yeniMekan.Any())
                    {
                        mekanID = yeniMekan.FirstOrDefault().mekanID;
                        mekanTip = yeniMekan.FirstOrDefault().mekanTip;
                    }
                }
            }

            if (mekanTip == 0)
            {
                eTip = 7;
            }

            var firmaAdresi = Db.frm
                .Where(x => x.frmID == CurrentCompany.frmID || x.frmBagID == CurrentCompany.frmID)
                .Single(x => x.frmID == addressId);
            var onayVarmi = Db.sifTip.Single(x => x.hrktSIF == 0 && x.hrktTipID == 1).hrktOnay == 0 ? 1 : 0;
            var result = Db.Database.SqlQuery<int>("EXEC sip_ekle " +
                                                   "@eID = {0}," +
                                                   "@eTip = {1}," +
                                                   "@eNo = {2}," +
                                                   "@frmID = {3}," +
                                                   "@eMekan = {4}," +
                                                   "@neden = {5}," +
                                                   "@eTarih = {6}," +
                                                   "@eTarihS = {7}," +
                                                   "@eTarihV = {8}," +
                                                   "@onay = {9}," +
                                                   "@oKisi = {10}," +
                                                   "@kKisi = {11}," +
                                                   "@eY1 = {12}," +
                                                   "@eY2 = {12}," +
                                                   "@eY3 = {12}," +
                                                   "@eY4 = {12}," +
                                                   "@eY5 = {12}," +
                                                   "@eT6 = {12}," +
                                                   "@eSakTur = {13}," +
                                                   "@eFirmaMkn = {14}," +
                                                   "@eOdm = {15}," +
                                                   "@eKtgr = {16}," +
                                                   "@emir = {17}," +
                                                   "@belgeNot = {18}," +
                                                   "@eNot = {19}," +
                                                   "@eOto = {20}",
                                                   0, eTip, string.Format(DateTime.Now.ToString("ddMMHHmm")),
                                                   CurrentCompany.frmID, mekanID, 0,
                                                   DateTime.Today, DateTime.Today.AddDays(CurrentCompany.frmTeslimGun),
                                                   DateTime.Today.AddDays(CurrentCompany.frmVade),
                                                   onayVarmi, CurrentAccount.ytkInsID, CurrentAccount.ytkInsID,
                                                   0, CurrentCompany.frmSakTur, firmaAdresi.frmID == CurrentCompany.frmID ? 0 : firmaAdresi.frmID,
                                                   1, 0, 0, "", "", 0);
            return result.Single();
        }

        private void SipSatirEkle(int eId, int sira, int stkId, decimal adet)
        {
            var urn = Db.urnSatis_vw.Single(x => x.stkID == stkId);
            var urnBrm = Db.urn.Single(x => x.stkID == stkId).urnBrm;
            var tutar = (CurrentCompany.frmFiyatTur == 1 ? urn.fiyatS1 : (CurrentCompany.frmFiyatTur == 2 ? urn.fiyatS2 : (CurrentCompany.frmFiyatTur == 3 ? urn.fiyatS3 : (CurrentCompany.frmFiyatTur == 4 ? urn.fiyatS4 : urn.fiyatS)))) * adet;

            var mrkIndirim = Db.sakMrk_vw
                .Where(x => x.skFrmID == CurrentCompany.frmID)
                .Where(x => x.skTur == CurrentCompany.frmVadeTur)
                .SingleOrDefault(x => x.skMrkID == urn.urnMrkID);

            var ekIndirim = Db.fytOzl
                .Where(x => x.fStkID == urn.stkID)
                .Where(x => x.fTip == 4)
                //.Where(x => x.fFrmID == 0)
                .Where(x => x.fTarih <= DateTime.Today && x.fTarihSon >= DateTime.Today)
                .Where(x => x.fTur == 0)
                .Where(x => x.onay == 1)
                .OrderByDescending(x => x.fTarih)
                .ThenByDescending(x => x.fID)
                .FirstOrDefault();

            var indirimler = new List<decimal>();
            if (mrkIndirim != null)
            {
                indirimler.Add(mrkIndirim.skY1);
                indirimler.Add(mrkIndirim.skY2);
                indirimler.Add(mrkIndirim.skY3);
            }
            if (ekIndirim != null)
            {
                indirimler.Add(ekIndirim.fInd1);
            }
            var indirimliFiyat = indirimler.Aggregate(tutar, (fiyat, indirim) => fiyat - (fiyat * indirim / 100.0m));
            var toplamIndirim = tutar - indirimliFiyat;

            var kdvYuzdesi = Db.urnKDV.Single(y => y.kdvID == urn.KDVs).kdvYuzde;
            var kdvTutari = indirimliFiyat * ((decimal)kdvYuzdesi / 100.0m);

            Db.Database.SqlQuery<dynamic>("EXEC sipSatir_ekle " +
                                          "@ehID = {0}," +
                                          "@ehSira = {1}," +
                                          "@ehStkID = {2}," +
                                          "@ehAdet = {3}," +
                                          "@ehAdetN = {4}," +
                                          "@ehTutar = {5}," +
                                          "@ehIndirim = {6}," +
                                          "@ehKDV = {7}," +
                                          "@ehTutarKDV = {8}," +
                                          "@ehi1 = {9}," +
                                          "@ehi2 = {10}," +
                                          "@ehi3 = {11}," +
                                          "@ehi4 = {12}," +
                                          "@ehi5 = {13}," +
                                          "@ehiT6 = {14}," +
                                          "@ehBirim = {15}",
                                          eId, sira, stkId, adet, adet,
                                          tutar, toplamIndirim, urn.KDVs, kdvTutari,
                                          mrkIndirim != null ? mrkIndirim.skY1 : 0,
                                          mrkIndirim != null ? mrkIndirim.skY2 : 0,
                                          mrkIndirim != null ? mrkIndirim.skY3 : 0,
                                          ekIndirim != null ? ekIndirim.fInd1 : 0,
                                          0, 0, urnBrm).ToList();

        }

        private void SipEklendi(int eId)
        {
            Db.Database.SqlQuery<dynamic>("EXEC sip_eklendi @eID = {0}", eId);
            Db.Database.SqlQuery<dynamic>("EXEC sipSatirKDVBelgeSonu_guncelle @eID = {0}", eId);
        }

        #endregion

        #region DropDown Data

        [HttpPost]
        public ActionResult CitiesDropDownData()
        {
            var model = Db.frmKent
                .Select(x => new DropDownListItem
                {
                    Value = x.alanKodu,
                    Text = x.ilAd
                })
                .OrderBy(x => x.Text)
                .ToList();

            return Json(model);
        }



        [HttpPost]
        public ActionResult districtDropDownData(int ilId)
        {
            var districts = from frmKentIlce in Db.frmKentIlce
                            where frmKentIlce.ilAlanKodu == ilId
                            select new DropDownListItem
                            {
                                Value = frmKentIlce.ilceKodu,
                                Text = frmKentIlce.ilceAdi
                            };
            return Json(districts.Distinct().OrderBy(x => x.Text));
        }


        private void PopulateSevkAdresleri()
        {
            var sevkAdresleri = Db.frm
                .Where(x => x.frmTip == 9)
                .Where(x => x.frmBagID == CurrentCompany.frmID)
                .Select(x => new OrderDeliveryAddressModel
                {
                    Id = x.frmID,
                    AdresAdi = x.frmAd,
                    AdresSatiri1 = x.adres1,
                    AdresSatiri2 = x.adres2,
                    PostaKodu = x.postaKod,
                    Ilce = Db.frmKentIlce.FirstOrDefault(y => y.ilceKodu == x.ilceKod).ilceAdi,
                    Il = Db.frmKent.FirstOrDefault(y => y.alanKodu == x.il).ilAd,
                })
                .OrderBy(x => x.AdresAdi);

            if (sevkAdresleri.Any())
            {
                ViewBag.SevkAdresleri = sevkAdresleri.ToList();
            }
            else
            {
                ViewBag.SevkAdresleri = new List<OrderDeliveryAddressModel>();
                ViewBag.SevkAdresleri.Add(new OrderDeliveryAddressModel
                {
                    Id = CurrentCompany.frmID,
                    AdresAdi = CurrentCompany.frmAd,
                    AdresSatiri1 = CurrentCompany.adres1,
                    AdresSatiri2 = CurrentCompany.adres2,
                    PostaKodu = CurrentCompany.postaKod,
                    Ilce = Db.frmKentIlce.FirstOrDefault(y => y.ilceKodu == CurrentCompany.ilceKod).ilceAdi,
                    Il = Db.frmKent.FirstOrDefault(y => y.alanKodu == CurrentCompany.il).ilAd,
                });
            }
        }

        #endregion


        private bool onaysizSiparisGoster()
        {
            var OnaysizSiparisGösterimi = Db.drn2ayar.FirstOrDefault(x => x.ayarID == 773);
            bool bekleyenSipGoster = true;
            if (CurrentAccountType == AccountType.Customer)
            {
                if ((OnaysizSiparisGösterimi == null ? 0 : Convert.ToInt32(OnaysizSiparisGösterimi.ayarDeger)) == 3 || (OnaysizSiparisGösterimi == null ? 0 : Convert.ToInt32(OnaysizSiparisGösterimi.ayarDeger)) == 2)
                {
                    bekleyenSipGoster = false;
                }
            }
            else
            {
                if ((OnaysizSiparisGösterimi == null ? 0 : Convert.ToInt32(OnaysizSiparisGösterimi.ayarDeger)) == 3 || (OnaysizSiparisGösterimi == null ? 0 : Convert.ToInt32(OnaysizSiparisGösterimi.ayarDeger)) == 1)
                {
                    bekleyenSipGoster = false;
                }
            }
            return bekleyenSipGoster;
        }




    }
}