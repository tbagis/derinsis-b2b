﻿namespace DerinSIS.B2B.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using Emails;
    using Helpers;
    using Infrastructure.Auth;
    using Models;
    using ViewModels;

    [B2BAuthorize(AccountType.Supplier | AccountType.Customer, Privilege.YONETIM)]
    public class ManagementController : BaseController
    {
        private readonly Mailer _mailer;
        private readonly SystemSettings _settings;

        public ManagementController(DbEntities db, Mailer mailer, SystemSettings settings) : base(db)
        {
            _mailer = mailer;
            _settings = settings;
        }

        public ActionResult Index()
        {
            return RedirectToAction("CompanyUsers");
        }

        public ActionResult CompanyUsers()
        {
            var admin = CurrentAccount;
            var model = new CompanyUsersModel
            {
                AdminSiparisYetkisi = admin.ytkB2bSiparisYetkisi != 0,
                AdminFaturaYetkisi = admin.ytkB2bFaturaYetkisi != 0,
                AdminMutabakatYetkisi = admin.ytkB2bMutabakatYetkisi != 0,
                AdminSevkiyatYetkisi = admin.ytkB2bSevkiyatYetkisi != 0,

                CompanyUsers = Db.frmYetkili
                                .Where(x => x.frm.frmID == CurrentCompany.frmID)
                                .Where(x => x.ytkB2B != 0)
                                .Select(x => new CompanyUsersModel.CompanyUser
                                {
                                    Id = x.ytkID,
                                    Ad = x.ytkAd,
                                    Eposta = x.ytkEposta,
                                    B2BYoneticisiMi = x.ytkB2B == 3,
                                    Unvan = x.ytkUnvan,

                                    AktifKullaniciMi = x.ytkB2B != 1,

                                    SiparisYetkisi = x.ytkB2bSiparisYetkisi != 0,
                                    FaturaYetkisi = x.ytkB2bFaturaYetkisi != 0,
                                    SevkiyatYetkisi = x.ytkB2bSevkiyatYetkisi != 0,
                                    MutabakatYetkisi = x.ytkB2bMutabakatYetkisi != 0,
                                    SifresiMevcutMu = !string.IsNullOrEmpty(x.ytkSifre),
                                })
                                .ToList(),
            };

            return View(model);
        }

        public ActionResult EditUser(int? id)
        {
            Check.NotNullOr404(id);

            var yetkili = Db.frmYetkili
                .Where(x => x.frm.frmID == CurrentCompany.frmID)
                .Where(x => x.ytkB2B != 3)
                .SingleOrDefault(x => x.ytkID == id);
            Check.NotNullOr404(yetkili);
            Check.CheckOr404(yetkili.ytkB2B != 3);

            var admin = CurrentAccount;

            var model = new EditUserModel
            {
                Id = yetkili.ytkID,
                EPosta = yetkili.ytkEposta,
                Isim = yetkili.ytkAd,
                Unvan = yetkili.ytkUnvan,

                SiparisYetkisi = yetkili.ytkB2bSiparisYetkisi != 0,
                FaturaYetkisi = yetkili.ytkB2bFaturaYetkisi != 0,
                SevkiyatYetkisi = yetkili.ytkB2bSevkiyatYetkisi != 0,
                MutabakatYetkisi = yetkili.ytkB2bMutabakatYetkisi != 0,
                AktifKullanici = yetkili.ytkB2B != 1,

                AdminFaturaYetkisi = admin.ytkB2bFaturaYetkisi != 0,
                AdminSiparisYetkisi = admin.ytkB2bSiparisYetkisi != 0,
                AdminSevkiyatYetkisi = admin.ytkB2bSevkiyatYetkisi != 0,
                AdminMutabakatYetkisi = admin.ytkB2bMutabakatYetkisi != 0,
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult EditUser(int? id, EditUserModel model)
        {
            Check.NotNullOr404(id);

            var yetkili = Db.frmYetkili
                .Where(x => x.frm.frmID == CurrentCompany.frmID)
                .SingleOrDefault(x => x.ytkID == id);
            Check.NotNullOr404(yetkili);
            Check.CheckOr404(yetkili.ytkB2B != 3);

            if (!ModelState.IsValid)
            {
                ShowErrorMessage();
                return View(model);
            }

            bool sonekvar = false;
            var sonEk = Db.drn2ayar.FirstOrDefault(x => x.ayarID == 777);
            if (sonEk != null)
            {
                if (sonEk.ayarDeger != "")
                {
                    sonekvar = true;
                }
            }

            if (model.EPosta.IndexOf('@') < 0)
            {
                if (sonekvar == false)
                {
                    ShowErrorMessage("Sistemde kullanıcı adı e-posta olma zorunluluğu vardır!");
                    return View(model);
                }
                else
                {
                    model.EPosta = model.EPosta + sonEk.ayarDeger;
                }
            }


            var admin = CurrentAccount;

            if (model.EPosta.IndexOf('@') > -1)
            {

                if (sonekvar)
                {
                    var userEmailExtension = model.EPosta.Substring(model.EPosta.IndexOf("@"));
                    if (userEmailExtension.ToLower() != sonEk.ayarDeger.ToLower())
                    {
                        var adminEmailExtension = admin.ytkEposta.Substring(admin.ytkEposta.IndexOf("@"));
                        if (adminEmailExtension.ToLower() == sonEk.ayarDeger.ToLower())
                        {
                            var ytkler = Db.frmYetkili.Where(x => x.ytkFrm == admin.ytkFrm && x.ytkB2B > 2 && !x.ytkEposta.ToLower().Contains(sonEk.ayarDeger.ToLower())).FirstOrDefault();
                            if (ytkler != null)
                            {
                                adminEmailExtension = ytkler.ytkEposta.Substring(ytkler.ytkEposta.IndexOf("@"));
                            }
                        }

                        if (userEmailExtension.ToLowerInvariant() != adminEmailExtension.ToLowerInvariant())
                        {
                            ModelState.AddModelError("EPosta", "Kuruma ait bir e-posta hesabı girmelisiniz.");
                            ShowErrorMessage();
                            return View(model);
                        }
                    }
                }
                else
                {
                    var userEmailExtension = model.EPosta.Substring(model.EPosta.IndexOf("@"));
                    var adminEmailExtension = admin.ytkEposta.Substring(admin.ytkEposta.IndexOf("@"));
                    if (userEmailExtension.ToLowerInvariant() != adminEmailExtension.ToLowerInvariant())
                    {
                        ModelState.AddModelError("EPosta", "Kuruma ait bir e-posta hesabı girmelisiniz.");
                        ShowErrorMessage();
                        return View(model);
                    }
                }




            }




            yetkili.ytkEposta = model.EPosta;
            yetkili.ytkAd = model.Isim;
            yetkili.ytkUnvan = model.Unvan;

            if (admin.ytkB2bFaturaYetkisi != 0)
            {
                yetkili.ytkB2bFaturaYetkisi = (byte)(model.FaturaYetkisi ? 1 : 0);
            }

            if (admin.ytkB2bMutabakatYetkisi != 0)
            {
                yetkili.ytkB2bMutabakatYetkisi = (byte)(model.MutabakatYetkisi ? 1 : 0);
            }


            if (admin.ytkB2bSiparisYetkisi != 0)
            {
                yetkili.ytkB2bSiparisYetkisi = (byte)(model.SiparisYetkisi ? 1 : 0);
            }

            if (admin.ytkB2bSevkiyatYetkisi != 0)
            {
                yetkili.ytkB2bSevkiyatYetkisi = (byte)(model.SevkiyatYetkisi ? 1 : 0);
            }

            if (yetkili.ytkB2B != 3)
            {
                if (model.AktifKullanici)
                {
                    yetkili.ytkB2B = 2;
                }
                else
                {
                    yetkili.ytkB2B = 1;
                }
            }

            Db.SaveChanges();

            ShowSuccessMessage("Kullanıcı bilgileri güncellenmiştir.");
            return RedirectToAction("CompanyUsers");
        }

        [HttpPost]
        public ActionResult ResetUserPassword(int? id)
        {
            Check.NotNullOr404(id);

            //var user = Db.frmYetkili
            //    .Where(x => x.ytkFrm == CurrentCompany.frmID)
            //    .SingleOrDefault(x => x.ytkID == id);
            //Check.NotNullOr404(user);

            //if(string.IsNullOrWhiteSpace(user.ytkEposta))
            //{
            //    ShowErrorMessage("Kullanıcının E-posta adresi mevcut değil. Lütfen önce kullanıcı bilgilerini güncelleyiniz.");
            //    return RedirectToAction("CompanyUsers");
            //}
            //var resetlemeTalebi = new b2bSifreResetlemeTalep
            //                      {
            //                          frmYetkili = user,
            //                          eposta = user.ytkEposta,
            //                          gKisi = CurrentAccount != null ? CurrentAccount.ytkInsID : 0,
            //                          gTarih = DateTime.Now,
            //                          talepDrm = 0,
            //                          Hash = Guid.NewGuid().ToString(),
            //                      };
            //Db.b2bSifreResetlemeTalep.Add(resetlemeTalebi);
            //Db.SaveChanges();

            //// resetleme mailini gönder
            //var mailModel = new ResetPasswordEmailModel
            //{
            //    KullaniciIsmi = user.ytkAd,
            //    ResetUrl = _settings.SiteAdresi + Url.Action("RecoverPasswordComplete", "Account", new { id = resetlemeTalebi.Hash }),
            //};
            //_mailer.SendResetPasswordMail(CurrentAccount, user.ytkEposta.ToLowerInvariant().Trim(), mailModel);

            return RedirectToAction("ResetUserPasswordComplete", new { id });
        }

        public ActionResult ResetUserPasswordComplete(int? id)
        {
            return View();
        }
    }
}