﻿namespace DerinSIS.B2B.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using Models;
    using Infrastructure.Auth;

    public abstract class BaseController : Controller
    {
        protected BaseController(DbEntities db)
        {
            Db = db;
        }

        protected DbEntities Db { get; private set; }

        #region sistemAyarlari

        public int BrutFiyatGostermiVar
        {
            get
            {
                var brutFiyatGosterilsin = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 766);
                return brutFiyatGosterilsin == null ? 0 : Convert.ToInt32(brutFiyatGosterilsin.ayarDeger);
            }
        }

        public int StkKontroluVar
        {
            get
            {
                var stokKontrolu = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 763);
                return stokKontrolu == null ? 0 : Convert.ToInt32(stokKontrolu.ayarDeger);
            }
        }

        public int StkGosterimiVar
        {
            get
            {
                var stokGosterim = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 764);
                return stokGosterim == null ? 0 : Convert.ToInt32(stokGosterim.ayarDeger);
            }
        }

        public int MarkaYerineGrupGosterimiVar
        {
            get
            {
                var markaYerineGrup = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 767);
                return markaYerineGrup == null ? 0 : Convert.ToInt32(markaYerineGrup.ayarDeger);
            }
        }

        public int MusteriMarkalarinaAitUrunleriGorsun
        {
            get
            {
                var markalaraAitUrunleriGorsun = Db.drn2ayar.SingleOrDefault(x => x.ayarID == 768);
                return markalaraAitUrunleriGorsun == null ? 0 : Convert.ToInt32(markalaraAitUrunleriGorsun.ayarDeger);
            }
        }

        #endregion

        #region Aktif Hesap Bilgileri

        private frmYetkili _currentAccount;
        public frmYetkili CurrentAccount
        {
            get
            {
                if (!User.Identity.IsAuthenticated)
                {
                    return null;
                }

                if (_currentAccount == null)
                {
                    int userID = Convert.ToInt32(User.Identity.Name);
                    _currentAccount = Db.frmYetkili
                        .SingleOrDefault(x => x.ytkID ==userID);
                }
                return _currentAccount;
            }
        }

        public frm CurrentCompany
        {
            get
            {
                if (CurrentAccount == null)
                {
                    return null;
                }

                return CurrentAccount.frm;
            }
        }

        public AccountType? CurrentAccountType
        {
            get
            {
                if (CurrentCompany == null)
                {
                    return null;
                }

                if (CurrentCompany.frmTip == 1)
                {

                    return AccountType.Customer;
                }

                if (CurrentCompany.frmTip == 0)
                {
                    return AccountType.Supplier;
                }

                throw new InvalidOperationException("Sadece frmTip=1 ve frmTip=0 olan kullanıcılar giriş yapabilirler.");
            }
        }

        #endregion

        #region Messages

        protected void ShowErrorMessage(string message = "İşlem yapılamadı. Lütfen bilgilerinizi kontrol edip tekrar deneyiniz")
        {
            var messages = (List<string>)TempData["_error_messages"] ?? new List<string>();
            messages.Add(message);
            TempData["_error_messages"] = messages;
        }

        protected void ShowSuccessMessage(string message)
        {
            var messages = (List<string>)TempData["_success_messages"] ?? new List<string>();
            messages.Add(message);
            TempData["_success_messages"] = messages;
        }

        #endregion
    }
}