﻿using System.Xml;

namespace DerinSIS.B2B.Controllers
{
    using System;
    using System.Web.Mvc;
    using Infrastructure.Auth;
    using Models;
    using ViewModels;
    using System.Linq;

    [B2BAuthorize(AccountType.Customer | AccountType.Supplier)]
    public class HomeController : BaseController
    {
        public HomeController(DbEntities db) : base(db)
        {
        }

        public ActionResult Index()
        {
            if (CurrentAccount.ytkB2bSiparisYetkisi != 0)
            {
                if (CurrentAccountType == AccountType.Customer)
                {
                    return RedirectToAction("Index", "Store");
                }

                if (CurrentAccountType == AccountType.Supplier)
                {
                    return RedirectToAction("Index", "Supplier");
                }
            }

            if (CurrentAccount.ytkB2bSevkiyatYetkisi != 0)
            {
                return RedirectToAction("Index", "Sales");
            }

            if (CurrentAccount.ytkB2bFaturaYetkisi != 0 || CurrentAccount.ytkB2bMutabakatYetkisi != 0)
            {
                return RedirectToAction("Index", "Accounting");
            }

            throw new InvalidOperationException("Account Type not supported");
        }

        [ChildActionOnly]
        public ActionResult MainMenu()
        {
            var anaSayfaAd = "DerinSIS B2B";
            string _helpMail = "";
            try
            {
                XmlTextReader okuyucu = new XmlTextReader("http://www.tcmb.gov.tr/kurlar/today.xml");
                XmlDocument dokuman = new XmlDocument();
                dokuman.Load(okuyucu);
                XmlNode dolar = dokuman.SelectSingleNode("/Tarih_Date/Currency[CurrencyName='US DOLLAR']");
                XmlNode euro = dokuman.SelectSingleNode("/Tarih_Date/Currency[CurrencyName='EURO']");
                String doviz = "$:" + dolar.ChildNodes[4].InnerText + ", €:" + euro.ChildNodes[4].InnerText;

                var siteAdi = Db.drn2ayar.FirstOrDefault(x => x.ayarID == 762);
                if (siteAdi != null)
                {
                    if (siteAdi.ayarDeger != "")
                    {
                        anaSayfaAd = siteAdi.ayarDeger;
                    }
                }

                var helpmail = Db.drn2ayar.FirstOrDefault(x => x.ayarID == 774);
                if (helpmail != null)
                {
                    if (helpmail.ayarDeger != "")
                    {
                        _helpMail = helpmail.ayarDeger;
                    }
                }

                var model = new MainMenuModel
                {
                    AccountType = CurrentAccountType.Value,
                    FaturaYetkisi = CurrentAccount.ytkB2bFaturaYetkisi != 0,
                    SiparisYetkisi = CurrentAccount.ytkB2bSiparisYetkisi != 0,
                    MutabakatYetkisi = CurrentAccount.ytkB2bMutabakatYetkisi != 0,
                    SevkiyatYetkisi = CurrentAccount.ytkB2bSevkiyatYetkisi != 0,
                    YoneticiYetkisi = CurrentAccount.ytkB2B == 3,
                    Doviz = doviz,
                    kAdi = CurrentAccount.ytkAd,
                    kPosta = CurrentAccount.ytkEposta,
                    anaSayfaAdi = anaSayfaAd,
                    helpMailAdress = _helpMail
                };
                return PartialView(model);
            }
            catch (Exception)
            {
                var model = new MainMenuModel
                {
                    AccountType = CurrentAccountType.Value,
                    FaturaYetkisi = CurrentAccount.ytkB2bFaturaYetkisi != 0,
                    SiparisYetkisi = CurrentAccount.ytkB2bSiparisYetkisi != 0,
                    MutabakatYetkisi = CurrentAccount.ytkB2bMutabakatYetkisi != 0,
                    SevkiyatYetkisi = CurrentAccount.ytkB2bSevkiyatYetkisi != 0,
                    YoneticiYetkisi = CurrentAccount.ytkB2B == 3,
                    Doviz = "$:, €:",
                    kAdi = CurrentAccount.ytkAd,
                    kPosta = CurrentAccount.ytkEposta,
                    anaSayfaAdi = anaSayfaAd,
                    helpMailAdress = _helpMail
                };
                return PartialView(model);
            }

            return PartialView();
        }
    }
}