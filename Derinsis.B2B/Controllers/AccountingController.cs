﻿using System.Web.Routing;

namespace DerinSIS.B2B.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using Helpers;
    using Infrastructure;
    using Infrastructure.Auth;
    using Models;
    using ViewModels;
    using System.IO;
    using System.Web.UI;
    using System.Collections.Generic;
    using System.Configuration;
    using DtpService;
    using File = DtpService.File;
    using System.Xml.Linq;
    using System.Text;
    using B2B.Helpers.Html;

    [B2BAuthorize(AccountType.Supplier | AccountType.Customer, Privilege.FATURA | Privilege.CARI)]
    public class AccountingController : BaseController
    {
        private readonly SystemSettings _settings;
        private readonly List<int> saleTypes;
        private readonly IEnumerable<int> product;
        private readonly List<int> totalSale;
        private readonly List<int> refundofSale;


        public AccountingController(DbEntities db, SystemSettings settings)
            : base(db)
        {
            _settings = settings;
            saleTypes = new List<int>() { 1, 3, 4, 5, 100, 101 };
            product = Db.urnFrm.Where(x => x.urnFrmFirmaID == CurrentCompany.frmID).Select(x => x.urnFrmStkID);
            totalSale = new List<int>() { 1, 4, 100 };
            refundofSale = new List<int>() { 3, 5, 101 };
        }

        #region Dashboard

        public ActionResult Index()
        {
            // Banka Hesap Bilgileri
            var bankaHesapBilgileri = Db.frmBnk
                .Where(x => x.frmBnkFirmaID == CurrentCompany.frmID && x.frmBnkOncelik == 0)
                .Select(x => new AccountingDashboardModel.BankaHesapBilgisi
                {
                    HesapNo = x.frmBnkHesapNo,
                    Sube = x.bnkSube.subeAd,
                    Banka = x.bnkSube.bnk.bankaAd,
                })
                .ToList();

            // Ödenmemiş Faturalar

            bool _musterimi = CurrentAccountType.Value == AccountType.Customer ? true : false;

            var odenmemisFaturalar = new List<AccountingDashboardModel.OdenmemisFatura>();
            if (_musterimi)
            {
                odenmemisFaturalar = Db.car
               .Where(x => x.cKod == CurrentCompany.frmID)
               .Where(x => x.cKalan != 0)
               .Join(Db.fat, car => car.cFatID, fat => fat.eID, (car, fat) => new { car, fat })
               .OrderByDescending(x => x.car.cTarih)
               .Select(x => new AccountingDashboardModel.OdenmemisFatura
               {
                   Id = x.fat.eID,
                   KalanTutar = x.car.cKalan,
                   EvrakNo = x.fat.eNo,
                   FaturaTarihi = x.fat.eTarih,
                   VadeTarihi = x.fat.eTarihV,
               })
               .ToList();
            }

            var dateTimeNow = DateTime.Now.Date;

            var odenmemisCekler = new List<AccountingDashboardModel.OdenmemisCek>();
            if (_musterimi)
            {
                odenmemisCekler = Db.car
                  .Where(x => x.cKod == CurrentCompany.frmID)
                  .Where(x => x.cTip == 200 || x.cTip == 201 || x.cTip == 202 || x.cTip == 203)
                  .Where(x => x.cTarihVade >= dateTimeNow)
                  .Join(Db.carTip, car => car.cTip, carTip => carTip.ctID, (car, carTip) => new { car, carTip })
                  .OrderByDescending(x => x.car.cTarih)
                  .Select(x => new AccountingDashboardModel.OdenmemisCek
                  {
                      Id = x.car.cID,
                      EvrakTipi = x.carTip.ctAd,
                      KalanTutar = x.car.cTutar,
                      EvrakNo = x.car.cEvrakNo,
                      Tarih = x.car.cTarih,
                      VadeTarihi = x.car.cTarihVade,
                  })
                  .ToList();
            }

            Decimal odenmemisCeklerToplam = 0;
            foreach (var satir in odenmemisCekler)
            {
                odenmemisCeklerToplam += satir.KalanTutar;
            }


            // Bakiye Bilgileri
            var borc = Db.car
                           .Where(x => x.cKod == CurrentCompany.frmID)
                           .Where(x => x.cBA == 1 /* Borç */)
                           .Sum(x => (decimal?)x.cTutar) ?? 0;

            var alacak = Db.car
                             .Where(x => x.cKod == CurrentCompany.frmID)
                             .Where(x => x.cBA == 0 /* Alacak */)
                             .Sum(x => (decimal?)x.cTutar) ?? 0;

            // Model
            var model = new AccountingDashboardModel
            {
                BankaHesapBilgileri = bankaHesapBilgileri,
                OdenmemisFaturalar = odenmemisFaturalar,
                OdenmemisCekler = odenmemisCekler,
                Borc = Math.Abs(borc),
                Alacak = Math.Abs(alacak),
                Bakiye = Math.Abs(borc) - Math.Abs(alacak),
                SistemMutabakatTarihi = CurrentCompany.frmCariTarih,
                FirmaMutabakatTarihi = CurrentCompany.frmCariTarihB2B,
                OdenmemisCeklerBakiyedenBuyukmu = CurrentCompany.frmBakiyeLimitCek == 0 ? 0 : ((odenmemisCeklerToplam >= CurrentCompany.frmBakiyeLimitCek) ? 1 : 0),
                BakiyeBakiyeLimitindenBuyukmu = CurrentCompany.frmBakiyeLimit == 0 ? 0 : (((Math.Abs(borc) - Math.Abs(alacak)) >= CurrentCompany.frmBakiyeLimit) ? 1 : 0),
                musterimi = _musterimi
            };
            return View(model);
        }

        #endregion

        #region Mutabakat


        [B2BAuthorize(AccountType.Supplier | AccountType.Customer, Privilege.FATURA)]
        public ActionResult Reconsilation()
        {

            DateTime bitTarih = Convert.ToDateTime("01.01.1900");
            if (Db.carMtbkt.Where(x => x.mtbktFrmID == CurrentCompany.frmID).Any())
            {
                bitTarih = Db.carMtbkt.Where(x => x.mtbktFrmID == CurrentCompany.frmID).Max(x => x.mtbktBitisTarih);
            }

            Decimal? bakiye = 0;
            if (Db.bakiye_vw.Where(m => m.cKod == CurrentCompany.frmID).Any())
            {
                bakiye = Db.bakiye_vw.SingleOrDefault(m => m.cKod == CurrentCompany.frmID).bakiye;
            }

            var reconsilations = new ReconsilationModel
            {
                Reconsilations =
                                Db.carMtbkt
                                .Select(x => new ReconsilationModel.Reconsilation
                                {
                                    EndDate = x.mtbktBitisTarih,
                                    StartDate = x.mtbktBaslangicTarih,
                                    BillAmount = x.mtbktAlisAdet,
                                    TotalAmount = x.mtbktAlisTutar,
                                    SellBillAmount = x.mtbktSatisAdet,
                                    SellTotalAmount = x.mtbktSatisTutar,
                                    Balance = x.mtbktBakiye,
                                    ReconsilationId = x.mtbktID
                                }).OrderByDescending(x => x.EndDate).ToList(),

                UnConfirmedReconsilations = new ReconsilationModel.Reconsilation
                {
                    EndDate = CurrentCompany.frmCariTarih,
                    BillAmount =
                          (from fat in Db.fat
                           let a = bitTarih
                           let b = CurrentCompany.frmCariTarih
                           where fat.eFirma == CurrentCompany.frmID
                           where fat.eTarih >= a && fat.eTarih < b
                           where fat.eGC == 0
                           select fat).Count(),
                    TotalAmount =
                           (from fatAyr in Db.fatAyr
                            join fat in Db.fat on fatAyr.ehID equals fat.eID
                            let a = bitTarih
                            let b = CurrentCompany.frmCariTarih
                            where fat.eFirma == CurrentCompany.frmID
                            where fat.eTarih >= a && fat.eTarih < b
                            where fat.eGC == 0
                            select (decimal?)fatAyr.ehTutar).Sum(),
                    SellBillAmount =
               (from fat in Db.fat
                let a = bitTarih
                let b = CurrentCompany.frmCariTarih
                where fat.eFirma == CurrentCompany.frmID
                where fat.eTarih >= a && fat.eTarih < b
                where fat.eGC == 1
                select fat).Count(),
                    SellTotalAmount =
                           (from fatAyr in Db.fatAyr
                            join fat in Db.fat on fatAyr.ehID equals fat.eID
                            let a = bitTarih
                            let b = CurrentCompany.frmCariTarih
                            where fat.eFirma == CurrentCompany.frmID
                            where fat.eTarih >= a && fat.eTarih < b
                            where fat.eGC == 1
                            select (decimal?)fatAyr.ehTutar).Sum(),
                    Balance = bakiye
                }
            };

            return View(reconsilations);
        }

        [B2BAuthorize(AccountType.Supplier | AccountType.Customer, Privilege.FATURA)]
        public ActionResult ReconsilationDetails(int? id)
        {
            if (id != null)
            {
                var mutabakat = Db.carMtbkt.Single(x => x.mtbktID == id);

                var model = new ReconsilationDetailsModel
                {
                    Reconsilate = true,
                    StartDate = mutabakat.mtbktBaslangicTarih,
                    EndDate = mutabakat.mtbktBitisTarih,
                    bakiye = mutabakat.mtbktBakiye,
                    Invoices =
                       (from fat in Db.fat
                        join frm in Db.frm on fat.eFirma equals frm.frmID
                        join sifTip in Db.sifTip on fat.eTip equals sifTip.hrktTipID
                        join sipNeden in Db.sipNeden on fat.Neden equals sipNeden.nedenID
                        join posMagaza in Db.posMagaza on fat.eMekan equals posMagaza.mekanID
                        join firmaMekan in Db.frm on fat.eFirmaMkn equals firmaMekan.frmID
                        /* Only Invoices for siftip */
                        where sifTip.hrktSIF == 2
                        where frm.frmID == CurrentAccount.frm.frmID
                        where fat.eTarih >= mutabakat.mtbktBaslangicTarih && fat.eTarih < mutabakat.mtbktBitisTarih
                        orderby fat.eTarih descending, fat.eID descending
                        select new ReconsilationDetailsModel.Invoice
                        {
                            Id = fat.eID,
                            Tarih = fat.eTarih,
                            EvrakNo = fat.eNo,
                            HareketTipi = sifTip.hrktTipAd,
                            Durum = (ReconsilationDetailsModel.InvoiceStatus)fat.eDurum,
                            VadeTarihi = fat.eTarihV,
                            EvrakNot = fat.eNot,
                            KDVHaricToplam = fat.fatAyr.Sum(x => x.ehTutar),
                            ToplamIndirim = fat.fatAyr.Sum(x => x.ehIndirim),
                            HareketGrubu = sipNeden.nedenAd,
                            SevkAdresi = frm.frmTip == 1 ? (fat.eFirmaMkn == 0 ? "" : firmaMekan.frmAd) : posMagaza.mekanAd,
                            eGCsi = fat.eGC,
                        }).ToList()
                };
                return View(model);
            }
            else
            {
                Decimal? bakiyesi = 0;
                if (Db.bakiye_vw.Where(m => m.cKod == CurrentCompany.frmID).Any())
                {
                    bakiyesi = Db.bakiye_vw.SingleOrDefault(m => m.cKod == CurrentCompany.frmID).bakiye;
                }

                DateTime bitTarih = Convert.ToDateTime("01.01.1900");
                if (Db.carMtbkt.Where(x => x.mtbktFrmID == CurrentCompany.frmID).Any())
                {
                    bitTarih = Db.carMtbkt.Where(x => x.mtbktFrmID == CurrentCompany.frmID).Max(x => x.mtbktBitisTarih);
                }

                var model = new ReconsilationDetailsModel
                {
                    Reconsilate = false,
                    EndDate = bitTarih,
                    bakiye = bakiyesi,
                    Invoices = (
                        from fat in Db.fat
                        join frm in Db.frm on fat.eFirma equals frm.frmID
                        join sifTip in Db.sifTip on fat.eTip equals sifTip.hrktTipID
                        join sipNeden in Db.sipNeden on fat.Neden equals sipNeden.nedenID
                        join posMagaza in Db.posMagaza on fat.eMekan equals posMagaza.mekanID
                        join firmaMekan in Db.frm on fat.eFirmaMkn equals firmaMekan.frmID
                        /* Only Invoices for siftip */
                        where sifTip.hrktSIF == 2
                        where frm.frmID == CurrentAccount.frm.frmID
                        where fat.eTarih >= bitTarih
                        where fat.eTarih < CurrentCompany.frmCariTarih
                        orderby fat.eTarih descending, fat.eID descending
                        select new ReconsilationDetailsModel.Invoice
                        {
                            Id = fat.eID,
                            Tarih = fat.eTarih,
                            EvrakNo = fat.eNo,
                            HareketTipi = sifTip.hrktTipAd,
                            Durum = (ReconsilationDetailsModel.InvoiceStatus)fat.eDurum,
                            VadeTarihi = fat.eTarihV,
                            EvrakNot = fat.eNot,
                            KDVHaricToplam = fat.fatAyr.Sum(x => x.ehTutar),
                            ToplamIndirim = fat.fatAyr.Sum(x => x.ehIndirim),
                            HareketGrubu = sipNeden.nedenAd,
                            SevkAdresi = frm.frmTip == 1 ? (fat.eFirmaMkn == 0 ? "" : firmaMekan.frmAd) : posMagaza.mekanAd,
                            eGCsi = fat.eGC,
                        }).ToList()
                };
                return View(model);
            }

        }
        [B2BAuthorize(AccountType.Supplier | AccountType.Customer, Privilege.FATURA)]
        public ActionResult ConfirmReconsilation(DateTime startdate, int count, decimal total, int sellcount, decimal selltotal, decimal balance)
        {
            var newReconsilation = new carMtbkt
            {
                mtbktBaslangicTarih = startdate,
                mtbktBitisTarih = CurrentCompany.frmCariTarih,
                mtbktAlisAdet = count,
                mtbktAlisTutar = total,
                mtbktSatisAdet = sellcount,
                mtbktSatisTutar = selltotal,
                mtbktBakiye = balance,
                gTarih = DateTime.Now,
                gKisi = CurrentAccount.ytkInsID,
                mtbktFrmID = CurrentCompany.frmID
            };
            Db.carMtbkt.Add(newReconsilation);
            Db.SaveChanges();

            var frm = Db.frm.SingleOrDefault(x => x.frmID == CurrentAccount.frm.frmID);
            frm.frmCariTarihB2B = frm.frmCariTarih;
            Db.SaveChanges();

            ShowSuccessMessage("Mutabakat başarıyla onaylandı!");
            return Redirect("ReconsilationDetails/" + newReconsilation.mtbktID + "");
        }

        #endregion

        #region Hesap Hareketleri

        [B2BAuthorize(AccountType.Supplier | AccountType.Customer, Privilege.CARI)]
        public ActionResult Activities(DocumentDateFilterModel dateFilter)
        {
            dateFilter.NotNullOrSetDefault(DateHelper.LastMonthBegin, DateHelper.LastMonthEnd);

            var activities =
                (
                    from car in Db.car
                    join carTipF in Db.carTipF on car.cFatTip equals carTipF.ctfID
                    join frm in Db.frm on car.cKod equals frm.frmID
                    where frm.frmID == CurrentAccount.frm.frmID
                    where (car.cTarih >= dateFilter.Begin) && (car.cTarih <= dateFilter.End)
                    orderby car.cTarih
                    let fat = Db.fat.FirstOrDefault(x => x.eID == car.cFatID)
                    select new AccountActivitiesModel.AccountActivity()
                    {
                        Id = car.cID,
                        IlgiliFaturaId = fat != null ? fat.eID : (int?)null,
                        IlgiliFaturaNo = fat != null ? fat.eNo : "",
                        Tarih = car.cTarih,
                        HareketTipi = carTipF.ctfAd,
                        EvrakNo = car.cEvrakNo,
                        VadeTarihi = car.cTarihVade,
                        Borc = (car.cBA == 1 ? (decimal?)Math.Abs(car.cTutar) : null),
                        Alacak = (car.cBA == 0 ? (decimal?)Math.Abs(car.cTutar) : null),
                    }
                ).ToList();


            // calculate activity balances
            var previousDebit = Db.car
                                    .Where(x => x.cKod == CurrentAccount.frm.frmID)
                                    .Where(x => x.cTarih < dateFilter.Begin)
                                    .Sum(x => ((decimal?)x.cTutar)) ?? 0;

            activities.Aggregate(previousDebit,
                                 (balance, activity) =>
                                 {
                                     if (activity.Alacak.HasValue)
                                     {
                                         balance += activity.Alacak.Value;
                                     }
                                     if (activity.Borc.HasValue)
                                     {
                                         balance -= activity.Borc.Value;
                                     }

                                     activity.Bakiye = balance;
                                     return balance;
                                 });


            return View(new AccountActivitiesModel
            {
                Activities = activities,
                DateFilter = dateFilter,
                MutabakatTarihi = CurrentCompany.frmCariTarih,
                DevredenBakiye = previousDebit,
            });
        }

        #endregion

        #region Faturalar

        [B2BAuthorize(AccountType.Supplier | AccountType.Customer, Privilege.FATURA)]
        public ActionResult Invoices(DocumentDateFilterModel dateFilter, int? productFilter, int? typeFilterId)
        {
            dateFilter.NotNullOrSetDefault(DateHelper.LastMonthBegin, DateHelper.LastMonthEnd);

            var invoices =
                (
                    from fat in Db.fat
                    join frm in Db.frm on fat.eFirma equals frm.frmID
                    join sifTip in Db.sifTip on fat.eTip equals sifTip.hrktTipID
                    join sipNeden in Db.sipNeden on fat.Neden equals sipNeden.nedenID
                    join posMagaza in Db.posMagaza on fat.eMekan equals posMagaza.mekanID
                    join firmaMekan in Db.frm on fat.eFirmaMkn equals firmaMekan.frmID
                    /* Only Invoices for siftip */
                    where sifTip.hrktSIF == 2
                    where typeFilterId == null || sifTip.hrktTipID == typeFilterId
                    where frm.frmID == CurrentAccount.frm.frmID
                    where fat.eTarih >= dateFilter.Begin && fat.eTarih <= dateFilter.End
                    where productFilter == null || fat.fatAyr.Select(x => x.ehStkID).Contains(productFilter.Value)
                    orderby fat.eTarih descending, fat.eID descending
                    select new InvoicesModel.Invoice
                    {
                        Id = fat.eID,
                        Tarih = fat.eTarih,
                        EvrakNo = fat.eNo,
                        HareketTipi = sifTip.hrktTipAd,
                        Durum = (InvoicesModel.InvoiceStatus)fat.eDurum,
                        VadeTarihi = fat.eTarihV,
                        EvrakNot = fat.eNot,
                        KDVHaricToplam = fat.fatAyr.Sum(x => x.ehTutar),
                        ToplamIndirim = fat.fatAyr.Sum(x => x.ehIndirim),
                        HareketGrubu = sipNeden.nedenAd,
                        SevkAdresi = frm.frmTip == 1 ? (fat.eFirmaMkn == 0 ? "" : firmaMekan.frmAd) : posMagaza.mekanAd,
                        fatArsivID = fat.eFatArsvID
                    }
                )
                    .ToList();

            return View(new InvoicesModel
            {
                Invoices = invoices,
                DateFilter = dateFilter,
                ProductFilterId = productFilter,
                ProductFilterLabel = productFilter.HasValue ? Db.urn.Single(x => x.stkID == productFilter).stkAd : "",
                TypeFilterId = typeFilterId,
                AvailableTypes = Db.sifTip
                                .Where(x => x.hrktSIF == 2)
                                .Select(x => new DocumentType
                                {
                                    Id = x.hrktTipID,
                                    Isim = x.hrktTipAd
                                })
                                .ToList(),
                GosterimSecenekleri =
                            new InvoicesModel.Settings
                            {
                                EvrakNotuGosterilsinMi = _settings.EvrakNotlariGosterilsin
                            }
            });
        }

        [B2BAuthorize(AccountType.Supplier | AccountType.Customer, Privilege.FATURA)]
        public ActionResult InvoiceDetail(int? id)
        {
            Check.NotNullOr404(id);

            var fat = Db.fat.SingleOrDefault(x => x.eID == id);
            Check.NotNullOr404(fat);

            var frm = fat.frm;
            // Başka firmalara ait kayıt gösterilmemeli
            Check.CheckOr404(frm.frmID == CurrentAccount.frm.frmID);

            var sifTip = Db.sifTip.Where(x => x.hrktSIF == 2).Single(x => x.hrktTipID == fat.eTip);
            var sipNeden = Db.sipNeden.Single(x => x.nedenID == fat.Neden);

            var sevkAdresiAd = "";
            frm sevkAdresi;
            if (frm.frmTip == 1)
            {
                // sevk adresi ismini firma mekandan alıyoruz.
                var firmaMekan = fat.eFirmaMkn == 0 ? fat.frm : Db.frm.Single(x => x.frmID == fat.eFirmaMkn);
                sevkAdresiAd = firmaMekan.frmAd;
                sevkAdresi = firmaMekan;
            }
            else
            {
                // sevk adresi ismini posMagaza'dan alıyoruz    
                var posMagaza = Db.posMagaza.Single(x => x.mekanID == fat.eMekan);
                sevkAdresiAd = posMagaza.mekanAd;
                sevkAdresi = Db.frm.Single(x => x.frmID == fat.eMekan);
            }

            string fatAyrSorgu = @" select ehID as Id,ehSira as Sira,ehStkID as UrunId,stkAd as UrunIsmi,stkKod as UrunKod,isnull(barkod,'') as UrunBarkod
                                  ,ehAdet as ToplamAdet,cast(urnKoli as decimal(18,2)) as KoliIciAdet,cast(urnPalet as decimal(18,2)) as PaletIciAdet,paketOlcu as UrunAgirligi,paketBrm as AgirlikBirim
                                  ,ehi1 as Indirim1,ehi2 as Indirim2,ehi3 as Indirim3,ehi4 as Indirim4,ehi5 as Indirim5,ehIndirim as IndirimTutari,ehTutar as KDVHaricToplam,cast(kdvYuzde as int) as KDVYuzde,ehTutarKDV as KDVTutar  
                                  from fatAyr 
                                  inner join urn on stkID=ehStkID
                                  inner join urnKDV on kdvID=ehKdv
                                  left join urnBarkod_vw on ehSTkID=urnBrkdStkID where ehID=" + fat.eID + " ";
            var fatAyrList = Db.Database.SqlQuery<InvoiceDetailModel.InvoiceDetail>(fatAyrSorgu).ToList();


            var model = new InvoiceDetailModel
            {
                Id = fat.eID,
                EvrakNo = fat.eNo,
                Tarih = fat.eTarih,
                SevkTarihi = fat.eTarihS,
                HareketTipi = sifTip.hrktTipAd,
                EvrakNot = fat.eNot,
                Evrak3SatirNot = fat.fatNot != null ? fat.fatNot.enNot : "",
                VadeTarihi = fat.eTarihV,
                HareketGrubu = sipNeden.nedenAd,
                Durum = (InvoiceDetailModel.InvoiceStatus)fat.eDurum,

                SevkAdresiAd = sevkAdresiAd,
                SevkAdresi1 = sevkAdresi.adres1,
                SevkAdresi2 = sevkAdresi.adres2,
                SevkAdresiIlce = Db.frmKentIlce.Single(x => x.ilceKodu == sevkAdresi.ilceKod).ilceAdi,
                SevkAdresiIl = Db.frmKent.Single(x => x.alanKodu == sevkAdresi.il).ilAd,
                SevkAdresiPostaKodu = sevkAdresi.postaKod,
                SevkAdresiTel = sevkAdresi.tel1,
                SevkAdresiFaks = sevkAdresi.telFaks,

                // Hareketler (Ayrıntılar)
                EvrakHareketleri = fatAyrList,

                // Gösterim Seçenekleri
                GosterimSecenekleri =
                                new InvoiceDetailModel.Settings
                                {
                                    EvrakNotuGosterilsinMi = _settings.EvrakNotlariGosterilsin,
                                },
                ControllerName = CurrentAccountType.Value == AccountType.Supplier ? "Supplier" : "Store"
            };

            model.IlgiliSiparisler = (from fatAyr in Db.fatAyr
                                      where fatAyr.ehID == id
                                      join sip in Db.sip on fatAyr.ehSipID equals sip.eID
                                      select new InvoiceDetailModel.RelatedOrder
                                      {
                                          Id = sip.eID,
                                          Tarih = sip.eTarih,
                                          EvrakNo = sip.eNo,
                                      })
                    .Distinct()
                    .ToList();



            model.IlgiliIrsaliyeler = (from fatAyr in Db.fatAyr
                                       where fatAyr.ehID == id
                                       join irs in Db.irs on fatAyr.ehIrsID equals irs.eID
                                       select new InvoiceDetailModel.RelatedDelivery
                                       {
                                           Id = irs.eID,
                                           Tarih = irs.eTarihS,
                                           EvrakNo = irs.eNo
                                       })
                        .Distinct()
                        .ToList();




            return View(model);
        }


        [B2BAuthorize(AccountType.Supplier | AccountType.Customer, Privilege.FATURA)]
        public ActionResult InvoiceEArsivDetail(int? id)
        {
            var donenHtml = "";
            var donenOrjHtml = "";


            string[] donenArray = new string[] { "", "" };

            Check.NotNullOr404(id);

            var fat = Db.fat.SingleOrDefault(x => x.eFatArsvID == id);
            Check.NotNullOr404(fat);

            var frm = fat.frm;
            // Başka firmalara ait kayıt gösterilmemeli
            Check.CheckOr404(frm.frmID == CurrentAccount.frm.frmID);

            //  return "<table><tr><td>döndü</td></tr></table>";
            var ozgunIDListe = Db.efatArsv.FirstOrDefault(x => x.efatArsvID == id);
            if (ozgunIDListe != null)
            {
                //var firma = Convert.ToByte(0);
                var firma = Convert.ToByte(Db.drn2ayar.FirstOrDefault(x => x.ayarID == 78).ayarDeger);
                //var sirketKod = "VATANMARKE";
                var sirketKod = Db.drn2ayar.FirstOrDefault(x => x.ayarID == 76).ayarDeger;
                //var kullanıcı = "wsadminuser";
                var kullanıcı = Db.drn2ayar.FirstOrDefault(x => x.ayarID == 75).ayarDeger;
                //var parola = "w3J+bP#l";
                var parola = Db.drn2ayar.FirstOrDefault(x => x.ayarID == 77).ayarDeger;

                var wsClient = new IntegrationServiceSoapClient();
                var ticket = wsClient.GetFormsAuthenticationTicket(sirketKod, kullanıcı, parola);

                if (ticket == "")
                {
                    donenArray[0] = "Hatalı kullanıcı adı ve parola!";
                    donenArray[1] = "Hatalı kullanıcı adı ve parola!";
                    return Json(donenArray, JsonRequestBehavior.AllowGet);
                }

                //var sonuc = wsClient.GetEArchiveInvoice(ticket, "72DF918A-9E68-4687-8C53-1FDF17AB15FB", "UUID", "UBL");
                var sonuc = wsClient.GetEArchiveInvoice(ticket, ozgunIDListe.efatArsvOzgunID, "UUID", "UBL");
                if (sonuc.ErrorCode != 0)
                {
                    donenArray[0] = sonuc.ServiceResultDescription.ToString();
                    donenArray[1] = "";
                    return Json(donenArray, JsonRequestBehavior.AllowGet);
                }

                if (!sonuc.ReturnValue.Any())
                {
                    donenArray[0] = "İçerik bulunamadı!";
                    donenArray[1] = "";
                    return Json(donenArray, JsonRequestBehavior.AllowGet);
                }

                var doc = XDocument.Parse(Encoding.UTF8.GetString(sonuc.ReturnValue));

                donenHtml = doc.FaturaHtmlView();
                donenOrjHtml = donenHtml;
                string geciciDonen = "";
                if (donenHtml.IndexOf("<style") > -1 && donenHtml.IndexOf("</style>") > -1)
                {
                    geciciDonen = donenHtml.Substring(0, donenHtml.IndexOf("<style"));
                    donenHtml = donenHtml.Substring(donenHtml.IndexOf("<style"), donenHtml.Length - donenHtml.IndexOf("<style"));
                    if (donenHtml.IndexOf("</style>") > -1)
                    {
                        donenHtml = donenHtml.Substring(donenHtml.IndexOf("</style>") + 8, donenHtml.Length - donenHtml.IndexOf("</style>") - 8);
                        donenHtml = geciciDonen + donenHtml;
                    }
                }
                donenArray[0] = donenHtml;
                donenArray[1] = donenOrjHtml;
                return Json(donenArray, JsonRequestBehavior.AllowGet);
            }
            donenArray[0] = "Ayrıntı bulunamadı!";
            donenArray[1] = "";
            return Json(donenArray, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InvoiceDetailExportClientsListToExcel(int fatIdsi)
        {
            var grid = new System.Web.UI.WebControls.GridView();

            grid.DataSource = (from x in Db.fatAyr
                               where x.ehID == fatIdsi
                               let barkod = Db.urnBarkod_vw.FirstOrDefault(y => y.urnBrkdStkID == x.ehStkID)
                               select new //OrderDetailModel.OrderDetail
                               {
                                   // Id = x.ehID,
                                   // Sira = x.ehSira,
                                   UrunId = x.urn.stkID,
                                   UrunIsmi = x.urn.stkAd,
                                   UrunKod = x.urn.stkKod,
                                   UrunBarkod = barkod != null ? barkod.barkod : "",
                                   KoliAdet = Math.Floor(x.ehAdet / x.urn.urnKoli),
                                   KoliIciAdet = x.urn.urnKoli,
                                   ToplamAdet = x.ehAdet,
                                   // PaletIciAdet = x.urn.urnPalet,
                                   //UrunAgirligi = x.urn.paketOlcu,
                                   //AgirlikBirim = x.urn.paketBrm,
                                   KalanAdet = x.ehAdetN,
                                   BirimFiyat = x.ehTutar / x.ehAdet,
                                   KDVHaricToplam = x.ehTutar,
                                   IndirimTutari = x.ehIndirim,
                                   KDVHaricNet = x.ehTutar - x.ehIndirim,

                                   // Durum = (OrderDetailModel.OrderDetailStatus)x.ehDurum,
                                   // Indirim1 = x.ehi1,
                                   // Indirim2 = x.ehi2,
                                   // Indirim3 = x.ehi3,
                                   // Indirim4 = x.ehi4,
                                   // Indirim5 = x.ehi5,

                                   // KDVYuzde = Db.urnKDV.Single(y => y.kdvID == x.ehKDV).kdvYuzde,
                                   // KDVTutar = x.ehTutarKDV,
                               }).ToList();

            grid.DataBind();

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=Exported_InvoiceDetail.xls");
            Response.ContentType = "application/excel";
            StringWriter sw = new StringWriter();
            Encoding.GetEncoding(1254).GetBytes(sw.ToString());
            Response.Charset = "";
            Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1254");
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            grid.RenderControl(htw);

            Response.Write(sw.ToString());

            Response.End();

            return null;
        }

        public ActionResult ActivitiesExportClientsListToExcel(string _dateFilterBegin, string _dateFilterEnd)
        {

            if (_dateFilterBegin != null)
            {
                DateTime dateBegin = Convert.ToDateTime(_dateFilterBegin);
                DateTime dateEnd = Convert.ToDateTime(_dateFilterEnd);

                var activities =
                 (
                  from car in Db.car
                  join carTipF in Db.carTipF on car.cFatTip equals carTipF.ctfID
                  join frm in Db.frm on car.cKod equals frm.frmID
                  where frm.frmID == CurrentAccount.frm.frmID
                  where (car.cTarih >= dateBegin) && (car.cTarih <= dateEnd)
                  orderby car.cTarih
                  let fat = Db.fat.FirstOrDefault(x => x.eID == car.cFatID)
                  select new AccountActivitiesModel.AccountActivity()
                  {
                      Id = car.cID,
                      IlgiliFaturaId = fat != null ? fat.eID : (int?)null,
                      IlgiliFaturaNo = fat != null ? fat.eNo : "",
                      Tarih = car.cTarih,
                      HareketTipi = carTipF.ctfAd,
                      EvrakNo = car.cEvrakNo,
                      VadeTarihi = car.cTarihVade,
                      Borc = (car.cBA == 1 ? (decimal?)Math.Abs(car.cTutar) : null),
                      Alacak = (car.cBA == 0 ? (decimal?)Math.Abs(car.cTutar) : null),
                  }
                ).ToList();

                var previousDebit = Db.car
                                .Where(x => x.cKod == CurrentAccount.frm.frmID)
                                .Where(x => x.cTarih < dateBegin)
                                .Sum(x => ((decimal?)x.cTutar)) ?? 0;

                activities.Aggregate(previousDebit,
                                     (balance, activity) =>
                                     {
                                         if (activity.Alacak.HasValue)
                                         {
                                             balance += activity.Alacak.Value;
                                         }
                                         if (activity.Borc.HasValue)
                                         {
                                             balance -= activity.Borc.Value;
                                         }

                                         activity.Bakiye = balance;
                                         return balance;
                                     });



                var grid = new System.Web.UI.WebControls.GridView();

                grid.DataSource = activities;

                grid.DataBind();

                Response.ClearContent();
                Response.AddHeader("content-disposition", "attachment; filename=Exported_Activities.xls");
                Response.ContentType = "application/excel";
                StringWriter sw = new StringWriter();
                Encoding.GetEncoding(1254).GetBytes(sw.ToString());
                Response.Charset = "";
                Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1254");
                HtmlTextWriter htw = new HtmlTextWriter(sw);

                grid.RenderControl(htw);

                Response.Write(sw.ToString());

                Response.End();


            }
            return null;
        }



        #endregion

        #region Raporlar

        #region Mağaza Bazlı Satış Raporu
        //public ActionResult SalesPerStoreReport(DocumentDateFilterModel dateFilter, string cityFilter = "", string groupFilter = "", string statueFilter = "", string categoryFilter = "", string brandFilter = "", string storeCount = "", string productCount = "")
        //{

        //    dateFilter.NotNullOrSetDefault(DateHelper.LastMonthBegin, DateHelper.LastMonthEnd);

        //    var stores = (from irsAyr in Db.irsAyr
        //                  join irs in Db.irs on irsAyr.ehID equals irs.eID
        //                  join urn in Db.urn on irsAyr.ehStkID equals urn.stkID
        //                  join posMagaza in Db.posMagaza on irs.eMekan equals posMagaza.mekanID
        //                  where irs.eTarih >= dateFilter.Begin && irs.eTarih <= dateFilter.End
        //                  where product.Contains(irsAyr.ehStkID)
        //                  where saleTypes.Contains(irs.eTip)
        //                  select new { irs, irsAyr, urn, posMagaza });

        //    ViewData["storeCount"] = storeCount;
        //    ViewData["productCount"] = productCount;
        //    ViewData["group"] = groupFilter;
        //    ViewData["city"] = cityFilter;
        //    ViewData["brand"] = brandFilter;
        //    ViewData["category"] = categoryFilter;
        //    ViewData["statue"] = statueFilter;

        //    var category = new List<int>();
        //    var brand = new List<int>();
        //    var city = new List<int>();
        //    var groups = new List<int>();
        //    var status = new List<int>();
        //    var statusList = new List<string>();
        //    string categoryName = string.Empty, brandName = string.Empty,
        //    cityName = string.Empty, groupName = string.Empty, statusName = string.Empty;

        //    if (categoryFilter != "")
        //    {
        //        category.AddRange(categoryFilter.Split(',').Select(item => Convert.ToInt32(item)));

        //        categoryName = string.Join(",", Db.urnKtgrAg.Where(x => category.Contains(x.kID)).Select(x => x.kAd));

        //        stores = (from store in stores
        //                  join urnKtgrAg in Db.urnKtgrAg on store.urn.urnKtgrID equals urnKtgrAg.k0
        //                  where urnKtgrAg.k1 == 0
        //                  where category.Contains(urnKtgrAg.kID)
        //                  select store);
        //        //stores = stores.Where(x => x.urnKtgrAg.k1 == 0 && category.Contains(x.urnKtgrAg.kID));
        //    }
        //    if (brandFilter != "")
        //    {

        //        brand.AddRange(brandFilter.Split(',').Select(item => Convert.ToInt32(item)));
        //        brandName = string.Join(",", Db.urnMrk.Where(x => brand.Contains(x.mrkID)).Select(x => x.mrkAd));

        //        stores = stores.Where(x => brand.Contains(x.urn.urnMrkID));
        //    }
        //    if (cityFilter != "")
        //    {

        //        city.AddRange(cityFilter.Split(',').Select(item => Convert.ToInt32(item)));
        //        cityName = string.Join(",", Db.frmKent.Where(x => city.Contains(x.alanKodu)).Select(x => x.ilAd));

        //        stores = from store in stores
        //                 join frm in Db.frm on store.irs.eMekan equals frm.frmID
        //                 where city.Contains(frm.il)
        //                 select store;
        //        //stores = stores.Where(x => city.Contains(x.frm.il));
        //    }
        //    if (groupFilter != "")
        //    {
        //        groups.AddRange(groupFilter.Split(',').Select(item => Convert.ToInt32(item)));
        //        groupName = string.Join(",", Db.posMagazaGrup.Where(x => groups.Contains(x.mekanGrupID)).Select(x => x.mekanGrupAd));
        //        stores = stores.Where(x => groups.Contains(x.posMagaza.mekanGrup));

        //    }
        //    if (statueFilter != "")
        //    {
        //        status.AddRange(statueFilter.Split(',').Select(item => Convert.ToInt32(item)));
        //        if (status.Contains(0)) { statusList.Add("Açık"); }
        //        if (status.Contains(1)) { statusList.Add("Kapalı"); }
        //        if (status.Contains(2)) { statusList.Add("Açılmamış"); }

        //        statusName = string.Join(",", statusList);

        //        if (status.Count == 1)
        //        {
        //            stores = !status.Contains(2) ?
        //                (status.Contains(0) ? stores.Where(x => x.posMagaza.mekanDurum == 0)
        //                : stores.Where(x => x.posMagaza.mekanDurum != 0))
        //                : stores.Where(x => x.posMagaza.mekanDurum == 0 && x.posMagaza.mekanAcilisTarih > DateTime.Now);
        //        }
        //        if (status.Count == 2)
        //        {
        //            if (!status.Contains(1))
        //            {
        //                stores = stores.Where(x => x.posMagaza.mekanDurum == 0);
        //            }
        //            if (!status.Contains(0))
        //            {
        //                stores = stores.Where(x => (x.posMagaza.mekanDurum == 0
        //                               && x.posMagaza.mekanAcilisTarih > DateTime.Now)
        //                               || x.posMagaza.mekanDurum != 0);
        //            }
        //        }
        //    }
        //    var model = new SalesPerStoreModel
        //    {
        //        Reports = from store in stores
        //                  group store.irsAyr by new { store.irs.eMekan } into g
        //                  select new SalesPerStoreModel.SalesPerStore
        //                  {
        //                      StoreName = Db.posMagaza.FirstOrDefault(x => x.mekanID == g.Key.eMekan).mekanAd,

        //                      TotalSaleCount = g.Count(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
        //         (-1) * g.Where(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehAdet) : 0,

        //                      RefundofSaleCount = g.Count(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
        //   g.Where(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehAdet) : 0,


        //                      TotalSale = g.Count(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
        //g.Where(x => totalSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehTutar - x.ehIndirim + x.ehTutarKDV) : 0,

        //                      RefundofSale = g.Count(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)) > 0 ?
        //g.Where(x => refundofSale.Contains(Db.irs.FirstOrDefault(y => y.eID == x.ehID).eTip)).Sum(x => x.ehTutar - x.ehIndirim + x.ehTutarKDV) : 0
        //                  },
        //        DateFilter = dateFilter,
        //        Categories = categoryName,
        //        Brands = brandName,
        //        ProductCount = productCount,
        //        Cities = cityName,
        //        Groups = groupName,
        //        Status = statusName,
        //        StoreCount = storeCount
        //    };

        //    return View(model);
        //}
        #endregion


        #region Cari hesap ektresi Raporu
        public ActionResult CurrentAccountExtractReport(DocumentDateFilterModel dateFilter, int? page)
        {
            var pageLength = 29;

            dateFilter.NotNullOrSetDefault(DateHelper.ThisMonthBegin, DateTime.Today);

            string sorgu = string.Format(@" exec [b2b].[sp_currentAccountExtract_Vw] @frmID={0},@page={1},@pageLength={2},@basTarih='{3}',@bitTarih='{4}' ", CurrentCompany.frmID, page ?? 1, pageLength, dateFilter.Begin.Value.Date.ToString("dd.MM.yyyy"), dateFilter.End.Value.Date.ToString("dd.MM.yyyy"));
            var AyrList = Db.Database.SqlQuery<CurrentAccountExtractModel.CurrentAccountExtract>(sorgu).ToList();

            var model = new CurrentAccountExtractModel
            {
                DateFilter = dateFilter,
                Page = page ?? 1,
                PageLength = pageLength,
                CurrentAccountExtracts = AyrList.ToSqlPagedList(page ?? 1, pageLength, AyrList.Count > 0 ? (AyrList.FirstOrDefault().topSay) : 0)
            };

            return View(model);

        }
        #endregion


        #region Sat-Öde ektresi Raporu
        public ActionResult SellAndPayExtractReport(DocumentDateFilterModel dateFilter, int? page)
        {
            var pageLength = 29;

            dateFilter.NotNullOrSetDefault(DateHelper.ThisMonthBegin, DateTime.Today);

            string sorgu = string.Format(@" exec [b2b].[sp_sellAndPayExtract_Vw] @frmID={0},@page={1},@pageLength={2},@basTarih='{3}',@bitTarih='{4}' ", CurrentCompany.frmID, page ?? 1, pageLength, dateFilter.Begin.Value.Date.ToString("dd.MM.yyyy"), dateFilter.End.Value.Date.ToString("dd.MM.yyyy"));
            var AyrList = Db.Database.SqlQuery<CurrentAccountExtractModel.CurrentAccountExtract>(sorgu).ToList();


            decimal toplamBORC = 0;
            decimal toplamALACAK = 0;
            var topUrun = AyrList.Count > 0 ? (AyrList.FirstOrDefault().topSay) : 0;
            int topSayfa = (topUrun / pageLength) + (topUrun % pageLength > 0 ? 1 : 0);
            if (topSayfa == (page ?? 1))
            {
                string sorguTop = string.Format(@" exec [b2b].[sp_sellAndPayExtract_Vw] @frmID={0},@page=0,@pageLength=0,@basTarih='{1}',@bitTarih='{2}' ", CurrentCompany.frmID, dateFilter.Begin.Value.Date.ToString("dd.MM.yyyy"), dateFilter.End.Value.Date.ToString("dd.MM.yyyy"));
                var listeTop = Db.Database.SqlQuery<CurrentAccountExtractModel.CurrentAccountExtract>(sorguTop).ToList();
                if (listeTop.Any())
                {
                    toplamBORC = listeTop.Sum(x => x.BORC);
                    toplamALACAK = listeTop.Sum(x => x.ALACAK);
                }
            }

            var model = new CurrentAccountExtractModel
            {
                DateFilter = dateFilter,
                Page = page ?? 1,
                PageLength = pageLength,
                topBORC = toplamBORC,
                topALACAK = toplamALACAK,
                CurrentAccountExtracts = AyrList.ToSqlPagedList(page ?? 1, pageLength, AyrList.Count > 0 ? (AyrList.FirstOrDefault().topSay) : 0),
                mesaj = (dateFilter.Begin.Value.Year + 1 < DateTime.Now.Year) ? "Sat öde ekstresi için son iki seneden öncesi gelmemektedir." : ""
            };

            return View(model);

        }

        public ActionResult SellAndPayExtractReportExport(string ilkTar = "", string sonTar = "")
        {

            string ilkTarKosul = DateTime.Now.Date.AddDays(-30).ToString("dd.MM.yyyy");
            string sonTarKosul = DateTime.Now.Date.ToString("dd.MM.yyyy");

            if (ilkTar != "")
            {
                try
                {
                    ilkTarKosul = Convert.ToDateTime(ilkTar).Date.ToString("dd.MM.yyyy");
                }
                catch
                {
                }
            }
            if (sonTar != "")
            {
                try
                {
                    sonTarKosul = Convert.ToDateTime(sonTar).Date.ToString("dd.MM.yyyy");
                }
                catch
                {
                }
            }


            string sorgu = string.Format(@" exec [b2b].[sp_sellAndPayExtract_Vw] @frmID={0},@page=0,@pageLength=0,@basTarih='{1}',@bitTarih='{2}' ", CurrentCompany.frmID, ilkTarKosul, sonTarKosul);
            var liste = Db.Database.SqlQuery<CurrentAccountExtractReportExcel>(sorgu).ToList();

            if (liste.Count() > 0)
            {
                var grid = new System.Web.UI.WebControls.GridView();

                grid.DataSource = liste;

                grid.DataBind();

                Response.ClearContent();
                Response.AddHeader("content-disposition", "attachment; filename=SatOdeEkstresi.xls");
                Response.ContentType = "application/excel";
                StringWriter sw = new StringWriter();
                Encoding.GetEncoding(1254).GetBytes(sw.ToString());
                Response.Charset = "";
                Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1254");
                HtmlTextWriter htw = new HtmlTextWriter(sw);

                grid.RenderControl(htw);

                Response.Write(sw.ToString());

                Response.End();
            }

            return null;
        }

        #endregion






        #endregion



    }
}