﻿namespace DerinSIS.B2B.Emails
{
    using System;
    using System.IO;
    using System.Web;
    using Models;
    using RazorEngine;
    using System.Net.Mail;

    public class Mailer
    {
        private DbEntities _db;

        public Mailer(DbEntities db)
        {
            _db = db;
        }

        public Mailer()
        {
        }

        public bool SendResetPasswordMail(ResetPasswordEmailModel model)
        {
            try
            {
                var body = MessageBodyFromTemplate("ResetPassword", model);
                var settings = Properties.Settings.Default;
                if (settings.Email_From == "")
                    return false;

                MailMessage mail = new MailMessage(settings.Email_From, settings.Email_To);
                mail.BodyEncoding = System.Text.Encoding.UTF8;
                mail.IsBodyHtml = true;
                mail.Subject = "Şifre sıfırlama isteği";
                mail.Body = body;
                using (var client = new SmtpClient())
                {
                    client.Port = settings.Email_Port;
                    client.EnableSsl = settings.Email_Ssl;
                    client.UseDefaultCredentials = false;
                    client.Timeout = 10000;
                    client.Credentials = new System.Net.NetworkCredential(settings.Email_User, settings.Email_Password);
                    client.Host = settings.Email_Smtp;

                    client.Send(mail);
                }

            }
            catch (Exception) { return false; }


            return true;
        }

        private static string MessageBodyFromTemplate(string templateName, dynamic model)
        {
            var templatePath = Path.Combine("~\\Emails\\Templates", templateName + ".cshtml");
            var absoluteTemplatePath = HttpContext.Current.Server.MapPath(templatePath);
            var templateContent = File.OpenText(absoluteTemplatePath).ReadToEnd();
            return Razor.Parse(templateContent, model);
        }
    }
}