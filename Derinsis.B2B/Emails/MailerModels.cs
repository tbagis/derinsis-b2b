﻿namespace DerinSIS.B2B.Emails
{
    using System;

    public class ResetPasswordEmailModel
    {
        public string KullaniciIsmi { get; set; }
        public string FirmaAdi { get; set; }
        public string FirmaKodu { get; set; }
        public string Eposta { get; set; }
        public string ResetUrl { get; set; }
    }
}