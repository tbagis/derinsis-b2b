﻿


$(document).ready(function () {

    function addItemsToActiveCart(items) {
        var data = {};
        for (var i in items) {
            var item = items[i];

            var productIdKey = "Items[" + i + "].ProductId";
            var productQuantityKey = "Items[" + i + "].Quantity";

            data[productIdKey] = item.productId;
            data[productQuantityKey] = item.quantity;
        }


        $.ajax({
            type: 'POST',
            url: $('#add-to-cart-url').val(),
            data: data,
            success: function (response) {
                if (response) {
                    // Success
                    var urunCesidi = response.UrunCesidi;
                    var toplamFiyat = response.ToplamFiyat;
                    var sonIkiUrun = response.SonIkiUrun;

                    $('.cart-urun-cesidi').html(urunCesidi);
                    $('.cart-kdv-dahil-toplam').html(toplamFiyat);
                    $('input[name=cart-urun-cesidi]').val(urunCesidi);
                    $('input[name=cart-kdv-dahil-toplam]').val(toplamFiyat);

                    var sonUrun = sonIkiUrun[0];
                    $('input[name=last-product]').val(sonUrun);
                    $.jGrowl(sonUrun + " sepete eklenmiştir.", { sticky: false });

                    if (sonIkiUrun.length > 1) {
                        var previousProduct = sonIkiUrun[1];
                        $('input[name=previous-product]').val(previousProduct);
                    }
                }
            }
        });
    }

    /* Sepete Ekle Butonu */
    $('.add-to-cart').live('click', function () {
        var button = $(this);
        var form = button.closest('form');
        var quantityField = form.find('input[name=quantity]');
        var quantity = quantityField.val();
        var productId = form.find('input[name=productId]').val();

        var _stkStokKontroluField = form.find('input[name=stokKontrolu]');
        var _stkStokKontrolu = _stkStokKontroluField.val();
        var _stkStokField = form.find('input[name=stkStok]');
        var _stkStok = _stkStokField.val();
     
        if (quantity === "" || isNaN(quantity) ||quantity==="0") {
            quantityField
            .stop()
            .css('background', 'inherit')
            .effect('highlight', { color: 'maroon' }, 2000);
            return;
        }

        var yeniDeger = _stkStok - quantity;
        if (_stkStokKontrolu >0) {
            if (yeniDeger<0) {
                quantityField.val(_stkStok);
                quantityField
               .stop()
               .css('background', 'inherit')
               .effect('highlight', { color: 'red' }, 3000);
                return;
            }
            _stkStokField.val(yeniDeger);
        }

        quantityField
            .stop()
            .css('background', 'inherit')
            .effect('highlight', { color: 'green' }, 2000);
        quantity = parseInt(quantity);
        quantityField.val("");

        addItemsToActiveCart([{
            "productId": productId,
            "quantity": quantity
        }]);
    });


    /* Adet kutularına rakamdan başka sayı girilemez. */
    $('.add-to-cart-form input[name="quantity"]').livequery(function () {
        $(this).jStepper({ minValue: 1 });
    });

    $('.cart-item-quantity input[name="NewQuantity"]').livequery(function () {
        $(this).jStepper({ minValue: 1 });
    });


    function getCartTooltipTitle() {
        var cartName = $('input[name=active-cart-name]').val();
        var totalAmount = $('input[name=cart-kdv-dahil-toplam]').val();
        return cartName + " <div class='pull-right'>" + totalAmount + "</div>";
    }

    function getCartTooltipContent() {
        var distinctProductCount = parseInt($('input[name=cart-urun-cesidi]').val());
        var lastItem = $('input[name=last-product]').val();
        var previousItem = $('input[name=previous-product]').val();

        var content = "";
        if (distinctProductCount > 0) {
            content += "<h6>TOPLAM " + distinctProductCount + " ÜRÜN ÇEŞİDİ MEVCUT</h6><br />";

            if (distinctProductCount >= 2) {
                content += "<h5>Son Eklenen 2 Ürün:</h5>";
            }
            else if (distinctProductCount == 1) {
                content += "<h5>Son Eklenen Ürün:</h5>";
            }

            content += "<ul>";
            if (lastItem) {
                content += "<li>" + lastItem + "</li>";
            }
            if (previousItem) {
                content += "<li>" + previousItem + "</li>";
            }
            content += "</ul>";
        }
        else {
            content += "<h6>ÜRÜN BULUNMAMAKTADIR</h6><br />";
        }
        return content;
    }

    $('.cartelement').popover({
        html: true,
        trigger: "hover",
        title: function () { return getCartTooltipTitle(); },
        content: function () { return getCartTooltipContent(); }
    });
});