﻿

$(document).ready(function () {
    $('a[data-page]').click(function () {
        var t = $(this);

        // find page form on the page
        var pagerForm = $('form.pager-form');
        var pagerInput = pagerForm.find('input[name=page]');

        var destinationPage = t.data('page');
        pagerInput.val(destinationPage);

        pagerForm.submit();
        return false;
    });
});

