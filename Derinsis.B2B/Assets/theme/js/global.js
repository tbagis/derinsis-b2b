﻿// put form submit buttons into the loading state after submit

$(document).ready(function () {

    $('form').live('submit', function () {
        $(this).find('[type=submit]').button('loading');
    });

    // activate carausels
    $('.carousel').carousel();

    // activate tooltips
    $('[rel=tooltip]').livequery(function () {
        $(this).tooltip();
    });

    $('[rel=popover]').livequery(function () {
        $(this).popover();
    });

    $('select[data-dropdown]').each(function () {
        var t = $(this);
        var href = t.data('dropdown');
        var initialValue = t.data('dropdown-initial');
        var dependsOn = t.data('dropdown-depends-on');
        if (dependsOn) {
            t.CascadingDropDown('#' + dependsOn, href, {
                promptText: '-- Lütfen Seçiniz --',
                loadingText: 'Yükleniyor...',

                onLoaded: function () {
                    t.find('option').each(function () {
                        if (parseInt(this.value) === initialValue) {
                            t.val(this.value);
                            var name = t.attr('name');
                            var tAlt = $('select[data-dropdown-depends-on=' + name + ']');
                            var href = $('select[data-dropdown-depends-on=' + name + ']').data('dropdown');
                            var initialValueAlt = $('select[data-dropdown-depends-on=' + name + ']').data('dropdown-initial');

                            param = name[0].toLowerCase() + name.substring(1, name.length);
                            data = param + '=' + initialValue;

                            $.ajax({
                                type: "post",
                                url: href,
                                data: data,
                                success: function (response) {
                                    for (var i in response) {
                                        var data = response[i];
                                        var option = $("<option />")
                                            .attr('value', data.Value)
                                            .html(data.Text);
                                        if (parseInt(data.Value) === initialValueAlt) {
                                            option.attr('selected', 'selected');
                                            tAlt.removeAttr('disabled');
                                        }
                                        option.appendTo(tAlt);
                                    }
                                    tAlt.change();
                                },
                                error: function (xhr, textStatus, error) {
                                    alert(xhr.statusText);
                                    alert(textStatus);
                                    alert(error);
                                }
                            });
                        }
                    });
                }
            });
        } else {
            $.post(href, function (response) {
                for (var i in response) {
                    var data = response[i];
                    var option = $("<option />")
                       .attr('value', data.Value)
                       .html(data.Text);
                    if (parseInt(data.Value) === initialValue) {
                        option.attr('selected', 'selected');

                    }
                    option.appendTo(t);
                }
                t.change();
            });
        }
    });


});
