//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace DerinSIS.B2B.Models
{
    public partial class urnBilgiTnm
    {
        public urnBilgiTnm()
        {
            this.urnBilgi = new HashSet<urnBilgi>();
        }
    
        public byte BilgiID { get; set; }
        public string BilgiAd { get; set; }
        public byte BilgiSira { get; set; }
        public byte bilgiKolonTip { get; set; }
        public byte bilgiGrup { get; set; }
        public byte bilgiZorunlu { get; set; }
        public string bilgiOndeger { get; set; }
    
        public virtual ICollection<urnBilgi> urnBilgi { get; set; }
    }
    
}
