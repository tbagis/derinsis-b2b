﻿
namespace DerinSIS.B2B.Models
{
    using System;
    using System.Security.Cryptography;
    using System.Text;

    public partial class frmYetkili
    {
        private static readonly SHA512Managed Provider = new SHA512Managed();

        public bool CheckPassword(string password)
        {
            var encryptedBytes = Provider.ComputeHash(Encoding.Unicode.GetBytes(password));
            var encryptedPassword = Convert.ToBase64String(encryptedBytes);

            return encryptedPassword == ytkSifre;
        }

        public void SetPassword(string newPassword)
        {
            var encryptedBytes = Provider.ComputeHash(Encoding.Unicode.GetBytes(newPassword));
            var encryptedPassword = Convert.ToBase64String(encryptedBytes);

            ytkSifre = encryptedPassword;
        }
    }
}