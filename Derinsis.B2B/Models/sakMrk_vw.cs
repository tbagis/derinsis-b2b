//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace DerinSIS.B2B.Models
{
    public partial class sakMrk_vw
    {
        public byte frmTip { get; set; }
        public int Gruptan { get; set; }
        public Nullable<int> skFrmGrpID { get; set; }
        public int skFrmID { get; set; }
        public int skID { get; set; }
        public short skMrkID { get; set; }
        public byte skTur { get; set; }
        public decimal skY1 { get; set; }
        public decimal skY2 { get; set; }
        public decimal skY3 { get; set; }
        public Nullable<System.DateTime> skTrhBas { get; set; }
        public Nullable<System.DateTime> skTrhBit { get; set; }
        public short skVade { get; set; }
    }
    
}
