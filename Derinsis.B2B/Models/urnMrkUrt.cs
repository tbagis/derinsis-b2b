//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace DerinSIS.B2B.Models
{
    public partial class urnMrkUrt
    {
        public urnMrkUrt()
        {
            this.urnMrk = new HashSet<urnMrk>();
        }
    
        public int urtID { get; set; }
        public string urtAd { get; set; }
        public string urtKAdKisa { get; set; }
    
        public virtual ICollection<urnMrk> urnMrk { get; set; }
    }
    
}
