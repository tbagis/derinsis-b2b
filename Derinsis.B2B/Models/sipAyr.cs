//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace DerinSIS.B2B.Models
{
    public partial class sipAyr
    {
        public int ehID { get; set; }
        public int ehSira { get; set; }
        public int ehStkID { get; set; }
        public decimal ehAdet { get; set; }
        public decimal ehAdetN { get; set; }
        public decimal ehTutar { get; set; }
        public decimal ehIndirim { get; set; }
        public byte ehKDV { get; set; }
        public decimal ehi1 { get; set; }
        public decimal ehi2 { get; set; }
        public decimal ehi3 { get; set; }
        public decimal ehi4 { get; set; }
        public decimal ehi5 { get; set; }
        public decimal ehiT6 { get; set; }
        public byte ehDurum { get; set; }
        public decimal ehTutarKDV { get; set; }
        public string ehBirim { get; set; }
        public Nullable<decimal> ehSevkAdet { get; set; }
        public decimal ehTutarDvz { get; set; }
        public decimal ehSevkKontrol { get; set; }
        public string ehNot { get; set; }
        public int ehSipBag { get; set; }
        public int ehAltStkID { get; set; }
    
        public virtual sip sip { get; set; }
        public virtual urn urn { get; set; }
    }
    
}
