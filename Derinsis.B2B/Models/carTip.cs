//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace DerinSIS.B2B.Models
{
    public partial class carTip
    {
        public byte ctID { get; set; }
        public string ctAd { get; set; }
        public byte ctSira { get; set; }
        public byte ctfKHzorunlu { get; set; }
        public int ctfKHondeger { get; set; }
        public byte ctOdeme { get; set; }
        public byte ctfENoZorunlu { get; set; }
        public byte ctEdefter { get; set; }
        public byte ctEdefterOdeme { get; set; }
        public byte ctfMagazaKasa { get; set; }
        public byte ctfVirman { get; set; }
    }
    
}
