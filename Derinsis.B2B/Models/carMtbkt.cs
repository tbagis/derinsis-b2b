//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace DerinSIS.B2B.Models
{
    public partial class carMtbkt
    {
        public int mtbktID { get; set; }
        public int mtbktFrmID { get; set; }
        public System.DateTime mtbktBaslangicTarih { get; set; }
        public System.DateTime mtbktBitisTarih { get; set; }
        public int mtbktSatisAdet { get; set; }
        public decimal mtbktSatisTutar { get; set; }
        public int mtbktAlisAdet { get; set; }
        public decimal mtbktAlisTutar { get; set; }
        public decimal mtbktBakiye { get; set; }
        public System.DateTime gTarih { get; set; }
        public int gKisi { get; set; }
        public Nullable<int> mtbktAdet { get; set; }
        public Nullable<decimal> mtbktTutar { get; set; }
    
        public virtual drn1 drn1 { get; set; }
        public virtual frm frm { get; set; }
    }
    
}
