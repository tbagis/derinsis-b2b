//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace DerinSIS.B2B.Models
{
    public partial class frm
    {
        public frm()
        {
            this.frmBnk = new HashSet<frmBnk>();
            this.posOdeme = new HashSet<posOdeme>();
            this.sip = new HashSet<sip>();
            this.urn = new HashSet<urn>();
            this.urnFrm = new HashSet<urnFrm>();
            this.carMtbkt = new HashSet<carMtbkt>();
            this.sakMrk = new HashSet<sakMrk>();
            this.fat = new HashSet<fat>();
            this.car = new HashSet<car>();
            this.car1 = new HashSet<car>();
            this.car2 = new HashSet<car>();
            this.irs = new HashSet<irs>();
            this.frmYetkili = new HashSet<frmYetkili>();
        }
    
        public int frmID { get; set; }
        public string frmKod { get; set; }
        public string frmAd { get; set; }
        public string frmAd2 { get; set; }
        public byte frmTip { get; set; }
        public byte frmDurum { get; set; }
        public string adres1 { get; set; }
        public string adres2 { get; set; }
        public string postaKod { get; set; }
        public string ilce { get; set; }
        public short il { get; set; }
        public string VD { get; set; }
        public string VN { get; set; }
        public string tel1 { get; set; }
        public string tel2 { get; set; }
        public string telMobil { get; set; }
        public string telFaks { get; set; }
        public short frmGrup1 { get; set; }
        public short frmGrup2 { get; set; }
        public short frmOdmYontem { get; set; }
        public string frmKartNo { get; set; }
        public short frmHesapTur { get; set; }
        public byte frmFiyatTur { get; set; }
        public string frmNotlar { get; set; }
        public short frmVade { get; set; }
        public System.DateTime frmCariTarih { get; set; }
        public short frmVade1 { get; set; }
        public short frmVade2 { get; set; }
        public byte frmVadeTur { get; set; }
        public int frmSorumlu { get; set; }
        public decimal frmBakiyeLimit { get; set; }
        public decimal frmBakiyeLimitCek { get; set; }
        public decimal frmBakiyeLimitCekCiro { get; set; }
        public byte frmSakTur { get; set; }
        public int frmBagID { get; set; }
        public byte etkinSiparis { get; set; }
        public byte etkinIrsaliye { get; set; }
        public byte etkinFatura { get; set; }
        public byte etkinFiyat { get; set; }
        public byte etkinCari { get; set; }
        public byte etkinCek { get; set; }
        public byte frmGrup3 { get; set; }
        public byte frmGrup4 { get; set; }
        public byte frmGrup5 { get; set; }
        public System.DateTime gTarih { get; set; }
        public int gKisi { get; set; }
        public int kKisi { get; set; }
        public System.DateTime kTarih { get; set; }
        public short frmAdat { get; set; }
        public short frmVade3 { get; set; }
        public byte frmSipBildYon { get; set; }
        public byte frmSipDsyTip { get; set; }
        public short frmVade4 { get; set; }
        public short ulke { get; set; }
        public byte frmSipGun1 { get; set; }
        public byte frmSipGun2 { get; set; }
        public byte frmSipGun3 { get; set; }
        public byte frmSipGun4 { get; set; }
        public byte frmSipGun5 { get; set; }
        public byte frmSipGun6 { get; set; }
        public byte frmSipGun7 { get; set; }
        public byte frmTeslimYer { get; set; }
        public byte frmTeslimGun { get; set; }
        public byte frmIadeKural { get; set; }
        public decimal frmKomisyon { get; set; }
        public byte frmSiparisBakiye { get; set; }
        public byte frmSiparisSevkAdet { get; set; }
        public short frmHesapTurAlt { get; set; }
        public byte frmPiyasaMarj { get; set; }
        public byte frmSiparisSapmaOran { get; set; }
        public byte frmSiparisOto { get; set; }
        public decimal frmCiro2009 { get; set; }
        public short frmVade5 { get; set; }
        public byte frmMinTslmYuzde { get; set; }
        public byte frmMinTslmOzlYuzde { get; set; }
        public byte frmMaxTslm { get; set; }
        public byte frmMaxTslmOzl { get; set; }
        public decimal frmEkIndIlkAlim { get; set; }
        public decimal frmEkIndYeniMgz { get; set; }
        public decimal frmEkIndOzl { get; set; }
        public Nullable<System.DateTime> frmCariTarihB2B { get; set; }
        public byte frmBA { get; set; }
        public byte ihracatKDV { get; set; }
        public Nullable<int> ilceKod { get; set; }
        public decimal frmStopaj { get; set; }
        public byte frmKDVDahil { get; set; }
        public byte etkinEFatura { get; set; }
        public byte etkinVGSiparis { get; set; }
        public byte frmDvzID { get; set; }
        public string adres3 { get; set; }
        public string frmMersisNo { get; set; }
        public string frmEposta { get; set; }
        public Nullable<System.DateTime> EFaturaTarih { get; set; }
        public byte frmSirketTip { get; set; }
        public string EfaturaEtiket { get; set; }
        public string frmNACE { get; set; }
        public byte frmKomisyonTip { get; set; }
        public byte frmMekansalSiparis { get; set; }
        public byte frmDonusumOto { get; set; }
        public byte EfaturaAlanUBL { get; set; }
        public byte EfaturaAlanERP { get; set; }
        public byte EArsivYontem { get; set; }
        public byte etkinEIrsaliye { get; set; }
        public Nullable<System.DateTime> EIrsaliyeTarih { get; set; }
    
        public virtual frmUlke frmUlke { get; set; }
        public virtual ICollection<frmBnk> frmBnk { get; set; }
        public virtual ICollection<posOdeme> posOdeme { get; set; }
        public virtual frmKent frmKent { get; set; }
        public virtual ICollection<sip> sip { get; set; }
        public virtual ICollection<urn> urn { get; set; }
        public virtual ICollection<urnFrm> urnFrm { get; set; }
        public virtual ICollection<carMtbkt> carMtbkt { get; set; }
        public virtual ICollection<sakMrk> sakMrk { get; set; }
        public virtual ICollection<fat> fat { get; set; }
        public virtual ICollection<car> car { get; set; }
        public virtual ICollection<car> car1 { get; set; }
        public virtual ICollection<car> car2 { get; set; }
        public virtual ICollection<irs> irs { get; set; }
        public virtual ICollection<frmYetkili> frmYetkili { get; set; }
    }
    
}
