﻿namespace DerinSIS.B2B.Helpers.Html
{
    using System.Web.Mvc;
    using System.Web.Mvc.Html;

    public static class MenuExtensions
    {
        public static MvcHtmlString MainMenuLink(this HtmlHelper htmlHelper, string linkText, string actionName, string controllerName, string realController="")
        {
            var currentController = htmlHelper.ViewContext.RouteData.GetRequiredString("controller");
            if (realController!="")
            {
                currentController = realController;
            }

            var link = htmlHelper.ActionLink(linkText, actionName, controllerName);

            var menuTag = "";
            if (controllerName.ToLowerInvariant() == currentController.ToLowerInvariant())
            {
                menuTag += "<li class=\"active\">";
            }
            else
            {
                menuTag += "<li>";
            }
            menuTag += link;
            menuTag += "</li>";

            return new MvcHtmlString(menuTag); ;
        }

        public static MvcHtmlString SidebarLink(this UrlHelper urlHelper, string linkText, string actionName, string controllerName, string iconName)
        {
            string currentAction = urlHelper.RequestContext.RouteData.GetRequiredString("action");
            string currentController = urlHelper.RequestContext.RouteData.GetRequiredString("controller");

            var iconTag = "";
            if (!string.IsNullOrWhiteSpace(iconName))
            {
                iconTag = "<i class=\"icon-" + iconName + "\"></i> ";
            }


            var link = "<a href=\" " + urlHelper.Action(actionName, controllerName) + "\">" + iconTag + linkText + "</a>";

            var menuTag = "";
            if (actionName == currentAction && controllerName == currentController)
            {
                menuTag += "<li class=\"active\">";
            }
            else
            {
                menuTag += "<li>";
            }
            menuTag += link;
            menuTag += "</li>";

            return new MvcHtmlString(menuTag);
        }
    }
}