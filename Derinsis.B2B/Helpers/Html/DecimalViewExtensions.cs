﻿namespace DerinSIS.B2B.Helpers.Html
{
    using System;

    public static class DecimalViewExtensions
    {
        public static string ToMoneyFormat(this decimal money, bool showNegative = false)
        {
            if (!showNegative)
            {
                return Math.Abs(money).ToString("N");
            }

            return money.ToString("N");
        }

        public static string ToQuantityFormat(this decimal d)
        {
            // Always display abs value for the quantities
            return Math.Abs(d).ToString("#,##0.###");
        }

        public static string ToStokFormat(this decimal d)
        {
            return string.Format("{0:0.00}", d);
        }
        public static string ToConvertibleDecimalStokFormat(this string str)
        {
            if (decimal.TryParse(str.Replace(".", ","), out decimal result))
            {
                return result.ToStokFormat();
            }
            return "";
        }

    }
}