﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Xsl;

namespace DerinSIS.B2B.Helpers.Html
{
    public static class GlobalStatics
    {
        private static XNamespace cac = "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2";
        private static XNamespace cbc = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2";
        public static string FaturaHtmlView(this XDocument doc, bool hata = false)
        {
            var xslDocumentStr = GetXsltForEFatura(doc);
            var xslTransform = new XslCompiledTransform();
            xslTransform.Load(XmlReader.Create(new StringReader(xslDocumentStr)));
            var result = new StringBuilder();
            using (var sw = new StringWriter(result))
            {
                using (var xw = XmlWriter.Create(sw))
                {
                    xslTransform.Transform(doc.CreateReader(), null, xw);
                }
            }

            return result.ToString();
        }
        private static string GetXsltForEFatura(XDocument doc)
        {
            var faturaNo = doc.Root.Element(cbc + "ID").Value;
            var xlstDocElem = doc.Root.Elements()
                .Where(x => x.Name == cac + "AdditionalDocumentReference")
                .Where(x => x.Element(cac + "Attachment") != null)
                .Select(x => x.Element(cac + "Attachment"))
                .Where(x => x.Element(cbc + "EmbeddedDocumentBinaryObject") != null)
                .Select(x => x.Element(cbc + "EmbeddedDocumentBinaryObject"))
                .Where(x => x.Attribute("filename") != null)
                .Where(x => x.Attribute("filename").Value.EndsWith(".xslt"))
                .Where(x => x.Attribute("filename").Value.Contains(faturaNo))
                .FirstOrDefault();

            var embbeddedXsltBytes = Convert.FromBase64String(xlstDocElem.Value);
            var xslTransformtest = new XslCompiledTransform();

            var s1 = Encoding.UTF8.GetString(embbeddedXsltBytes);
            if (Encoding.ASCII.GetBytes(s1[0].ToString())[0].ToString() == "63")
                s1 = s1.Substring(1, s1.Length - 1);
            xslTransformtest.Load(XmlReader.Create(new StringReader(s1)));

            return s1;
        }
    }
}