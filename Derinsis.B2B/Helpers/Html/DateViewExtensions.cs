﻿namespace DerinSIS.B2B.Helpers.Html
{
    using System;
    using System.Globalization;
    using System.Web.Mvc;

    public static class DateViewExtensions
    {
        public static string ToDateString(this DateTime date)
        {
            return date.ToShortDateString();
        }

        public static string ToInvariantDateString(this DateTime date)
        {
            return date.ToString(CultureInfo.InvariantCulture);
        }

        public static string DateRangeToString(this HtmlHelper helper, DateTime begin, DateTime end)
        {
            if (begin == end)
            {
                return begin.ToDateString();
            }

            return string.Format("{0} - {1}", begin.ToDateString(), end.ToDateString());
        }

        public static string DateRangeSingleToString(this HtmlHelper helper, DateTime begin)
        {
            return begin.ToDateString();
        }

        public static bool IsLastDayOfMonth(this DateTime date)
        {
            return date.Month != date.AddDays(1).Month;
        }
    }
}