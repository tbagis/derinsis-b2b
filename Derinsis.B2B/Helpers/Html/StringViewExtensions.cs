﻿namespace DerinSIS.B2B.Helpers.Html
{
    using System;

    public static class StringViewExtensions
    {
        public static string SafeSubString(this string str, int index, int length)
        {
            if (str.Length <= index)
                return "";

            var safeLength = Math.Min(length, str.Length - index);
            return str.Substring(index, safeLength);
        }
        public static string TruncateAtWord(this string value, int length)
        {
            if (value == null || value.Length < length)
                return value;

            return $"{value.Substring(0, length)} ...";
        }
    }
}