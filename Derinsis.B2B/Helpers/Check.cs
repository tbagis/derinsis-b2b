﻿namespace DerinSIS.B2B.Helpers
{
    using System.Web;

    public static class Check
    {
        public static void CheckOr404(bool check)
        {
            if(!check)
            {
                throw new HttpException(404, "Item is null");
            }
        }

        public static void NotNullOr404(string item)
        {
            if(string.IsNullOrWhiteSpace(item))
            {
                throw new HttpException(404, "Item is null");
            }
        }

        public static void NotNullOr404<T>(T item) where T: class 
        {
             if (item == null)
             {
                 throw new HttpException(404, "Item is null");
             }
         }

        public static void NotNullOr404<T>(T? item) where T: struct 
        {
            if (item == null)
            {
                throw new HttpException(404, "Item is null");
            }
        }
    }
}