﻿namespace DerinSIS.B2B.Helpers
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;



    public interface IPagedList : IList
    {
        int Page { get; }
        int PageLength { get; }
        int TotalElements { get; }
        int TotalPages { get; }
        bool HasNextPage { get; }
        bool HasPrevPage { get; }
        int FirstDisplayingElementIndex { get; }
        int LastDisplayinElementIndex { get; }
    }

    public class PagedList<T> : List<T>, IPagedList
    {
        public PagedList(IEnumerable<T> collection, int page, int totalElements, int pageLength) : base(collection)
        {
            Page = page;
            PageLength = pageLength;
            TotalElements = totalElements;
        }

        public int Page { get; private set; }
        public int PageLength { get; set; }
        public int TotalElements { get; private set; }

        public int TotalPages
        {
            get { return (int)Math.Ceiling((decimal)TotalElements / PageLength); }
        }

        public bool HasNextPage
        {
            get { return TotalPages > Page; }
        }

        public bool HasPrevPage
        {
            get { return Page > 1; }
        }

        public int FirstDisplayingElementIndex
        {
            get { return Math.Min((Page - 1) * PageLength + 1, TotalElements); }
        }

        public int LastDisplayinElementIndex
        {
            get { return Math.Min(Page * PageLength, TotalElements); }
        }
    }

    public static class PagingExtensions
    {
        public static int DefaultPageLength = 29;


        public static PagedList<T> ToSqlPagedList<T>(this IEnumerable<T> col, int? page, int? count = null)
        {
            return ToSqlPagedList(col, page, DefaultPageLength, count);
        }

        public static PagedList<T> ToSqlPagedList<T>(this IEnumerable<T> col, int? page, int pageLength, int? count)
        {
            page = page ?? 1;
            if (page < 1)
                throw new HttpException(404, "Page Not Found");

            List<T> list = col.ToList();
            int totalElements = count ?? list.Count();

            if (list.Any() == false && page != 1)
                throw new HttpException(404, "Page Not Found");

            return new PagedList<T>(list, page.Value, totalElements, pageLength);
        }

        public static PagedList<T> ToPagedList<T>(this IEnumerable<T> col, int? page, int? count = null)
        {
            return ToPagedList(col, page, DefaultPageLength, count);
        }

        public static PagedList<T> ToPagedList<T>(this IEnumerable<T> col, int? page, int pageLength, int? count)
        {
            page = page ?? 1;
            if (page < 1)
                throw new HttpException(404, "Page Not Found");

            List<T> list = col.ToList();
            int totalElements = count ?? list.Count();
            List<T> items = list
                .Skip((page.Value - 1) * pageLength)
                .Take(pageLength)
                .ToList();

            if (items.Any() == false && page != 1)
                throw new HttpException(404, "Page Not Found");

            return new PagedList<T>(items, page.Value, totalElements, DefaultPageLength);
        }

        public static PagedList<T> ToPagedList<T>(this IQueryable<T> query, int? page)
        {
            return ToPagedList(query, page, DefaultPageLength);
        }

        public static PagedList<T> ToPagedList<T>(this IQueryable<T> query, int? page, int pageLength)
        {
            page = page ?? 1;
            if (page < 1)
                throw new HttpException(404, "Page Not Found");

            int totalElements = query.Count();
            List<T> items = query
                .Skip((page.Value - 1) * pageLength)
                .Take(pageLength)
                .ToList();

            if (items.Any() == false && page != 1)
                throw new HttpException(404, "Page Not Found");

            return new PagedList<T>(items, page.Value, totalElements, pageLength);
        }

        public static string ToPage(this UrlHelper helper, int page)
        {
            Uri url = helper.RequestContext.HttpContext.Request.Url;
            NameValueCollection queryParams = HttpUtility.ParseQueryString(url.Query);
            queryParams["page"] = page.ToString();

            string onlyPath = string.IsNullOrEmpty(url.Query) ? url.ToString() : url.ToString().Replace(url.Query, "");
            return "?" + string.Join("&", queryParams);
        }
    }
    
}