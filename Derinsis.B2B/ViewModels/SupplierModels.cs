﻿namespace DerinSIS.B2B.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    #region Product List

    public class SupplierProductListQueryModel
    {
        public int stkID { get; set; }
        public string stkKod { get; set; }
        public string stkAd { get; set; }
        public string mrkAd { get; set; }
        public string grpAd { get; set; }

        public byte urnKtgrID { get; set; }
        public byte urnKtgrID1 { get; set; }
        public byte urnKtgrID2 { get; set; }
        public byte urnKtgrID3 { get; set; }

        public int urtID { get; set; }
        public string urtAd { get; set; }

        public Int16 urnMrkID { get; set; }

        public int urnGrpID { get; set; }
        public string barkod { get; set; }
        public int rtb { get; set; }
        public Decimal odemeKur { get; set; }
        public Decimal fiyatA { get; set; }
        public string odemeKisaAd { get; set; }
        public int urnMrkGrpID { get; set; }
        public string urnMrkGrpAd { get; set; }
        public Int16 urnKoli { get; set; }
        public Int16 urnKutu { get; set; }
        public Decimal stok { get; set; }

        public byte urnKdvYuzde { get; set; }
        public string gosterilenStok { get; set; }

        public int topSay { get; set; }
    }

    //public class NewProductModel
    //{
    //    [Required(ErrorMessage = "Barkod alanı gereklidir.")]
    //    [Display(Name = "Barkod")]
    //    public string Barkod { get; set; }

    //    [Required(ErrorMessage = "Ürün Adı alanı gereklidir.")]
    //    [Display(Name = "Ürün Adı")]
    //    public string UrunAdi { get; set; }

    //    [Required(ErrorMessage = "Dağıtıcı Kodu alanı gereklidir.")]
    //    [Display(Name = "Dağıtıcı Kodu")]
    //    public string DagiticiKodu { get; set; }

    //    [Required(ErrorMessage = "KDV Oranı alanı gereklidir.")]
    //    [Display(Name = "KDV Oranı")]
    //    public int KDVOrani { get; set; }
    //}

    //public class EditProductModel
    //{
    //    [HiddenInput(DisplayValue = false)]
    //    public int Id { get; set; }

    //    [Required(ErrorMessage = "Barkod alanı gereklidir.")]
    //    [Display(Name = "Barkod")]
    //    public string Barkod { get; set; }

    //    [Required(ErrorMessage = "Ürün Adı alanı gereklidir.")]
    //    [Display(Name = "Ürün Adı")]
    //    public string UrunAdi { get; set; }

    //    [Required(ErrorMessage = "Dağıtıcı Kodu alanı gereklidir.")]
    //    [Display(Name = "Dağıtıcı Kodu")]
    //    public string DagiticiKodu { get; set; }


    //    [Required(ErrorMessage = "KDV Oranı alanı gereklidir.")]
    //    [Display(Name = "KDV Oranı")]
    //    public int KDVOrani { get; set; }
    //}

    #endregion



    #region ProductStockActionReport
    public class ProductStockActionModel
    {
        public DocumentDateFilterModel DateFilter { get; set; }
        public int? Page { get; set; }
        public int? PageLength { get; set; }
        public string Mekanlar { get; set; }
        public ICollection<ProductStock> ProductStocks { get; set; }
        public class ProductStock
        {
            public int stkID { get; set; }
            public string stkKod { get; set; }
            public string dagiticiKod { get; set; }
            public string barkod { get; set; }
            public string stkAd { get; set; }
            public decimal devir { get; set; }
            public decimal giren { get; set; }
            public decimal cikan { get; set; }
            public decimal stok { get; set; }
            public decimal fiyat { get; set; }
            public decimal stokTutar { get; set; }
            public int topSay { get; set; }
        }
    }
    #endregion


    #region ProductListReport
    public class ProductListReportModel
    {
        public string Sorgu { get; set; }
        public int? Page { get; set; }
        public int? PageLength { get; set; }
        public ICollection<ProductListReport> ProductListReports { get; set; }
        public class ProductListReport
        {
            public string stkKod { get; set; }
            public string barkod { get; set; }
            public string stkAd { get; set; }
            public string KategoriAd { get; set; }
            public string dagiticiKod { get; set; }
            public string mrkAd { get; set; }
            public DateTime listelemeTarih { get; set; }
            public decimal netAlis { get; set; }
            public decimal kdvliAlis { get; set; }
            public decimal satis { get; set; }
            public decimal kdvliSatis { get; set; }
            public int topSay { get; set; }
        }
    }
    #endregion


    #region PrivateProductPriceListReport
    public class PrivateProductPriceListReportModel
    {
        public string Sorgu { get; set; }
        public string Mekanlar { get; set; }
        public DocumentSingleDateFilterModel DateFilter { get; set; }
        public int? Page { get; set; }
        public int? PageLength { get; set; }

        public ICollection<PrivateProductPriceListReport> PrivateProductPriceListReports { get; set; }
        public class PrivateProductPriceListReport
        {
            public int stkID { get; set; }
            public string stkKod { get; set; }
            public string barkod { get; set; }
            public string stkAd { get; set; }
            public string dagiticiKod { get; set; }
            public decimal netAlis { get; set; }
            public decimal kdvliAlis { get; set; }
            public decimal satis { get; set; }
            public decimal kdvliSatis { get; set; }
            public DateTime tarih { get; set; }
            public DateTime tarihSon { get; set; }
            public string KategoriAd { get; set; }
            public int topSay { get; set; }
        }
    }
    #endregion


    #region ProductStockReport
    public class ProductStockModel
    {
        public string Sorgu { get; set; }
        public int? Page { get; set; }
        public int? PageLength { get; set; }
        public string Mekanlar { get; set; }
        public ICollection<ProductStock> ProductStocks { get; set; }
        public class ProductStock
        {
            public int stkID { get; set; }
            public string stkKod { get; set; }
            public string dagiticiKod { get; set; }
            public string barkod { get; set; }
            public string stkAd { get; set; }
            public decimal stok { get; set; }
            public int topSay { get; set; }
        }
    }
    #endregion




    #region PurchaseReturnReport
    public class PurchaseReturnModel
    {
        public DocumentDateFilterModel DateFilter { get; set; }
        public int? Page { get; set; }
        public int? PageLength { get; set; }
        public string Mekanlar { get; set; }
        public ICollection<PurchaseReturn> PurchaseReturns { get; set; }
        public class PurchaseReturn
        {
            public string mekanAd { get; set; }
            public int eID { get; set; }
            public string eNo { get; set; }
            public DateTime eTarih { get; set; }
            public decimal adet { get; set; }
            public decimal tutar { get; set; }
            public decimal indirim { get; set; }
            public decimal kdv { get; set; }
            public decimal toplam { get { return tutar - indirim + kdv; } }

            public int topSay { get; set; }
        }
    }
    #endregion

}