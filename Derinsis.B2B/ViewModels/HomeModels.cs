﻿namespace DerinSIS.B2B.ViewModels
{
    using DerinSIS.B2B.Infrastructure.Auth;

    public class MainMenuModel
    {
        public AccountType AccountType { get; set; }
        public bool SiparisYetkisi { get; set; }
        public bool SevkiyatYetkisi { get; set; }
        public bool FaturaYetkisi { get; set; }
        public bool MutabakatYetkisi { get; set; }
        public bool YoneticiYetkisi { get; set; }
        public string Doviz { get; set; }

        public string kAdi { get; set; }
        public string kPosta { get; set; }
        public string anaSayfaAdi { get; set; }
        public string helpMailAdress { get; set; }
    }
}