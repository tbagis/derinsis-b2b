﻿
namespace DerinSIS.B2B.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Web.Mvc;
    using Helpers;


    #region Product List

    public class ProductListModel
    {
        // Arama Filtereleri
        public string Sorgu { get; set; }
        public int? KategoriIdFiltresi { get; set; }
        public string KategoriFiltreEtiketi { get; set; }
        public int? UreticiIdFiltresi { get; set; }
        public string UreticiFiltreEtiketi { get; set; }
        public string MarkaIdFiltresi { get; set; }
        public string FiyatFiltresi { get; set; }
        public string FiyatFiltreEtiketi { get; set; }
        public string MarkaFiltreEtiketi { get; set; }
        public bool OzelFiyat { get; set; }
        public int UrunYeniler { get; set; }
        public int UrunStogaYeniGirenler { get; set; }
        public int UrunOneCikanlar { get; set; }

        public string siralama { get; set; }
        public int StokGosterimiVar { get; set; }

        public int? Page { get; set; }
        public int? PageLength { get; set; }
        public int brutFiyatGosterimVar { get; set; }


        // Arama Sonuçları
        public ICollection<ProductListItemModel> AramaSonuclari { get; set; }

        public abstract class ProductListItemModel
        {
            public int Id { get; set; }
            public string Isim { get; set; }
            public virtual bool ModelMi { get; set; }
            public string KucukResimUrl { get; set; }
            public string BuyukResimUrl { get; set; }
            public int Rutbe { get; set; }

        }

        public class GetProductModel : ProductListItemModel
        {
            public string StokKodu { get; set; }
            public string kategori1 { get; set; }
            public string kategori2 { get; set; }
            public string kategori3 { get; set; }
            public string kategori4 { get; set; }

            public byte kategoriKod1 { get; set; }
            public byte kategoriKod2 { get; set; }
            public byte kategoriKod3 { get; set; }
            public byte kategoriKod4 { get; set; }

            public int KDVOrani { get; set; }
            public decimal KDVHaricFiyat { get; set; }
            public decimal Indirim1 { get; set; }
            public decimal Indirim2 { get; set; }
            public decimal Indirim3 { get; set; }
            public decimal Indirim4 { get; set; }
            public string Birim { get; set; }
            public decimal Kur { get; set; }
            public decimal stok { get; set; }
            public int stokKontrolu { get; set; }


            public decimal KDVHaricNet
            {
                get
                {
                    var indirimler = new[] { Indirim1, Indirim2, Indirim3, Indirim4 };
                    return indirimler.Aggregate(KDVHaricFiyat, (fiyat, indirim) => fiyat - (fiyat * indirim / 100.0m));
                }
            }

            public decimal KDVTutari { get { return KDVHaricNet * (decimal)KDVOrani / 100.0m; } }
            public decimal KDVDahilToplam { get { return decimal.Round(KDVHaricNet + KDVTutari, 2, MidpointRounding.AwayFromZero); } }


            public string markaAd { get; set; }
            public decimal PSF { get; set; }


        }

        public class ProductModel : ProductListItemModel
        {
            public string Barkod { get; set; }
            public string StokKodu { get; set; }
            public int KDVOrani { get; set; }
            public decimal KDVHaricFiyat { get; set; }
            public decimal Indirim1 { get; set; }
            public decimal Indirim2 { get; set; }
            public decimal Indirim3 { get; set; }
            public decimal Indirim4 { get; set; }
            public string Birim { get; set; }
            public decimal Kur { get; set; }
            public override bool ModelMi { get { return false; } }
            public int BagliOlduguModelId { get; set; }
            public short urnKoli { get; set; }
            public short urnKutu { get; set; }
            public decimal stok { get; set; }
            public int stokKontrolu { get; set; }

            public string gosterilenStok { get; set; }
            public int stokGosterimi { get; set; }
            public int brutFiyatGosterimVar { get; set; }

            public decimal KDVHaricNet
            {
                get
                {
                    var indirimler = new[] { Indirim1, Indirim2, Indirim3, Indirim4 };
                    return indirimler.Aggregate(KDVHaricFiyat, (fiyat, indirim) => fiyat - (fiyat * indirim / 100.0m));
                }
            }

            public decimal KDVTutari { get { return KDVHaricNet * (decimal)KDVOrani / 100.0m; } }
            public decimal KDVDahilToplam { get { return decimal.Round(KDVHaricNet + KDVTutari, 2, MidpointRounding.AwayFromZero); } }

            public int topSay { get; set; }


        }
        public class ProductGroupModel : ProductListItemModel
        {
            public IEnumerable<ProductModel> Urunler { get; set; }
            public decimal Indirim1 { get; set; }
            public decimal Indirim2 { get; set; }
            public decimal Indirim3 { get; set; }
            public decimal Indirim4 { get; set; }

            public string gosterilenStok { get; set; }
            public int stokGosterimi { get; set; }
            public override bool ModelMi { get { return true; } }
        }


    }

    #endregion


    #region Product Detail Pages

    public class ProductDetailModel
    {
        public int Id { get; set; }
        public string UrunKodu { get; set; }
        public string Isim { get; set; }
        public string StokKisaAd { get; set; }

        // Ölçüler
        public string UrunBirimi { get; set; }
        public decimal PaketOlcu { get; set; }
        public string PaketBirimi { get; set; }
        public int KoliOlcu { get; set; }
        public string KoliBirimi { get; set; }
        public int KutuOlcu { get; set; }
        public int PaletOlcu { get; set; }

        // Kategoriler
        public int Kategori0Id { get; set; }
        public int Kategori1Id { get; set; }
        public int Kategori2Id { get; set; }
        public string Kategori0Ad { get; set; }
        public string Kategori1Ad { get; set; }
        public string Kategori2Ad { get; set; }

        // Üretici - Marka - Model
        public int UreticiId { get; set; }
        public string UreticiAd { get; set; }
        public string MarkaId { get; set; }
        public string MarkaAd { get; set; }

        public int ModelId { get; set; }
        public string ModelAd { get; set; }

        public int bagliOlduguGrupIdsi { get; set; }


        // Ürün Kodları
        public bool UrunKod1GosterilsinMi { get; set; }
        public bool UrunKod2GosterilsinMi { get; set; }
        public bool UrunKod3GosterilsinMi { get; set; }
        public bool UrunKod4GosterilsinMi { get; set; }
        public bool UrunKod5GosterilsinMi { get; set; }
        public string UrunKod1Baslik { get; set; }
        public string UrunKod2Baslik { get; set; }
        public string UrunKod3Baslik { get; set; }
        public string UrunKod4Baslik { get; set; }
        public string UrunKod5Baslik { get; set; }

        public string UrunKod1 { get; set; }
        public string UrunKod2 { get; set; }
        public string UrunKod3 { get; set; }
        public string UrunKod4 { get; set; }
        public string UrunKod5 { get; set; }

        public decimal stok { get; set; }
        public int stokKontrolu { get; set; }

        public string gosterilenStok { get; set; }
        public int stokGosterimi { get; set; }

        public bool kategoriGosterimiYok { get; set; }


        // Ürün Özellikleri
        public IEnumerable<BilgiGrubu> BilgiGruplari { get; set; }
        public class BilgiGrubu
        {
            public string GrupAdi { get; set; }
            public IEnumerable<Bilgi> Bilgiler { get; set; }

            public class Bilgi
            {
                public string Ad { get; set; }
                public string Deger { get; set; }
            }
        }


        // Stok Bilgileri
        public IEnumerable<StokBilgisi> StokBilgileri { get; set; }
        public class StokBilgisi
        {
            public string MekanAdi { get; set; }
            public decimal Stok { get; set; }
        }

        // Fiyatlar ve İndirimler
        public decimal KDVHaricSatisFiyati { get; set; }
        public string Birim { get; set; }
        public decimal SatisKur { get; set; }

        public decimal Indirim1 { get; set; }
        public decimal Indirim2 { get; set; }
        public decimal Indirim3 { get; set; }
        public decimal Indirim4 { get; set; }

        public decimal KDVHaricNet
        {
            get
            {
                var indirimler = new[] { Indirim1, Indirim2, Indirim3, Indirim4 };
                return indirimler.Aggregate(KDVHaricSatisFiyati, (fiyat, indirim) => fiyat - (fiyat * indirim / 100.0m));
            }
        }

        public decimal ToplamIndirim { get { return KDVHaricNet - KDVHaricSatisFiyati; } }
        public int KDVOrani { get; set; }
        public decimal KDVTutari { get { return KDVHaricNet * (decimal)KDVOrani / 100.0m; } }
        public decimal KDVDahilToplam { get { return KDVHaricNet + KDVTutari; } }

        public IEnumerable<BarkodBilgisi> BarkodBilgileri { get; set; }
        public class BarkodBilgisi
        {
            public string Barkod { get; set; }
            public int Adet { get; set; }
            public string AdetBirim { get; set; }
        }

        public IEnumerable<UrunSiparis> Siparisler { get; set; }
        public class UrunSiparis
        {
            public int Id { get; set; }
            public string EvrakNo { get; set; }
            public DateTime Tarih { get; set; }
            public DateTime SevkTarihi { get; set; }
            public string HareketTipi { get; set; }
            public string HareketGrubu { get; set; }
            public decimal UrunKoliAdet { get { return UrunToplamAdet / UrunKoliIciAdet; } }
            public decimal UrunKoliIciAdet { get; set; }
            public decimal UrunToplamAdet { get; set; }
            public decimal UrunBirimFiyat { get { return UrunKDVHaricToplam / UrunToplamAdet; } }
            public decimal UrunKDVHaricToplam { get; set; }
            public decimal UrunIndirimTutari { get; set; }
            public decimal UrunKDVHaricNet { get { return UrunKDVHaricToplam - UrunIndirimTutari; } }
            public decimal ToplamKDVHaricNet { get; set; }
            public decimal ToplamKDVDahilTutar { get; set; }

        }

        public IEnumerable<UrunIrsaliye> Irsaliyeler { get; set; }
        public class UrunIrsaliye
        {
            public int Id { get; set; }
            public string EvrakNo { get; set; }
            public DateTime Tarih { get; set; }
            public DateTime SevkTarihi { get; set; }
            public string HareketTipi { get; set; }
            public string HareketGrubu { get; set; }
            public decimal UrunKoliAdet { get { return UrunToplamAdet / UrunKoliIciAdet; } }
            public decimal UrunKoliIciAdet { get; set; }
            public decimal UrunToplamAdet { get; set; }
            public decimal UrunBirimFiyat { get { return UrunKDVHaricToplam / UrunToplamAdet; } }
            public decimal UrunKDVHaricToplam { get; set; }
            public decimal UrunIndirim1 { get; set; }
            public decimal UrunIndirim2 { get; set; }
            public decimal UrunIndirim3 { get; set; }
            public decimal UrunIndirim4 { get; set; }
            public decimal UrunIndirim5 { get; set; }
            public decimal UrunIndirimTutari { get; set; }
            public decimal UrunKDVHaricNet { get { return UrunKDVHaricToplam - UrunIndirimTutari; } }
            public decimal ToplamKDVHaricNet { get; set; }
            public decimal ToplamKDVDahilTutar { get; set; }
        }

        public IEnumerable<UrunFatura> Faturalar { get; set; }
        public class UrunFatura
        {
            public int Id { get; set; }
            public string EvrakNo { get; set; }
            public DateTime Tarih { get; set; }
            public DateTime VadeTarihi { get; set; }
            public string HareketTipi { get; set; }
            public string HareketGrubu { get; set; }
            public decimal UrunKoliAdet { get { return UrunToplamAdet / UrunKoliIciAdet; } }
            public decimal UrunKoliIciAdet { get; set; }
            public decimal UrunToplamAdet { get; set; }
            public decimal UrunBirimFiyat { get { return UrunKDVHaricToplam / UrunToplamAdet; } }
            public decimal UrunKDVHaricToplam { get; set; }
            public decimal UrunIndirim1 { get; set; }
            public decimal UrunIndirim2 { get; set; }
            public decimal UrunIndirim3 { get; set; }
            public decimal UrunIndirim4 { get; set; }
            public decimal UrunIndirim5 { get; set; }
            public decimal UrunIndirimTutari { get; set; }
            public decimal UrunKDVHaricNet { get { return UrunKDVHaricToplam - UrunIndirimTutari; } }
            public decimal ToplamKDVHaricNet { get; set; }
            public decimal ToplamKDVDahilTutar { get; set; }
        }

        public string ControllerName { get; set; }

    }

    public class ProductGroupDetailModel
    {
        public int Id { get; set; }
        public string ModelAdi { get; set; }

        // Üretici - Marka - Model
        public int UreticiId { get; set; }
        public string UreticiAdi { get; set; }
        public int MarkaId { get; set; }
        public string MarkaAdi { get; set; }

        // Kategori
        public int Kategori0Id { get; set; }
        public int Kategori1Id { get; set; }
        public int Kategori2Id { get; set; }
        public string Kategori0Ad { get; set; }
        public string Kategori1Ad { get; set; }
        public string Kategori2Ad { get; set; }

        // Fiyat - İndirim
        public decimal KDVHaricSatisFiyati { get; set; }
        public decimal Indirim1 { get; set; }
        public decimal Indirim2 { get; set; }
        public decimal Indirim3 { get; set; }
        public decimal Indirim4 { get; set; }
        public decimal KDVHaricNet
        {
            get
            {
                var indirimler = new[] { Indirim1, Indirim2, Indirim3, Indirim4 };
                return indirimler.Aggregate(KDVHaricSatisFiyati, (fiyat, indirim) => fiyat - (fiyat * indirim / 100.0m));
            }
        }
        public decimal ToplamIndirim { get { return KDVHaricNet - KDVHaricSatisFiyati; } }
        public int KDVOrani { get; set; }
        public decimal KDVTutari { get { return KDVHaricNet * KDVOrani / 100.0m; } }
        public decimal KDVDahilToplam { get { return KDVHaricNet + KDVTutari; } }

        public int Brand_GroupValue { get; set; }

        public int grupStokKontrolu { get; set; }
        public int grupStokGosterimi { get; set; }
        public int brutFiyatGosterimVar { get; set; }
        public bool kategoriGosterimiYok { get; set; }
        public ICollection<Product> Products { get; set; }
        public class Product
        {
            public int Id { get; set; }
            public string Isim { get; set; }
            public string StokKodu { get; set; }
            public string Barkod { get; set; }
            public decimal KDVHaricSatisFiyati { get; set; }
            public decimal Indirim1 { get; set; }
            public decimal Indirim2 { get; set; }
            public decimal Indirim3 { get; set; }
            public decimal Indirim4 { get; set; }
            public decimal KDVHaricNet
            {
                get
                {
                    var indirimler = new[] { Indirim1, Indirim2, Indirim3, Indirim4 };
                    return indirimler.Aggregate(KDVHaricSatisFiyati, (fiyat, indirim) => fiyat - (fiyat * indirim / 100.0m));
                }
            }
            public decimal ToplamIndirim { get { return KDVHaricNet - KDVHaricSatisFiyati; } }
            public int KDVOrani { get; set; }
            public decimal KDVTutari { get { return KDVHaricNet * KDVOrani / 100.0m; } }
            public decimal KDVDahilToplam { get { return KDVHaricNet + KDVTutari; } }
            public decimal stok { get; set; }
            public string gosterilenStok { get; set; }
            public string Birim { get; set; }


        }
        public string ControllerName { get; set; }

    }

    public class ProductPromoDetailModel
    {
        public int Id { get; set; }
        public string Ad { get; set; }
        public string resim { get; set; }

        public int promoStokKontrolu { get; set; }
        public int promoStokGosterimi { get; set; }
        public int brutFiyatGosterimVar { get; set; }
        public ICollection<Product> Products { get; set; }
        public class Product
        {
            public int Id { get; set; }
            public string Isim { get; set; }
            public string StokKodu { get; set; }
            public string Barkod { get; set; }
            public decimal KDVHaricSatisFiyati { get; set; }
            public string Birim { get; set; }
            public decimal Indirim1 { get; set; }
            public decimal Indirim2 { get; set; }
            public decimal Indirim3 { get; set; }
            public decimal Indirim4 { get; set; }
            public decimal KDVHaricNet
            {
                get
                {
                    var indirimler = new[] { Indirim1, Indirim2, Indirim3, Indirim4 };
                    return indirimler.Aggregate(KDVHaricSatisFiyati, (fiyat, indirim) => fiyat - (fiyat * indirim / 100.0m));
                }
            }
            public decimal ToplamIndirim { get { return KDVHaricNet - KDVHaricSatisFiyati; } }
            public int KDVOrani { get; set; }
            public decimal KDVTutari { get { return KDVHaricNet * KDVOrani / 100.0m; } }
            public decimal KDVDahilToplam { get { return KDVHaricNet + KDVTutari; } }

            public decimal stok { get; set; }
            public string gosterilenStok { get; set; }
        }

        public string ControllerName { get; set; }
    }
    #endregion


}