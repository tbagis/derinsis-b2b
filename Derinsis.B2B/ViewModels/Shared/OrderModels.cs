﻿
namespace DerinSIS.B2B.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Web.Mvc;
    using Helpers;



    #region Make Order Models

    public class DeliveryCityModel
    {
        public int AlanKodu { get; set; }
        public string IlAdi { get; set; }
    }

    public class OrderDeliveryAddressModel
    {
        public int Id { get; set; }
        public string AdresAdi { get; set; }
        public string AdresSatiri1 { get; set; }
        public string AdresSatiri2 { get; set; }
        public string PostaKodu { get; set; }
        public string Ilce { get; set; }
        public string Il { get; set; }

    }

    public class CreateDeliveryAddressModel
    {
        [ScaffoldColumn(false)]
        public int? Id { get; set; }

        [Required(ErrorMessage = "Adres Başlığı bilgisi gereklidir.")]
        [Display(Name = "Adres Başlığı")]
        public string AdresIsmi { get; set; }

        [Required(ErrorMessage = "Adres 1 bilgisi gereklidir.")]
        [Display(Name = "Adres 1")]
        public string Adres1 { get; set; }

        [Required(ErrorMessage = "Adres 2 bilgisi gereklidir.")]
        [Display(Name = "Adres 2")]
        public string Adres2 { get; set; }

        [Display(Name = "Posta Kodu")]
        [StringLength(5, MinimumLength = 0, ErrorMessage = "Posta Kodu En Fazla 5 karakterden oluşmalıdır.")]
        public string PostaKodu { get; set; }

        [Display(Name = "İl")]
        [Required(ErrorMessage = "Il seçilmesi gereklidir.")]
        [UIHint("DropDownList")]
        [AdditionalMetadata("DataController", "Store")]
        [AdditionalMetadata("DataAction", "CitiesDropDownData")]
        public int? IlId { get; set; }

        [Display(Name = "İlçe")]
        [Required(ErrorMessage = "İlçe bilgisi gereklidir.")]
        [UIHint("DependentDropDownList")]
        [AdditionalMetadata("DataController", "Store")]
        [AdditionalMetadata("DataAction", "districtDropDownData")]
        [AdditionalMetadata("DependsOn", "IlId")]
        public int ilceSecim { get; set; }

        [StringLength(30, MinimumLength = 0, ErrorMessage = "Vergi dairesi en fazla 30 karakterden oluşmalıdır.")]
        [Display(Name = "Vergi Dairesi")]
        public string VD { get; set; }

        [StringLength(20, MinimumLength = 0, ErrorMessage = "Vergi numarası en fazla 20 karakterden oluşmalıdır.")]
        [Display(Name = "Vergi Numarası")]
        public string VN { get; set; }

    }

    public class OrderSummaryModel
    {
        public int AktifSepetId { get; set; }
        public string AktifSpetAdi { get; set; }

        public int AdresId { get; set; }
        public string AdresAdi { get; set; }
        public string AdresSatiri1 { get; set; }
        public string AdresSatiri2 { get; set; }
        public string AdresPostaKodu { get; set; }
        public string AdresIlce { get; set; }
        public string AdresIl { get; set; }

        public IEnumerable<CartDetail> Urunler { get; set; }

        public class CartDetail
        {
            public int ProductId { get; set; }
            public string ProductName { get; set; }
            public decimal Quantity { get; set; }
            public decimal ProductUnitPrice { get; set; }
            public decimal ProductUnitDiscountedPrice { get; set; }
            public decimal TotalPrice { get; set; }
            public string DvzBirim { get; set; }
            public int KDVOrani { get; set; }
            public decimal KDVTutari { get; set; }
        }
    }

    public class OrderCompletedModel
    {
        public int SiparisId { get; set; }
        public string SiparisNotu { get; set; }

        public ICollection<SiparisSatiri> SiparisSatirlari { get; set; }
        public class SiparisSatiri
        {
            public int Sira { get; set; }
            public int UrunId { get; set; }
            public string UrunAdi { get; set; }
            public string StokKodu { get; set; }
            public decimal Adet { get; set; }
            public decimal Tutar { get; set; }
            public decimal Matrah { get; set; }
        }


    }

    #endregion

}