﻿
namespace DerinSIS.B2B.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Web.Mvc;
    using Helpers;


    #region Category & Brand Trees

    public class CategoryTreeModel
    {
        public IList<CategoryItem> Items { get; set; }

        public bool HasChildren(CategoryItem item)
        {
            return Items.Any(x => x.ParentId == item.Id);
        }

        public IList<CategoryItem> GetChildrenFor(CategoryItem item)
        {
            return Items
                .Where(x => x.ParentId == item.Id)
                .OrderBy(x => x.Isim)
                .ToList();
        }


        public IList<CategoryItem> GetRootLevelItems()
        {
            return Items
                .Where(x => x.ParentId == null)
                .OrderBy(x => x.Isim)
                .ToList();
        }

        public class CategoryItem
        {
            public int Id { get; set; }
            public int? ParentId { get; set; }

            public string Isim { get; set; }
            public int Order { get; set; }
        }
    }

    public class BrandData
    {
        public int UreticiID { get; set; }
        public int MarkaID { get; set; }
        public string markaAd { get; set; }
        public int markaCount { get; set; }
    }
    public class BrandTreeModel
    {
        public int Brand_GroupValue { get; set; }
        public IEnumerable<Producer> Producers { get; set; }

        public class Producer
        {
            public int Id { get; set; }
            public string Name { get; set; }

            public IEnumerable<Brand> Brands { get; set; }

        }

        public class Brand
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public int Count { get; set; }
        }

    }

    public class PricesModel
    {
        public ICollection<Price> Prices { get; set; }

        public class Price
        {
            public string Name { get; set; }
            public int Min { get; set; }
            public int Max { get; set; }
        }
    }

    public class Categories
    {
        public short kID { get; set; }
        public string kAd { get; set; }

        public byte k0 { get; set; }
        public byte k1 { get; set; }
        public byte k2 { get; set; }
        public byte k3 { get; set; }

        public string k0Ad { get; set; }
        public string k1Ad { get; set; }
        public string k2Ad { get; set; }
        public string k3Ad { get; set; }
    }
    


    #endregion



}