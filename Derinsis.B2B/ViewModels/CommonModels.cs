﻿namespace DerinSIS.B2B.ViewModels
{
    using System;
    using System.Collections.Generic;

    public class DocumentDateFilterModel
    {
        public DateTime? Begin { get; set; }
        public DateTime? End { get; set; }
    }

    public class DocumentType
    {
        public int Id { get; set; }
        public string Isim { get; set; }
    }

    public class DropDownListItem
    {
        public int Value { get; set; }
        public string Text { get; set; }
    }

    public class DocumentSingleDateFilterModel
    {
        public DateTime? Begin { get; set; }
    }
}