﻿namespace DerinSIS.B2B.ViewModels
{
    using System.Collections.Generic;

    public class ProductSlideShowModel
    {
        public ProductSlideShowModel()
        {
            Images = new List<string>();
        }

        public ICollection<string> Images { get; set; }
    }

    public class ProductGroupSlideShowModel
    {
        public ProductGroupSlideShowModel()
        {
            Images = new List<string>();
        }
        public ICollection<string> Images { get; set; }
    }
}