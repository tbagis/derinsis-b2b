﻿using System.Linq;

namespace DerinSIS.B2B.ViewModels
{
    using Helpers;
    using System;
    using System.Collections.Generic;
    using System.Data;

    #region Dashboard Models

    public class SalesDashboardModel
    {
        public string ControllerName { get; set; }
        public IEnumerable<BekleyenSiparis> BekleyenSiparisler { get; set; }
        public class BekleyenSiparis
        {
            public int Id { get; set; }
            public DateTime SiparisTarihi { get; set; }
            public bool OnayliMi { get; set; }
            public DateTime SevkTarihi { get; set; }
        }

        public IEnumerable<YeniUrun> YeniUrunler { get; set; }
        public class YeniUrun
        {
            public int Id { get; set; }
            public string UrunAdi { get; set; }
            public decimal UrunFiyati { get; set; }
            public DateTime EklenmeTarihi { get; set; }
        }

        public IList<SiparisGunu> SiparisGunleri { get; set; }
        public class SiparisGunu
        {
            public int HaftaninGunu { get; set; }
            public string GunIsmi { get; set; }
            public bool SiparisVerilebilirMi { get; set; }
        }

        public bool acikSipGoster { get; set; }
    }

    #endregion

    #region Order Models

    public class OrdersModel
    {
        public IEnumerable<Order> Orders { get; set; }
        public DocumentDateFilterModel DateFilter { get; set; }
        public int? ProductFilterId { get; set; }
        public string ProductFilterLabel { get; set; }
        public ICollection<DocumentType> AvailableTypes { get; set; }
        public int? TypeFilterId { get; set; }

        public Settings GosterimSecenekleri { get; set; }


        public class Order
        {
            public int Id { get; set; }
            public DateTime Tarih { get; set; }
            public string HareketTipi { get; set; }
            public string EvrakNo { get; set; }
            public OrderStatus Durum { get; set; }
            public bool OnayliMi { get; set; }
            public DateTime VadeTarihi { get; set; }
            public string EvrakNot { get; set; }
            public decimal KDVHaricToplam { get; set; }
            public decimal ToplamIndirim { get; set; }
            public decimal KDVHaricNet { get { return KDVHaricToplam - ToplamIndirim; } }
            public string HareketGrubu { get; set; }
            public string SevkAdresi { get; set; }
        }

        public enum OrderStatus
        {
            ACIK = 0,
            FATURALASMIS = 1,
            IPTAL = 2
        }

        public class Settings
        {
            public bool EvrakNotuGosterilsinMi { get; set; }
        }
    }

    public class OrderDetailModel
    {
        public int Id { get; set; }
        public DateTime Tarih { get; set; }
        public DateTime VadeTarihi { get; set; }
        public DateTime SevkTarihi { get; set; }
        public string EvrakNo { get; set; }
        public string HareketTipi { get; set; }
        public string EvrakNot { get; set; }
        public string Evrak3SatirNot { get; set; }
        public string HareketGrubu { get; set; }

        public string SevkAdresiAd { get; set; }
        public string SevkAdresi1 { get; set; }
        public string SevkAdresi2 { get; set; }
        public string SevkAdresiPostaKodu { get; set; }
        public string SevkAdresiIlce { get; set; }
        public string SevkAdresiIl { get; set; }
        public string SevkAdresiTel { get; set; }
        public string SevkAdresiFaks { get; set; }

        public OrderStatus Durum { get; set; }
        public bool Onaylimi { get; set; }

        public string gKisiAd { get; set; }
        public DateTime gTarih { get; set; }
        public string kKisiAd { get; set; }
        public DateTime kTarih { get; set; }
        public IEnumerable<OrderDetail> EvrakHareketleri { get; set; }




        public IEnumerable<RelatedDelivery> IlgiliIrsaliyeler { get; set; }
        public IEnumerable<RelatedInvoice> IlgiliFaturalar { get; set; }

        public Settings GosterimSecenekleri { get; set; }

        public string ControllerName { get; set; }
        public class OrderDetail
        {
            public int Id { get; set; }
            public int Sira { get; set; }
            public int UrunId { get; set; }
            public string UrunKod { get; set; }
            public string UrunBarkod { get; set; }
            public string UrunIsmi { get; set; }

            public decimal ToplamAdet { get; set; }
            public decimal KoliIciAdet { get; set; }
            public decimal KoliAdet { get { return Math.Floor(ToplamAdet / KoliIciAdet); } }
            public decimal PaletIciAdet { get; set; }
            public decimal PaletAdet { get { return Math.Floor(ToplamAdet / PaletIciAdet); } }
            public decimal UrunAgirligi { get; set; }
            public decimal ToplamAgirlik { get { return UrunAgirligi * ToplamAdet; } }
            public string AgirlikBirim { get; set; }
            public decimal KalanAdet { get; set; }

            public decimal BirimFiyat { get { return KDVHaricToplam / ToplamAdet; } }
            public decimal KDVHaricToplam { get; set; }
            public decimal Indirim1 { get; set; }
            public decimal Indirim2 { get; set; }
            public decimal Indirim3 { get; set; }
            public decimal Indirim4 { get; set; }
            public decimal Indirim5 { get; set; }
            public decimal IndirimTutari { get; set; }
            public decimal KDVHaricNet { get { return KDVHaricToplam - IndirimTutari; } }
            public int KDVYuzde { get; set; }
            public decimal KDVTutar { get { return KDVHaricNet * (KDVYuzde / 100m); } }
            public decimal KDVDahilTutar { get { return KDVHaricNet + KDVTutar; } }

            public bool BedelsizUrunMu { get { return KDVHaricNet == 0; } }

            public byte ehDurum { get; set; }

            public OrderDetailStatus Durum { get { return (OrderDetailModel.OrderDetailStatus)ehDurum; } }

        }

        public class RelatedDelivery
        {
            public int Id { get; set; }
            public string EvrakNo { get; set; }
            public DateTime Tarih { get; set; }
        }

        public class RelatedInvoice
        {
            public int Id { get; set; }
            public string EvrakNo { get; set; }
            public DateTime Tarih { get; set; }
        }

        public enum OrderStatus
        {
            ACIK = 0,
            FATURALASMIS = 1,
            IPTAL = 2
        }

        public enum OrderDetailStatus
        {
            ACIK = 0,
            IPTAL = 1,
        }

        public class Settings
        {
            public bool EvrakNotuGosterilsinMi { get; set; }
        }
    }

    #endregion

    #region Delivery Models

    public class DeliveriesModel
    {
        public IEnumerable<Delivery> Deliveries { get; set; }
        public ICollection<DocumentType> AvailableTypes { get; set; }
        public DocumentDateFilterModel DateFilter { get; set; }
        public int? ProductFilterId { get; set; }
        public string ProductFilterLabel { get; set; }


        public Settings GosterimSecenekleri { get; set; }

        public class Delivery
        {
            public int Id { get; set; }
            public DateTime Tarih { get; set; }
            public string HareketTipi { get; set; }
            public string EvrakNo { get; set; }
            public DeliveryStatus Durum { get; set; }
            public DateTime VadeTarihi { get; set; }
            public string EvrakNot { get; set; }
            public decimal KDVHaricToplam { get; set; }
            public decimal ToplamIndirim { get; set; }
            public decimal KDVHaricNet { get { return KDVHaricToplam - ToplamIndirim; } }
            public string HareketGrubu { get; set; }
            public string SevkAdresi { get; set; }
        }

        public enum DeliveryStatus
        {
            ACIK = 0,
            FATURALASMIS = 1
        }


        public class Settings
        {
            public bool EvrakNotuGosterilsinMi { get; set; }
        }
    }

    public class DeliveryDetailModel
    {
        public int Id { get; set; }
        public string EvrakNo { get; set; }
        public DateTime Tarih { get; set; }
        public DateTime VadeTarihi { get; set; }
        public DateTime SevkTarihi { get; set; }
        public string HareketTipi { get; set; }
        public string EvrakNot { get; set; }
        public string Evrak3SatirNot { get; set; }
        public string HareketGrubu { get; set; }
        public string SevkAdresiAd { get; set; }
        public string SevkAdresi1 { get; set; }
        public string SevkAdresi2 { get; set; }
        public string SevkAdresiPostaKodu { get; set; }
        public string SevkAdresiIlce { get; set; }
        public string SevkAdresiIl { get; set; }
        public string SevkAdresiTel { get; set; }
        public string SevkAdresiFaks { get; set; }

        public DeliveryStatus Durum { get; set; }

        public IEnumerable<DeliveryDetail> EvrakHareketleri { get; set; }

        public IEnumerable<RelatedOrder> IlgiliSiparisler { get; set; }
        public IEnumerable<RelatedInvoice> IlgiliFaturalar { get; set; }

        public Settings GosterimSecenekleri { get; set; }

        public string ControllerName { get; set; }
        public class DeliveryDetail
        {
            public int Id { get; set; }
            public int Sira { get; set; }
            public int UrunId { get; set; }
            public string UrunKod { get; set; }
            public string UrunBarkod { get; set; }
            public string UrunIsmi { get; set; }

            public decimal ToplamAdet { get; set; }
            public decimal KoliIciAdet { get; set; }
            public decimal KoliAdet { get { return Math.Floor(ToplamAdet / KoliIciAdet); } }
            public decimal PaletIciAdet { get; set; }
            public decimal PaletAdet { get { return Math.Floor(ToplamAdet / PaletIciAdet); } }
            public decimal UrunAgirligi { get; set; }
            public string AgirlikBirim { get; set; }
            public decimal ToplamAgirlik { get { return UrunAgirligi * ToplamAdet; } }
            public decimal KalanAdet { get; set; }

            public decimal BirimFiyat { get { return KDVHaricToplam / ToplamAdet; } }
            public decimal KDVHaricToplam { get; set; }
            public decimal Indirim1 { get; set; }
            public decimal Indirim2 { get; set; }
            public decimal Indirim3 { get; set; }
            public decimal Indirim4 { get; set; }
            public decimal Indirim5 { get; set; }
            public decimal IndirimTutari { get; set; }
            public decimal KDVHaricNet { get { return KDVHaricToplam - IndirimTutari; } }
            public int KDVYuzde { get; set; }
            public decimal KDVTutar { get; set; }
            public decimal KDVDahilTutar { get { return KDVHaricNet + KDVTutar; } }

            public bool BedelsizUrunMu { get { return KDVHaricNet == 0; } }
        }

        public class RelatedOrder
        {
            public int Id { get; set; }
            public string EvrakNo { get; set; }
            public DateTime Tarih { get; set; }
        }

        public class RelatedInvoice
        {
            public int Id { get; set; }
            public string EvrakNo { get; set; }
            public DateTime Tarih { get; set; }
        }

        public enum DeliveryStatus
        {
            ACIK = 0,
            FATURALASMIS = 1
        }

        public class Settings
        {
            public bool EvrakNotuGosterilsinMi { get; set; }
        }
    }

    #endregion

    #region MyDiscounts Models

    public class MyDiscountModel
    {
        public int MarkaId { get; set; }
        public string MarkaIsmi { get; set; }

        public decimal Indirim1 { get; set; }
        public decimal Indirim2 { get; set; }
        public decimal Indirim3 { get; set; }
    }

    #endregion




    #region SalesPerCategoryReport

    public class SalesPerCategoryModel
    {
        public string ProductCount { get; set; }
        public string Categories { get; set; }
        public string Brands { get; set; }
        public string Cities { get; set; }
        public string Groups { get; set; }
        public string Status { get; set; }
        public string StoreCount { get; set; }
        public DocumentDateFilterModel DateFilter { get; set; }
        public IEnumerable<SalesPerCategory> Reports { get; set; }
        public class SalesPerCategory
        {
            public string CategoryName { get; set; }
            public decimal TotalSale { get; set; }
            public decimal RefundofSale { get; set; }
            public decimal TotalSaleCount { get; set; }
            public decimal RefundofSaleCount { get; set; }

        }
    }

    #endregion


    #region SalesPerBrandReport


    public class SalesPerBrandModel
    {
        public string ProductCount { get; set; }
        public string Categories { get; set; }
        public string Brands { get; set; }
        public string Cities { get; set; }
        public string Groups { get; set; }
        public string Status { get; set; }
        public string StoreCount { get; set; }
        public DocumentDateFilterModel DateFilter { get; set; }
        public IEnumerable<SalesPerBrand> Reports { get; set; }
        public class SalesPerBrand
        {
            public string BrandName { get; set; }
            public decimal TotalSale { get; set; }
            public decimal RefundofSale { get; set; }
            public decimal TotalSaleCount { get; set; }
            public decimal RefundofSaleCount { get; set; }
        }
    }



    #endregion

    #region SalesPerWeekReport


    public class SalesPerWeekModel
    {
        public string ProductCount { get; set; }
        public string Categories { get; set; }
        public string Brands { get; set; }
        public string Cities { get; set; }
        public string Groups { get; set; }
        public string Status { get; set; }
        public string StoreCount { get; set; }
        public DocumentDateFilterModel DateFilter { get; set; }
        public IEnumerable<SalesPerWeek> Reports { get; set; }
        public class SalesPerWeek
        {
            public int Year { get; set; }
            public int Month { get; set; }
            public decimal Week { get; set; }
            public decimal TotalSale { get; set; }
            public decimal RefundofSale { get; set; }
            public decimal TotalSaleCount { get; set; }
            public decimal RefundofSaleCount { get; set; }
        }
    }



    #endregion

    #region SalesPerMonthReport


    public class SalesPerMonthModel
    {
        public string ProductCount { get; set; }
        public string Categories { get; set; }
        public string Brands { get; set; }
        public string Cities { get; set; }
        public string Groups { get; set; }
        public string Status { get; set; }
        public string StoreCount { get; set; }
        public DocumentDateFilterModel DateFilter { get; set; }
        public IEnumerable<SalesPerMonth> Reports { get; set; }
        public class SalesPerMonth
        {
            public int Year { get; set; }
            public int Month { get; set; }
            public decimal TotalSale { get; set; }
            public decimal RefundofSale { get; set; }
            public decimal TotalSaleCount { get; set; }
            public decimal RefundofSaleCount { get; set; }
        }
    }



    #endregion

    #region SalesPerRegionReport

    public class SalesPerRegionModel
    {
        public string ProductCount { get; set; }
        public string Categories { get; set; }
        public string Brands { get; set; }
        public string Cities { get; set; }
        public string Groups { get; set; }
        public string Status { get; set; }
        public string StoreCount { get; set; }
        public DocumentDateFilterModel DateFilter { get; set; }
        public IEnumerable<SalesPerRegion> Reports { get; set; }
        public class SalesPerRegion
        {
            public string Region { get; set; }
            public decimal TotalSale { get; set; }
            public decimal RefundofSale { get; set; }
            public decimal TotalSaleCount { get; set; }
            public decimal RefundofSaleCount { get; set; }
        }

    }
    #endregion

    #region SalesPerCityReport

    public class SalesPerCityModel
    {
        public string ProductCount { get; set; }
        public string Categories { get; set; }
        public string Brands { get; set; }
        public string Cities { get; set; }
        public string Groups { get; set; }
        public string Status { get; set; }
        public string StoreCount { get; set; }
        public DocumentDateFilterModel DateFilter { get; set; }
        public IEnumerable<SalesPerCity> Reports { get; set; }
        public class SalesPerCity
        {
            public string City { get; set; }
            public decimal TotalSale { get; set; }
            public decimal RefundofSale { get; set; }
            public decimal TotalSaleCount { get; set; }
            public decimal RefundofSaleCount { get; set; }
        }

    }
    #endregion


    #region SalesPerGroupReport

    public class SalesPerGroupModel
    {
        public string ProductCount { get; set; }
        public string Categories { get; set; }
        public string Brands { get; set; }
        public string Cities { get; set; }
        public string Groups { get; set; }
        public string Status { get; set; }
        public string StoreCount { get; set; }
        public DocumentDateFilterModel DateFilter { get; set; }
        public IEnumerable<SalesPerGroup> Reports { get; set; }
        public class SalesPerGroup
        {
            public string Group { get; set; }
            public decimal TotalSale { get; set; }
            public decimal RefundofSale { get; set; }
            public decimal TotalSaleCount { get; set; }
            public decimal RefundofSaleCount { get; set; }
        }

    }
    #endregion

    #region SalesPerSizeReport

    public class SalesPerSizeModel
    {
        public string ProductCount { get; set; }
        public string Categories { get; set; }
        public string Brands { get; set; }
        public string Cities { get; set; }
        public string Groups { get; set; }
        public string Status { get; set; }
        public string StoreCount { get; set; }
        public DocumentDateFilterModel DateFilter { get; set; }
        public IEnumerable<SalesPerSize> Reports { get; set; }
        public class SalesPerSize
        {
            public string Size { get; set; }
            public decimal TotalSale { get; set; }
            public decimal RefundofSale { get; set; }
            public decimal TotalSaleCount { get; set; }
            public decimal RefundofSaleCount { get; set; }
        }

    }
    #endregion

    #region ReportFilter

    public class ReportFilterModel
    {
        public decimal OpenStoreCount { get; set; }
        public decimal ClosedStoreCount { get; set; }
        public decimal PreparedStoreCount { get; set; }

        public IEnumerable<Region> Regions { get; set; }

        public class Region
        {
            public int RegionId { get; set; }
            public string RegionName { get; set; }
            public decimal StoreCount { get; set; }

        }
        public IEnumerable<City> Cities { get; set; }
        public class City
        {
            public int RegionId { get; set; }
            public int CityId { get; set; }
            public string CityName { get; set; }
            public decimal StoreCount { get; set; }
        }
        public IEnumerable<Group> Groups { get; set; }
        public class Group
        {
            public int GroupId { get; set; }
            public string GroupName { get; set; }
            public decimal StoreCount { get; set; }
        }


        public IEnumerable<Category> Categories { get; set; }
        public decimal TotalCount { get; set; }
        public class Category
        {
            public int CategoryId { get; set; }
            public string CategoryName { get; set; }
            public decimal ProductCount { get; set; }


        }

        public IEnumerable<Producer> Producers { get; set; }
        public class Producer
        {
            public int ProducerId { get; set; }
            public string ProducerName { get; set; }
            public decimal ProductCount { get; set; }

        }
        public IEnumerable<Brand> Brands { get; set; }
        public class Brand
        {
            public int BrandId { get; set; }
            public string BrandName { get; set; }
            public decimal ProductCount { get; set; }
            public int ProducerId { get; set; }

        }

    }
    public class mekanFiltreModel
    {
        public IEnumerable<Mekan> Mekanlar { get; set; }

        public class Mekan
        {
            public int MekanID { get; set; }
            public string MekanAd { get; set; }
        }

    }

    #endregion




    #region ProductBasedOrderReport
    public class ProductBasedOrderModel
    {
        public string Sorgu { get; set; }
        public DocumentDateFilterModel DateFilter { get; set; }
        public int stokKontrolu { get; set; }
        public int stokGosterimi { get; set; }
        public int? Page { get; set; }
        public int? PageLength { get; set; }

        public int musterimi { get; set; }


        public decimal topAdet { get; set; }
        public decimal topKalan { get; set; }
        public decimal topSevk { get; set; }
        public decimal topTutar { get; set; }

        public ICollection<ProductBasedOrder> ProductBasedOrders { get; set; }
        public class ProductBasedOrder
        {
            public int eID { get; set; }
            public string eNo { get; set; }
            public string ehNot { get; set; }
            public string hrktTipAd { get; set; }
            public int stkID { get; set; }
            public string stkKod { get; set; }
            public string stkAd { get; set; }
            public string barkod { get; set; }

            public DateTime eTarih { get; set; }
            public DateTime eTarihS { get; set; }
            public int mekanID { get; set; }
            public string mekanAd { get; set; }
            public decimal siparisAdet { get; set; }
            public decimal kalanAdet { get; set; }
            public decimal sevkAdet { get; set; }
            public decimal kalanTutar { get; set; }
            public string dagiticiKod { get; set; }
            public decimal stok { get; set; }
            public string gosterilenStok { get; set; }

            public int topSay { get; set; }
        }
    }
    #endregion

    #region RemainderOrderReport
    public class RemainderOrderModel
    {
        public string Sorgu { get; set; }
        public DocumentDateFilterModel DateFilter { get; set; }
        public int? Page { get; set; }
        public int? PageLength { get; set; }
        public ICollection<RemainderOrder> RemainderOrders { get; set; }
        public class RemainderOrder
        {
            public int eID { get; set; }
            public string eNo { get; set; }
            public string eNot { get; set; }
            public DateTime eTarih { get; set; }
            public DateTime eTarihS { get; set; }
            public int mekanID { get; set; }
            public string mekanAd { get; set; }
            public string nedenAd { get; set; }
            public string hrktTipAd { get; set; }
            public decimal siparisAdet { get; set; }
            public decimal siparisTutar { get; set; }
            public decimal kalanAdet { get; set; }
            public decimal kalanTutar { get; set; }
            public decimal sevkAdet { get; set; }
            public decimal sevkTutar { get; set; }
            public decimal ehIndirim { get; set; }
            public decimal teslimatYuzde { get; set; }
            public string belgeSonuNot { get; set; }
            public int topSay { get; set; }
        }
    }
    #endregion

    #region ProductSalesReport
    public class ProductSalesModel
    {
        public string Sorgu { get; set; }
        public DocumentDateFilterModel DateFilter { get; set; }
        public int? Page { get; set; }
        public int? PageLength { get; set; }
        public string Mekanlar { get; set; }
        public ICollection<ProductSale> ProductSales { get; set; }
        public class ProductSale
        {
            public int stkID { get; set; }
            public string stkKod { get; set; }
            public string dagiticiKod { get; set; }
            public string barkod { get; set; }
            public string stkAd { get; set; }
            public decimal ehAdet { get; set; }
            public decimal ehTutar { get; set; }
            public int topSay { get; set; }
        }
    }
    #endregion

    #region HistoricalSalesReport
    public class HistoricalSalesModel
    {
        public DocumentDateFilterModel DateFilter { get; set; }
        public int? Page { get; set; }
        public int? PageLength { get; set; }
        public ICollection<HistoricalSale> HistoricalSales { get; set; }
        public class HistoricalSale
        {
            public string raporDonemi { get; set; }
            public decimal adet { get; set; }
            public decimal tutar { get; set; }
            public int topSay { get; set; }
        }
    }
    #endregion

    #region StoreSalesReport
    public class StoreSalesModel
    {
        public DocumentDateFilterModel DateFilter { get; set; }
        public int? Page { get; set; }
        public int? PageLength { get; set; }
        public ICollection<StoreSale> StoreSales { get; set; }
        public decimal topAdet { get; set; }
        public decimal topTutar { get; set; }

        public class StoreSale
        {
            public int mekanID { get; set; }
            public string mekanAd { get; set; }
            public decimal adet { get; set; }
            public decimal tutar { get; set; }
            public int topSay { get; set; }
        }
    }
    #endregion

    #region GeneralStoreSalesReport
    public class GeneralStoreSalesModel
    {
        public string Sorgu { get; set; }
        public DocumentDateFilterModel DateFilter { get; set; }
        public int? Page { get; set; }
        public int? PageLength { get; set; }
        public IEnumerable<List<string>> cBaslik { get; set; }
        public int sBaslik { get; set; }

        public ICollection<GStoreSale> GStoreSales { get; set; }
        public class GStoreSale
        {
            public int stkID { get; set; }
            public string stkKod { get; set; }
            public string dagiticiKod { get; set; }
            public string barkod { get; set; }

            public string stkAd { get; set; }
            public int topSay { get; set; }
            public DataRow satir { get; set; }
            public decimal satirTopTutar { get; set; }
        }
    }
    #endregion













}