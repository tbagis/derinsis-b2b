﻿
namespace DerinSIS.B2B.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Web.Mvc;
    using Helpers;

    #region Product List

    public class ProductListQueryModel
    {
        public int stkID { get; set; }
        public string stkKod { get; set; }
        public string stkAd { get; set; }
        public string mrkAd { get; set; }
        public string grpAd { get; set; }

        public byte urnKtgrID { get; set; }
        public byte urnKtgrID1 { get; set; }
        public byte urnKtgrID2 { get; set; }
        public byte urnKtgrID3 { get; set; }

        public int urtID { get; set; }
        public Int16 urnMrkID { get; set; }
        public int urnGrpID { get; set; }
        public string barkod { get; set; }
        public int rtb { get; set; }
        public Decimal odemeKur { get; set; }
        public Decimal fiyatS { get; set; }
        public string odemeKisaAd { get; set; }
        public Decimal indirim1 { get; set; }
        public Decimal indirim2 { get; set; }
        public Decimal indirim3 { get; set; }
        public Decimal indirimek { get; set; }
        public int urnMrkGrpID { get; set; }
        public Int16 urnKoli { get; set; }
        public Int16 urnKutu { get; set; }
        public Decimal stok { get; set; }

        public byte urnKdvYuzde { get; set; }
        public string gosterilenStok { get; set; }

        public int topSay { get; set; }
    }

    public class ProductSearchResultModel
    {
        public int Id { get; set; }
        public string Isim { get; set; }
        public bool ModelMi { get; set; }
    }


    public class ProductListModelExcel
    {
        public string Sorgu { get; set; }
        //public int? KategoriIdFiltresi { get; set; }
        //public string KategoriFiltreEtiketi { get; set; }
        //public int? UreticiIdFiltresi { get; set; }
        //public string UreticiFiltreEtiketi { get; set; }
        //public int? MarkaIdFiltresi { get; set; }
        //public string FiyatFiltresi { get; set; }
        //public string FiyatFiltreEtiketi { get; set; }
        //public string MarkaFiltreEtiketi { get; set; }

        public ICollection<ProductListItemModelExcel> AramaSonuclariExcel { get; set; }

        public abstract class ProductListItemModelExcel
        {
            public int Id { get; set; }
            public string Isim { get; set; }
            public decimal Adeti { get; set; }
            public virtual bool ModelMi { get; set; }
            public int Rutbe { get; set; }
        }

        public class ProductModelExcel : ProductListItemModelExcel
        {
            public string Barkod { get; set; }
            public string StokKodu { get; set; }
            public override bool ModelMi { get { return false; } }
            public int BagliOlduguModelId { get; set; }
        }

        public class ProductGroupModelExcel : ProductListItemModelExcel
        {
            public IEnumerable<ProductModelExcel> Urunler { get; set; }
            public override bool ModelMi { get { return true; } }
        }

        public ICollection<ExListe> eListe { get; set; }
        public class ExListe
        {
            public int Id { get; set; }
            public string Barkod { get; set; }
            public decimal Adet { get; set; }
        }

        public ICollection<ExListeHatali> eListeHatali { get; set; }
        public class ExListeHatali
        {
            public string Barkod { get; set; }
            public string Adet { get; set; }
        }



    }

    public class PasiveProductListModel
    {
        public int sepetAyrID { get; set; }
        public int urunID { get; set; }
        public string urunAd { get; set; }
        public Decimal Adet { get; set; }

    }

    public class ProductListItemGroupModel
    {
        public int GroupId { get; set; }
        public string Name { get; set; }

        public IEnumerable<GroupItem> Items { get; set; }

        public class GroupItem
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public decimal Fiyat { get; set; }
            public string StokKodu { get; set; }
            public string Barkod { get; set; }
        }
    }
    #endregion


    #region My Carts Models

    public class MyCartModel
    {
        public int Id { get; set; }
        public string Isim { get; set; }
        public string Aciklamalar { get; set; }
        public bool AktifMi { get; set; }
        public int Adet { get; set; }
    }

    public class CreateNewCartModel
    {
        [Required(ErrorMessage = "Sepet ismi zorunludur.")]
        [Display(Name = "Sepet İsmi")]
        [DataType(DataType.Text)]
        public string Isim { get; set; }

        [Display(Name = "Açıklamalar")]
        [DataType(DataType.MultilineText)]
        public string Aciklamalar { get; set; }
    }

    public class EditCartModel
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }


        [Required(ErrorMessage = "Sepet ismi zorunludur.")]
        [Display(Name = "Sepet İsmi")]
        [DataType(DataType.Text)]
        public string Isim { get; set; }

        [Display(Name = "Açıklamalar")]
        [DataType(DataType.MultilineText)]
        public string Aciklamalar { get; set; }
    }

    #endregion

    #region Cart Models

    public class CartMenuElementModel
    {
        public string AktifSepetIsmi { get; set; }
        public int ToplamUrunCesidi { get; set; }
        public string KDVDahilTutar { get; set; }

        public string silinenUrn { get; set; }
        public ICollection<Urun> SonEklenen2Urun { get; set; }
        public class Urun
        {
            public string UrunIsmi { get; set; }
        }
    }

    public class CartSidebarElementModel
    {
        public string AktifSepetIsmi { get; set; }
        public int ToplamUrunCesidi { get; set; }
        // Döviz Değişikliği public decimal KDVDahilTutar { get; set; }
        public string[,] TutarBilgisi { get; set; }

    }

    public class CartModel
    {
        public decimal? mesajVer { get; set; }
        public int AktifSepetId { get; set; }
        public ICollection<Sepet> Sepetlerim { get; set; }
        public class Sepet
        {
            public int Id { get; set; }
            public string Isim { get; set; }
            public string Aciklama { get; set; }
        }

        public int modelStokKontrolu { get; set; }

        public IEnumerable<Urun> AktifSepetUrunleri { get; set; }
        public class Urun
        {
            public int UrunId { get; set; }
            public string UrunAdi { get; set; }
            public string Barkod { get; set; }
            public string StokKodu { get; set; }

            public decimal KDVHaricSatisFiyati { get; set; }
            public decimal Indirim1 { get; set; }
            public decimal Indirim2 { get; set; }
            public decimal Indirim3 { get; set; }
            public decimal Indirim4 { get; set; }

            public decimal KDVHaricNet
            {
                get
                {
                    var indirimler = new[] { Indirim1, Indirim2, Indirim3, Indirim4 };
                    return indirimler.Aggregate(KDVHaricSatisFiyati, (fiyat, indirim) => fiyat - (fiyat * indirim / 100.0m));
                }
            }

            public int KDVOrani { get; set; }
            public decimal KDVTutari
            {
                get { return KDVHaricNet * KDVOrani / 100.0m; }
            }

            public decimal KDVDahilToplam
            {
                get { return KDVHaricNet + KDVTutari; }
            }

            public decimal Adet { get; set; }

            public decimal Stok { get; set; }
            public int stokKontrolu { get; set; }
            public decimal SatirToplami { get { return KDVDahilToplam * Adet; } }

            public string DvzBirim { get; set; }

        }
    }

    public class AddToActiveCartModel
    {
        public IEnumerable<CartItem> Items { get; set; }

        public class CartItem
        {
            public int ProductId { get; set; }
            public decimal Quantity { get; set; }
        }
    }

    public class CartModelDoldur
    {
        public IEnumerable<CartUrn> Items { get; set; }

        public class CartUrn
        {
            public int stkIDsi { get; set; }
            public decimal Sayisi { get; set; }
        }
    }

    public class UpdateActiveCartItemModel
    {
        public int ProductId { get; set; }
        public decimal NewQuantity { get; set; }

    }

    #endregion

    public class PromoModel
    {
        public ICollection<promo> Promolar { get; set; }
        public class promo
        {
            public byte sira { get; set; }
            public string resim { get; set; }
            public string baslik { get; set; }
            public byte etkiTip { get; set; }
            public int etkiId { get; set; }
            public string etkiDosya { get; set; }
            public int linkAktif { get; set; }
        }
    }




}