﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DerinSIS.B2B.ViewModels
{

    public class PrivateProductPriceListReportExcel
    {
        public string stkKod { get; set; }
        public string dagiticiKod { get; set; }
        public string barkod { get; set; }
        public string stkAd { get; set; }
        public decimal netAlis { get; set; }
        public decimal kdvliAlis { get; set; }
        public decimal satis { get; set; }
        public decimal kdvliSatis { get; set; }
        public DateTime tarih { get; set; }
        public DateTime tarihSon { get; set; }
        public string KategoriAd { get; set; }
    }
    public class ProductListReportExcel
    {
        public string stkKod { get; set; }
        public string barkod { get; set; }
        public string stkAd { get; set; }
        public string KategoriAd { get; set; }
        public string dagiticiKod { get; set; }
        public string mrkAd { get; set; }
        public DateTime listelemeTarih { get; set; }
        public decimal netAlis { get; set; }
        public decimal kdvliAlis { get; set; }
        public decimal satis { get; set; }
        public decimal kdvliSatis { get; set; }
    }
    public class ProductStockReportExcel
    {
        public int stkID { get; set; }
        public string stkKod { get; set; }
        public string dagiticiKod { get; set; }
        public string barkod { get; set; }
        public string stkAd { get; set; }
        public decimal stok { get; set; }
    }

    public class ProductStockActionReportExcel
    {
        public int stkID { get; set; }
        public string stkKod { get; set; }
        public string dagiticiKod { get; set; }
        public string barkod { get; set; }
        public string stkAd { get; set; }
        public decimal devir { get; set; }
        public decimal giren { get; set; }
        public decimal cikan { get; set; }
        public decimal stok { get; set; }
        public decimal fiyat { get; set; }
        public decimal stokTutar { get; set; }
    }

    public class PurchaseReturnReportExcel
    {
        public string mekanAd { get; set; }
        public int eID { get; set; }
        public string eNo { get; set; }
        public DateTime eTarih { get; set; }
        public decimal adet { get; set; }
        public decimal tutar { get; set; }
        public decimal indirim { get; set; }
        public decimal kdv { get; set; }
        public decimal toplam { get { return tutar - indirim + kdv; } }
    }

    public class CurrentAccountExtractReportExcel
    {
        public int cID { get; set; }
        public int cKod { get; set; }
        public string frmKod { get; set; }
        public string frmAd { get; set; }
        public string ctfAd { get; set; }
        public string cEvrakNo { get; set; }
        public DateTime cTarih { get; set; }
        public DateTime cTarihVade { get; set; }
        public decimal BORC { get; set; }
        public decimal ALACAK { get; set; }
        public string cNot { get; set; }
    }

    public class ProductSaleReportExcel
    {
        public int stkID { get; set; }
        public string stkKod { get; set; }
        public string dagiticiKod { get; set; }
        public string barkod { get; set; }
        public string stkAd { get; set; }
        public decimal ehAdet { get; set; }
        public decimal ehTutar { get; set; }
    }

    public class HistoricalSaleReportExcel
    {
        public string raporDonemi { get; set; }
        public decimal adet { get; set; }
        public decimal tutar { get; set; }
    }

    public class StoreSaleReportExcel
    {
        public int mekanID { get; set; }
        public string mekanAd { get; set; }
        public decimal adet { get; set; }
        public decimal tutar { get; set; }
    }

    public class RemainderOrderReportExcel
    {
        public int eID { get; set; }
        public string eNo { get; set; }
        public string hrktTipAd { get; set; }
        public DateTime eTarih { get; set; }
        public DateTime eTarihS { get; set; }
        public int mekanID { get; set; }
        public string mekanAd { get; set; }
        public decimal siparisAdet { get; set; }
        public decimal siparisTutar { get; set; }
        public decimal kalanAdet { get; set; }
        public decimal kalanTutar { get; set; }
        public decimal ehIndirim { get; set; }
        public decimal sevkAdet { get; set; }
        public decimal sevkTutar { get; set; }
        public decimal teslimatYuzde { get; set; }
        public string eNot { get; set; }
        public string belgeSonuNot { get; set; }
    }

    public class ProductBasedOrderReportExcel
    {
        public int eID { get; set; }
        public string eNo { get; set; }
        public string hrktTipAd { get; set; }
        public DateTime eTarih { get; set; }
        public DateTime eTarihS { get; set; }
        public int mekanID { get; set; }
        public string mekanAd { get; set; }
        public int stkID { get; set; }
        public string stkKod { get; set; }
        public string stkAd { get; set; }
        public string barkod { get; set; }

        public decimal siparisAdet { get; set; }
        public decimal kalanAdet { get; set; }
        public decimal sevkAdet { get; set; }
        public decimal kalanTutar { get; set; }
        public string dagiticiKod { get; set; }
        public string ehNot { get; set; }
    }
}