﻿namespace DerinSIS.B2B.Infrastructure.Auth
{
    using System;

    [Flags]
    public enum AccountType
    {
        Supplier = 0x1,
        Customer = 0x2,
    }
}