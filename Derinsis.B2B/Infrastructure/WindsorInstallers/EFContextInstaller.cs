﻿namespace DerinSIS.B2B.Infrastructure.WindsorInstallers
{
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;
    using Models;

    public class EFContextInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            var context = Component
                .For<DbEntities>()
                .LifeStyle.PerWebRequest;

            container.Register(context);
        }
    }
}