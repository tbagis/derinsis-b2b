﻿namespace DerinSIS.B2B.Infrastructure.WindsorInstallers
{
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;
    using Helpers;

    public class SettingsInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            var mailerComponent = Component
                .For<SystemSettings>()
                .LifeStyle.PerWebRequest;

            container.Register(mailerComponent);
        }
    }
}