﻿namespace DerinSIS.B2B.Infrastructure.WindsorInstallers
{
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;
    using Emails;

    public class EmailInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            var mailerComponent = Component
                .For<Mailer>()
                .LifeStyle.PerWebRequest;

            container.Register(mailerComponent);
        }
    }
}